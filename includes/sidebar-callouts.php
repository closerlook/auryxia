<div class="grid-3 grid-3 right">

  <?php
  // Show Portal Nav
  if (is_page(array(1095, 1097, 1100, 1574, 1707, 1710, 1712))){
    echo '<div class="portal-menu">';
    wp_nav_menu(array('menu' => 'Portal Nav','container_class' => 'false'));
    echo '</div>';
  } 
  ?>

  <?php if(have_rows('cta')): ?>
  <div class="cta-links">
    <?php 
    $count = 0; 
    while(have_rows('cta')): 
      the_row();  
      $count++;
      $class = "cta animC cta-" . $count;
    ?>
    <a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="<?php echo get_sub_field('title'); ?>" href="<?php echo get_sub_field('link'); ?>" class="<?php echo $class; ?>">
      <span class="cta-title"><?php echo get_sub_field('title'); ?></span>
      <span class="cta-text"><?php echo get_sub_field('text'); ?></span>
    </a>
    <?php endwhile; ?>
  </div>
  <?php endif; ?>
  
  <?php
  if (get_field('sidebar_image')){
    echo '<img src="' . get_field('sidebar_image') . '" class="sidebar-img" />';
  }  
  ?>
  
  <div class="cta-isi-sidebar">
    <a href="#isi" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="ISI"><span>See <span style="text-decoration: underline;">Important Safety Information</span> below</span></a>
  </div>
</div>