<div class="home-campaign cf">
  <img src="<?php the_field('campaign_image'); ?>" id="home-hand" class="left" />
  <div class="home-info right">
    <div class="camp-wrap">
      <div class="campaign-tagline"><?php the_field('campaign_tagline'); ?></div>
      <div class="campaign-text"><?php the_field('campaign'); ?></div>
    </div>
    <div id="assist-home-logo"></div>
  </div>
</div>
