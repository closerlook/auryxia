<div class="post-titles">
<?php 
  $hImage = get_field('heading_image');
  $h1 = get_field('heading_1');
  $h2 = get_field('heading_2');
  $h3 = get_field('heading_3');
  if ($hImage){ echo "<img src='$hImage' />";}
  if ($h1){ echo "<h2>$h1</h2>"; }
  if ($h2){ echo "<h3>$h2</h3>"; }
  if ($h3){ echo "<h4>$h3</h4>"; }
  if (setSiteType(get_the_ID()) == "assistance"){
    echo '<div id="assist-logo"></div>';
  }
?>
</div>