<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package mgunited
 */

include_once get_template_directory() . "/api-tools.php";





// ????? not sure where to place this; on MG United it's in "template-functions.php"


/*
 * General purpose method for forwarding requests to the MG United API -
 * Returns the raw result if the call was successful
 */

//ini_set('display_errors', 1);
//error_reporting(E_ALL ^ E_NOTICE);

function call_api_for_callback($callback) {
	$response = null;

	// Verify nonce, unless a parameter named 'skip-nonce-check'
	// was included in the request
	if (!array_key_exists('skip-nonce-check', $_REQUEST))
	{
		if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
//			header(':', true, 500);
//			header('X-PHP-Response-Code: 500', true, 500);
			http_response_code(500);
			die();
		}
	}

	// Determine the method we'll call on the API, as specified by a 'method'
	// parameter in the POST or query string
	$requestApiMethod;
	if (array_key_exists('method', $_REQUEST)) {
		$requestApiMethod = $_REQUEST['method'];
	} else {
//		header(':', true, 500);
//		header('X-PHP-Response-Code: 500', true, 500);
		http_response_code(500);
		die();
	}

	// Check for a supplied 'data' parameter
	$requestData;
	if (array_key_exists('data', $_REQUEST))
		$requestData = stripslashes($_REQUEST['data']);
	else
		$requestData = '';

	// Perform API call using the supplied API method & request data
	$apiResponse = call_user_func($callback, $requestApiMethod, $requestData);
	if ($apiResponse != null) {
		// Forward along any cookies set by the API
		foreach ($apiResponse['headers'] as $header) {
			if (strpos(strtolower($header), 'set-cookie:') === 0) {
				header($header);
				break;
			}
		}

		$response = json_decode($apiResponse['result']);
	} else {
//		header(':', true, 500);
//		header('X-PHP-Response-Code: 500', true, 500);
		http_response_code(500);

		die();
	}

	// Output the response
	wp_send_json($response);
	die();
}

add_action("wp_ajax_signupApi","signup_api");
add_action("wp_ajax_nopriv_signupApi","signup_api");
function signup_api() {
	call_api_for_callback("call_auryxia_api");
}

add_action("wp_ajax_npiApi","npi_api");
add_action("wp_ajax_nopriv_npiApi","npi_api");
function npi_api() {
	call_api_for_callback("call_npi_api");
}

/*
 * Queries API status
 */

add_action("wp_ajax_connectivityTest","connectivity_test");
add_action("wp_ajax_nopriv_connectivityTest","connectivity_test");
function connectivity_test() {
	// Call the "ConnectivityTest" API method
	$apiResponse = call_auryxia_api('ConnectivityTest', null);
	if ($apiResponse == null) {
		echo FAIL_MESSAGE;
		return;
	}

	// Parse the result JSON
	$result = json_decode($apiResponse['result']);

	// "status" is a short indicating the state of the API's connectivitytest page
	switch ($result->status) {
		case CONNECTIVITY_TEST_STATUS['Pass']:
			echo CONNECTIVITY_TEST_PASS_MESSAGE;
			break;

		default:
			echo CONNECTIVITY_TEST_FAIL_MESSAGE;
			break;
	}

	// And that's it - all we do is print one of those two messages
	die();
}
