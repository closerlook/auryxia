<?php
if (isset($post)){
  $pageParentTitle = strtoupper(get_the_title($post->post_parent));
  if (is_page() && $post->post_parent){
    $childPages = wp_list_pages('title_li=&child_of=' . $post->post_parent . '&echo=0');
  } else {
  	$childPages = wp_list_pages(
  	  'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0'
  	 );
  }
  
  // Patients
  $pageID = $post->ID;
  $children = get_pages('child_of='.$pageID);
  if($post->post_parent == 11){
    if (count($children) == 0){
      $childPages = false;
    } else{
      $childPages = wp_list_pages(
  	    'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0&exclude_tree=11'
  	 );
    }
  }
  
  // Patient Assistance
  if($post->post_parent == 13){
    if (count($children) == 0){
      $childPages = false;
    } else{
      $childPages = wp_list_pages(
  	    'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0&exclude_tree=13'
  	 );
    }
  }
  
    
  if ($childPages){
    echo '<div class="grid breadcrumbs">';
      echo '<ul class="grid-12 cf">' . $childPages . '</ul>';
    echo '</div>';
  }
}
?>

