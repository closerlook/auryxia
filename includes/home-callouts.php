<?php if(have_rows('callouts')): ?>
<div class="callouts cf">
  <?php $count = 0; while( have_rows('callouts') ): the_row();  ?>
    <?php 
      $count++;
      if ($count == 3){ $class = "callout animC nom grid-4 left callout-$count"; } 
      else{ $class = "callout grid-4 animC left callout-$count"; }
      $link = get_sub_field('link');
    ?>
    <a href="<?php echo $link; ?>" class="<?php echo $class; ?>" data-element="default" data-category="CTA" data-action="Click" data-label="<?php echo get_sub_field('main_text');?>">
      <span class="callout-top"></span>
      <span class="main_text"><?php echo get_sub_field('main_text'); ?></span>
      <span class="sub_text"><?php echo get_sub_field('subtext'); ?></span>
      <span class="square"></span>
    </a>
    <?php endwhile; ?>
</div>
<?php endif; ?>