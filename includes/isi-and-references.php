<?php

$siteType = setSiteTypeTwo(get_the_ID());

if ($siteType !== "assistance"){?>
<div class="grid-12 cf" id="legal-notes">

  <div id="isi" data-isi="<?php echo $siteType; ?>">
	  <?php
	  	$isi_post_id = 31; // isi hcp id
	  	if ( $siteType === "patient" ) $isi_post_id = 234;
	  	$isi = get_post($isi_post_id);
	  	echo wpautop($isi->post_content);
	  ?>
  </div>

  <?php
	  $references = get_field('references');
	  if ($references ){
	    echo '<div class="references">';
      $count = substr_count($references,'<li>');
      if ($count > 1){
	      echo '<span>References</span>';
      } else{
	      echo '<span>Reference</span>';
      }
      echo $references;
	    echo '</div>';
	  }
  ?>

</div>
<?php } ?>
