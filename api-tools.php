<?php

include_once "environment-detection.php";

/*
	General method for calling methods on the MG United API
*/
function call_api_for_url($baseApiUrl, $method, $data) {
	// Assemble the URL we'll call
	$requestUrl = $baseApiUrl . $method;

	// Assemble the HTTP headers for our request
	$httpHeader = "Content-type: application/json\r\n"
		. "Accept: application/json\r\n"
		. "Content-Length: " . strlen($data);

	if (array_key_exists('HTTP_COOKIE', $_SERVER))
		$httpHeader = $httpHeader . "\r\nCookie: " . $_SERVER['HTTP_COOKIE'];

	// Assemble our HTTP request
	$request = array(
		'method'  => 'POST',
		'header'  => $httpHeader,
		'content' => $data
	);

	// Assemble our stream context
	$streamContextOptions = array(
		'http' => $request
	);

	$streamContext  = stream_context_create($streamContextOptions);

	// Perform the API call
	$result = @file_get_contents($requestUrl, false, $streamContext);

	// retry up to 5 times if request fails
	if ($result === FALSE) {
		$newData = $data;
		$newData = substr($newData, 0, -1) . ',"includeId":true}';

		// Reassemble the HTTP headers for our request
		$newHttpHeader = "Content-type: application/json\r\n"
			. "Accept: application/json\r\n"
			. "Content-Length: " . strlen($newData);

		if (array_key_exists('HTTP_COOKIE', $_SERVER))
			$newHttpHeader = $newHttpHeader . "\r\nCookie: " . $_SERVER['HTTP_COOKIE'];

		// Reassemble our HTTP request
		$newRequest = array(
			'method'  => 'POST',
			'header'  => $newHttpHeader,
			'content' => $newData
		);

		// Reassemble our stream context
		$newStreamContextOptions = array(
			'http' => $newRequest
		);

		$newStreamContext  = stream_context_create($newStreamContextOptions);

		for ($x = 0; $x < 5; $x+=1) {
			$result = @file_get_contents($requestUrl, false, $newStreamContext);
			if ($result !== FALSE) {
				// success
				break;
			}
		}
		if ($result === false) {
			// An error occurred
//			header(':', true, 500);
//			header('X-PHP-Response-Code: 500', true, 500);
			http_response_code(500);
			return null;
		}

	}

	// Success! Return the result JSON as well as the array of HTTP headers
	return array(
		'result' => $result,
		'headers' => $http_response_header
	);
}

/*
	This calls the main Auryxia sign up API endpoint
*/
function call_auryxia_api($method, $data) {
	// Switch API based on the environment we're running in
	if (auryxia_use_production_api()) {
		// Use the production API
		$baseApiUrl = 'https://auryxia-api.closerlook.com/api/sign-up/';
	} else {
		// Use the QA API
		$baseApiUrl = 'https://auryxia-api-qa.closerlook.com/api/sign-up/';
	}

	return call_api_for_url($baseApiUrl, $method, $data);
}

/*
	This calls the main Auryxia NPI API endpoint
*/
function call_npi_api($method, $data) {
	// Switch API based on the environment we're running in
	if (auryxia_use_production_api()) {
		// Use the production API
		$baseApiUrl = 'https://auryxia-api.closerlook.com/api/npi-lookup/';
	} else {
		// Use the QA API
		$baseApiUrl = 'https://auryxia-api-qa.closerlook.com/api/npi-lookup/';
	}

	return call_api_for_url($baseApiUrl, $method, $data);
}

/*
	Uses the API to return all of the static data used when presenting
	form fields associated with Argenx API methods; it'd be nicer to make
	form-specific API methods for this, but at the moment we're only dealing
	with a small amount of data. It's expected that WP Engine will cache
	pages rendered using this data.
*/
// function get_static_form_data() {
// 	// Call the "GetStaticData" API method
// 	$response = call_mg_united_api('GetStaticData', null);
// 	if ($response == null)
// 		$result = null;
// 	else
// 		$result = json_decode($response['result']);

// 	return $result;
// }
