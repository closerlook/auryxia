<footer class="cf">
  <div class="grid-12 cf">
    <div class="grid-2 right akebia-footer-logo">
      <a href="<?php the_field('footer_logo_link', 'option'); ?>" target="_blank" data-element="default" data-category=“Footer” data-action="Click" data-label="Keryx Logo">
        <img src="<?php the_field('footer_logo', 'option'); ?>" alt="<?php the_field('footer_brand_name', 'option'); ?>"  
        />
      </a>
    </div>
    <div id="footer-info" class="grid-10 left footer-links">
      <div class="false">
        <ul id="menu-footer-links" class="menu">
          <li>
            <a target="_blank" href="https://akebia.com/contact/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Contact Us">Contact Us</a><div class="borderRightFooterDesktop"></div></li>
          <li>
            <a target="_blank" href="https://akebia.com/legal/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Terms & Conditions">Terms and Conditions</a><div class="borderRightFooterDesktop"></div></li>
          <li>
            <a target="_blank" href="https://akebia.com/legal/privacy-policy.aspx" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Privacy Policy">Privacy Policy</a><div class="borderRightFooterDesktop"></div></li>
          <li>
            <a target="_blank" href="http://akebia.com" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Keryx.Com">AKEBIA.COM</a></li>
        </ul>
      </div>
      <div class="legal">
        <?php 
        if (is_page(76)){
          echo '©2020 Akebia Therapeutics, Inc.';
        } else {
          the_field('copyright', 'option');
        }
        ?>
        <div class="footer-spacer"></div>
        <?php 
          if (is_page(array(1095, 1097, 1100))){
            echo 'PP-ZER-US-0045';
          } else if (is_page(76)){
            echo 'PP-AUR-US-0980';
          } else {
            the_field('legal_code', 'option'); 
          }
        ?>
        <div class="footer-spacer"></div>
        <?php
        if (is_page(76)){ 
          echo '01/20';
        } else {
          the_field('legal_date', 'option'); 
        } ?>
      </div>
    </div>
  </div>
</footer>