<?php /* Template Name: HCP Default */ 
get_header(); if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php include_once(TEMPLATEPATH . '/includes/sub-nav.php'); ?>

  <div class="grid top-banner desktop">
    <div class="banner-bg">
      <div class="grid-12 banner-text"><?php the_field('banner_text', 'option'); ?></div>
    </div>
  </div>
<div class="grid-12 interior cf">     


  <div class="post-titles">
  <?php 
    $hImage = get_field('heading_image');
    $h1 = get_field('heading_1');
    $h2 = get_field('heading_2');
    $h3 = get_field('heading_3');
    if ($hImage){ echo "<img src='$hImage' />";;}
    if ($h1){ echo "<h2>$h1</h2>"; }
    if ($h2){ echo "<h3>$h2</h3>"; }
    if ($h3){ echo "<h4>$h3</h4>"; }
  ?>
  </div>
  <div class="clear"></div>
  
  <div class="grid-9 left copy">
    <?php 
    the_content(); 
    $study = get_field('study_design');
    if ($study){
      echo '<div class="study-design">';
      echo $study;  
      echo '</div>';
    } 
    $formulation = get_field('formulation_text', 'option');
    if ($formulation){
      echo '<br /><div class="formulation">';
      echo '<span>Formulation</span>' . $formulation . '</div>';
    }
    ?>
  </div>
   
  <?php include_once(TEMPLATEPATH . '/includes/sidebar-callouts.php'); ?>

</div><!-- Single Page -->


<?php 
	include_once(TEMPLATEPATH . '/includes/isi-and-references.php');  
	endwhile; else : endif; get_footer();
?>