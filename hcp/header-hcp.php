<header>
  <div class="grid cf">
       <div class="header-links-hcp cf mobile">
      <a href="/hyperphosphatemia/important-safety-information" class="nom " data-element="default" data-category="Header" data-action="Click" data-label="ISI">Important Safety Information</a>
      <span class="sep  ">|</span>
      <a href="/hyperphosphatemia/prescribing-information/" target="_blank" class="pi-link" data-element="default" data-category="Header" data-action="Click" data-label="Full PI">Full Prescribing Information</a>
    </div>
    <div class="intent-mobile mobile ">
        This site is for u.s. healthcare professionals only
      </div>
    <h1>
      <a href="/" data-element="default" data-category="Header" data-action="Click" data-label="Logo">
        <img id="logo" src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>" />
      </a>
    </h1>




     <div class="header-links cf desktop">
      <a href="/hyperphosphatemia/important-safety-information" class="nom desktop" data-element="default" data-category="Header" data-action="Click" data-label="ISI">Important Safety Information</a>
      <span class="sep desktop">|</span>
      <a href="/hyperphosphatemia/prescribing-information/" target="_blank" class="pi-link" data-element="default" data-category="Header" data-action="Click" data-label="Full PI">Full Prescribing Information</a>
      <span class="sep desktop">|</span>
      <a href="/hyperphosphatemia/patients" class="desktop nav-copy" data-element="default" data-category="Header" data-action="Click" data-label="For Patients">For Patients</a>
      <span class="sep desktop">|</span>
      <a href="/akebiacares/" class="desktop nav-copy" data-element="default" data-category="Header" data-action="Click" data-label="AkebiaCares">AkebiaCares</a>
      <span class="sep desktop">|</span>
	    <a href="/iron-deficiency-anemia" class="desktop nav-copy popup" data-modal="ext" data-element="default" data-category="Header" data-action="Click" data-label="Iron Deficiency Anemia">Iron Deficiency Anemia</a>
    </div>

    <div class="clear"></div>

    <div class="intent desktop">
      <?php the_field('header_for_text', 'option'); ?>
    </div>

    <div class="grid top-banner-mobile mobile">
    <div class="banner-bg-mobile">
      <div class="grid-12 banner-text"><?php the_field('banner_text', 'option'); ?></div>
    </div>
  </div>

    <a href="#" class="nt mobile" id="mobile-menu">Menu</a>

  </div><!-- Grid -->
</header>

<nav class="cf">
  <?php
    wp_nav_menu(
      array(
        'menu' => 'HCP Menu',
        'container_class' => 'grid-12 cf'
      )
    );
  ?>
</nav>
<?php if (is_page(76)){ ?>
<div class="sub-buttons-container">
  <div class="sub-button"><a href="http://www.auryxiarxlocator.com" target="_blank">Find specialty pharmacies that dispense AURYXIA</a></div>
  <div class="sub-button"><a href="https://akebiarxcoverage.caremetx.com/" target="_blank">Check your patient's prescription benefits</a></div>
</div>
<?php } ?>
<script>


</script>
