<?php /* Template Name: HCP Home */ 
get_header(); if (have_posts()) : while (have_posts()) : the_post(); ?>

</div>
 
<div class="grid cf hcp-home-bg">


  <div class="grid-12">
    <?php include_once(TEMPLATEPATH . '/includes/home-campaign.php'); ?>
  </div><!-- 12 -->
</div><!-- Grid -->

<div class="content">

  <div class="grid cf">
    <div class="grid-12">
      <?php include_once(TEMPLATEPATH . '/includes/home-callouts.php'); ?>
      <?php 
      $formulation = get_field('formulation_text', 'option');
      if ($formulation){
        echo '<br /><div class="formulation">';
        echo '<span>Formulation</span>' . $formulation . '</div>';
      }
      ?>
    </div><!-- 12 -->
    <div class="cta-isi-homepage">
      <a href="#isi" data-element="default" data-category="Internal Link" data-action="Click" data-label="ISI"><span>See <span style="text-decoration: underline;">Important Safety Information</span> below</span></a>
    </div>
  </div><!-- Grid -->

  <?php 
  include_once(TEMPLATEPATH . '/includes/isi-and-references.php'); 
endwhile; else : endif; get_footer();
?>
