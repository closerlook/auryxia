<?php
//  $siteType = setSiteType(get_the_ID());
  $siteType = setSiteTypeTwo(get_the_ID());
  get_template_part('head');
  echo "<!-- trying to get the head -->";
?>

<body id="<?php echo $siteType; ?>" <?php body_class(); ?>>
<?php wp_body_open(); ?>
  <?php
  switch ($siteType) {
    case "patient":
	  get_template_part('patients/header','patients');
//      include_once(TEMPLATEPATH . '/patients/header-patients.php');
      break;
    case "assistance":
	  get_template_part('assistance/header','assist');
//      include_once(TEMPLATEPATH . '/assistance/header-assist.php');
      break;
    default:
	  echo "<!-- showing hcp -->";
	  get_template_part('hcp/header','hcp');
	  break;
//      include_once(TEMPLATEPATH . '/hcp/header-hcp.php');
  }
  ?>

  <?php
  if (is_user_logged_in()){
    echo '<div class="dev-tools">';
      echo '<a href="#" class="btn-grid nt">Show Grid</a>';
      echo '<div id="dev-grid"></div>';
      edit_post_link('Edit Page');
    echo '</div>';
  }
  ?>

  <div class="content content-1">

<div class="bannerNewPatient">
 <img class="bannerImgPatient" src="/wp-content/uploads/banner.png" alt="">
 <div class="grid-12 banner-text pateintBannerTxt"><strong>Hyperphosphatemia</strong> CKD On Dialysis</div>
</div>
