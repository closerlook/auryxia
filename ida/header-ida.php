<header>
  <div class="grid cf">
    
    <h1>
      <a href="<?php echo home_url(); ?>/ida">
        <img id="logo" src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>" />
      </a>
    </h1>
    
    <div class="grid-10 right">

      <div class="header-links cf">
        <a href="/important-safety-information" class="nom">Important Safety Information</a>
        <span class="sep desktop">|</span>    
        <a href="/prescribing-information/" target="_blank" class="pi-link">Full Prescribing Information</a>
        <span class="sep desktop">|</span>
        <a href="/patients" class="desktop nav-copy">For Patients</a>
        <span class="sep desktop">|</span> 
        <a href="/akebiacares" class="desktop nav-copy">Keryx Patient Plus</a>
        <span class="sep desktop">|</span>
        <a href="/hyperphosphatemia" class="desktop nav-copy popup" data-modal="ext">Hyperphosphatemia</a>
      </div>
      
      <div class="clear"></div>
      <a href="#" class="nt mobile" id="mobile-menu">Menu</a>
      <nav>
        <?php wp_nav_menu(array('menu' => 'IDA Menu')); ?>
      </nav>
      
    </div><!-- Grid 10 -->
    
  </div><!-- Grid -->
</header>