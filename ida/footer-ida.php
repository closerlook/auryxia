<footer class="cf">
  <div class="grid-12 cf">
  
    <div class="grid-7 left">
      <a href="<?php the_field('footer_logo_link', 'option'); ?>" target="_blank">
        <img 
          class="grid-2"
          src="<?php the_field('footer_logo', 'option'); ?>" 
          alt="<?php the_field('footer_brand_name', 'option'); ?>" 
        />
      </a>
      <div class="legal">
        <?php the_field('copyright', 'option'); ?>
        <div class="footer-spacer"></div>
        <?php 
          if (is_page(array(1095, 1097, 1100))){
            echo 'PP-ZER-US-0045';
          } else{
            the_field('legal_code', 'option'); 
          }
        ?>
        <div class="footer-spacer"></div>
        <?php the_field('legal_date', 'option'); ?>
      </div>
    </div>
    
    <div id="footer-info" class="grid-5 right footer-links">
      <?php wp_nav_menu(array('menu' => 'Footer Links','container_class' => 'false')); ?>
    </div> 
  
  </div>
</footer>