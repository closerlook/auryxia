(($) => {
  const init = () => {

    const pathName = window.location.pathname;

    // Hide Fonts.com Icon
    $('#mti_wfs_colophon, #mti_wfs_colophon img').hide();
    $('#mti_wfs_colophon, #mti_wfs_colophon_anchor, #mti_wfs_colophon_anchor img').css('visibility', 'hidden');

    // Scroll To ISI
    // cta-isi-homepage on index page
    // cta-isi-sidebar on other pages

    $('.cta-isi-homepage').click(function(e) {
      e.preventDefault();
      TweenMax.to(window, 0.5, {scrollTo:{y:'#isi-start'}, ease:Power2.easeOut});
    });

    $('.cta-isi-sidebar').click(function(e) {
      e.preventDefault();
      TweenMax.to(window, 0.5, {scrollTo:{y:'#isi-start'}, ease:Power2.easeOut});
    });

    $('#mobile-menu').click(function() {
      $(this).toggleClass("open");
      $("nav").toggleClass("mobile-open");
    });

    $( '.callout' ).hover(function() {
      var el = $(this).find('.callout-arrow-right');
      TweenMax.to(el, 0.25, {borderLeft: '7px solid #FFFFFF'});
    }, function() {
      var el = $(this).find('.callout-arrow-right');
      TweenMax.to(el, 0.25, {borderLeft: '7px solid #5B6770'});
    });

    if (pathName === '/iron-deficiency-anemia/') {
      TweenMax.set('.icon-home-fill', {fill:'#FF8500'});
    } else {
      TweenMax.set('.icon-home-fill', {fill:'#5B6770'});
      $( "#nav-home" )
        .mouseover(function() {
          TweenMax.set('.icon-home-fill', {fill:'#FF8500'});
        }).mouseout(function() {
          TweenMax.set('.icon-home-fill', {fill:'#5B6770'});
        });
    }

    if ( $( "#tab-wrap" ).length )
    {
      // Efficacy and Safety pages tabs
      $(".tab-names a").click(function() {
        tabPosition = $(this).index();
        $(this).addClass("active").siblings().removeClass("active");
        $(".tab").eq(tabPosition).addClass("active").siblings().removeClass("active");
        return false;
      });

      // Trial Design Toggle
      $('#trial-design-collapse').on('hidden.bs.collapse', function () {
        $('#trial-design-expanded').css('opacity', '0');
        $('#trial-design-expanded').css('display', 'none');

        $('#trial-design-toggle .toggle-on').css('display', 'block');
        $('.tab-toggle').attr('data-label', 'Closed');
        $('#trial-design-toggle .toggle-off').css('display', 'none');

        $('.trial-design-arrow').css('transform', 'rotate(0deg)');
        $('.trial-design-arrow').css('margin-top', '3px');
      });

      $('#trial-design-collapse').on('show.bs.collapse', function () {
        $('#trial-design-expanded').css('opacity', '1');
        $('#trial-design-expanded').css('display', 'block');

        $('#trial-design-toggle .toggle-on').css('display', 'none');
        $('.tab-toggle').attr('data-label', 'Opened');
        $('#trial-design-toggle .toggle-off').css('display', 'block');

        $('.trial-design-arrow').css('transform', 'rotate(180deg)');
        $('.trial-design-arrow').css('margin-top', '0');
      });

      $('#trial-design-collapse').collapse('hide');
    }

    // MOA Interactive
    if ( $( "#moa-interactive" ).length ) {
      setupMOAInteractive();
    }

    if ( $("#library-items").length ) {
      $( '.download-btn' ).hover(
        function() {
          var el = $(this).find('.icon-download-color');
          TweenMax.to(el, 0.25, {fill:'white'});
        }, function() {
          var el = $(this).find('.icon-download-color');
          TweenMax.to(el, 0.25, {fill:'#005F9E'});
        }
      );
    }

    // if ( $("#form-step-sign-up").length ) {
    //   const npi = $('#npi');
    //   const npiLookupButton = $('#npi-lookup-button');
    //   const npiToggle = $('#no-npi');
    //   const signUpButton = $('#sign-up-button');


    //   npi.keypress(function(event) {
    //     if(event.which !== 8 && isNaN(String.fromCharCode(event.which)) || npi.val().length === 10){
    //       event.preventDefault(); //stop character from entering input
    //     }
    //   })
    //   npi.blur(function() {
    //     if(npi.val().length !== 10){
    //       console.log('just invalid invalid');
    //     } else {
    //       console.log('network validate');
    //     }
    //   })

    //   npiLookupButton.click(function() {
    //     console.log('NPI Lookup');
    //   })

    //   npiToggle.click(function() {
    //     if (npiToggle.is(":checked")) {
    //       npi.prop('disabled', true);
    //       npi.val('');
    //     } else {
    //       npi.prop('disabled', false);
    //     }
    //   })

    //   signUpButton.click(function(event){
    //     event.preventDefault();
    //     console.log('form submission handler');
    //   })
    // }

  }

  function setupMOAInteractive() {
    TweenMax.to('#moa-interactive', 0.5, {autoAlpha:1});
    TweenMax.set(['.moa-img2', '.moa-img3'], {autoAlpha:0});
    TweenMax.set(['.moa-img1'], {autoAlpha:1});

    TweenMax.set('.moa-btn1', {backgroundPosition: '0px -58px'});

    $( ".moa-btn1" ).click(function()
    {
      TweenMax.to(['.moa-img2', '.moa-img3'], 0.5, {autoAlpha:0});
      TweenMax.to(['.moa-img1'], 0.5, {autoAlpha:1});

      TweenMax.set('.moa-btn1', {backgroundPosition: '0px -58px'});
      TweenMax.set(['.moa-btn2', '.moa-btn3'], {backgroundPosition: '0px 0px'});
    });

    $( ".moa-btn2" ).click(function()
    {
      TweenMax.to(['.moa-img1', '.moa-img3'], 0.5, {autoAlpha:0});
      TweenMax.to(['.moa-img2'], 0.5, {autoAlpha:1});

      TweenMax.set('.moa-btn2', {backgroundPosition: '0px -58px'});
      TweenMax.set(['.moa-btn1', '.moa-btn3'], {backgroundPosition: '0px 0px'});
    });

    $( ".moa-btn3" ).click(function()
    {
      TweenMax.to(['.moa-img2', '.moa-img1'], 0.5, {autoAlpha:0});
      TweenMax.to(['.moa-img3'], 0.5, {autoAlpha:1});

      TweenMax.set('.moa-btn3', {backgroundPosition: '0px -58px'});
      TweenMax.set(['.moa-btn2', '.moa-btn1'], {backgroundPosition: '0px 0px'});
    });
  }


  $(document).ready(init);
})(jQuery);
