'use strict';

(function () {
  document.addEventListener('DOMContentLoaded', function (evt) {
    var taggedItems = [];

    var hrefTags = gtmHrefTagger();

    var linkTags = gtmLinksTagger(hrefTags);
    taggedItems = taggedItems.concat(linkTags);

    var extLinks = gtmOuterLinksTagger(hrefTags);
    taggedItems = taggedItems.concat(extLinks);

    var interstitials = gtmInterstitialTagger();
    taggedItems = taggedItems.concat(interstitials);

    var temp = [];

    var topNavTags = gtmNavTagger('top');
    taggedItems = taggedItems.concat(topNavTags);

    temp = temp.concat(topNavTags);

    var headerTags = gtmHeaderTagger();

    taggedItems = taggedItems.concat(headerTags);
    temp = temp.concat(headerTags);

    var footerTags = gtmFooterTagger();
    taggedItems = taggedItems.concat(footerTags);
    temp = temp.concat(footerTags);

    var subNavTags = gtmNavTagger('sub');
    taggedItems = taggedItems.concat(subNavTags);
    temp = temp.concat(subNavTags);

    var gtmCTAs = gtmCTATagger();
    taggedItems = taggedItems.concat(gtmCTAs);

    var pdfTags = gtmPDFFilter(hrefTags);
    taggedItems = taggedItems.concat(pdfTags);

    var shareTags = gtmShareTagger(extLinks);
    taggedItems = taggedItems.concat(shareTags);

    var accordionTags = gtmAccordionTagger();
    taggedItems = taggedItems.concat(accordionTags);

    var formTags = gtmFormTagger();
    taggedItems = taggedItems.concat(formTags);

    var sectionScrollTags = gtmSectionScrollTagger();
    taggedItems = taggedItems.concat(sectionScrollTags);

    var manualCategoryTags = gtmManualCategory();
    taggedItems = taggedItems.concat(manualCategoryTags);

    var searchTags = gtmSearchTagger();
    taggedItems = taggedItems.concat(searchTags);

    taggedItems = taggedItems.filter(function (item) {
      return item !== null ? item : null;
    });

    taggedItems = gtmLabelFix(taggedItems);

    var fixedAnchorEls = gtmAnchorFix();
    fixedAnchorEls;
    // console.dir(fixedAnchorEls);

    var tested = testGTMClasses(taggedItems);
    console.assert(tested, {
      tested: tested,
      error: 'Not all elements have a single class starting with "gtm-".'
    });

    // console.dir(taggedItems);
    // console.dir(exceptions);
  });
  var trimSpaces = new RegExp(/\s+/);
  var exceptions = [];

  function gtmLabelFix(tags) {
    return tags.map(function (tag) {
      var temp = tag.dataset.gtmEventLabel || 'TEXT NOT FOUND';
      // tag.dataset.gtmEventLabel = temp.split(new RegExp(/[\s\r\n\W]+/, 'gi')).join(' ');
      var regex = new RegExp(/[\W\r\n]+/, 'gmi');
      tag.dataset.gtmEventLabel = temp.replace(regex, ' ');

      // fix for the accordion items that only want the first two words of their items' closest label candidate
      if (tag.classList.contains('gtm-accordion')) {
        var short = temp.split(/[\W\r\n]+/).slice(0, 2).join(' ');
        tag.dataset.gtmEventLabel = short;
      }
      return tag;
    });
  }

  function gtmSearchTagger() {
    var value = [];
    var searchTexts = Array.prototype.slice.call(document.body.querySelectorAll('.gtm-search-text'));
    searchTexts = searchTexts.map(function (tag) {
      tag.dataset.gtmEventCategory = 'Site Search';
      tag.dataset.gtmEventAction = 'Search';
      var parent = tag.parentElement;
      var mySearch = parent.querySelector('.gtm-search');
      while (mySearch === null) {
        parent = parent.parentElement;
        mySearch = parent.querySelector('.gtm-search');
      }
      if (!!tag.dataset.value || !!tag.value) {
        if (!!tag.value) {
          tag.dataset.gtmEventLabel = tag.value;
        }
        mySearch.dataset.value = tag.dataset.value || tag.value;
        mySearch.dataset.gtmEventLabel = tag.dataset.value || tag.value;
      }

      tag.onkeypress = function (e) {
        if (!e) e = window.event;
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
          mySearch.dataset.value = e.target.value;
          mySearch.dataset.gtmEventLabel = e.target.value;
        }
      };

      tag.addEventListener('blur', function (evt) {
        // tag.dataset.value = evt.target.value;
        mySearch.dataset.gtmEventLabel = evt.target.value;
      });

      tag.addEventListener('change', function (evt) {
        // tag.dataset.value = evt.target.value;
        mySearch.dataset.gtmEventLabel = evt.target.value;
      });

      mySearch.addEventListener('click', function (e) {
        mySearch.dataset.gtmEventLabel = tag.dataset.value || '';
      });

      mySearch.addEventListener('submit', function (e) {
        mySearch.dataset.gtmEventLabel = tag.dataset.value || '';
      });

      return tag;
    });

    value = value.concat(searchTexts);

    return value;
  }

  function testGTMClasses(tags) {
    var value = tags.every(function (el) {
      var classList = Array.prototype.slice.call(el.classList);
      var count = 0;
      if (classList.length > 1) classList.forEach(function (attr) {
        count = attr.indexOf('gtm-') === 0 ? count + 1 : count;
        if (count > 1) {
          exceptions.push(el);
        }
      });

      return count <= 1 ? true : false;
    });
    return value;
  }

  // marks elements with manually set classes starting with "gtm-" as elements not to be modded, changes/adds a dataset attr that is checked for "gtm" class mods
  function markGTMClassTags() {
    var value = Array.prototype.slice.call(document.body.querySelectorAll('*'));
    value = value.map(function (tag) {
      var classList = Array.prototype.slice.call(tag.classList);
      classList.forEach(function (attr) {
        if (attr.indexOf('gtm-') === 0) {
          tag.dataset.gtmManualClass = attr;
        }
      });
      return tag;
    });
    return value;
  }

  // returns all tags with an href
  function gtmHrefTagger() {
    var value = [];
    var allItems = markGTMClassTags(); // sets up the pre-marking of all elements in DOM
    var filter = allItems.filter(function (tag) {
      return !!tag.href && !!!tag.classList.contains('gtm-cta');
    });

    filter = filter.map(function (tag) {
      var label = getTagLabel(tag);
      tag = dataGTMOverride(tag, 'data-gtm-event-action', 'Click');
      tag = dataGTMOverride(tag, 'data-gtm-event-label', label);
      return tag;
    });
    value = value.concat(filter);
    return value;
  }

  function gtmPDFFilter(tags) {
    var filter = new RegExp('pdf$');
    return tags.filter(function (tag) {
      if (filter.test(tag.href)) {
        tag = dataGTMOverride(tag, 'data-gtm-event-action', 'Download', 'Download');
        tag = classListCheck(tag, 'pdf');
        tag = dataGTMOverride(tag, 'data-gtm-event-category', 'PDF', 'PDF');
        return tag;
      }
    });
  }

  function gtmLinksTagger(tags) {
    var value = [];
    var filter = new RegExp('pdf$');

    var links = tags.filter(function (tag) {
      return !filter.test(tag.href) && !tag.classList.contains('gtm-cta');
    });

    value = links.map(function (tag) {
      tag = classListCheck(tag, 'link-internal');
      var label = getTagLabel(tag);
      tag = dataGTMOverride(tag, 'data-gtm-event-label', label);
      tag = dataGTMOverride(tag, 'data-gtm-event-category', 'Internal Link');
      return tag;
    });
    return value;
  }

  function gtmOuterLinksTagger(tags) {
    var value = [];
    var hostName = location.hostname;
    var links = tags.filter(function (tag) {
      if (tag.href.indexOf(hostName) <= -1) {
        tag = classListCheck(tag, 'link-external');
        //   tag = dataGTMOverride(tag, 'data-gtm-event-category', 'Outbound Link', 'Outbound Link');
        return tag;
      }
    });
    value = value.concat(links);
    return value;
  }

  // fixes elements nested inside of anchors w/ ancestral a tag dataset and class that starts w/ 'gtm-'
  function gtmAnchorFix() {
    var value = [];
    var anchors = Array.prototype.slice.call(document.body.querySelectorAll('a')).filter(function (link) {
      return new RegExp(/^gtm-/).test(link.className);
    });

    anchors = anchors.map(function (tag) {
      var className = tag.className.match(/^gtm-[-\w]+/)[0];
      var label = tag.dataset.gtmEventLabel;
      var category = tag.dataset.gtmEventCategory;
      var action = tag.dataset.gtmEventAction;

      Array.prototype.slice.call(tag.querySelectorAll('*')).forEach(function (kid) {
        kid.dataset.gtmManualClass = className;
        kid = classListCheck(kid, className.substr(4));
        kid.dataset.gtmEventLabel = label;
        kid.dataset.gtmEventCategory = category;
        kid.dataset.gtmEventAction = action;
        value = value.concat(kid);
      });
      return tag;
    });
    value = value.concat(anchors);
    return value;
  }

  function gtmNavTagger(targetEl) {
    var isTop = targetEl === 'top' ? true : false;
    var gtmClass = isTop ? 'gtm-top-nav' : 'gtm-sub-nav';
    var gtmCategory = isTop ? 'Top Navigation' : 'Sub Navigation';
    var selector = '.' + gtmClass + '-parent';

    var navKids = searchforAnchorButton(selector, gtmCategory, gtmClass);
    navKids = navKids.map(function (tag) {
      if (targetEl === 'top') {
        tag = classListCheck(tag, 'top-nav');
      } else {
        tag = classListCheck(tag, 'sub-nav');
      }
      return tag;
    });
    return navKids;
  }
  // takes a ending to a string that SHOULDN'T match and a string to be tested against, returns true if it begins with 'gtm-' but does not end the same as notSuffix

  function notMatch(notSuffix, testStr) {
    var unMatch = new RegExp('^gtm-(?!' + notSuffix + ').*', 'gi');
    return unMatch.test(testStr);
  }

  // returns element where specific classes that START w/ 'gtm-' but DO NOT end w/ notSuffix are removed
  function classListCheck(el, notSuffix) {
    var classList = Array.prototype.slice.call(el.classList);
    notSuffix = el.dataset.gtmManualClass === undefined ? notSuffix : el.dataset.gtmManualClass.substr(4);

    if (el.dataset.gtmManualClass === undefined) {
      var tempClass = 'gtm-' + notSuffix;
      el.classList.add(tempClass);
    }

    classList.forEach(function (term) {
      if (notMatch(notSuffix, term)) {
        el.classList.remove(term);
      }
    });
    return el;
  }

  function gtmAccordionTagger() {
    var value = [];
    var accordionParent = document.body.querySelector('.gtm-accordion-parent');
    var action = 'Expand/Close';
    var category = 'Accordion';
    var accKids = !!accordionParent ? Array.prototype.slice.call(accordionParent.children) : [];
    accKids = accKids.map(function (el) {
      var label = getTagLabel(el);
      el.dataset.gtmEventLabel = label;
      el.dataset.gtmEventAction = action;
      el.dataset.gtmEventCategory = category;
      el.classList.add('gtm-accordion');
      var progeny = Array.prototype.slice.call(el.querySelectorAll('*')).map(function (desc) {
        desc.dataset.gtmEventLabel = label; //resolve this!!
        desc.dataset.gtmEventAction = action;
        desc.dataset.gtmEventCategory = category;
        desc.classList.add('gtm-accordion');
        return desc;
      });
      value = value.concat(progeny);
      return el;
    });
    value = value.concat(accordionParent, accKids);
    return value;
  }

  function gtmHeaderTagger() {
    var headerTags = headerFooterTagger(true);
    headerTags = headerTags !== null ? headerTags.map(function (tag) {
      tag = classListCheck(tag, 'header');
      return tag;
    }) : [];
    return headerTags;
  }

  function gtmFooterTagger() {
    var footerTags = headerFooterTagger(false);
    footerTags = footerTags !== null ? footerTags.map(function (tag) {
      tag = classListCheck(tag, 'footer');
      return tag;
    }) : [];
    return footerTags;
  }

  function headerFooterTagger(isHeader) {
    var selector = isHeader ? 'header, .gtm-header' : 'footer, .gtm-footer';
    var className = isHeader ? 'gtm-header' : 'gtm-footer';
    var category = isHeader ? 'Header' : 'Footer';
    var innerTags = searchforAnchorButton(selector, category, className);
    return innerTags;
  }

  function gtmInterstitialTagger() {
    var allTags = Array.prototype.slice.call(document.body.getElementsByClassName('gtm-interstitial')) || [];
    allTags = allTags.map(function (tag) {
      var label = getTagLabel(tag);
      tag = classListCheck(tag, 'interstitial');
      tag = dataGTMOverride(tag, 'data-gtm-event-category', 'Interstitial');
      tag = dataGTMOverride(tag, 'data-gtm-event-action', 'Click');
      tag = dataGTMOverride(tag, 'data-gtm-event-label', label);
      return tag;
    });
    return allTags;
  }

  function gtmCTATagger() {
    var gtmCTAs = document.body.getElementsByClassName('gtm-cta');
    gtmCTAs = gtmCTAs !== null ? Array.prototype.slice.call(gtmCTAs).map(function (tag) {
      var label = getTagLabel(tag);
      tag = classListCheck(tag, 'cta');
      // tag = dataGTMOverride(tag, 'data-gtm-event-category', 'Main CTA', 'Main CTA');
      tag = dataGTMOverride(tag, 'data-gtm-event-label', label);
      tag = dataGTMOverride(tag, 'data-gtm-event-action', 'Click', 'Click');
      return tag;
    }) : [];
    return gtmCTAs;
  }

  // returns the closest innerText of a tag if text is nested
  function getTagLabel(el) {
    var content = el.dataset.gtmEventLabel || el.textContent.trim() || '';
    content = content.replace(trimSpaces, ' ').trim();
    if (content.length > 0 && content !== null) {
      return content;
    } else {
      var innerTags = Array.prototype.slice.call(el.querySelectorAll('*')) || [];
      // grabs everything...
      return innerTags.length > 0 ? function () {
        var value = '';
        for (var i = 0; i < innerTags.length; i++) {
          var currText = innerTags[i].textContent || '';
          if (!!currText && currText.length >= 1 && curText !== '') {
            value = currText.split(new RegExp(/[\s\r\n\W]+/, 'gi')).join(' ');
            break;
          } else {
            value = 'TEXT NOT FOUND';
          }
        }
        return value;
      }() : 'TEXT NOT FOUND';
    }
  }

  function gtmFormTagger() {
    var allFormTags = [];
    var formTags = Array.prototype.slice.call(document.body.querySelectorAll('input[type=text], input[type=check], input[type=radio]'));
    formTags = formTags.filter(function (input) {
      if (input.classList.contains('gtm-search-text') === false) {
        return input;
      }
    });

    formTags = Array.isArray(formTags) ? formTags : [];
    formTags = handleFormTags(formTags);
    allFormTags = allFormTags.concat(formTags);

    var selectTags = Array.prototype.slice.call(document.body.querySelectorAll('select'));
    selectTags = Array.isArray(selectTags) ? selectTags : [];
    selectTags = handleSelectTags(selectTags);
    allFormTags = allFormTags.concat(selectTags);

    var submitTags = Array.prototype.slice.call(document.body.querySelectorAll('submit'));
    submitTags = Array.isArray(submitTags) ? submitTags : [];
    submitTags = handleSubmitTags(submitTags);
    allFormTags = allFormTags.concat(submitTags);

    allFormTags = allFormTags.map(function (tag) {
      tag = classListCheck(tag, 'form');
      return tag;
    });
    return allFormTags;
  }

  function handleSubmitTags(submitTags) {
    if (submitTags.length <= 0) {
      return submitTags;
    } else {
      return submitTags.map(function (tag) {
        tag = dataGTMOverride(tag, 'data-gtm-event-category', 'Registration Form');
        tag = dataGTMOverride(tag, 'data-gtm-event-action', 'Click');
        tag.addEventListener('click', function (e) {
          tag = dataGTMOverride(tag, 'data-gtm-event-label', 'Successful Submit');
        });
        return tag;
      });
    }
  }

  function handleSelectTags(selectTags) {
    if (selectTags.length <= 0) {
      return selectTags;
    } else {
      return selectTags.map(function (tag) {
        tag = dataGTMOverride(tag, 'data-gtm-event-category', 'Registration Form');
        tag.addEventListener('change', function (evt) {
          tag = dataGTMOverride(tag, 'data-gtm-event-label', evt.target.value);
        });
        tag = dataGTMOverride(tag, 'data-gtm-event-action', 'Click');
        return tag;
      });
    }
  }

  function handleFormTags(formTags) {
    if (formTags.length <= 0) {
      return formTags;
    } else {
      return formTags.map(function (tag) {
        tag = dataGTMOverride(tag, 'data-gtm-event-category', 'Registration Form');
        tag = classListCheck(tag, 'form');
        tag = handleFormLabel(tag);
        tag = handleFormActions(tag);
        return tag;
      });
    }
  }

  // returns all external links, changes those with social media links
  function gtmShareTagger(links) {
    var shareRegEx = new RegExp('(facebook|twitter|youtube|medium.com|instagram|pinterest|linkedin\.com/company){1,}');
    var valueTags = links.map(function (tag) {
      if (shareRegEx.test(tag.href)) {
        var label = tag.href.match(shareRegEx)[0];
        label = label[0].toUpperCase() + label.substr(1);
        label = label.replace(trimSpaces, label);
        tag = classListCheck(tag, 'share');
        tag = dataGTMOverride(tag, 'data-gtm-event-label', label, label);
        tag = dataGTMOverride(tag, 'data-gtm-event-category', 'Share', 'Share');
        tag = dataGTMOverride(tag, 'data-gtm-event-action', 'Click');
        Array.prototype.slice.call(tag.querySelectorAll('*')).forEach(function (child) {
          child = classListCheck(child, 'share');
          child = dataGTMOverride(child, 'data-gtm-event-label', label, label);
          child = dataGTMOverride(child, 'data-gtm-event-category', 'Share', 'Share');
          child = dataGTMOverride(child, 'data-gtm-event-action', 'Click');
        });
      }
      return tag;
    });
    return valueTags;
  }

  // looks for elements with the class 'gtm-category' assigns its children with its custom category (dataset.gtmEventCategory)
  // NOTE: this does not work on nested tags w/ gtm-category class
  function gtmManualCategory() {
    var manualCategoryTags = Array.prototype.slice.call(document.body.querySelectorAll('.gtm-category')) || [];
    var value = [];
    var categories = manualCategoryTags.forEach(function (manualTag) {
      var categoryCounter = 1;

      if (!!!manualTag.dataset.gtmEventCategory) {
        var tempCat = 'Category-' + categoryCounter;
        manualTag = dataGTMOverride(manualTag, 'data-gtm-event-category', tempCat);
        categoryCounter += 1;
      }
      manualTag.dataset.gtmEventLabel = getTagLabel(manualTag);
      manualTag = dataGTMOverride(manualTag, 'data-gtm-event-action', 'Click');

      var label = manualTag.dataset.gtmEventLabel;
      var tempCategory = manualTag.dataset.gtmEventCategory;
      var defAction = manualTag.dataset.gtmEventAction;

      var progeny = Array.prototype.slice.call(manualTag.querySelectorAll('*'));

      progeny = progeny.map(function (desc) {
        if (desc.classList.contains('gtm-category') === false) {
          desc.dataset.gtmEventCategory = tempCategory;
          desc = dataGTMOverride(desc, 'data-gtm-event-label', label);
          desc = dataGTMOverride(desc, 'data-gtm-event-action', defAction, 'Click');
        }
        return desc;
      });
      value.push(manualTag);
      value = value.concat(progeny);

      manualTag = classListCheck(manualTag, 'category');
      return manualTag;
    });

    return value;
  }

  function gtmSectionScrollTagger() {
    var sections = Array.prototype.slice.call(document.body.querySelectorAll('section')) || [];
    sections = sections.length > 0 ? sections.map(function (tag) {
      tag = dataGTMOverride(tag, 'data-gtm-event-category', 'Scroll to Section');
      tag = dataGTMOverride(tag, 'data-gtm-event-action', 'Scroll');
      var label = tag.name || tag.id;
      tag = dataGTMOverride(tag, 'data-gtm-event-label', label);
      tag = classListCheck(tag, 'section-scroll');
      return tag;
    }) : [];
    return sections;
  }

  function handleFormLabel(tag) {
    var el = tag;
    var label = el.name.trim() || el.id.trim();
    if (tag.type.toLowerCase() === 'radio') {
      label = tag.value.trim();
    }
    label = label.replace(trimSpaces, label);
    // label for attr. refers to tag id or name
    el = dataGTMOverride(el, 'data-gtm-event-label', label);
    return el;
  }

  function handleFormActions(tag) {
    var el = tag;
    var type = '';
    switch (el.type.toLowerCase()) {
      case 'text':
        type = 'Filled';
        break;

      case 'check':
      case 'radio':
        type = 'Checked';
        break;

      default:
        break;
    }
    el = dataGTMOverride(el, 'data-gtm-event-action', type);
    return el;
  }

  function searchforAnchorButton(selector, category, className) {
    var action = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'Click';

    var element = document.body.querySelector(selector);
    var value = element !== null ? Array.prototype.slice.call(element.querySelectorAll('*')).filter(function (el) {
      var tagName = el.tagName.toLowerCase();
      if (tagName === 'a' || tagName === 'button') {
        var label = getTagLabel(el);
        el = dataGTMOverride(el, 'data-gtm-event-action', action);
        el = dataGTMOverride(el, 'data-gtm-event-category', category);
        el = dataGTMOverride(el, 'data-gtm-event-label', label);
        var notSuffix = className.substr(4);
        el = classListCheck(el, notSuffix);
        return el;
      }
    }) : [];

    return value;
  }

  function dataGTMOverride(el, attrName, setAttr) {
    var failSafe = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

    el.setAttribute(attrName, failSafe || el.getAttribute(attrName) || setAttr);
    return el;
  }

  var runSearchCheck = setTimeout(gtmSearchTagger, 200);
  runSearchCheck;
})();