'use strict';

// ACCORDION JS


// $('.collapseBtn').prop("disabled",true);
$('.collapseBtn').css("pointer-events", 'none');

// $('.collapseBtn').css('opacity','0.5');
$(".expandBtn").on('click', function () {

    $('.toggle').attr('data-gtm-event-action', 'Close');

    $('.faqBody .backdropContainer').css('height', '3500px');

    //$('.expandBtn').prop("disabled",true);
    $('.expandBtn').css("pointer-events", 'none');

    $('.expandBtn').css("background", "#ff9522");
    $('.expandBtn').css("color", "#fff");
    $('.collapseBtn').css("background", "#fff");
    $('.collapseBtn').css("color", "#ff9522");

    // $('.collapseBtn').prop("disabled",false);
    $('.collapseBtn').css("pointer-events", 'auto');
    $(".inner").addClass('show');
    $(".inner").removeClass('hide');
    $('.inner').slideDown(150);
    $('.minusIcon').css('display', 'block');
    $('.plusIcon').css('display', 'none');
});

$(".collapseBtn").on('click', function () {

    $('.gtm-accordion').attr('data-gtm-event-action', 'Expand');

    $('.faqBody .backdropContainer').css('height', '2480px');

    $('.collapseBtn').css("background", "#ff9522");
    $('.collapseBtn').css("color", "#fff");
    $('.expandBtn').css("background", "#fff");
    $('.expandBtn').css("color", "#ff9522");

    // $('.expandBtn').prop("disabled",false);
    $('.expandBtn').css("pointer-events", 'auto');

    // $('.collapseBtn').prop("disabled",true);
    $('.collapseBtn').css("pointer-events", 'none');
    $('.inner').slideUp(150);
    $(".inner").removeClass('show');
    $(".inner").addClass('hide');
    $('.minusIcon').css('display', 'none');
    $('.plusIcon').css('display', 'block');
});

$('.toggle').click(function () {

    $('.expandBtn').css("background", "#fff");
    $('.expandBtn').css("color", "#ff9522");
    $('.collapseBtn').css("background", "#fff");
    $('.collapseBtn').css("color", "#ff9522");

    var $this = $(this);

    if ($this.next().hasClass('show')) {

        $(this).attr('data-gtm-event-action', 'Expand');

        //alert("close");
        $this.next().removeClass('show');
        $this.next().addClass('hide');
        $this.next().slideUp(150);

        $this.find('.minusIcon').css('display', 'none');
        $this.find('.plusIcon').css('display', 'block');
    } else {

        $(this).attr('data-gtm-event-action', 'Close');

        $this.find('.minusIcon').css('display', 'block');
        $this.find('.plusIcon').css('display', 'none');
        $this.next().removeClass('hide');

        $this.next().addClass('show');
        $this.next().slideDown(150);
    }

    if ($('.inner').hasClass('show')) {
        // $('.collapseBtn').prop("disabled",false);
        $('.collapseBtn').css("pointer-events", 'auto');
    }

    if ($('ul').hasClass('hide')) {

        //alert("hide");
        //$('.expandBtn').prop("disabled",false);
        $('.expandBtn').css("pointer-events", 'auto');
    }

    // if (($('.accordion1 ul').hasClass('hide')) && ($('.accordion2 ul').hasClass('hide')) && ($('.accordion3 ul').hasClass('hide')) && ($('.accordion4 ul').hasClass('hide')) && ($('.accordion5 ul').hasClass('hide')) && ($('.accordion6 ul').hasClass('hide')) && ($('.accordion7 ul').hasClass('hide')) && ($('.accordion8 ul').hasClass('hide')) && ($('.accordion9 ul').hasClass('hide')) && ($('.accordion10 ul').hasClass('hide'))){


    //    // $('.expandBtn').prop("disabled",false);
    //     $('.expandBtn').css("pointer-events",'auto');

    //     $('.collapseBtn').css("background","#ff9522");
    //     $('.collapseBtn').css("color","#fff");
    //     $('.expandBtn').css("background","#fff");
    //     $('.expandBtn').css("color","#ff9522");

    //    // $('.collapseBtn').prop("disabled",true);
    //     $('.collapseBtn').css("pointer-events",'none');

    // }

    if ($('.accordionWrapper ul').hasClass('hide')) {
        $('.faqBody .backdropContainer').css('height', '2480px');
    }

    if ($('.accordion1 ul').hasClass('show') && $('.accordion2 ul').hasClass('show') && $('.accordion3 ul').hasClass('show') && $('.accordion4 ul').hasClass('show') && $('.accordion5 ul').hasClass('show') && $('.accordion6 ul').hasClass('show') && $('.accordion7 ul').hasClass('show') && $('.accordion8 ul').hasClass('show') && $('.accordion9 ul').hasClass('show') && $('.accordion10 ul').hasClass('show')) {
        // $('.expandBtn').prop("disabled",true);
        //     $('.expandBtn').css("pointer-events",'none');

        //     $('.expandBtn').css("background","#ff9522");
        //     $('.expandBtn').css("color","#fff");
        //     $('.collapseBtn').css("background","#fff");
        // $('.collapseBtn').css("color","#ff9522");


        $('.faqBody .backdropContainer').css('height', '3500px');

        //      //   $('.collapseBtn').prop("disabled",false);
        //         $('.collapseBtn').css("pointer-events",'auto');

    }
});

if ($('body').hasClass('homePage')) {

    $('.homeIconOrange').css('display', 'inline');
    $('.homeIcon').css('display', 'none');
} else {

    $('.homeLink').on('mouseover', function () {
        $('.homeIconOrange').css('display', 'inline');
        $('.homeIcon').css('display', 'none');
    });

    $('.homeLink').on('mouseout', function () {
        $('.homeIconOrange').css('display', 'none');
        $('.homeIcon').css('display', 'inline');
    });
}

// let diseaseLink = document.querySelector('.diseaseLink');
// let arrow2 = document.querySelector(".nav-triangle2");

// diseaseLink.addEventListener('mouseover', () => {
//    arrow2.style.display = "block";
//   })

//   diseaseLink.addEventListener('mouseout', () => {
//     arrow2.style.display = "none";
//    })

$('.faqLink, .diseaseLink, .auryxiaLink, .supportLink, #auryxiaNav').mouseover(function () {
    var $this = $(this);
    $this.find('.nav-link').css('color', '#ff9522');
});

$('.faqLink, .diseaseLink, .auryxiaLink, .supportLink, #auryxiaNav').mouseout(function () {
    var $this = $(this);
    $this.find('.nav-link').css('color', '#5c6871');
});

var has_submenu = document.querySelector('.has-submenu');
var submenu = document.querySelector('.submenu');
var submenu_height = submenu.childElementCount * 35;

has_submenu.addEventListener('mouseover', function () {

    submenu.style.height = submenu_height + 'px';
    $('.nav-triangle3').css('display', 'block');
    $(' .auryxiaDesktopLink a').attr('id', 'hoverColor');
});
has_submenu.addEventListener('mouseout', function () {

    submenu.style.height = '0px';
    $('.nav-triangle3').css('display', 'none');
    $(' .auryxiaDesktopLink a').attr('id', '');
});

submenu.addEventListener('mouseover', function () {
    $('.nav-triangle3').css('display', 'block');
    submenu.style.height = submenu_height + 'px';
    $(' .auryxiaDesktopLink a').attr('id', 'hoverColor');
});
submenu.addEventListener('mouseout', function () {
    $('.nav-triangle3').css('display', 'none');
    submenu.style.height = '0px';
    $(' .auryxiaDesktopLink a').attr('id', '');
});

$('.navbar-toggler-icon').on('click', function () {
    if ($("#navbarSupportedContent").is(":visible")) {
        $('.navbar-toggler .navbar-toggler-icon').css('background-image', 'url(/wp-content/themes/keryx/assets/img/ida-patient/menu-Icon.png)');
    } else {
        $('.navbar-toggler .navbar-toggler-icon').css('background-image', 'url(/wp-content/themes/keryx/assets/img/ida-patient/xIcon.png)');
    }
});

//LINKS
$('.homePage .boxOne').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/disease-101');
});

$('.homePage .boxTwo').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/101');
});

$('.homePage .boxThree').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/access-support');
});

$('.diseasePage .rightBoxOne').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/101');
});

$('.diseasePage .rightBoxTwo').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/Taking-AURYXIA');
});

$('.AuryxiaPage .rightBoxOne').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/disease-101');
});

$('.AuryxiaPage .rightBoxTwo').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/access-support');
});

$('.takingAuryxiaPage .rightBoxOne').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/101');
});

$('.takingAuryxiaPage .rightBoxTwo').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/disease-101');
});

$('.talkingDoctorPage .rightBoxOne').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/101');
});

$('.talkingDoctorPage .rightBoxTwo').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/access-support');
});

$('.supportPage .rightBoxOne').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/disease-101');
});

$('.supportPage .rightBoxTwo').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/access-support');
});

$('.resourcesPage .rightBoxOne').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/101');
});

$('.resourcesPage .rightBoxTwo').on('click', function () {
    $(location).attr('href', '/iron-deficiency-anemia/patient/access-support');
});

// $('.footerDate').on('click', function(){
//     submenu.style.height = submenu_height + 'px';
//     $('.nav-triangle3').css('display', 'block');
//     $('.auryxiaLink a').css('color', '#f6861f');
// })
