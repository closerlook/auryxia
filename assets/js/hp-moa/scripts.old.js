/*!
    * Start Bootstrap - Creative v6.0.3 (https://startbootstrap.com/themes/creative)
    * Copyright 2013-2020 [object Object]
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-creative/blob/master/LICENSE)
    */
(function($) {
  "use strict"; // Start of use strict
  $(document).ready(function () {
    function toggleDropdown (e) {
      const _d = $(e.target).closest('.dropdown'),
        _m = $('.dropdown-menu', _d);
      setTimeout(function(){
        const shouldOpen = e.type !== 'click' && _d.is(':hover');
        _m.toggleClass('show', shouldOpen);
        _d.toggleClass('show', shouldOpen);
        $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
      }, e.type === 'mouseleave' ? 300 : 0);
    }

    function updateSmallLady() {
      const vpWidth = $(window).width();
      const $hpLady = $('.page-template-template-new-moa-hp-page .patients-section #mobile-card-wrapper');
      const $idaMan = $('.page-template-template-new-moa-ida-page .patients-section #mobile-card-wrapper');

      if ( $hpLady.length ) {
        const smallLadyWidth = 440;
        if ( vpWidth > 750 && vpWidth <= 1160 ) {
          $hpLady.css('background-position-x', vpWidth-smallLadyWidth);
        } else {
          $hpLady.css('background-position-x', 'right');
        }
      }

      if ( $idaMan.length ) {
        const idaManWidth = 390;
        if ( vpWidth > 750 && vpWidth <= 900 ) {
          $idaMan.css('background-position-x', vpWidth-idaManWidth);
        } else {
          $idaMan.css('background-position-x', '90%');
        }
      }
    }

    $('body')
      .on('mouseenter mouseleave','.dropdown',toggleDropdown)
      .on('click', '.dropdown-menu a', toggleDropdown)

    $(window).resize(updateSmallLady);

    $('.collapse-button').click(function() {
      $(this).find('svg').toggleClass('fa-chevron-right fa-chevron-down');
    });

    const videoTitleCard = new TimelineMax({paused: true})
      .to(['.video-controls', '.video-shadow'], 1, {autoAlpha: 0});

    $('.play-video').click(function(e) {
      e.preventDefault();
      // $('.video-controls, .video-shadow').addClass('d-none');
      videoTitleCard.restart();
      videojs.getPlayer('home-bg-video').ready(function() {
        this.play();
      });
    });

    videojs.getPlayer('home-bg-video').ready(function() {
      this.on('ended', function() {
        videoTitleCard.reverse();
        this.currentTime(1);
      });
    });

    updateSmallLady();

    // scroll
    const duration = 300;
    const videoButtonAnimation = new TimelineMax({paused: true})
      .to(".video-controls",duration,{top: '50%',ease: "Sine.easeOut"}, 0)
      .to(".video-controls .stick",duration,{height: 0,ease: "Sine.easeOut"}, 0)
      .to(".video-controls .play-video",duration,{fontSize: 20, lineHeight: '24px', ease: "Sine.easeOut", autoRound: false}, 0)
      .to(".video-controls .play-video img",duration,{width: 40, ease: "Sine.easeOut", autoRound: false}, 0);

    let requestId = null;
    const triggerOffset = 100;
    $(window).scroll(function () {
      if (!requestId) {
        requestId = requestAnimationFrame(update);
        // console.log(`requestId: ${requestId}`);
      }
    });

    function update() {
      let delayStart = window.pageYOffset - triggerOffset;
      // if (delayStart > 0 && ($(window).width() > 767)) {
      if (delayStart > 0) {
        videoButtonAnimation.time(delayStart);
      } else {
        videoButtonAnimation.time(0);
      }
      // console.log(`window + offset: ${window.pageYOffset - triggerOffset}`);
      requestId = null;
    }

    if ( $('.page-template-template-new-moa-hp-page').length ) {
      const HPAnimationOne = new TimelineMax({delay: 1, repeat: -1, repeatDelay: 1})
        .to("#hp-icon-one #bubble_1", 2, {x: +17, y: +16}, 0)
        .to("#hp-icon-one #bubble_2", 2, {x: +7, y: +1}, 0)
        .to("#hp-icon-one #bubble_3", 2, {x: +5, y: +5}, 0)
        .to("#hp-icon-one #bubble_4", 2, {x: +5, y: +5}, 0)
        .to("#hp-icon-one #bubble_5", 2, {x: +5, y: +5}, 0)
        .to("#hp-icon-one #bubble_6", 2, {x: +5, y: +5}, 0)
        .to("#hp-icon-one #bubble_7", 2, {x: +5, y: +5}, 0)
        .to("#hp-icon-one #bubble_8", 2, {x: +5, y: +5}, 0)
        .to("#hp-icon-one #bubble_9", 2, {x: +5, y: +5}, 0);

      const HPAnimationTwo = new TimelineMax({repeat: -1, repeatDelay: 1, ease: "Linear.easeNone"})
        .to("#hp-icon-two #bubble_1", 6, {x: +102, y: +67}, 0)
        .to("#hp-icon-two #bubble_2", 6, {x: +96, y: +85}, 0)
        .to("#hp-icon-two #bubble_3", 6, {x: +96, y: +85}, 0)
        .to("#hp-icon-two #bubble_4", 6, {x: +90, y: +80}, 0)
        .to("#hp-icon-two #bubble_5", 6, {x: +90, y: +80}, 0)
        .to("#hp-icon-two #bubble_10", 6, {x: +90, y: +90}, 0)
        .to("#hp-icon-two #bubble_6", 6, {x: +85, y: +70}, 0)
        .to("#hp-icon-two #bubble_7", 6, {x: +85, y: +70}, 0)
        .to("#hp-icon-two #bubble_8", 6, {x: +85, y: +70}, 0)
        .to("#hp-icon-two #bubble_9", 6, {x: +85, y: +70}, 0);

      const HPAnimationThree = new TimelineMax({repeat: -1, repeatDelay: 1})
        .to("#hp-icon-three #bubble_1", 4, {x:+8, y:+3}, 0)
        .to("#hp-icon-three #bubble_3", 4, {x:-12, y:+6}, 0)
        .to("#hp-icon-three #bubble_4", 4, {x:+16, y: -9}, 0)
        .to("#hp-icon-three #bubble_8", 4, {x:-17, y: +6}, 0)
        .to("#hp-icon-three #bubble_5", 4, {x:+5, y:+5}, 0)
        .to("#hp-icon-three #bubble_5", 4, {autoAlpha: 0}, 0.3)
        .to("#hp-icon-three #bubble_6", 2, {x:+5, y:+5}, 0.5)
        .to("#hp-icon-three #bubble_6", 2, {autoAlpha: 0}, 0.8)
        .to("#hp-icon-three #bubble_7", 2, {x:+5, y:+5}, 1)
        .to("#hp-icon-three #bubble_7", 2, {autoAlpha: 0}, 1.3)
        .to("#hp-icon-three #bubble_9", 2, {x:+5, y:+5}, 1.5)
        .to("#hp-icon-three #bubble_9", 2, {autoAlpha: 0}, 1.8)
        .to("#hp-icon-three #bubble_2", 2, {x:+5, y:+5}, 2)
        .to("#hp-icon-three #bubble_2", 2, {autoAlpha: 0}, 2.3)
    }

    if ( $('.page-template-template-new-moa-ida-page').length ) {
      CustomWiggle.create("Wiggle.uniform", {wiggles:20, type:"uniform"});
      const IDAnimationOne = new TimelineMax({repeat: -1,repeatDelay:2})
        .to("#ida-icon-one #bubble_1", 1, {x: 0.5, ease:"Wiggle.uniform"},0)
        .to("#ida-icon-one #bubble_1 circle", 0.5, {fill: "#ffc61f"}, 0.5)
        .to("#ida-icon-one #bubble_1 #first-path", 0.5, {fill: "#ffb51e"}, 0.5)
        .to("#ida-icon-one #bubble_1 #three", 0.5, {autoAlpha: 0}, 0.5)
        .to("#ida-icon-one #bubble_1 #two", 0.5, {autoAlpha: 1}, 0.5);

      const IDAnimationTwo = new TimelineMax({repeat: -1,repeatDelay:1})
        .to("#ida-icon-two #bubble_1", 4, {x:+105, y:+80});

      const IDAnimationThree = new TimelineMax({repeat: -1,repeatDelay:2})
        .to("#ida-icon-three #bubble_1", 1, {x: 0.5, ease:"Wiggle.uniform"},0)
        .to("#ida-icon-three #bubble_1 circle", 0.5, {fill: "#8a9292"}, 0.5)
        .to("#ida-icon-three #bubble_1 #first-path", 0.5, {fill: "#797e80"}, 0.5)
        .to("#ida-icon-three #bubble_1 #two", 0.5, {autoAlpha: 0}, 0.5)
        .to("#ida-icon-three #bubble_1 #three", 0.5, {autoAlpha: 1}, 0.5);

      const IDAnimationFour = new TimelineMax({repeat: -1, repeatDelay: 2})
        .to("#ida-icon-four #bubble_1", 1, {y:-29, ease: "Linear.easeNone"})
        .to("#ida-icon-four #bubble_1", 0.5, {
          transformOrigin: "50% 0%",
          scaleX: 1.5,
          scaleY: 0.6,
          ease: "Power1.easeInOut",
          yoyo:true,
          repeat:1
        });
    }
  });
})(jQuery); // End of use strict
