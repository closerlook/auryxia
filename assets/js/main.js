var winW = "",
    winH = "";
mobileB = 767, // mobileBreakpoint
    isiPad = "",
    isMobile = false,
    assetDir = '/wp-content/themes/keryx/assets',
    ajaxURL = location.protocol + "//" + document.domain + "/wp-admin/admin-ajax.php";

$(document).ready(function() {

    if (top.location.pathname === '/hyperphosphatemia/patients/'){
        $('.bannerImgPatient, .bannerNewPatient').addClass('bannerImgPatientHidden');
        }
    updateView();
    resizeEvents();

  /*  if ($("body").hasClass("page-id-76") ) {
	$('#supplynote').show();
}*/
    // setupISI();
    // setupGoogleTrack();
    modalEvents();
    globalClickEvents();
    footerCode();

    $(".btn-grid").click(function() {
        $("#dev-grid").fadeToggle();
        return false;
    });

    $(".callout").hover(function() {
        $(this).toggleClass("hover");
    });

    $("#mobile-menu").click(function() {
        $(this).toggleClass("open");
        $("nav").toggleClass("mobile-open");
    });


    // Navigation Dropdowns
    if (!isMobile) {
        $(".menu-item-has-children").hover(
            function() {
                $(this).addClass("active-state");
                $(this).find(".sub-menu").stop().slideDown();
            },
            function() {
                $(this).removeClass("active-state");
                $(this).find(".sub-menu").stop().slideUp();
            }
        );
    }


    // Mobile Nav Prep
    navCopy = "";
    jj = 0;
    $(".nav-copy").each(function() {
        jj++;
        navCopy += '<li class="mobile-copy mobile-item-'+jj+'">' + $(this).prop('outerHTML') + '</li>';
    });
    $("nav .menu").append(navCopy);
    $(".mobile-copy").addClass("mobile");
    $(".mobile-copy a").removeClass("desktop");


    $('.faqs').exists(function() {
        setupFAQs();
    });

    $('#tab-wrap').exists(function() {
        setupTabs();
    });


    $('.portal-wrap').exists(function() {
        setupVeevaPortal();
    });

    // Main Navigation Analytics
    $("#menu-item-2606").on("click", function(){
        $("#menu-item-2606 a").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Home'
        });
    });
    $("#menu-item-204").on("click", function () {
        $("#menu-item-204 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Efficacy'
        });
    });
    $("#menu-item-206").on("click", function () {
        $(".sub-menu #menu-item-206 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'UNMET NEED'
        });
    });
    $("#menu-item-205").on("click", function () {
        $(".sub-menu #menu-item-205 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'PIVOTAL PHASE III TRIAL'
        });
    });
    $("#menu-item-207").on("click", function () {
        $(".sub-menu #menu-item-207 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'EFFICACY RESULTS'
        });
    });
    $("#menu-item-211").on("click", function () {
        $("#menu-item-211 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Dosing'
        });
    });
    $("#menu-item-213").on("click", function () {
        $(".sub-menu #menu-item-213 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'Guidelines'
        });
    });
    $("#menu-item-212").on("click", function () {
        $(".sub-menu #menu-item-212 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'Administration'
        });
    });
    $("#menu-item-208").on("click", function () {
        $("#menu-item-208 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Safety'
        });
    });
    $("#menu-item-2674").on("click", function () {
        $("#menu-item-2674 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Clinical Pharmacology'
        });
    });
    $("#menu-item-50").on("click", function () {
        $(".sub-menu #menu-item-50 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'MOA'
        });
    });
    $("#menu-item-51").on("click", function () {
        $(".sub-menu #menu-item-51 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'Pharmacodynamics'
        });
    });
    $("#menu-item-54").on("click", function () {
        $("#menu-item-54 a").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Library'
        });
    });
    $("#menu-item-260").on("click", function () {
        $("#menu-item-260 a").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Why Auryxia'
        });
    });
    $("#menu-item-261").on("click", function () {
        $("#menu-item-261 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'How Auryxia Works'
        });
    });
    $("#menu-item-262").on("click", function () {
        $("#menu-item-262 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'How you take it'
        });
    });
    $("#menu-item-263").on("click", function () {
        $("#menu-item-263 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'What to tell your doctor'
        });
    });
    $("#menu-item-264").on("click", function () {
        $("#menu-item-264 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Patient Support'
        });
    });
    $("#menu-item-265").on("click", function () {
        $("#menu-item-265 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'Assistance Programs'
        });
    });
    $("#menu-item-266").on("click", function () {
        $("#menu-item-266 a").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'FAQs'
        });
    });
    $("#menu-item-351").on("click", function () {
        $("#menu-item-351 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Patient Coverage'
        });
    });
    $("#menu-item-674").on("click", function () {
        $(".sub-menu #menu-item-674 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'By Insurance Type'
        });
    });
    $("#menu-item-347").on("click", function () {
        $("#menu-item-347 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Our Assistance Programs'
        });
    });
    $("#menu-item-350").on("click", function () {
        $(".sub-menu #menu-item-350 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'Reimbursement'
        });
    });
    $("#menu-item-348").on("click", function () {
        $(".sub-menu #menu-item-348 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'Copay Program'
        });
    });
    $("#menu-item-349").on("click", function () {
        $(".sub-menu #menu-item-349 a:first").attr({
            'data-element': 'default',
            'data-category': 'Top Nav/Sub',
            'data-action': 'Click',
            'data-label': 'Patient Assistance'
        });
    });
    $("#menu-item-352").on("click", function () {
        $("#menu-item-352 a").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Forms'
        });
    });
    $("#menu-item-327").on("click", function () {
        $("#menu-item-327 a").attr({
            'data-element': 'default',
            'data-category': 'Top Nav',
            'data-action': 'Click',
            'data-label': 'Information For Patients'
        });
    });

    switch(window.location.pathname) {
        case "/hyperphosphatemia/patient-services/patient-coverage/coverage-by-insurance-type/":
        $("#tab-wrap .tab-names.cf a:first-child").on("click", function(){
            $(this).attr({
                'data-element': 'default',
                'data-category': 'Tab',
                'data-action': 'Click',
                'data-label': 'Medicare Part D Tab'
            });
        });
        $("#tab-wrap .tab-names.cf a:nth-child(2)").on("click", function () {
            $(this).attr({
                'data-element': 'default',
                'data-category': 'Tab',
                'data-action': 'Click',
                'data-label': 'Dual Eligible Tab'
            });
        });
        $("#tab-wrap .tab-names.cf a:nth-child(3)").on("click", function () {
            $(this).attr({
                'data-element': 'default',
                'data-category': 'Tab',
                'data-action': 'Click',
                'data-label': 'Commercial Tab'
            });
        });
        $("#tab-wrap .tab-names.cf a:last-child").on("click", function () {
            $(this).attr({
                'data-element': 'default',
                'data-category': 'Tab',
                'data-action': 'Click',
                'data-label': 'Uninsured Patients Tab'
            });
        });
        break;
        case "/hyperphosphatemia/patient-services/our-assistance-programs/reimbursement/":
        $("#tab-wrap .tab-names.cf a:first-child").on("click", function () {
            $(this).attr({
                'data-element': 'tab',
                'data-category': 'Tab',
                'data-action': 'Click',
                'data-label': 'Before Enrollment Tab'
            });
        });
        $("#tab-wrap .tab-names.cf a:nth-child(2)").on("click", function () {
            $(this).attr({
                'data-element': 'tab',
                'data-category': 'Tab',
                'data-action': 'Click',
                'data-label': 'After Enrollment Tab'
            });
        });
        break;
        case "/hyperphosphatemia/mechanism-of-action/pharmacodynamics/":
        $("#tab-wrap .tab-names.cf a:first-child").on("click", function () {
            $(this).attr({
                'data-element': 'tab',
                'data-category': 'Chart',
                'data-action': 'Click',
                'data-label': 'TSAT'
            });
        });
        $("#tab-wrap .tab-names.cf a:nth-child(2)").on("click", function () {
            $(this).attr({
                'data-element': 'tab',
                'data-category': 'Chart',
                'data-action': 'Click',
                'data-label': 'Ferritin'
            });
        });
        break;
        case "/hyperphosphatemia/library/":
        $('.copy a br').remove();
        break;
        case "/hyperphosphatemia/patients/faqs/":
    }

    // Hide Fonts.com Icon
    $('#mti_wfs_colophon, #mti_wfs_colophon img').hide();
    $('#mti_wfs_colophon, #mti_wfs_colophon_anchor, #mti_wfs_colophon_anchor img').css('visibility', 'hidden');

});



/* Update Values */
function updateView() {
    winW = $(window).width();
    winH = $(window).height();
    if (winW < mobileB) {
        isMobile = true;
        $('#vimeo_vid').exists(function() {
            vimeoResize();
        });
    } else {
        isMobile = false;
        $("nav").removeClass("mobile-open");
    }
}


function resizeEvents() {

    /* Window Reize on End Event */
    $(window).bind('resize', function(e) {
        window.resizeEvt;
        $(window).resize(function() {
            clearTimeout(window.resizeEvt);
            window.resizeEvt = setTimeout(function() {
                //Code to be executed after window resize
                updateView();
            }, 250);
        });
    });

    /* Touch Based Events */
    if (Modernizr.touch) {
        window.addEventListener("orientationchange", function() {
            updateView();
        }, false);
    }

}




function setupISI() {
    sectionID = $("#isi").attr("data-isi");
    actionURL = "load_isi_" + sectionID;
    console.log(`ActionURL: ${actionURL}`);
    $.get(ajaxURL, { 'action': actionURL, dataType: "html" },
        function(response) {
            if (response !== 0) {
                /* Append New Markup to Section */
                $('#isi').empty().append(response);
                delete response;
                modalEvents();
                globalClickEvents();

            }
        }
    );

}






/* Basic Click Events that are Global */
function globalClickEvents() {

}




function setupFAQs() {

    $(".faq-controls a").click(function() {
        $(".faq-controls a").removeClass("active");
        $(this).addClass("active");
        if ($(this).hasClass("faq-expand")) {
            // Expand
            $(".faq").addClass("open");
            $(".faq-answer").slideDown();
            // ga('send', 'event', 'FAQ', 'Click', 'Expand All');
            $('.faq-expand').attr({
                'data-element': 'default',
                'data-category': 'Internal Link',
                'data-action': 'Click',
                'data-label': 'Expand All'
            });
        } else {
            // Collapse
            $(".faq").removeClass("open");
            $(".faq-answer").slideUp();
            ga('send', 'event', 'FAQ', 'Click', 'Collapse All');
        }
        return false;
    });

    $(".faq").click(function(e) {
        $(".faq-controls a").removeClass("active");
        $(this).toggleClass("open");

        if ($(this).find(".faq-answer").is(":hidden")) {
            faqText = $(this).find(".faq-question").html();
            // ga('send', 'event', 'FAQ', 'Click', faqText);
            $(this).attr({
                'data-element': 'default',
                'data-category': 'Internal Link',
                'data-action': 'Click',
                'data-label': 'Expand' + ' ' + faqText
            });
        }
        if ($(this).find(".faq-answer").is(":visible")) {
            faqText = $(this).find(".faq-question").html();
            $(this).attr({
                'data-element': 'default',
                'data-category': 'Internal Link',
                'data-action': 'Click',
                'data-label': 'Collapse' + ' ' + faqText
            });
        }

        $(this).find(".faq-answer").slideToggle();
        return false;
    });

    // Internal Links within FAQs
    $(".faq a").click(function() {
        targetType = $(this).attr("target");
        targetLink = $(this).attr("href");
        if (targetType == "_blank") {
            window.open(targetLink);
        } else {
            window.location = targetLink;
        }
        return false;
    });

}


function setupTabs() {

    $(".tab-names a").click(function() {
        tabPosition = $(this).index();
        $(this).addClass("active").siblings().removeClass("active");
        $(".tab").eq(tabPosition).addClass("active").siblings().removeClass("active");
        return false;
    });

    // Tab Hash Values
    if (window.location.hash) {
        hashC = window.location.hash;
        if (hashC.indexOf("tab") >= 0) {
            scrollT = $("#tab-wrap").offset().top;
            $('html, body').animate({ scrollTop: scrollT }, 500);

            tabNum = hashC.slice(4);
            if (tabNum != "") {
                if (tabNum < 5) {
                    //tabNum = Math.abs(tabNum - 1);
                    tabNum = Math.abs(tabNum);
                    $( ".tab-names a").removeClass("active");
                    $( ".tab-names a:nth-child("+tabNum+")").addClass("active");

                    $( ".tab").removeClass("active");
                    $( ".tab:nth-child("+tabNum+")").addClass("active");

                    //$(".tab-names a").eq(tabNum).addClass("active").siblings().removeClass("active");
                    //$(".tab").eq(tabNum).addClass("active").siblings().removeClass("active");
                }
            }
        }
    }

}


function vimeoResize() {
    videoW = winW * 0.9;
    videoH = videoW * (394 / 700);
    $('#vimeo_vid').height(videoH).width(videoW);
}


function setupVeevaPortal() {
    var urlP = window.location.href;
    urlP = urlP.split("?");
    urlP = urlP[1];

    if (!urlP) {
        // URL Parameters Does Not Exists
        portalContent();
    }

}

function showVeeva() {
    var tag = document.createElement('script');
    tag.src = "//js.veevavault.com/ps/vault-v1.js";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var viewer;

    function onVeevaVaultIframeAPIReady() {
        viewer = new VV.Viewer('viewer', {
            height: '630',
            width: '660',
            error: 'We\'re sorry. The document you are trying to view is not available..'
        });
    }
}

function footerCode() {
    if ($('.page-id-1574').length) {
        $(".legal").replaceWith("<div class='legal'>©2015 Keryx Biopharmaceuticals, Inc.<div class='footer-spacer'></div>PP-AUR-US-0178<div class='footer-spacer'></div>09/15</div>");
    }
}

function portalContent() {
    portalContent = '';

    // Journey of the Pill
    $('.page-id-1095').exists(function() {
        portalContent = '<iframe id="vimeo_vid" src="//player.vimeo.com/video/140222302?title=0&amp;byline=0&amp;portrait=0&amp;color=FF8500" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
    });

    // Auryxia Flash Card
    $('.page-id-1097').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/AURYXIA_Flashcard.pdf" target="_blank" class="btn-site">Download AURYXIA Flashcard</a></div>';
    });

    // Patient Plus Flash Card
    $('.page-id-1100').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/Patient_Services_Brochure.pdf" target="_blank" class="btn-site">Download Keryx Patient Plus Flashcard</a></div>';
    });

    // Medicare Part D Coverage Grid
    $('.page-id-1574').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/Access_Flashcard.pdf" target="_blank" class="btn-site">Download Acccess Flashcard</a></div>';
    });

    $('.page-id-1707').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/Keryx-Patient-Plus-Application_from-website.pdf" target="_blank" class="btn-site">Download Keryx Patient Plus Application</a></div>';
    });

    $('.page-id-1710').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/Co-Pay-Coupon-Online.pdf" target="_blank" class="btn-site">Download AURYXIA Coupon</a></div>';
    });


    $('.page-id-1712').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/Renal-Dietitian-Flashcard_final-from-Vault.pdf" target="_blank" class="btn-site">Download Dietitian Flashcard</a></div>';
    });

    $("#viewer").append(portalContent);




}

if (top.location.pathname === '/hyperphosphatemia/clinical-pharmacology/pharmacodynamics/'){
    $('.menu-item-208 a').addClass('orangeColor');
}


$('#hcp #mobile-menu').on('click',function(){
    $('.mobileMenu').toggle();
})



$('#patient #mobile-menu').on('click',function(){
    $('.mobileMenuPatient').toggle();

    if ($('.mobileMenuPatient').is(":visible")){
        $(this).addClass("newMobileImg");
    }else{
        $(this).removeClass("newMobileImg");
    }
})


if ( $('body').attr('id') === 'patient'){

    $('mobileMenu').addClass('hiddenMobile');
    $('.footer-links').addClass('footerAdjust');
  $('.content').addClass("contentMargin");
    $('#footer-info img').addClass('patientFooterLogo');
}


if ( $('body').attr('id') === 'hcp'){
    $('.bannerImgPatient, .bannerNewPatient, .mobileMenuPatient').addClass('bannerImgPatientHidden');
}


if (top.location.pathname !== '/hyperphosphatemia/patients/'){
  $('#patient #menu-patient-navigation #menu-item-3273 a').addClass("cursorHide");
    }



    // $( window ).resize(function() {
    //     if ($(window).width() < 737) {
    //         document.querySelector("#patient #home-hand").src="http://auryxia-dual.fcbstage.com/hyperphosphatemia/wp-content/uploads/0.0_HP_Patient_Header_Home1.jpg";
    //     }else{
    //         document.querySelector("#patient #home-hand").src="https://auryxia-dual.fcbstage.com/hyperphosphatemia/wp-content/uploads/HP_Patient_Header-1.jpg";
    //     }
    //   });



    $('#menu-item-261 a:first, #menu-item-264 a:first').removeAttr("href");
    $('#menu-item-261 a, #menu-item-264 a').css("cursor","pointer");

$('#menu-item-261, #menu-item-264').hover(function(){
    $('#menu-item-261 a:first, #menu-item-264 a:first').removeAttr("href");
    $('#menu-item-261 a, #menu-item-264 a').css("cursor","pointer");
})

// <a style="position: absolute; float: right !important;" class="impSafetyLink" href="#isi">Important Safety Information</a>
$('#patient .cta-links').append('<p class="impSafetyLink-patient">See <a href="#isi">Important Safety Information</a> below</p>');


// $('.page-template-patientspage-faqs-php .cta-isi-sidebar').append('<p class="impSafetyLink-patientFAQ">Please see <a href="#isi">Important Safety Information</a> below</p>');

// $('.page-template-patientspage-patients-home-php .callouts').append('<p class="impSafetyLink-patientHome">Please see <a href="#isi">Important Safety Information</a> below</p>');

$('.page-template-patientspage-patients-home-php .content .grid > .grid-12').append(`
<div class="mobileMidBox-Wrap">
<div class="mobileMidBox">

<img src="/wp-content/themes/keryx/assets/img/ida-patient/home/assistance-icon.png" alt="" class="assistance-icon">
<p class="assistance-p1">LOOKING FOR PERSONALIZED ACCESS ASSISTANCE?</p>
<p class="assistance-p2">Call one of our dedicated AkebiaCares Case Managers</p>
<div class="centerBox">


<ul class="nav justify-content-center">
<li class="nav-item navBorder">
     <a class="nav-link gtm-cta" href="tel:+18556868601" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="call 855-686-8601">855-686-8601</a>
</li>
<li class="nav-item navBorder">
      <a class="nav-link disabled">Monday-Friday</a>
 </li>
  <li class="nav-item">
     <a class="nav-link disabled" >8<span style="font-size: 11px !important;" class="AM">AM</span>-7<span style="font-size: 11px !important;" class="AM">PM</span> ET</a>
 </li>
</ul>
</div>
<p class="impSafetyLink-patientHome">See <a href="#isi">Important Safety Information</a> below</p>
</div></div>`);

