var winW = "",
    winH = "";
    mobileB = 768, // mobileBreakpoint
    isiPad = "",
    isMobile = false,
    assetDir = '/wp-content/themes/keryx/assets',
      oldSite = '/wp-content/themes/keryx/oldsite/akebiacares';
    //ajaxURL = location.protocol + "//" + document.domain + "/wp-admin/admin-ajax.php";

$(document).ready(function() {

    if ($(window).width() <= mobileB) {
        isMobile = true;

    } else {
        isMobile = false;
    }

    resizeEvents();

    //setupISI();

    footerCode();

    $(".btn-grid").click(function() {
        $("#dev-grid").fadeToggle();
        return false;
    });

    $(".callout").hover(function() {
        $(this).toggleClass("hover");
    });

    $("#mobile-menu").click(function() {
        $(this).toggleClass("open");
        $("nav").toggleClass("mobile-open");
    });

    var param_array = window.location.href.split('?')[1];
    if (param_array) {
        var correctTab = param_array.split('=');
        correctTab = correctTab[1];

        if (correctTab === "hyp") {
            $(".tab-patient-coverage > .tab-names > a").removeClass("active");
            $(".tab-patient-coverage > .tabs > .tab").removeClass("active");
            $(".tab-patient-coverage > .tab-names > a:first-child").addClass("active");
            $(".tab-patient-coverage > .tabs > .tab:first-child").addClass("active");
        } else if (correctTab === "iron") {
            $(".tab-patient-coverage > .tab-names > a").removeClass("active");
            $(".tab-patient-coverage > .tabs > .tab").removeClass("active");
            $(".tab-patient-coverage > .tab-names > a:last-child").addClass("active");
            $(".tab-patient-coverage > .tabs > .tab:last-child").addClass("active");
        }
    }

    $(".hyperphosphatemia").click(function() {
        $(".tab-patient-coverage > .tab-names > a").removeClass("active");
        $(".tab-patient-coverage > .tabs > .tab").removeClass("active");
        $(".tab-patient-coverage > .tab-names > a:first-child").addClass("active");
        $(".tab-patient-coverage > .tabs > .tab:first-child").addClass("active");
    });

     $(".iron-deficiency").click(function() {
        $(".tab-patient-coverage > .tab-names > a").removeClass("active");
        $(".tab-patient-coverage > .tabs > .tab").removeClass("active");
        $(".tab-patient-coverage > .tab-names > a:last-child").addClass("active");
        $(".tab-patient-coverage > .tabs > .tab:last-child").addClass("active");
    });

    // Navigation Dropdowns
    /*$(".sub-menu").hover(
            function() {
                $(this).parent("li").addClass("active");
            },
            function() {
                $(this).parent("li").removeClass("active");
            }
        );*/

    if (!isMobile) {
        $(".menu-item-has-children").hover(
            function() {
                $(this).addClass("active-state");
                $(this).find(".sub-menu").stop().slideDown();
            },
            function() {
                $(this).removeClass("active-state");
                $(this).find(".sub-menu").stop().slideUp();
            }
        );

        $("nav .sub-menu li a").click(function() {
            $(this).closest(".sub-menu").stop().slideUp();
        });
    }

    $("nav a").click(function(e){
        $("nav").removeClass("mobile-open");
        $("#mobile-menu").removeClass("open");
    });

    $("nav .menu-item-has-children > a").click(function(e) {
        e.preventDefault();
    });

    // Mobile Nav Prep
    let navCopy = "";
    $(".nav-copy").each(function() {
        navCopy += '<li class="mobile-copy">' + $(this).prop('outerHTML') + '</li>';
    });
    $("nav .menu").append(navCopy);
    $(".mobile-copy").addClass("mobile");
    $(".mobile-copy a").removeClass("desktop");

    setupTabs();

    $("li.home > a").mouseenter(function() {

        $(this).children("img").attr("src", `${assetDir}/img/akebiacares/icon-home.png`);
    });

    $("li.home > a").mouseleave(function() {
        if (!$(this).parent("li").hasClass("active")) {
            $(this).children("img").attr("src", `${assetDir}/img/akebiacares/icon-home-off.png`);
        }

    });

    /*if (window.matchMedia('(min-width: 1024px)').matches) {
        var e = document.getElementsByClassName("phone-fix");
        for (i = 0; i < e.length; i++)
            e[i].setAttribute("x-ms-format-detection", "none");
    }*/

});

function resizeEvents() {

    /* Window Reize on End Event */
    $(window).bind('resize', function(e) {
        window.resizeEvt;
        $(window).resize(function() {
            clearTimeout(window.resizeEvt);
            window.resizeEvt = setTimeout(function() {
                //Code to be executed after window resize
                updateView();
            }, 250);
        });
    });

}

/*function setupISI() {
    sectionID = $("#isi").attr("data-isi");
    actionURL = "load_isi_" + sectionID;
    $.get(ajaxURL, { 'action': actionURL, dataType: "html" },
        function(response) {
            if (response != 0) { */
                /* Append New Markup to Section */
                /* $('#isi').empty().append(response);
                delete response;
                modalEvents();
                globalClickEvents();

            }
        }
    );

}*/

/* Basic Click Events that are Global */
function globalClickEvents() {

}

function setupFAQs() {

    $(".faq-controls a").click(function() {
        $(".faq-controls a").removeClass("active");
        $(this).addClass("active");
        if ($(this).hasClass("faq-expand")) {
            // Expand
            $(".faq").addClass("open");
            $(".faq-answer").slideDown();
            ga('send', 'event', 'FAQ', 'Click', 'Expand All');
        } else {
            // Collapse
            $(".faq").removeClass("open");
            $(".faq-answer").slideUp();
            ga('send', 'event', 'FAQ', 'Click', 'Collapse All');
        }
        return false;
    });

    $(".faq").click(function(e) {
        $(".faq-controls a").removeClass("active");
        $(this).toggleClass("open");

        if ($(this).find(".faq-answer").is(":hidden")) {
            faqText = $(this).find(".faq-question").html();
            ga('send', 'event', 'FAQ', 'Click', faqText);
        }

        $(this).find(".faq-answer").slideToggle();
        return false;
    });

    // Internal Links within FAQs
    $(".faq a").click(function() {
        targetType = $(this).attr("target");
        targetLink = $(this).attr("href");
        if (targetType == "_blank") {
            window.open(targetLink);
        } else {
            window.location = targetLink;
        }
        return false;
    });

}

function setupTabs() {

    $(".tab-names a").click(function() {
        tabPosition = $(this).index();
        $(this).addClass("active").siblings().removeClass("active");
        $(".tab").eq(tabPosition).addClass("active").siblings().removeClass("active");
        return false;
    });

    // Tab Hash Values
    if (window.location.hash) {
        hashC = window.location.hash;
        if (hashC.indexOf("tab") >= 0) {
            scrollT = $("#tab-wrap").offset().top;
            $('html, body').animate({ scrollTop: scrollT }, 500);

            tabNum = hashC.slice(4);
            if (tabNum != "") {
                if (tabNum < 5) {
                    tabNum = tabNum - 1;
                    $(".tab-names a").eq(tabNum).addClass("active").siblings().removeClass("active");
                    $(".tab").eq(tabNum).addClass("active").siblings().removeClass("active");
                }
            }
        }
    }

}

function vimeoResize() {
    videoW = winW * 0.9;
    videoH = videoW * (394 / 700);
    $('#vimeo_vid').height(videoH).width(videoW);
}

function setupVeevaPortal() {
    var urlP = window.location.href;
    urlP = urlP.split("?");
    urlP = urlP[1];

    if (!urlP) {
        // URL Parameters Does Not Exists
        portalContent();
    }

}

function showVeeva() {
    var tag = document.createElement('script');
    tag.src = "//js.veevavault.com/ps/vault-v1.js";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var viewer;

    function onVeevaVaultIframeAPIReady() {
        viewer = new VV.Viewer('viewer', {
            height: '630',
            width: '660',
            error: 'We\'re sorry. The document you are trying to view is not available..'
        });
    }
}

function footerCode() {
    if ($('.page-id-1574').length) {
        $(".legal").replaceWith("<div class='legal'>©2015 Keryx Biopharmaceuticals, Inc.<div class='footer-spacer'></div>PP-AUR-US-0178<div class='footer-spacer'></div>09/15</div>");
    }
}

function portalContent() {
    portalContent = '';

    // Journey of the Pill
    $('.page-id-1095').exists(function() {
        portalContent = '<iframe id="vimeo_vid" src="//player.vimeo.com/video/140222302?title=0&amp;byline=0&amp;portrait=0&amp;color=FF8500" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
    });

    // Auryxia Flash Card
    $('.page-id-1097').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/AURYXIA_Flashcard.pdf" target="_blank" class="btn-site">Download AURYXIA Flashcard</a></div>';
    });

    // Patient Plus Flash Card
    $('.page-id-1100').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/Patient_Services_Brochure.pdf" target="_blank" class="btn-site">Download Keryx Patient Plus Flashcard</a></div>';
    });

    // Medicare Part D Coverage Grid
    $('.page-id-1574').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/Access_Flashcard.pdf" target="_blank" class="btn-site">Download Acccess Flashcard</a></div>';
    });

    $('.page-id-1707').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/Keryx-Patient-Plus-Application_from-website.pdf" target="_blank" class="btn-site">Download Keryx Patient Plus Application</a></div>';
    });

    $('.page-id-1710').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/Co-Pay-Coupon-Online.pdf" target="_blank" class="btn-site">Download AURYXIA Coupon</a></div>';
    });


    $('.page-id-1712').exists(function() {
        portalContent = '<div class="portal-download"><a href="/wp-content/uploads/Renal-Dietitian-Flashcard_final-from-Vault.pdf" target="_blank" class="btn-site">Download Dietitian Flashcard</a></div>';
    });

    $("#viewer").append(portalContent);

}
