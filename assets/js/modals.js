function modalEvents() {

    // Handle External Links for Modals
    hostname = new RegExp(location.host);
    $('a').each(function() {
        var url = $(this).attr("href");
        if (hostname.test(url)) {
            // Local Link. Do Nothing
        } else {
            // Valid Exernal Link
            if (url && url.toLowerCase().indexOf("keryx.com") >= 0) {
                // White List Domains
                $(this).addClass("whitelisted");
            } else {
                if ($(this).hasClass('note-close')) {

                } else {
                    $(this).addClass('popup').attr("data-modal", "ext");
                }
            }
        }
    });

    $("#note-close").click(function() {
        $('.medicare-notice').exists(function() {
            ga('send', 'event', 'Interstitial', 'Click', 'Close');
        });
        $(this).parents('.modal').parent().fadeOut();
        $("#supplynote").fadeOut().remove();
    });

/*
    $(".popup").click(function() {
        modalLink = $(this).attr("href");
        modalType = $(this).attr("data-modal");
        showModal(modalLink, modalType);
        return false;
    }); */

}


function showModal(modalLink, modalType) {
    overlayHTMl = '<div id="overlay"><div class="modal"></div></div>';
    $("body").append(overlayHTMl);
    loadURL = assetDir + "/html/" + "modal-" + modalType + ".html";

    $(".modal").load(loadURL, function() {
        $(".btn-yes").attr("href", modalLink);
        // Center & Show Modal
        modalH = $(".modal").height();
        modalT = $('.no-cssgradients').length ? 0 : (winH - modalH) / 2;
        $(".modal").css("margin-top", modalT);
        if (modalType == 'medicare') $(".modal").css("height", 200);
        $("#overlay").fadeIn();
        modalFlow();
    });

}


modalFlow();

function modalFlow() {

    $(".modal").click(function(e) {
        e.stopPropagation();
    });

    $(".btn-yes").click(function() {
        $("#supplynote").fadeOut().remove();
        ga('send', 'event', 'Button', 'Click', 'Download Coupon Interstitial - Yes');
    });

}

/*// Auto Triggers
if( location.pathname == '/patient-services/') {
  if( sessionStorage.getItem('medicareTriggered') == null) {
    showModal('#', 'medicare');
    sessionStorage.setItem('medicareTriggered', true);
  }
}

if( location.pathname == '/') {
  if( sessionStorage.getItem('medicareTriggered') == null) {
    showModal('#', 'medicare');
    sessionStorage.setItem('medicareTriggered', true);
  }
}
*/

//  $(document).ready(function(e) {
// if ($("body").hasClass("page-id-76") ) {
//    if (location.pathname.indexOf('patient') === -1) {
//             $("#supplynote").fadeIn();
//             $("#note-close").click(function() {
//                 $("#supplynote").fadeOut();
//                 window.localStorage.setItem('production-message', 'yes');
//             });

//     }
//     }
//     // Center Modal
//     modalH = $(".modal").height();
//     modalT = $('.no-cssgradients').length ? 0 : (winH - modalH) / 2;
//     $(".modal").css("margin-top", modalT);

// });
