<?php
/**
* Template Name: IDA Patient Sub Page
*/
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8"><![endif]-->
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, maximum-scale=1.0 , initial-scale=1.0">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<!-- Nav Start -->
<?php get_template_part('template-parts/nav', 'ida-patient'); ?>
<!-- Nav End -->

<?php
	$slug = $post->post_name;
	if ( is_page('patient') ) { $slug = 'index'; }
	get_template_part("template-parts/content-ida-patient", $slug);
?>

<div class="footerInclude">
	<?php get_template_part('template-parts/footer', 'ida-patient'); ?>
</div>
<?php wp_footer(); ?>
</body>
</html>
