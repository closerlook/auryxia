
    </div><!-- Content -->

	<?php
	switch ($siteType = setSiteTypeTwo(get_the_ID())) {
		case "patient":
			get_template_part('patients/footer','patients');
			break;
		case "assistance":
			get_template_part('assistance/footer','assist');
			break;
		default:
			get_template_part('hcp/footer','hcp');
			break;
	}
	?>

    <div class="mobileMenu">
      <div class="mobileNav">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active orangeLi" id="hcp1">
            <a href="/hyperphosphatemia" class="nav-link gtm-top-nav-parent" >HOME</a>
          </li>
          <li class="nav-item active orangeLi"  id="hcp2">
            <a href="/hyperphosphatemia/efficacy/unmet-need/" class="nav-link gtm-top-nav-parent" >EFFICACY</a>
          </li>
          <li class="nav-item active subNav" id="hcp3">
            <a href="/hyperphosphatemia/efficacy/unmet-need/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt">UNMET NEED</a>
          </li>
          <li class="nav-item active subNav" id="hcp4">
            <a href="/hyperphosphatemia/efficacy/trial-endpoints/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt">PIVOTAL PHASE III TRIAL</a>
          </li>
          <li class="nav-item active subNav" id="hcp5">
            <a href="/hyperphosphatemia/efficacy/efficacy-results/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt">EFFICACY RESULTS</a>
          </li>
          <li class="nav-item orangeLi" id="hcp6">
            <a href="/hyperphosphatemia/dosing/dosing-guidelines/" data-gtm-event-category="Top Navigation" class="nav-link disabled gtm-top-nav-parent"  >DOSING</a>
          </li>
          <li class="nav-item active subNav" id="hcp7">
            <a href="/hyperphosphatemia/dosing/dosing-guidelines/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt">GUIDELINES</a>
          </li>
          <li class="nav-item active subNav" id="hcp8">
            <a href="/hyperphosphatemia/dosing/administration/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent"  id="orangeTxt">ADMINISTRATION</a>
          </li>
          <li class="nav-item dropdownTaking orangeLi" id="hcp9">
            <a href="/hyperphosphatemia/safety/" data-gtm-event-category="Sub Navigation" class="nav-link gtm-sub-nav-parent" >SAFETY</a>
          </li>
          <li class="nav-item active subNav" id="hcp10">
            <a href="/hyperphosphatemia/safety/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt">SAFETY AND TOLERABILITY</a>
          </li>
          <li class="nav-item active subNav" id="hcp11">
            <a href="/hyperphosphatemia/clinical-pharmacology/pharmacodynamics/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt">PHARMACODYNAMICS</a>
          </li>
          <li class="nav-item orangeLi" id="hcp12">
            <a href="/hyperphosphatemia/clinical-pharmacology/mechanism-of-action/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" >MOA</a>
          </li>
          <li class="nav-item orangeLi" id="hcp13">
            <a href="/hyperphosphatemia/library/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" >LIBRARY</a>
          </li>
		  <li class="nav-item orangeLi" id="hcp14">
		    <a href="/hyperphosphatemia/sign-up/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" >SIGN UP</a>
		  </li>
          <li class="nav-item grayLi" id="hcp15">
            <a href="/hyperphosphatemia/patients/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt" >FOR PATIENTS</a>
          </li>
          <li class="nav-item akebiaLi tealLi" id="hcp16">
            <a href="/akebiacares/" data-gtm-event-category="Header" class="nav-link gtm-header" >AkebiaCares</a>
          </li>
          <li class="nav-item heatlhCareLi blueLi" id="hcp17">
            <a href="/iron-deficiency-anemia/" data-gtm-event-category="Header" class="nav-link gtm-header" >IRON DEFICIENCY ANEMIA</a>
          </li>
          <?php if (is_page(76)) { ?>
          <li class="nav-item externalLinkButtons" id="hcp18">
            <a href="http://www.auryxiarxlocator.com" target="_blank" data-gtm-event-category="Top Navigation" class="nav-link gtm-header" id="externalLinkOrange">SPECIALTY PHARMACY LOCATOR</a>
          </li>
          <li class="nav-item externalLinkButtons" id="hcp19">
            <a href="https://akebiarxcoverage.caremetx.com/" target="_blank" data-gtm-event-category="Top Navigation" class="nav-link gtm-header" id="externalLinkOrange">PRESCRIPTION BENEFITS PORTAL</a>
          </li>
          <?php } ?>
        </ul>
      </div>
    </div>


	<div class="mobileMenuPatient">
	  <div class="mobileNav">
		<ul class="navbar-nav mr-auto">
		  <li class="nav-item active orangeLi" id="hcp1">
			<a href="/hyperphosphatemia/patients/" class="nav-link gtm-top-nav-parent" >HOME</a>
		  </li>
		  <li class="nav-item active orangeLi"  id="hcp2">
			<a href="/hyperphosphatemia/patients/why-auryxia/" class="nav-link gtm-top-nav-parent" >DISEASE 101</a>
		  </li>
		  <li class="nav-item orangeLi" id="hcp6">
			<a  data-gtm-event-category="Top Navigation" class="nav-link  gtm-top-nav-parent"  disabled>AURYXIA 101</a>
		  </li>

			  <li class="nav-item active subNav" id="hcp7">
			<a href="/hyperphosphatemia/patients/how-auryxia-works/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt">HOW AURYXIA WORKS</a>
		  </li>

		  <li class="nav-item active subNav" id="hcp7">
			<a href="/hyperphosphatemia/patients/how-auryxia-works/how-you-take-it/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt">TAKING AURYXIA</a>
		  </li>
		  <li class="nav-item active subNav" id="hcp8">
			<a href="/hyperphosphatemia/patients/how-auryxia-works/what-to-tell-your-doctor/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent"  id="orangeTxt">TALKING TO YOUR DOCTOR</a>
		  </li>
		  <li class="nav-item dropdownTaking orangeLi" id="hcp9">
			<a  data-gtm-event-category="Sub Navigation" class="nav-link gtm-sub-nav-parent" disabled>PATIENT SUPPORT</a>
		  </li>

		  <li class="nav-item active subNav" id="hcp10">
			<a href="/hyperphosphatemia/patients/patient-support/your-system-support/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt">YOUR SUPPORT SYSTEM</a>
		  </li>

		  <li class="nav-item active subNav" id="hcp10">
			<a href="/hyperphosphatemia/patients/patient-support/assistance-programs/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" id="orangeTxt">SUPPORT PROGRAMS</a>
		  </li>
		  <li class="nav-item orangeLi" id="hcp11">
			<a href="/hyperphosphatemia/patients/faqs/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" >FAQs</a>
		  </li>
		  <li class="nav-item orangeLi" id="hcp12">
			<a href="/hyperphosphatemia/patients/resources/" data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" >RESOURCES</a>
		  </li>
		  <li class="nav-item akebiaLi tealLi" id="hcp14">
			<a href="/akebiacares/" data-gtm-event-category="Header" class="nav-link gtm-header" >AkebiaCares</a>
		  </li>
		  <li class="nav-item heatlhCareLi grayLi" id="hcp15">
			<a href="/" data-gtm-event-category="Header" class="nav-link gtm-header" id="grayLiText" >For U.S. Healthcare Professionals</a>
		  </li>
		  <li class="nav-item heatlhCareLi blueLi" id="hcp16">
			<a href="/iron-deficiency-anemia/patient" data-gtm-event-category="Header" class="nav-link gtm-header" >For Patients Not on Dialysis </a>
		  </li>
		</ul>
	  </div>
	</div>

	<?php get_template_part('includes','floodlight-tags'); ?>
	<?php wp_footer(); ?>
  </body>
</html>
