<?php /* Template Name: Portal */
  get_header(); if (have_posts()) : while (have_posts()) : the_post(); 
?>

<div class="portal-spacer"></div>

<div class="grid-12 interior cf portal-page">
  <div class="grid-9 left copy">
    <div class="portal-wrap">
      <div align="center" id="viewer"></div>
    </div>
    <?php
    $formulation = get_field('formulation_text', 'option');
    if ($formulation){
      echo '<br /><div class="formulation">';
      echo '<span>Formulation</span>' . $formulation . '</div>';
    }  
    ?>
  </div>
  <?php include_once(TEMPLATEPATH . '/includes/sidebar-callouts.php'); ?>
</div><!-- Single Page -->



<script>
  var tag = document.createElement('script');
  tag.src = "//js.veevavault.com/ps/vault-v1.js";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  var viewer;
  function onVeevaVaultIframeAPIReady() {
    viewer = new VV.Viewer('viewer', {
      height: '630',
      width: '660',
      error: 'We\'re sorry. The document you are trying to view is not available..'
    });
  }
</script>





<?php 
  include_once(TEMPLATEPATH . '/includes/isi-and-references.php'); 
  endwhile; else : endif; get_footer();
?>