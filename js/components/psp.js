import { Utilities } from "../utilities/utilities.js";
// import { enableBodyScroll, disableBodyScroll } from "../utilities/body-scroll-lock.js";
import { WindowMetrics } from "../utilities/window-metrics.js";

var
    // Defines how the PSP is sized prior to collapse
    sizeMode = {
        // Fixed height
        fixedHeight: 0,

        // Dynamic, via the "preCollapsePositionElementSelector" stuff
        // in the settings below
        dynamic: 1
    },

    // Defines the types of PSP collapse behavior
    collapseMode = {
        // Never collapse
        off: 0,

        // Collapse once the user scrolls the page
        onScroll: 1,

        // Collapse when a button is clicked
        onButtonClick: 2,

        // Collapse once the on-page ISI becomes visible
        // and the PSP becomes unsticky / hidden
        onIsiVisible: 3
    },
    
    settings = {
        // PSP pre-collapse size mode
        sizeMode: sizeMode.dynamic,

        // Value for "fixedHeight" size mode; specify an integer for a specific
        // size(e.g, "200"), or a fraction for a percent of the viewport height
        // (e.g., "0.333")
        fixedHeightValue: 0.333,

        // PSP collapse mode
        collapseMode: collapseMode.onButtonClick,

        // This specifies an element within the PSP bar that will collapse the
        // PSP when clicked if the collapse mode is set to "onButtonClick"
        collapseButtonSelector: ".psp-bar-collapse-button",

        // Sets a cookie once the PSP is collapsed; on subsequent page loads,
        // the presense of this cookie will cause the PSP to be initialized
        // in a collapsed state
        enableCollapseCookie: true,
        pspCollapsedCookieName: "psp-collapsed",

        // This specifies an element within the PSP ISI content that must be
        // initially visible - for example, the header for a black box warning
        preCollapsePositionElementSelector: {
            // The element's selector
            element: {
                mobile: ".cl-psp-close-position-mobile",
                desktop: ".cl-psp-close-position-desktop"
            },

            // When the element's height is less than this value, we use it instead
            minHeight: null,

            // When the element's height is greater than this value, we use it instead
            maxHeight: null//17 * 4 // 4 lines of line-height: 17px
        },

        // Animation settings
        collapseDuration: 0.5,
        openDuration: 0.5,
        closeDuration: 0.5,

        // Class names that are applied for the various states of the PSP
        classNames: {
            sticky: "sticky",
            visible: "visible",
            open: "open",
            closing: "closing",
            collapsed: "collapsed"
        }
    },

    adjustStickiness = function () {
        var t = this;
        
        // Check if the body content is at a fixed position
        // (i.e., the mobile menu is open)
        var scrollPosition;
        if (Utilities.contentPositionIsFixed) {
            scrollPosition = Utilities.windowScrollTopBeforeContentFixed;
            //scrollPosition -= t.header.offsetHeight;
        } else {
            scrollPosition = window.pageYOffset;
            //scrollPosition -= t.main.offsetTop;
        }

        // Determine the scroll position at which we stop being sticky
        var stickyScrollPosition = t.calculateStickyScrollPosition.call(t);
        if (t.isCollapsed)
            stickyScrollPosition += t.onPageBar.clientHeight;
        else
            stickyScrollPosition += t.preCollapseHeight;

        // Check if we're at or beyond our sticky toggle scroll position
        if (scrollPosition >= stickyScrollPosition) {
            // Make unsticky if necessary
            if (t.isSticky) {
                // Stop any ongoing animation
                if (t.openCloseTimeline)
                    t.openCloseTimeline.progress(1);

                t.isSticky = false;
                t.stickyPsp.classList.remove(settings.classNames.visible);

                // If we're not yet collapsed and configured to collapse
                // once the ISI page becomes visible, do so now
                if (settings.collapseMode == collapseMode.onIsiVisible) {
                    if (!t.isCollapsed && t.initialized && !Utilities.contentPositionIsFixed) {
                        // Collapse the PSP
                        t.collapse(false);
                    }
                }
            }
            
            Utilities.resetStyles(t.stickyPsp);
            Utilities.resetStyles(t.stickyPspIsi);
        } else {
            // Make sticky if necessary
            if (!t.isSticky) {
                // Stop any ongoing animation
                if (t.openCloseTimeline)
                    t.openCloseTimeline.progress(1);

                t.isSticky = true;
                t.stickyPsp.classList.add(settings.classNames.visible);
            }
        }

        // We need to always maintain the sticky PSP's height since other things on the site need to detect
        // the available vertical real estate
        if (!t.isOpen) {
            t.stickyPsp.style.height = Utilities.numberToPx(t.isCollapsed ? t.onPageBar.clientHeight : t.preCollapseHeight);
            Utilities.resetStyles(t.stickyPspIsi);
        }
    };

export class Psp {
    constructor(calculateStickyScrollPosition, settingsOverrides = null) {
        var t = this;

        t.calculateStickyScrollPosition = calculateStickyScrollPosition;
        t.initialized = false;
        t.isSticky = false;
        t.isOpen = false;
        t.isCollapsed = false;
        t.openCloseTimeline = null;

        // If settings overrides were provided, apply them to our settings object
        if (settingsOverrides) {
            // IE11 doesn't support Object.assign...
            //settings = Object.assign(settings, settingsOverrides);

            for (let key in settingsOverrides)
                settings[key] = settingsOverrides[key];
        }

        // Get references to our DOM elements
        t.onPagePsp = document.getElementById("psp");
        t.header = document.querySelector("header");
        t.main = document.querySelector("main");

        // Construct our sticky PSP
        t.stickyPsp = document.createElement("div");
        t.stickyPsp.classList.add("psp");
        t.stickyPsp.classList.add("sticky");
        t.stickyPsp.innerHTML = t.onPagePsp.innerHTML;

        // Check collapsed cookie
        if (settings.enableCollapseCookie) {
            if (window.sessionStorage.getItem(settings.pspCollapsedCookieName) == "true")
                t.collapse(true);
        }

        document.body.appendChild(t.stickyPsp);

        t.onPageBar = t.onPagePsp.querySelector(".psp-bar");
        t.stickyPspBar = t.stickyPsp.querySelector(".psp-bar");

        t.stickyPspIsi = t.stickyPsp.querySelector(".isi");

        switch (settings.sizeMode) {
            case sizeMode.dynamic:
                t.stickyPspClosePositionElement = {
                    mobile: t.stickyPspIsi.querySelector(settings.preCollapsePositionElementSelector.element.mobile),
                    desktop: t.stickyPspIsi.querySelector(settings.preCollapsePositionElementSelector.element.desktop)
                };
                break;

            default:
                break;
        }
        
        // Setup window resize event and perform initial resize call
        var
            resizeCallback = function () {
                // Recalculate preCollapseHeight if necessary
                switch (settings.sizeMode) {
                    case sizeMode.fixedHeight:
                        // Check if "fixedHeightValue" is a fraction
                        if (settings.fixedHeightValue < 1 && settings.fixedHeightValue > 0) {
                            // Set height based on a fraction of the viewport height
                            t.preCollapseHeight = Math.round(window.innerHeight * settings.fixedHeightValue);
                        } else {
                            // Use the specified fixedHeightValue
                            t.preCollapseHeight = Math.round(settings.fixedHeightValue);
                        }
                        break;

                    case sizeMode.dynamic:
                        // A positioning element was found that will determine the height of the PSP
                        var containerBoundingBox = t.stickyPsp.getBoundingClientRect(),
                            closePositionBoundingBox;

                        if (WindowMetrics.isMobile())
                            closePositionBoundingBox = t.stickyPspClosePositionElement.mobile.getBoundingClientRect();
                        else
                            closePositionBoundingBox = t.stickyPspClosePositionElement.desktop.getBoundingClientRect();

                        var closePositionBoundingBoxHeight = closePositionBoundingBox.height;

                        // Limit the height we consider the positioning element to be, if specified
                        if (settings.preCollapsePositionElementSelector.minHeight !== null) {
                            if (settings.preCollapsePositionElementSelector.minHeight > closePositionBoundingBoxHeight)
                                closePositionBoundingBoxHeight = settings.preCollapsePositionElementSelector.minHeight;
                        }

                        if (settings.preCollapsePositionElementSelector.maxHeight !== null) {
                            if (settings.preCollapsePositionElementSelector.maxHeight < closePositionBoundingBoxHeight)
                                closePositionBoundingBoxHeight = settings.preCollapsePositionElementSelector.maxHeight;
                        }

                        t.preCollapseHeight = (closePositionBoundingBox.top + closePositionBoundingBoxHeight) - containerBoundingBox.top;
                        break;
                }
                
                // Adjust open height or stickiness
                if (t.isOpen) {
                    t.stickyPspIsi.style.height = "calc(100% - " + t.onPageBar.clientHeight + "px)";
                } else {
                    adjustStickiness.call(t);
                }
            };

        window.addEventListener("resize", resizeCallback);
        resizeCallback();

        // Doing this immediately seems to be problematic on some browsers,
        // particularly on older versions of iOS & Android; instead, we're
        // performing this after the DOM has finished loading & a short delay
        window.addEventListener("load", function () {
            setTimeout(
                resizeCallback,
                200);
        });

        // Setup window scroll event
        window.addEventListener("scroll", function () {
            if (t.isOpen)
                return;

            adjustStickiness.call(t);

            // If we're sticky, not yet collapsed, and configured to collapse
            // on scroll, collapse the PSP now
            if (t.isCollapsed || !t.initialized || Utilities.contentPositionIsFixed)
                return;

            if (settings.collapseMode != collapseMode.onScroll)
                return;

            // Collapse the PSP
            t.collapse(false);
        });

        // Setup open/close button
        var clickCallback = function () {
            if (t.isOpen)
                t.close(false);
            else
                t.open(false);
        };

        t.stickyPspBar.addEventListener("click", clickCallback);

        // Setup collapse button
        if (settings.collapseMode == collapseMode.onButtonClick) {
            t.collapseButton = t.stickyPspBar.querySelector(settings.collapseButtonSelector);
            if (t.collapseButton) {
                t.collapseButton.addEventListener("click", function (event) {
                    event.stopPropagation();

                    // Collapse the PSP
                    t.collapse(false);
                });
            }
        }

        // EDIT: using touchstart to trigger opening and closing the PSP isn't great
        // because it prevents users from scrolling if they happen to initially touch
        // somewhere inside the bar. The bar is really prominent onscreen on mobile
        // due to the 30% PSP height
        //t.stickyPspBar.addEventListener("touchstart", function (event) {
        //    event.preventDefault();
        //    event.stopPropagation();

        //    clickCallback();
        //});
        
        // Set our initialization flag
        setTimeout(
            function () {
                t.initialized = true;
            },
            500);
    }

    static get sizeMode() {
        return sizeMode;
    }

    static get collapseMode() {
        return collapseMode;
    }

    open(immediate) {
        var t = this;

        if (!t.isSticky)
            return;
        if (t.isOpen)
            return;

        // Stop any ongoing animation
        if (t.openCloseTimeline)
            t.openCloseTimeline.progress(1);

        t.isOpen = true;

        // Fix content position
        Utilities.lockScrolling(t.stickyPspIsi);

        if (immediate) {
            // Transition immediately
            t.stickyPsp.classList.add(settings.classNames.open);
            t.stickyPsp.style.height = "100%";
            t.stickyPspIsi.style.height = "100%";
        } else {
            // Set initial conditions
            t.stickyPsp.classList.add(settings.classNames.open);
            Utilities.resetStyles(t.stickyPspIsi);
            t.stickyPspIsi.style.height = "calc(100% - " + t.onPageBar.clientHeight + "px)";

            // Animate transition
            var
                progress = {
                    heightPercent: (t.isCollapsed ? t.onPageBar.clientHeight : t.preCollapseHeight) / window.innerHeight
                };

            t.openCloseTimeline = new TimelineMax();

            t.openCloseTimeline.to(
                progress,
                settings.openDuration,
                {
                    ease: Power3.easeOut,
                    heightPercent: 1,
                    onUpdate: function () {
                        var height = (Math.round(progress.heightPercent * 10000) / 100).toString() + "%";
                        t.stickyPsp.style.height = height;
                    },
                    onComplete: function () {
                        t.openCloseTimeline = null;
                    }
                });
        }
    }

    close(immediate) {
        var t = this;

        if (!t.isSticky)
            return;
        if (!t.isOpen)
            return;

        // Stop any ongoing animation
        if (t.openCloseTimeline)
            t.openCloseTimeline.progress(1);

        // Unfix content position
        Utilities.unlockScrollingForTargetElement(t.stickyPspIsi);

        if (immediate) {
            // Transition immediately
            t.stickyPsp.classList.remove(settings.classNames.open);
            t.stickyPsp.style.height = Utilities.numberToPx(t.isCollapsed ? t.onPageBar.clientHeight : t.preCollapseHeight);

            Utilities.resetStyles(t.stickyPspIsi);

            // Check if the sticky state needs to change
            adjustStickiness.call(t);
        } else {
            // Set initial conditions
            t.stickyPsp.style.height = "100%";
            Utilities.resetStyles(t.stickyPspIsi);

            t.stickyPsp.classList.add(settings.classNames.closing);

            // Animate transition
            var
                progress = {
                    heightPercent: 1
                };

            t.openCloseTimeline = new TimelineMax();

            t.openCloseTimeline.to(
                progress,
                settings.closeDuration,
                {
                    ease: Power3.easeOut,
                    heightPercent: (t.isCollapsed ? t.onPageBar.clientHeight : t.preCollapseHeight) / window.innerHeight,
                    onUpdate: function () {
                        var height = (Math.round(progress.heightPercent * 10000) / 100).toString() + "%";
                        t.stickyPsp.style.height = height;
                    },
                    onComplete: function () {
                        t.openCloseTimeline = null;
                        t.isOpen = false;

                        t.stickyPsp.classList.remove(settings.classNames.open);
                        t.stickyPsp.classList.remove(settings.classNames.closing);

                        // Check if the sticky state needs to change
                        adjustStickiness.call(t);
                    }
                });
        }
    }

    collapse(immediate) {
        const t = this;

        if (t.isCollapsed)
            return;

        // Set collapsed flage & create cookie if we're configured to do so
        t.isCollapsed = true;
        if (settings.enableCollapseCookie)
            window.sessionStorage.setItem(settings.pspCollapsedCookieName, "true");

        // Add a "collapsed" class to the PSP
        t.stickyPsp.classList.add(settings.classNames.collapsed);

        if (t.isSticky) {
            // Stop any ongoing animation
            if (t.openCloseTimeline)
                t.openCloseTimeline.progress(1);

            // Set initial conditions
            t.stickyPsp.style.height = Utilities.numberToPx(t.preCollapseHeight);

            Utilities.resetStyles(t.stickyPspIsi);

            if (immediate) {
                // Transition immediately
                t.stickyPsp.height = t.onPageBar.clientHeight;
                t.openCloseTimeline = null;
            } else {
                // Animate transition
                t.openCloseTimeline = new TimelineMax();

                t.openCloseTimeline.to(
                    t.stickyPsp,
                    settings.collapseDuration,
                    {
                        ease: Power3.easeOut,
                        height: t.onPageBar.clientHeight,
                        onComplete: function () {
                            t.openCloseTimeline = null;
                        }
                    });
            }
        }
    }
}
