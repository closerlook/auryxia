﻿import { Utilities } from "../utilities/utilities.js";

var
    // General settings
    settings = {
        templates: {
            backdrop: `<div class=\"cl-modal-backdrop\"></div>`,
            modal: `<div class="cl-modal">
    <div class="cl-modal-close-button">
        <span></span>
        <span></span>
    </div>
    <div class="cl-modal-content"></div>
</div>`
        },

        open: {
            duration: 0.5,
            initialScale: {
                scaleUp: 0.9,
                scaleDown: 1.15
            },
            easing: Power2.easeOut
        },

        close: {
            duration: 0.5,
            endScale: {
                scaleUp: 1.1,
                scaleDown: 0.95
            },
            easing: Power2.easeOut
        },

        minimumTimeBeforeAllowingClose: 200,
        slowMoScale: 5,

        fadeAndPartialVerticalEnvelope: {
            envelopeAmount: 0.25
        }
    },

    presentationTypes = {
        fade: 0,
        scaleUp: 1,
        scaleDown: 2,
        verticalEnvelope: 3,
        fadeAndPartialVerticalEnvelope: 4
    },
    
    openModals = [];

export class Modal {
    static show(template, className, showPresentationType, closePresentationType, immediate, callbacks, event) {
        var t = {
            timeline: null,

            bodyContent: {
                header: document.body.querySelector("header"),
                main: document.body.querySelector("main"),
                footer: document.body.querySelector("footer")
            },

            close: function (immediate, presentationType, event) {
                if (t.timeline !== null)
                    return false;
                
                close.call(t, callbacks, immediate, presentationType, event);
                return false;
            }
        };
        
        // Remove tabindex values from everything
        var tabIndexElements = document.querySelectorAll("*[tabindex]");
        for (var i = 0; i < tabIndexElements.length; i++) {
            var tabIndexElement = tabIndexElements[i],
                tabIndex = tabIndexElement.getAttribute("tabindex");

            // Skip elements already out of the tabindex flow
            if (parseInt(tabIndex) < 0)
                continue;

            // Store original tab index (this would not work right if we
            // were to allow more than one modal onscreen at a time, I think)
            tabIndexElement.setAttribute("data-original-tabindex", tabIndex);
            tabIndexElement.setAttribute("tabindex", "-1");
        }

        // Setup DOM elements
        t.backdrop = Utilities.createElementFromTemplate(settings.templates.backdrop);
        t.container = Utilities.createElementFromTemplate(settings.templates.modal)
        t.closeButton = t.container.querySelector(".cl-modal-close-button");
        t.content = t.container.querySelector(".cl-modal-content");

        if (className)
            t.container.classList.add(className);

        // Add the template content into the modal
        t.content.innerHTML = template;

        // Append elements to the end of the DOM
        t.backdrop.style.opacity = 0;
        document.body.appendChild(t.backdrop);

        t.container.style.opacity = 0;
        document.body.appendChild(t.container);

        // Make content scrollable if it's too tall to fit in the available space
        t.adjustContentHeight = function () {
            // Reset everything
            t.container.classList.remove("short-modal");

            t.container.style.top = "";
            t.container.style.height = "";
            t.container.style.transform = "";
            t.container.style.height = "";

            t.content.style.overflowY = "";
            t.content.style.height = "auto";

            // Determine the size of the viewport and the content container
            var availableHeight = t.container.clientHeight,
                style = getComputedStyle(t.content),
                contentHeight = t.content.offsetHeight + parseInt(style.marginTop) + parseInt(style.marginBottom);

            if (contentHeight > availableHeight) {
                // There isn't enough space in the viewport to display all the content -
                // determine the size of the scrollable view
                var
                    margin = contentHeight - t.content.clientHeight,
                    scrollableViewHeight = availableHeight - margin;

                // If the scrollable view is really narrow, adjust the positioning of the modal to maximize
                // the amount of scrollable space onscreen - this is largely to accomodate mobile devices
                // in landscape orientation, where the popup keyboard shrinks the available space to almost
                // nothing
                if (scrollableViewHeight < 100) {
                    scrollableViewHeight = window.innerHeight;
                    t.container.classList.add("short-modal");
                    immediate = true;
                }

                t.content.style.height = Utilities.numberToPx(scrollableViewHeight);
                t.content.style.overflowY = "scroll";

                t.includesScrollingContent = true;
            } else {
                // There is enough room on screen to present the modal without a scrollbar -
                // just set our flag
                t.includesScrollingContent = false;
            }
        };

        if (t.inhibitYScrolling !== true) {
            window.addEventListener("resize", function () {
                t.adjustContentHeight();
            });
        }

        // Setup events
        var
            openStartTime = (new Date()).getTime(),

            closeCallback = function (event) {
                event.preventDefault();
                event.stopPropagation();

                // Check if we're animating
                if (t.timeline !== null) {
                    // Check if the minimum amount of time has elapsed since starting
                    // the animation; if it hasn't, stop here
                    let elapsed = (new Date()).getTime() - openStartTime;
                    if (elapsed < settings.minimumTimeBeforeAllowingClose)
                        return false;
                }

                close.call(t, callbacks, false, closePresentationType, event);
            };

        t.backdrop.addEventListener("click", closeCallback);
        t.closeButton.addEventListener("click", closeCallback);
        
        // Run "preShow" callback, if specified
        if (callbacks) {
            if (typeof callbacks.preShow == "function")
                callbacks.preShow.call(t);
        }

        // Do this after preShow is called, in case that callback adjusts the
        // height of the modal's content
        if (t.inhibitYScrolling !== true)
            t.adjustContentHeight();
        
        // Fix content position
        Utilities.lockScrolling(t.content);

        // Present modal
        if (immediate) {
            // Transition immediately
            t.backdrop.style.opacity = 1;
            t.container.style.opacity = 1;

            // Run "postShow" callback, if specified
            if (callbacks) {
                if (typeof callbacks.postShow == "function")
                    callbacks.postShow.call(t);
            }
        } else {
            // Animate transition
            var
                duration = settings.open.duration,

                progress = {
                    percentComplete: 0
                },

                onCompleteCallback = function () {
                    t.timeline = null;

                    // Run "postShow" callback, if specified
                    if (callbacks) {
                        if (typeof callbacks.postShow == "function")
                            callbacks.postShow.call(t);
                    }
                };

            // Scale duration if the shift key is pressed
            if (event) {
                if (event.shiftKey === true)
                    duration *= settings.slowMoScale;
            }

            // Create timeline
            t.timeline = new TimelineMax();
            
            switch (showPresentationType) {
                case presentationTypes.scaleUp:
                    t.timeline.to(
                        progress,
                        duration,
                        {
                            ease: settings.open.easing,
                            percentComplete: 1,

                            onUpdate: function () {
                                var scale = Math.round((settings.open.initialScale.scaleUp + ((1 - settings.open.initialScale.scaleUp) * progress.percentComplete)) * 1000) / 1000;
                                scale = scale.toString();

                                var transform = "scale(" + scale + ", " + scale + ")";
                                if (t.modalIsPositionedAbsolutely !== true)
                                    transform = "translate(-50%, -50%) " + transform;

                                t.backdrop.style.opacity = progress.percentComplete;
                                t.container.style.opacity = progress.percentComplete;
                                t.container.style.transform = transform;
                            },

                            onComplete: onCompleteCallback
                        });
                    break;

                case presentationTypes.scaleDown:
                    t.timeline.to(
                        progress,
                        duration,
                        {
                            ease: settings.open.easing,
                            percentComplete: 1,

                            onUpdate: function () {
                                var scale = Math.round((settings.open.initialScale.scaleDown + ((1 - settings.open.initialScale.scaleDown) * progress.percentComplete)) * 1000) / 1000;
                                scale = scale.toString();

                                var transform = "scale(" + scale + ", " + scale + ")";
                                if (t.modalIsPositionedAbsolutely !== true)
                                    transform = "translate(-50%, -50%) " + transform;

                                t.backdrop.style.opacity = progress.percentComplete;
                                t.container.style.opacity = progress.percentComplete;
                                t.container.style.transform = transform;
                            },

                            onComplete: onCompleteCallback
                        });
                    break;

                case presentationTypes.verticalEnvelope:
                    {
                        var containerHeight = t.container.clientHeight;

                        t.container.style.height = Utilities.numberToPx(0);
                        t.container.style.opacity = 1;

                        t.timeline.to(
                            progress,
                            duration,
                            {
                                ease: settings.open.easing,
                                percentComplete: 1,

                                onUpdate: function () {
                                    var height = progress.percentComplete * containerHeight,

                                        // 2 = inner content remains still
                                        // > 2 = inner content shifts upward
                                        // < 2 = inner content shifts downward
                                        offset = (height - containerHeight) / 2.5;

                                    t.container.style.height = Utilities.numberToPx(height);
                                    t.content.style.top = Utilities.numberToPx(offset);

                                    t.closeButton.style.transform = `translate(0px, ${offset}px)`;

                                    t.backdrop.style.opacity = progress.percentComplete;
                                },

                                onComplete: function () {
                                    t.container.style.height = "";
                                    t.content.style.top = "";
                                    t.closeButton.style.transform = "";
                                    onCompleteCallback();
                                }
                            });
                    }
                    break;

                case presentationTypes.fadeAndPartialVerticalEnvelope:
                    {
                        containerHeight = t.container.clientHeight;

                        t.timeline.to(
                            progress,
                            duration,
                            {
                                ease: settings.open.easing,
                                percentComplete: 1,

                                onUpdate: function () {
                                    let height = Math.round((((1 - settings.fadeAndPartialVerticalEnvelope.envelopeAmount) * containerHeight) + (settings.fadeAndPartialVerticalEnvelope.envelopeAmount * containerHeight * progress.percentComplete)) * 1000) / 1000;

                                    t.backdrop.style.opacity = progress.percentComplete;

                                    t.container.style.opacity = progress.percentComplete;
                                    t.container.style.height = Utilities.numberToPx(height);
                                },

                                onComplete: function () {
                                    t.container.style.height = "";
                                    onCompleteCallback();
                                }
                            });
                    }
                    break;

                // Default to fade transition
                default:
                    t.timeline.to(
                        progress,
                        duration,
                        {
                            ease: settings.open.easing,
                            percentComplete: 1,

                            onUpdate: function () {
                                t.backdrop.style.opacity = progress.percentComplete;
                                t.container.style.opacity = progress.percentComplete;
                            },

                            onComplete: onCompleteCallback
                        });
                    break;
            }
        }

        // Add this modal to the reference array
        openModals.push(t);
    }

    static get numberOfOpenModals() {
        return openModals.length;
    }

    static get anyOpenModalContainsScrollingContent() {
        for (var i = 0; i < openModals.length; i++) {
            if (openModals[i].includesScrollingContent)
                return true;
        }

        return false;
    }

    static get presentationTypes() {
        return presentationTypes;
    }
}

// Private methods
let
    close = function (callbacks, immediate, presentationType, event) {
        var
            t = this,

            restoreTabIndices = function () {
                // Remove tabindex values from everything
                var tabIndexElements = document.querySelectorAll("*[data-original-tabindex]");

                for (var i = 0; i < tabIndexElements.length; i++) {
                    var tabIndexElement = tabIndexElements[i],
                        originalTabIndex = tabIndexElement.getAttribute("data-original-tabindex");

                    tabIndexElement.setAttribute("tabindex", originalTabIndex);
                    tabIndexElement.removeAttribute("data-original-tabindex");
                }
            };

        if (t.closing === true)
            return;

        t.closing = true;

        // Force immediate transition if we're in "short" mode
        if (t.container.classList.contains("short-modal"))
            immediate = true;

        // Stop any ongoing animation
        if (t.timeline)
            t.timeline.progress(1);

        // Run "preClose" callback, if specified
        if (callbacks) {
            if (typeof callbacks.preClose == "function")
                callbacks.preClose.call(t);
        }

        // Unfix content position
        Utilities.unlockScrollingForTargetElement(t.content);

        if (immediate) {
            // Transition immediately
            t.backdrop.parentNode.removeChild(t.backdrop);
            t.container.parentNode.removeChild(t.container);

            // Run "postClose" callback, if specified
            if (callbacks) {
                if (typeof callbacks.postClose == "function")
                    callbacks.postClose.call(t);
            }

            // Restore tabindex values
            restoreTabIndices();
        } else {
            // Animate transition
            var
                duration = settings.close.duration,

                progress = {
                    percentComplete: 0
                },

                onCompleteCallback = function () {
                    t.backdrop.parentNode.removeChild(t.backdrop);
                    t.container.parentNode.removeChild(t.container);

                    t.timeline = null;

                    // Run "postClose" callback, if specified
                    if (callbacks) {
                        if (typeof callbacks.postClose == "function")
                            callbacks.postClose.call(t);
                    }

                    // Restore tabindex values
                    restoreTabIndices();
                };

            // Scale duration if the shift key is pressed
            if (event) {
                if (event.shiftKey === true)
                    duration *= settings.slowMoScale;
            }

            // Create timeline
            t.timeline = new TimelineMax();

            switch (presentationType) {
                case presentationTypes.scaleUp:
                    t.timeline.to(
                        progress,
                        duration,
                        {
                            ease: settings.close.easing,
                            percentComplete: 1,

                            onUpdate: function () {
                                var scale = Math.round((1 + ((settings.close.endScale.scaleUp - 1) * progress.percentComplete)) * 1000) / 1000;
                                scale = scale.toString();

                                var transform = "scale(" + scale + ", " + scale + ")";
                                if (t.modalIsPositionedAbsolutely !== true)
                                    transform = "translate(-50%, -50%) " + transform;

                                t.backdrop.style.opacity = 1 - progress.percentComplete;
                                t.container.style.opacity = 1 - progress.percentComplete;
                                t.container.style.transform = transform;
                            },

                            onComplete: onCompleteCallback
                        });
                    break;

                case presentationTypes.scaleDown:
                    t.timeline.to(
                        progress,
                        duration,
                        {
                            ease: settings.close.easing,
                            percentComplete: 1,

                            onUpdate: function () {
                                var scale = Math.round((1 + ((settings.close.endScale.scaleDown - 1) * progress.percentComplete)) * 1000) / 1000;
                                scale = scale.toString();

                                var transform = "scale(" + scale + ", " + scale + ")";
                                if (t.modalIsPositionedAbsolutely !== true)
                                    transform = "translate(-50%, -50%) " + transform;

                                t.backdrop.style.opacity = 1 - progress.percentComplete;
                                t.container.style.opacity = 1 - progress.percentComplete;
                                t.container.style.transform = transform;
                            },

                            onComplete: onCompleteCallback
                        });
                    break;

                case presentationTypes.verticalEnvelope:
                    var containerHeight = t.container.clientHeight;

                    t.timeline.to(
                        progress,
                        duration,
                        {
                            ease: settings.close.easing,
                            percentComplete: 1,

                            onUpdate: function () {
                                var height = (1 - progress.percentComplete) * containerHeight,

                                    // 2 = inner content remains still
                                    // > 2 = inner content shifts upward
                                    // < 2 = inner content shifts downward
                                    offset = (height - containerHeight) / 2.5;

                                t.container.style.height = Utilities.numberToPx(height);
                                t.content.style.top = Utilities.numberToPx(offset);

                                t.closeButton.style.transform = `translate(0px, ${offset}px)`;

                                t.backdrop.style.opacity = 1 - progress.percentComplete;
                            },

                            onComplete: function () {
                                t.container.style.height = "";
                                t.content.style.top = "";
                                t.closeButton.style.transform = "";

                                onCompleteCallback();
                            }
                        });
                    break;

                // Default to fade transition
                default:
                    t.timeline.to(
                        progress,
                        duration,
                        {
                            ease: settings.close.easing,
                            percentComplete: 1,

                            onUpdate: function () {
                                t.backdrop.style.opacity = 1 - progress.percentComplete;
                                t.container.style.opacity = 1 - progress.percentComplete;
                            },

                            onComplete: onCompleteCallback
                        });
                    break;
            }
        }

        // Remove this modal from the reference array
        for (var i = 0; i < openModals.length; i++) {
            if (openModals[i] !== t)
                continue;

            openModals.splice(i, 1);
            break;
        }
    };
