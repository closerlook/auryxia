﻿import { Utilities } from "../utilities/utilities.js"

var
    settings = {
        // Minimum duration for API calls, in milliseconds;
        // increase this if you want to make sure the progress
        // indicators appear for a beat before the call completes
        apiMinDelay: 0,

        // Redirect paths
        paths: {
            error: "/error"
        },

        // Extra space to figure into calculations when keeping fields in view
        scrollPadding: 10
    },

    // header = document.getElementsByTagName("header")[0],
    //psp = document.getElementById("psp"),

    makeSureElementIsInView = function (element, callback) {
        var t = this;

        if (element === null)
            return;

        let elementBounds = element.getBoundingClientRect();

        // Sticky header
        // if (header.classList.contains("sticky")) {
        //     if (elementBounds.top - settings.scrollPadding < header.clientHeight) {
        //         Utilities.scrollWindowToPosition(
        //             window.pageYOffset + (elementBounds.top - settings.scrollPadding - header.clientHeight),
        //             callback);
        //         return;
        //     }
        // }

        // Check top of viewport
        if (elementBounds.top < settings.scrollPadding) {
            // This transition may cause the sticky header to appear
            // t.header.suppressSticky = true;
            Utilities.scrollWindowToPosition(
                window.pageYOffset + elementBounds.top - settings.scrollPadding,
                function () {
                    // t.header.suppressSticky = false;

                    if (callback)
                        callback();
                });
            return;
        }

        //// PSP
        //if (psp !== null) {
        //    // Only perform check if it's not expanded
        //    if (!psp.classList.contains("expanded") && psp.style.display !== "none") {
        //        var pspOverlap = (element.offsetTop + element.clientHeight + settings.scrollPadding - window.pageYOffset) - psp.offsetTop;
        //        if (pspOverlap > 0) {
        //            Utilities.scrollWindowToPosition(
        //                window.pageYOffset + pspOverlap,
        //                callback);
        //            return;
        //        }
        //    }
        //}

        // Check bottom of viewport
        var viewportOverlap = elementBounds.bottom - (window.innerHeight - settings.scrollPadding);
        if (viewportOverlap > 0) {
            Utilities.scrollWindowToPosition(
                window.pageYOffset + viewportOverlap,
                callback);
            return;
        }
    };

// Preload images
//Utilities.preloadImage("/Images/error-indicator.svg");

export class Form {
    constructor(parameters, header) {
        var
            t = this,
            elements;

        t.header = header;
        t.form = document.querySelector("form");
        if (t.form === null)
            return;

        // Remove all links from the tabindex flow
        elements = document.getElementsByTagName("a");
        for (var i = 0; i < elements.length; i++)
            elements[i].setAttribute("tabindex", "-1");

        // Setup field focus & blur effects
        elements = t.form.querySelectorAll("input:not([type='submit']), select");
        for (let i = 0; i < elements.length; i++) {
            let
                element = elements[i],
                label = document.querySelector(`label[for='${element.id}']`);

            element.addEventListener("focus", function () {
                // Add "focus" class
                element.parentNode.classList.add("focus");

                // Make sure element is in view
                makeSureElementIsInView.call(t, Utilities.getClosestElement(element, ".form-field, .checkbox-field"));
            });

            element.addEventListener("blur", function () {
                // Remove "focus" class
                element.parentNode.classList.remove("focus");
            });
        }

        // Set up button focus effects
        elements = t.form.querySelectorAll("input[type='submit']");
        for (let i = 0; i < elements.length; i++) {
            let element = elements[i];

            element.addEventListener("focus", function () {
                // Make sure element is in view
                makeSureElementIsInView.call(t, Utilities.getClosestElement(element, ".form-field"));
            });
        }
    }

    clearFieldErrors() {
        var t = this;

        // Find all input containers on the page the have the "error" class applied to them
        var containers = t.form.querySelectorAll(".form-field.error, .checkbox-field.error, .form-error-container.error");
        for (var i = 0; i < containers.length; i++) {
            var
                container = containers[i],
                errorContainer = container.querySelector(".error");

            container.classList.remove("error");
            Utilities.resetStyles(errorContainer);
        }
    }

    clearFieldError(input) {
        var
            container = Utilities.getClosestElement(input, ".form-field, .checkbox-field, .form-error-container"),
            errorContainer = container.querySelector(".error");

        if (errorContainer === null) {
            container = Utilities.getClosestElement(container, ".form-error-container");
            errorContainer = container.querySelector(".error");
        }

        container.classList.remove("error");
        Utilities.resetStyles(errorContainer);
    }

    showFieldErrors(errors, scrollToTopmostError) {
        var
            t = this,
            topmostContainer = null,
            topmostContainerBounds = null,
            topmostInput = null;

        // Show errors for all the error records returned by the API
        for (let i = 0; i < errors.length; i++) {
            let
                error = errors[i],
                input = document.getElementById(error.id);

            if (input === null)
                return;

            let
                container = Utilities.getClosestElement(input, ".form-field, .checkbox-field, .form-error-container"),
                errorContainer = container.querySelector(".error");

            if (errorContainer === null) {
                container = Utilities.getClosestElement(container, ".form-error-container");
                errorContainer = container.querySelector(".error");
            }

            container.classList.add("error");

            errorContainer.innerHTML = error.e;
            errorContainer.style.display = "block";

            // Keep track of the topmost container so that
            // we may scroll to it if necessary
            if (topmostContainer === null) {
                topmostContainer = container;
                topmostContainerBounds = container.getBoundingClientRect();
                topmostInput = input;
            } else {
                let containerBounds = container.getBoundingClientRect();

                if (containerBounds.top < topmostContainerBounds.top) {
                    topmostContainer = container;
                    topmostContainerBounds = containerBounds;
                    topmostInput = input;
                }
            }
        }

        if (scrollToTopmostError) {
            if (topmostContainer) {
                if (errors.length > 1) {
                    makeSureElementIsInView.call(t, topmostContainer, function () {
                        topmostInput.focus();
                    });
                } else {
                    makeSureElementIsInView.call(t, topmostContainer);
                }
            }
        }
    }
}
