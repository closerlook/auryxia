﻿import { Utilities } from "../utilities/utilities.js";
import { Api } from "../utilities/api.js";
import { Form } from "./form.js";
import { Modal } from "./modal.js";

var
    settings = {
        // API action for NPI endpoint calls
        npiApiAction: "npiApi"
    },

    template = null;

export class NpiLookupModal extends Form {
    constructor(parameters, header, associatedFormInputs, event) {
        super(parameters, header);

        var
            t = this,
            awaitingServerResponse = false,

            addFieldValueToRequest = function (input, request) {
                request[input.id.replace(/\-/g, "")] = input.value;
            };

        // Load template off the page
        if (template == null) {
            template = document.getElementById("npi-lookup-modal");
            if (template) {
                template.parentNode.removeChild(template);
                template = template.outerHTML;
            }

            // Stop if it couldn't be loaded
            if (template == null)
                return;
        }

        Modal.show(
            template,
            "npi-lookup",
            Modal.presentationTypes.fadeAndPartialVerticalEnvelope,
            Modal.presentationTypes.scaleDown,
            false,
            {
                preShow: function () {
                    var
                        modal = this,
                        modalInputs = {
                            firstName: modal.container.querySelector("#npi-first-name"),
                            lastName: modal.container.querySelector("#npi-last-name"),
                            city: modal.container.querySelector("#npi-city"),
                            state: modal.container.querySelector("#npi-state")
                        },
                        noResultsFoundContainer = modal.container.querySelector("#npi-lookup-no-results"),
                        resultsContainer = modal.container.querySelector("#npi-lookup-results"),

                        setupModalValidator = function (input) {
                            // Setup API call on blur events
                            input.addEventListener("blur", function () {
                                Api.callApiForAction(
                                    settings.npiApiAction,
                                    "Validate",
                                    {
                                        id: input.id,
                                        value: input.value
                                    },
                                    function (result) {
                                        if (result.errors) {
                                            // Show the error
                                            t.showFieldErrors(result.errors, false);
                                            modal.adjustContentHeight();
                                        } else {
                                            // Clear the error
                                            t.clearFieldError(input);
                                        }
                                    });
                            });
                        };
                    
                    // Setup input validators
                    setupModalValidator(modalInputs.firstName);
                    setupModalValidator(modalInputs.lastName);
                    setupModalValidator(modalInputs.city);
                    setupModalValidator(modalInputs.state);

                    // Setup submit button
                    modal.container.querySelector("form").addEventListener("submit", function (event) {
                        event.preventDefault();
                        event.stopPropagation();

                        document.activeElement.blur();

                        Utilities.resetStyles(noResultsFoundContainer);
                        Utilities.resetStyles(resultsContainer);
                        modal.adjustContentHeight();

                        // Stop if the user has already attempted to submit
                        if (awaitingServerResponse)
                            return;

                        awaitingServerResponse = true;

                        // Assemble our submission
                        var request = {};
                        addFieldValueToRequest(modalInputs.firstName, request);
                        addFieldValueToRequest(modalInputs.lastName, request);
                        addFieldValueToRequest(modalInputs.city, request);
                        addFieldValueToRequest(modalInputs.state, request);

                        // Call the API
                        Api.callApiForAction(
                            settings.npiApiAction,
                            "SearchNpi",
                            request,
                            function (response) {
                                awaitingServerResponse = false;

                                if (response.errors) {
                                    t.clearFieldErrors();
                                    t.showFieldErrors(response.errors, true);
                                    modal.adjustContentHeight();
                                    return;
                                }

                                var scrollTop = modal.content.scrollTop;
                                if (response.records.length <= 0) {
                                    // Show no results found message
                                    noResultsFoundContainer.style.display = "block";
                                } else {
                                    // Show results
                                    resultsContainer.style.display = "block";

                                    var tbody = resultsContainer.querySelector("tbody");
                                    while (tbody.firstChild)
                                        tbody.removeChild(tbody.firstChild);

                                    for (let i = 0; i < response.records.length; i++) {
                                        let record = response.records[i],
                                            tableRow = document.createElement("tr"),
                                            tableCell;

                                        tableCell = document.createElement("td");
                                        tableCell.innerHTML = record.firstName + " " + record.lastName;
                                        tableRow.appendChild(tableCell);

                                        tableCell = document.createElement("td");
                                        tableCell.innerHTML = record.npi;
                                        tableRow.appendChild(tableCell);

                                        tableCell = document.createElement("td");
                                        tableCell.innerHTML = record.state;
                                        tableRow.appendChild(tableCell);

                                        tableCell = document.createElement("td");
                                        tableCell.innerHTML = record.zip;
                                        tableRow.appendChild(tableCell);

                                        tableCell = document.createElement("td");
                                        tableCell.innerHTML = "Use this NPI number";
                                        tableRow.appendChild(tableCell);

                                        tableCell.addEventListener("click", function (event) {
                                            // Populate inputs using the retrieved NPI data
                                            if (associatedFormInputs.npi)
                                                associatedFormInputs.npi.value = record.npi;

                                            if (associatedFormInputs.firstName)
                                                associatedFormInputs.firstName.value = record.firstName;

                                            if (associatedFormInputs.lastName)
                                                associatedFormInputs.lastName.value = record.lastName;

                                            if (associatedFormInputs.profession) {
                                                if (record.profession != null && record.profession.length > 0) {
                                                    for (var i = 0; i < associatedFormInputs.profession.options.length; i++) {
                                                        var option = associatedFormInputs.profession.options[i];
                                                        if (option.innerText == record.profession) {
                                                            associatedFormInputs.profession.selectedIndex = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }

                                            // Close the modal
                                            modal.close(
                                                false,
                                                Modal.presentationTypes.scaleUp,
                                                event);
                                        });

                                        tbody.appendChild(tableRow);
                                    }

                                    if (response.records.length < response.totalRecordsFound) {
                                        var tableRow = document.createElement("tr"),
                                            tableCell = document.createElement("td");

                                        tableCell.setAttribute("colspan", "5");
                                        tableCell.innerHTML = "Displaying first 50 of " + response.totalRecordsFound + " results found.";
                                        tableRow.appendChild(tableCell);

                                        tbody.appendChild(tableRow);
                                    }
                                }

                                modal.adjustContentHeight();
                                if (modal.content.style.overflowY == "scroll") {
                                    modal.content.scrollTop = scrollTop;

                                    if (response.records.length > 0) {
                                        Utilities.scrollElementToPosition(
                                            modal.content,
                                            modal.container.querySelector("#npi-lookup-results").offsetTop - 60,
                                            null);
                                    } else {
                                        Utilities.scrollElementToPosition(
                                            modal.content,
                                            modal.container.querySelector("#npi-lookup-no-results").offsetTop - 60,
                                            null);
                                    }
                                }
                            });
                    });

                    // Prepopulate form fields
                    if (associatedFormInputs.firstName)
                        modalInputs.firstName.value = associatedFormInputs.firstName.value;

                    if (associatedFormInputs.lastName)
                        modalInputs.lastName.value = associatedFormInputs.lastName.value;
                },

                postShow: function () {
                    // Set focus (not working...)
                    //this.container.querySelector("#npi-first-name").focus();
                }
            },
            event);
    }

    static show(parameters, header, associatedFormInputs, event) {
        new NpiLookupModal(parameters, header, associatedFormInputs, event);
    }
}