﻿export class Cookies {
    static create(name, value, days) {
        var expires;

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        
        document.cookie = name + "=" + value + expires + ";path=/;SameSite=Lax";
    }

    static read(name) {
        var cookies = document.cookie.split(';');
        name = name + "=";

        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            while (cookie.charAt(0) == ' ')
                cookie = cookie.substring(1, cookie.length);

            if (cookie.indexOf(name) == 0)
                return cookie.substring(name.length, cookie.length);
        }

        return null;
    }
}
