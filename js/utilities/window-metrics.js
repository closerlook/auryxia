﻿var
    settings = {
        // Responsive breakpoints
        breakpoints: {
            small: 0,
            medium: 768,
            large: 960,
            extraLarge: 1200,
            largeWithMargins: 1344
        }
    },

    sortedBreakpoints = [],
    lastBreakpoint = null,
    currentBreakpoint = null,
    breakpointChangeCallbacks = [],

    updateWindowMetrics = function () {
        // Update the current responsive breakpoint
        lastBreakpoint = currentBreakpoint;
        for (var i = 0; i < sortedBreakpoints.length; i++) {
            var breakpoint = sortedBreakpoints[i];

            if (window.matchMedia("(min-width: " + breakpoint + "px)").matches) {
                currentBreakpoint = breakpoint;
                break;
            }
        }

        // Check if the breakpoint has changed
        if (lastBreakpoint !== currentBreakpoint && lastBreakpoint !== null) {
            // Determine if we're crossing the mobile-desktop threshold
            var mobileDesktopTransition = false;
            if (WindowMetrics.isMobile(lastBreakpoint))
                mobileDesktopTransition = !WindowMetrics.isMobile();
            else
                mobileDesktopTransition = WindowMetrics.isMobile();

            // Execute breakpoint change callbacks
            for (var i = 0; i < breakpointChangeCallbacks.length; i++)
                breakpointChangeCallbacks[i](mobileDesktopTransition);
        }
    };

// Initialize the sorted breakpoint array
for (var breakpointName in settings.breakpoints)
    sortedBreakpoints.push(settings.breakpoints[breakpointName]);

sortedBreakpoints.sort(function (a, b) {
    return b - a;
});

// Setup resize callback
window.addEventListener("resize", updateWindowMetrics);
updateWindowMetrics();

export class WindowMetrics {
    static get breakpoints() {
        return settings.breakpoints;
    }

    static get lastBreakpoint() {
        return lastBreakpoint;
    }

    static get currentBreakpoint() {
        return currentBreakpoint;
    }

    static isMobile(breakpointToTest) {
        if (typeof breakpointToTest == "number")
            return breakpointToTest <= settings.breakpoints.small;
        else
            return currentBreakpoint <= settings.breakpoints.small;
    }

    static addBeakpointChangeCallback(callback) {
        breakpointChangeCallbacks.push(callback);
    }
}
