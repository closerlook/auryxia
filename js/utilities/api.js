const
    settings = {
        // API URL
        apiUrl: myAjax.ajaxurl,

        // Minimum duration for API calls, in milliseconds;
        // increase this if you want to make sure the progress
        // indicators appear for a beat before the call completes
        apiMinDelay: 0,

        // Redirect paths
        paths: {
            error: "/error"
        },

        performErrorRedirect: false
    };

export class Api {
	static callApiForAction(action = "signupApi", method, data, callback) {
        var
            apiCallStart = (new Date()).getTime(),
            request = new XMLHttpRequest();
        request.open("POST", settings.apiUrl, true);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                // Success
                var response = JSON.parse(request.response);

                // Perform callback, if specified
                if (typeof (callback) == "function") {
                    var apiCallEnd = (new Date()).getTime();
                    var apiCallDuration = apiCallEnd - apiCallStart;

                    // Check if the api call duration was smaller than
                    // our minimum wait time
                    if (apiCallDuration < settings.apiMinDelay) {
                        // Call took less than our minimum wait time;
                        // perform callback after the appropriate delay
                        setTimeout(
                            function () {
                                callback(response);
                            },
                            settings.apiMinDelay - apiCallDuration);
                    } else {
                        // Call took equal or more than our minimum wait
                        // time; perform callback immediately
                        callback(response);
                    }
                }
            } else {
                // Error

                if (window.location.href.indexOf("sign-up") != -1) {
                  window.location = 'error';
                }

                else if (settings.performErrorRedirect)
                    window.location.href = settings.paths.error;
                else
                    throw "";
            }
        };

        request.onerror = function () {
            // Error
            if (settings.performErrorRedirect)
                window.location.href = settings.paths.error;
            else
                throw "";
        };

        // Assemble our POST
        var postData = [];
        postData.push(`action=${action}`);
        postData.push("method=" + encodeURIComponent(method));
        postData.push("nonce=" + encodeURIComponent(myAjax.signup_nonce));

        if (data)
            postData.push("data=" + encodeURIComponent(JSON.stringify(data)));

        postData = postData.join("&");
        request.send(postData);
    }

	static callApi(method, data, callback) {
        // Call the default API action
        Api.callApiForAction(undefined, method, data, callback);
    }

    static get basicResponseCodes() {
        return basicResponseCodes;
    }

    static get signUpErrorCodes() {
        return signUpErrorCodes;
    }

    static showUnknownError(response) {
        if (response)
            console.log(response);

        // Error
        if (settings.performErrorRedirect)
            window.location.href = settings.paths.error;
        else
            throw "";
    }
}
