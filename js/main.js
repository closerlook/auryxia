import {IdaSignUpPage, HpSignUpPage} from "./pages/sign-up.js";
import HpCompetitiveLandingPage from "./pages/hp-competitive-landing.js";

export class Auryxia {
  constructor(parameters) {
    const t = this;

    switch (document.body.id) {
      case "sign-up":
        if (document.body.classList.contains('page-template-template-ida')) {
          //console.log('new ida page');
          new IdaSignUpPage(parameters, null);
        } else if (document.body.classList.contains('page-template-template-new-signup-hp-page')) {
          //console.log('new HP page');
          new HpSignUpPage(parameters, null);
        } else {console.log('neither......')}
        // new SignUpPage(parameters, null);
        break;

      case "template-new-competitive-landing-hp-page":
        new HpCompetitiveLandingPage(parameters);
        break;
    }
  }
}

window.Auryxia = Auryxia;
export default Auryxia;

(function($) {
  "use strict"; // Start of use strict
  $(document).ready(function() {
    function toggleDropdown(e) {
      const _d = $(e.target).closest('.dropdown'),
        _m = $('.dropdown-menu', _d);
      setTimeout(function() {
        const shouldOpen = e.type !== 'click' && _d.is(':hover');
        _m.toggleClass('show', shouldOpen);
        _d.toggleClass('show', shouldOpen);
        $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
      }, e.type === 'mouseleave' ? 300 : 0);
    }

    $('body')
      .on('mouseenter mouseleave', '.dropdown', toggleDropdown)
      .on('click', '.dropdown-menu a', toggleDropdown)
  })
}
)(jQuery);
