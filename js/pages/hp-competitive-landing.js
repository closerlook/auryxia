import { Utilities } from "../utilities/utilities.js";
import { Psp } from "../components/psp.js";

export default class HpCompetitiveLandingPage {
    constructor(parameters) {
        // Setup PSP, supplying a function to calculate the scroll
        // position at which it becomes sticky
        if (!Utilities.screenshotMode) {
            this.psp = new Psp(
                function () {
                    // Note: "this" is the Psp in the scope of this function
                    return this.onPagePsp.offsetTop - window.innerHeight;
                });
        }
    }
}
