﻿import { Utilities } from "../utilities/utilities.js";
import { Api } from "../utilities/api.js";
import { Form } from "../components/form.js";
import { NpiLookupModal } from "../components/npi-lookup.js";

export class SignUpPage extends Form {
    constructor(parameters, header, formType) {
        super(parameters, header);

        if (formType === 'ida') {
          this.formMethod = 'SubmitIronDeficiencyAnemia';
        } else if (formType === 'hp') {
          this.formMethod = 'SubmitHyperphosphatemia'
        }

      // add class to perform form specific
      if (formType === 'hp') {
        document.getElementById('sign-up-button').className += ' hp';
        document.getElementById('npi-sign-up-button').className += ' hp';
      }


        var
            t = this,

            setupValidator = function (input, associatedInput) {
                // Setup API call on blur events
                input.addEventListener("blur", function () {
                    var data = {
                        id: input.id,
                        value: input.value
                    };

                    if (associatedInput)
                        data.associatedValue = associatedInput.value;

                    Api.callApi(
                        "Validate",
                        data,
                        function (result) {
                            if (result.errors) {
                                // Show the error
                                t.showFieldErrors(result.errors, false);
                            } else {
                                // Clear the error
                                t.clearFieldError(input);
                            }
                        });
                });
            },

            awaitingServerResponse = false,

            addFieldValueToRequest = function (input, request) {
                request[input.id.replace(/\-/g, "")] = input.value;
            },

            addFieldCheckedStatusToRequest = function (input, request) {
                request[input.id.replace(/\-/g, "")] = input.checked;
            };

        // Get references to DOM elements
        t.inputs = {
            npi: document.getElementById("npi"),
            npiLookupButton: document.getElementById("npi-lookup-button"),
            noNpi: document.getElementById("no-npi"),
            firstName: document.getElementById("first-name"),
            lastName: document.getElementById("last-name"),
            email: document.getElementById("email")
        };

        t.npiContainer = Utilities.getClosestElement(t.inputs.npi, ".form-field");

        // Setup NPI lookup modal
        t.inputs.npiLookupButton.addEventListener("click", function (event) {
            event.preventDefault();
            event.stopPropagation();

            t.clearFieldErrors();

            // Show the modal, passing it the relevant inputs from our form
            NpiLookupModal.show(
                parameters,
                header,
                {
                    npi: t.inputs.npi,
                    firstName: t.inputs.firstName,
                    lastName: t.inputs.lastName
                },
                event);
        });

        // Automatically opens the form on load; for testing purposes
        //setTimeout(
        //    function () {
        //        t.inputs.npiLookupButton.click();
        //    },
        //    100);

        // Setup NPI opt-out checkbox
        var npiOptOutChangeCallback = function () {
            if (t.inputs.noNpi.checked) {
                t.npiContainer.classList.add("disabled");
                t.inputs.npi.disabled = true;
                t.clearFieldError(t.inputs.npi);
            } else {
                t.npiContainer.classList.remove("disabled");
                t.inputs.npi.disabled = false;
            }
        };

        t.inputs.noNpi.addEventListener("change", npiOptOutChangeCallback);
        npiOptOutChangeCallback();

        // Add npi numeric format validation
        Utilities.addNpiCodeInputValidation(t.inputs.npi);

        // Setup input validators
        setupValidator(t.inputs.npi, t.inputs.lastName);
        setupValidator(t.inputs.firstName);
        setupValidator(t.inputs.email);

        // The last name validator is a bit more complex, as it functions
        // differently depending on the state of the "noNpi" checkbox
        t.inputs.lastName.addEventListener("blur", function () {
            var data = {
                id: t.inputs.lastName.id,
                value: t.inputs.lastName.value
            };

            if (!t.inputs.noNpi.checked)
                data.associatedValue = t.inputs.npi.value;

            Api.callApi(
                "Validate",
                data,
                function (result) {
                    if (result.errors) {
                        // Show the error
                        t.showFieldErrors(result.errors, false);
                    } else {
                        // Clear the error
                        t.clearFieldError(t.inputs.lastName);
                        t.clearFieldError(t.inputs.npi);
                    }
                });
        });

        // Setup submit button
        t.form.addEventListener("submit", function (event) {
            event.preventDefault();
            event.stopPropagation();

            document.activeElement.blur();

            // Stop if the user has already attempted to submit
            if (awaitingServerResponse)
                return;

            awaitingServerResponse = true;

            // Assemble our submission
            var request = {};
            addFieldValueToRequest(t.inputs.npi, request);
            addFieldCheckedStatusToRequest(t.inputs.noNpi, request);
            addFieldValueToRequest(t.inputs.firstName, request);
            addFieldValueToRequest(t.inputs.lastName, request);
            addFieldValueToRequest(t.inputs.email, request);

            // Call the API
            Api.callApi(
                t.formMethod,
                request,
                function (response) {
                    awaitingServerResponse = false;

                    if (response.errors) {
                        t.clearFieldErrors();
                        t.showFieldErrors(response.errors, true);
                        return;
                    }

                    let gtmEventCallback = function () {
                        // Proceed to the thank you page
                        window.location.href = "thank-you";
                    };

                    window.dataLayer = window.dataLayer || [];
                    window.dataLayer.push({
                        event: "data-completion",
                        "data-completion": "completed",
                        "signUpID": response.responseId,
                        "eventCallback": gtmEventCallback
                    });

                    // If GTM is slow to respond for whatever reason (or it's disabled)
                    // just redirect after a moment anyhow
                    setTimeout(gtmEventCallback, 2000);
                });
        });
    }
}

export class IdaSignUpPage extends SignUpPage {
  constructor(parameters, header) {
    super(parameters, header, 'ida');
  }
}

export class HpSignUpPage extends SignUpPage {
  constructor(parameters, header) {
    super(parameters, header, 'hp');
  }
}
