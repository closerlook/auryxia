<?php /* Template Name: Redirect */
  
  $homeID = 76; /* Home Page */
  $redirectID = $homeID; 
  
  if (is_page(37)){ $redirectID = 177;} /* Safety */
  if (is_page(43)){ $redirectID = 112;} /* Safety */
  if (is_page(45)){ $redirectID = 169;} /* Dosing */
  
  /* Generate Redirect URL */
  $redirectURL = get_permalink($redirectID);
  wp_redirect($redirectURL); exit; 
  

?>