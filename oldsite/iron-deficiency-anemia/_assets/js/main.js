$(document).ready(function(){

  // let resourceLocation = "/wp-content/themes/keryx/oldsite/";
  // var resourceBaseURL = `/wp-content/themes/keryx/oldsite${window.location.pathname}_assets/includes/`;
  // var resourceIDAURL = `/wp-content/themes/keryx/oldsite/iron-deficiency-anemia/_assets/includes/`;

  var baseURL = '/wp-content/themes/keryx/oldsite/';
  var idaBaseURL = '/wp-content/themes/keryx/oldsite/iron-deficiency-anemia/';
  var pathName = window.location.pathname;
  console.log(pathName);

    $('.rightSide').css('opacity','1');

  // Hide Fonts.com Icon
  $('#mti_wfs_colophon, #mti_wfs_colophon img').hide();
  $('#mti_wfs_colophon, #mti_wfs_colophon_anchor, #mti_wfs_colophon_anchor img').css('visibility', 'hidden');
  // Includes Path
  var includes = "_assets/includes/";

  // Scroll To ISI
  // cta-isi-homepage on index page
  // cta-isi-sidebar on other pages
  if ($('div.cta-isi-homepage').length)
  {
    $('div.cta-isi-homepage').click(function(e)
    {
      e.preventDefault();
      TweenMax.to(window, 0.5, {scrollTo:{y:'#isi-start'}, ease:Power2.easeOut});
    });
  }
  else
  if ($('div.cta-isi-sidebar').length)
  {
    $('div.cta-isi-sidebar').click(function(e)
    {
      e.preventDefault();
      TweenMax.to(window, 0.5, {scrollTo:{y:'#isi-start'}, ease:Power2.easeOut});
    });
  }

  /* Load Header */
  // Load header on all pages EXCEPT the gateway index page
  if (window.location.pathname !== '/')
  {
    if ( window.location.pathname !== '/important-safety-information/') {
      // let headerURL = `${resourceLocation}${window.location.pathname}${includes}header.html`;
      // console.log(`Header was trying to load ${includes}header.html`);
      // console.log(`updated Header was trying to load ${resourceBaseURL}`);
      // $('#header').load(resourceBaseURL +'header.html', header);
      $('#header').load(idaBaseURL + includes +'header.html', header);
      // console.log('loading header');
    }
  }

  if (window.location.pathname === '/iron-deficiency-anemia/')
  {
    // callouts rollovers
    $( '.callout' ).hover(
      function() {
        var el = $(this).find('.callout-arrow-right');
        TweenMax.to(el, 0.25, {borderLeft: '7px solid #FFFFFF'});
      }, function() {
        var el = $(this).find('.callout-arrow-right');
        TweenMax.to(el, 0.25, {borderLeft: '7px solid #5B6770'});
      }
    );
  }

  function header()
  {
    $('#mobile-menu').click(function()
    {
      $(this).toggleClass("open");
      $("nav").toggleClass("mobile-open");
    });
  }

  /* Load indication Banner */
  // console.log(`indication banner was trying to load ${includes}indication-banner.html`);
  // console.log(`updated indication banner was trying to load ${resourceBaseURL}indication-banner.html`);
  $('#indication-banner').load(idaBaseURL + includes +'indication-banner.html', indicationBanner);

  function indicationBanner() {

  }

  /* Load Navigation */
  if (window.location.pathname === '/iron-deficiency-anemia/')
  {
    //$('#footer').load(includes + 'footer-home.html', footer);
    // console.log(`footer was trying to load ${includes}nav-home.html`);
    // console.log(`updated footer banner was trying to load ${resourceBaseURL}nav-home.html`);
    $('#navigation').load(idaBaseURL + includes +'nav-home.html', navigation);
  }
  else
  {
    // console.log(`nav was trying to load ${includes}nav-home.html`);
    // console.log(`updated footer banner was trying to load ${resourceBaseURL}nav.html`);
    $('#navigation').load(idaBaseURL + includes +'nav.html', navigation);
  }

  function navigation()
  {
    var pathname = window.location.pathname;
    /* Add active classes to navigation menu items */
    switch (pathname)
    {
      case '/iron-deficiency-anemia/':
        $('#nav-home').addClass('active');
        break;
      case '/iron-deficiency-anemia/efficacy/':
        $('#nav-efficacy').addClass('active');
        break;
      case '/iron-deficiency-anemia/tolerability-and-safety/':
        $('#nav-tolerability-and-safety').addClass('active');
        break;
      case '/iron-deficiency-anemia/dosing/':
        $('#nav-dosing').addClass('active');
        break;
      case '/iron-deficiency-anemia/moa/':
        $('#nav-moa').addClass('active');
        break;
      case '/iron-deficiency-anemia/interactive-library/':
        $('#nav-interactive-library').addClass('active');
        break;
      case '/iron-deficiency-anemia/sign-up/':
        $('#nav-sign-up').addClass('active');
        break;
      case '/iron-deficiency-anemia/sign-up/thank-you':
        $('#nav-sign-up').addClass('active');
        break;
    }

    /* This updates the Home icon colors */
    if (pathname === '/iron-deficiency-anemia/')
    {
      TweenMax.set('.icon-home-fill', {fill:'#FF8500'});
    }
    else
    {
      TweenMax.set('.icon-home-fill', {fill:'#5B6770'});
    }

    $( "#nav-home" ).mouseover(function() {
      TweenMax.set('.icon-home-fill', {fill:'#FF8500'});
    });

    $( "#nav-home" ).mouseout(function()
    {
      if (pathname === '/iron-deficiency-anemia/')
      {
        TweenMax.set('.icon-home-fill', {fill:'#FF8500'});
      }
      else
      {
        TweenMax.set('.icon-home-fill', {fill:'#5B6770'});
      }
    });
  }

  /* Load ISI */

  if ($('body').hasClass("impSafeHome")){
    $('#isi-content').load( idaBaseURL + includes + 'home-isi.html', isi);
  }

  if (window.location.pathname === '/')
  {
    $('#isi-content').load( idaBaseURL + includes + 'home-isi.html', isi);
  }
  else if (window.location.pathname !== '/important-safety-information/')
  {
    console.log('attempting to load isi');
    $('#isi-content').load(idaBaseURL + includes + 'isi.html', isi);
  }

  function isi()
  {

  }
  console.log("path name is: " + window.location.pathname);
  // Load Footer on ida pages only...
  if (window.location.pathname === '/iron-deficiency-anemia/')
  {
    // console.log('attempting to load ida');
    // $('#footer').load(idaBaseURL + includes + 'footer-home.html', footer);

    $.get(idaBaseURL + includes + 'footer-home.html', function (response) {
          $('#footer').empty().append(response);
        })
      .fail(function (res, status, jqXhr) {
        console.log('error loading page');
      });
  }
  else
  {
    $('#footer').load(idaBaseURL + includes + 'footer.html', footer);
  }

  function footer()
  {
    // footer is loaded - do footer stuff
  }


  if ( $( "#tab-wrap" ).length )
  {
    // Efficacy and Safety pages tabs
    $(".tab-names a").click(function() {
      tabPosition = $(this).index();
      $(this).addClass("active").siblings().removeClass("active");
      $(".tab").eq(tabPosition).addClass("active").siblings().removeClass("active");
      return false;
    });

    // Trial Design Toggle
    $('#trial-design-collapse').on('hidden.bs.collapse', function () {
      $('#trial-design-expanded').css('opacity', '0');
      $('#trial-design-expanded').css('display', 'none');

      $('#trial-design-toggle .toggle-on').css('display', 'block');
      $('.tab-toggle').attr('data-label', 'Closed');
      $('#trial-design-toggle .toggle-off').css('display', 'none');

      $('.trial-design-arrow').css('transform', 'rotate(0deg)');
      $('.trial-design-arrow').css('margin-top', '3px');
    });

    $('#trial-design-collapse').on('show.bs.collapse', function () {
      $('#trial-design-expanded').css('opacity', '1');
      $('#trial-design-expanded').css('display', 'block');

      $('#trial-design-toggle .toggle-on').css('display', 'none');
      $('.tab-toggle').attr('data-label', 'Opened');
      $('#trial-design-toggle .toggle-off').css('display', 'block');

      $('.trial-design-arrow').css('transform', 'rotate(180deg)');
      $('.trial-design-arrow').css('margin-top', '0');
    });

    $('#trial-design-collapse').collapse('hide');
  }

  // MOA Interactive
  if ( $( "#moa-interactive" ).length )
  {
    setupMOAInteractive();
  }

  if ( $("#library-items").length )
  {
    $( '.download-btn' ).hover(
      function() {
        var el = $(this).find('.icon-download-color');
        TweenMax.to(el, 0.25, {fill:'white'});
      }, function() {
        var el = $(this).find('.icon-download-color');
        TweenMax.to(el, 0.25, {fill:'#005F9E'});
      }
    );
  }

});

function setupMOAInteractive()
{
  TweenMax.to('#moa-interactive', 0.5, {autoAlpha:1});
  TweenMax.set(['.moa-img2', '.moa-img3'], {autoAlpha:0});
  TweenMax.set(['.moa-img1'], {autoAlpha:1});

  TweenMax.set('.moa-btn1', {backgroundPosition: '0px -58px'});

  $( ".moa-btn1" ).click(function()
  {
    TweenMax.to(['.moa-img2', '.moa-img3'], 0.5, {autoAlpha:0});
    TweenMax.to(['.moa-img1'], 0.5, {autoAlpha:1});

    TweenMax.set('.moa-btn1', {backgroundPosition: '0px -58px'});
    TweenMax.set(['.moa-btn2', '.moa-btn3'], {backgroundPosition: '0px 0px'});
  });

  $( ".moa-btn2" ).click(function()
  {
    TweenMax.to(['.moa-img1', '.moa-img3'], 0.5, {autoAlpha:0});
    TweenMax.to(['.moa-img2'], 0.5, {autoAlpha:1});

    TweenMax.set('.moa-btn2', {backgroundPosition: '0px -58px'});
    TweenMax.set(['.moa-btn1', '.moa-btn3'], {backgroundPosition: '0px 0px'});
  });

  $( ".moa-btn3" ).click(function()
  {
    TweenMax.to(['.moa-img2', '.moa-img1'], 0.5, {autoAlpha:0});
    TweenMax.to(['.moa-img3'], 0.5, {autoAlpha:1});

    TweenMax.set('.moa-btn3', {backgroundPosition: '0px -58px'});
    TweenMax.set(['.moa-btn2', '.moa-btn1'], {backgroundPosition: '0px 0px'});
  });
}
