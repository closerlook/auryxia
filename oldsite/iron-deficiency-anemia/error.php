<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, maximum-scale=1.0 , initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Interactive Library: Take advantage of helpful AURYXIA® (ferric citrate) and iron deficiency anemia related resources">
    <meta property="og:title" content="AURYXIA® (ferric citrate) for Iron Deficiency Anemia | Interactive Library">
    <meta property="og:description" content="Interactive Library: Take advantage of helpful AURYXIA® (ferric citrate) and iron deficiency anemia related resources">
    <meta property="og:image" content="https://www.Auryxia.com/iron-deficiency-anemia/Auryxia-logo.png" />

    <link rel="icon" href="_assets/images/favicon.ico">
    <link rel="canonical" href="https://www.Auryxia.com/iron-deficiency-anemia/interactive-library">
    <title>AURYXIA® (ferric citrate) for Iron Deficiency Anemia | Interactive Library</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="_assets/css/bootstrap.min.css">
    <!-- Main styles -->
    <link rel="stylesheet" href="_assets/css/styles.css">

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-P5T5HJB');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body class="idahcpError">


    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P5T5HJB"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <section>
        <!-- HTML includes -->
        <header id="header"></header>
        <section id="navigation"></section>
        <section id='indication-banner' class='container col-centered'></section>
        <!--content goes here-->
        <section class='container col-centered page-content'>
            <section class='col-md-12 col-centered interior'>
                <div class='post-titles'>
                    <br>
                    <h2>LOOKING FOR AURYXIA INFORMATION?</h2>
                    <h3 class="idahcpErrorh3"style="color: #FF8500; font-size: 15px !important;margin-top: 5px;">404 error. Page not found</h3>

<a href="/iron-deficiency-anemia/"><button class="idahcpErrorBtn" style="color: #FF8500; border-radius: 10px; border: 4px solid #FF8500; font-size: 24px; background: #fff; cursor: pointer; width: 400px; height: 70px; font-weight: bolder; margin-top: 20px;margin-bottom: 240px;  ">GO TO THE HCP HOME PAGE</button></a>

                </div>
              
              
            </section>
        </section>
        <!-- HTML includes -->
       
        <footer id="footer"></footer>
    </section>
    <!-- jQuery -->
    <script src='_assets/js/vendor/jquery-3.2.1.min.js'></script>
    <!-- Greensock -->
    <script src='_assets/js/vendor/TweenMax.min.js'></script>
    <script src='_assets/js/vendor/ScrollToPlugin.min.js'></script>
    <!-- Bootstrap Javascript -->
    <script src='_assets/js/vendor/bootstrap.min.js'></script>
    <!-- Main JS -->
    <script src='_assets/js/main.js'></script>
    <!-- Fonts -->
    <script type='text/javascript' src='https://fast.fonts.net/jsapi/79ad4d54-144b-476c-9335-9765e30f0fae.js'></script>
</body>

</html>