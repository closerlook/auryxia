<!DOCTYPE html>
<html lang="en">
<head>
  <!-- OneTrust Cookies Consent Notice start -->
  <script src="https://cookie-cdn.cookiepro.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="20038081-6e78-46f0-8bde-230c7dcd52c2"></script>
  <script type="text/javascript">
      function OptanonWrapper() { }
  </script>
  <!-- OneTrust Cookies Consent Notice end -->
  <!-- Google Tag Manager --> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NNJLGDX');</script> <!-- End Google Tag Manager -->
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

 

    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">









    <link rel="image_src" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO" />
    <meta itemprop="image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

    <meta property="og:image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

       <meta name="description" content='AURYXIA® (ferric citrate) for adults with iron deficiency anemia in CKD not on dialysis ' />
   
   <meta property="og:title" content='AURYXIA | Official IDA Patient Site' />
   <meta property="og:description" content='AURYXIA® (ferric citrate) for adults with iron deficiency anemia in CKD not on dialysis ' />
   <meta property="og:url" content='https://www.AURYXIA.com/iron-deficiency-anemia/patient' />
   <!-- <meta property="og:image" content='/static/sofvel/www-epclusa-com/v3/images/logo.png' /> -->
   <link rel="canonical" href='https://www.AURYXIA.com/iron-deficiency-anemia/patient' />
   <link rel="alternate" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient" hreflang="en-us" />




    <title>AURYXIA | Official IDA Patient Site</title>

    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/nav.css" />
    <link rel="stylesheet" type="text/css" href="./css/home.css" />
</head>
<body class="homePage">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNJLGDX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    
   <!-- Nav Start -->
   <?php include 'includes/nav.php'; ?>
   <!-- Nav End -->

<div class="layoutBody">
    <div class="container outerContainer ">
        <div class="backdropContainer">
        <img src="images/Graphics_Backdrop.png" alt="" class="bgImgFaq">
       </div>
        <div class="container innerContainer">
        <div class="homeImgContainer">
        <img src="/iron-deficiency-anemia/patient/images/home/homeHeaderImg.png" alt="The only oral iron designed for you" class="headerImg index-DesktopHeader indexDesktopBg">
        <img src="/iron-deficiency-anemia/patient//images/home/homeHeaderImgOld.png" alt="The only oral iron designed for you" class="headerImg index-DesktopHeader indexDesktopbg991">
        <p>AURYXIA is the only iron tablet that's approved to treat<br class="desktopBr"> iron deficiency anemia specifically in adults with chronic<br class="desktopBr">kidney disease (CKD) not on dialysis</p>
        <span class="patientTextHome">Hypothetical patient portrayals.</span>
        </div>
          
        <img src="images/mobileHeader-Index.png" alt="" class="bannerMobile index-MobileHeader" id="MobileHeader">
        <!-- <span class="patientTextHome-mobile">Hypothetical patient portrayals.</span> -->
            <div class="contentInner">
              
                <div class="container boxContainers">
                    <div class="row">
                        
                        <div class="col-md box boxOne gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="What is Iron Deficiency Anemia">
                        <p class="p1">WHAT IS IRON DEFICIENCY ANEMIA?</p>
                        <p class="p2" id="homep2Txt">Learn more about your condition</p>
                        </div>

                        <div class="col-md box boxTwo gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Auryxia 101">
                        <p class="p1">AURYXIA 101</p>
                        <p class="p2" id="homep2Txt">Explore how AURYXIA can help</p>
                        </div>

                        <div class="col-md box boxThree gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Save on the Cost of Auryxia">
                        <p class="p1">SAVE ON THE COST OF AURYXIA</p>
                        <p class="p2 " id="homep2Txt">Depending on your insurance, you may get AURYXIA for free</p>
                        </div>
                    </div>
                </div>


<div class="mobileMidBox">
<img src="/iron-deficiency-anemia/patient/images/home/assistance-icon.png" alt="Access assistant icon" class="assistance-icon">
<p class="assistance-p1">LOOKING FOR PERSONALIZED ACCESS ASSISTANCE?</p>
<p class="assistance-p2">Call one of our dedicated AkebiaCares Case Managers</p>
<div class="centerBox">


<ul class="nav justify-content-center">
<li class="nav-item navBorder">
     <a class="nav-link gtm-cta" href="tel:8556868601" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="call 855-686-8601">855-686-8601</a>
</li>
<li class="nav-item navBorder">
      <a class="nav-link disabled">Monday-Friday</a>
 </li>
  <li class="nav-item">
     <a class="nav-link disabled" >8<span class="AM">AM</span>-7<span class="AM">PM</span> ET</a>
 </li>
</ul>
</div>
<p class="isiTxt">See <a data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important Safety Information</a> below</p>
</div>


                <div class="container">
                    <div class="row">
                            <div class="col-md-3">
                            
                            </div>
                            <div class="col-md-6 midBox">
                                <div class="assistanceBox">
                                    <img src="/iron-deficiency-anemia/patient/images/home/assistance-icon.png" alt="Access assistant icon" class="assistance-icon">
                                    <div class="textBox">
                                    <p class="assistance-p1">LOOKING FOR PERSONALIZED ACCESS ASSISTANCE?</p>
                                    <p class="assistance-p2">Call one of our dedicated AkebiaCares Case Managers</p>

                                           <div class="centerBox">

                                           
                                           <ul class="nav">
                                           
                                            <li class="nav-item navBorder">
                                                <a class="nav-link gtm-cta" href="tel:8556868601" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="call 855-686-8601">855-686-8601</a>
                                            </li>
                                           
                                            <li class="nav-item navBorder">
                                                <a class="nav-link disabled" >Monday-Friday</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link disabled" >8<span class="AM">AM</span>-7<span class="AM">PM</span> ET</a>
                                            </li>
                                            </ul>

                                            </div>


                                            <p class="isiTxt homeIsiSee">See <a class="gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important Safety Information</a> below</p>
                                            </div>
                                </div>
                            
                            </div>
                            <div class="col-md-3">
                          
                            </div>
                    </div>
                </div>


<div class="isi">
<?php include 'includes/isi.php'; ?>
</div>




            </div>

         

        </div>
    </div>
</div>


<div class="footerInclude">
<?php include 'includes/footer.php'; ?>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
    crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
    crossorigin="anonymous"></script>
<script>window.jQuery.fn.modal || document.write('<script src="js/bootstrap.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/is-in-viewport/3.0.4/isInViewport.min.js"></script>


    <script src="./dist/script.js"></script>    <script src="./dist/fcbTracker_v8.js"></script>

</body>
</html>