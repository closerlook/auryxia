<div class="isiContainer">
    <div id="important-safety-information"></div>
    <p class="p1ISI">IMPORTANT SAFETY INFORMATION</p>
    <p class="p2">This information does not take the place of talking to your healthcare provider about your medical condition or treatment</p>
    <p class="p3">What is the most important information I should know about AURYXIA<sup class="isiSup">®</sup><span class="ferricSpace">(ferric citrate)?</span></p>
    <p class="p2">AURYXIA contains iron. Keep it away from children to prevent an accidental ingestion of iron and potentially fatal poisoning. Call a poison control center or your healthcare provider if a child swallows AURYXIA</p>
    <p class="p2">AURYXIA can increase iron levels in your blood. Iron absorbed from AURYXIA may also increase iron in your body. Your healthcare provider will monitor your iron levels. If you are receiving intravenous (IV) iron, your IV iron dose may be adjusted or discontinued</p>
    <p class="p3">What is AURYXIA?</p>
    <p class="p2">AURYXIA is a prescription medicine that can lower the amount of phosphate in the blood for adults with chronic kidney disease (CKD) who ARE on dialysis</p>
    <p class="p2">For adults who have iron deficiency anemia, as well as CKD, and are NOT on dialysis, AURYXIA is a prescription medicine that can increase hemoglobin levels and increase iron levels in the body</p>
    <p class="p3">Who should not take AURYXIA?</p>
    <p class="p2">Do not take AURYXIA if you have been diagnosed with an iron overload syndrome, such as hemochromatosis</p>
    <p class="p3">AURYXIA may not be right for you. Before starting AURYXIA, tell your healthcare provider if you:</p>
    <ul class="ulBlock">
        <li><span>have any other medical conditions</span></li>
        <li><span>are pregnant, plan to become pregnant, are breastfeeding or plan to breastfeed</span></li>
    </ul>
    <p class="p3">Tell your healthcare provider about all of the medicines you take, including: </p>
    <ul class="ulBlock">
        <li><span>the antibiotics doxycycline or ciprofloxacin</span></li>
        <li><span>prescription and over-the-counter medicines, vitamins, and herbal supplements</span></li>
    </ul>
    <p class="p2">Know the medicines you take. Keep a list of them and show it to your healthcare provider and pharmacist when you get a new medicine</p>
    <p class="p3">How should I take AURYXIA?</p>
    <ul class="ulBlock">
        <li><span>Take AURYXIA exactly as prescribed by your healthcare provider</span></li>
        <li><span>Take AURYXIA with meals and adhere to any diet prescribed by your healthcare provider</span></li>
        <li><span>Your healthcare provider will tell you how much AURYXIA to take and may change your dose if necessary</span></li>
        <li><span>Swallow AURYXIA whole. Do not chew or crush.</span></li>
        <li><span>If you are taking the antibiotics doxycycline or ciprofloxacin, you will need to take it separately from AURYXIA. Follow your healthcare provider's instructions on when to take doxycycline or ciprofloxacin while you are also taking AURYXIA</span></li>
    </ul>
    <p class="p3">What are the most common side effects of AURYXIA?</p>
    <!-- <p class="p2"> The most common side effects of AURYXIA when it is used to lower phosphorus in the blood for adult patients with CKD on dialysis include:</p>
    <ul class="inlineUl">
       <li><span class="bulletPoint">•</span><span>diarrhea</span></li>
       <li><span class="bulletPoint">•</span><span>nausea</span></li>
       <li><span class="bulletPoint">•</span><span>constipation</span></li>
       <li><span class="bulletPoint">•</span><span>vomiting</span></li>
       <li><span class="bulletPoint">•</span><span>cough</span></li>
    </ul> -->
    <p class="p2">The most common side effects of AURYXIA for use in the treatment of iron deficiency anemia in adult patients with CKD not on dialysis include:</p>
    <ul class="inlineUl">
       <li><span class="bulletPoint">•</span><span>diarrhea</span></li>
       
       <li><span class="bulletPoint">•</span><span>constipation</span></li>
       <li><span class="bulletPoint">•</span><span>nausea</span></li>
       <li><span class="bulletPoint">•</span><span>abdominal pain</span></li>
       <li><span class="bulletPoint">•</span><span>high levels of potassium in the blood</span></li>
     
    </ul>
    <p class="p2">AURYXIA contains iron and may cause dark stools, which are considered normal with oral medications containing iron</p>
    <p class="p2">These are not all the side effects of AURYXIA. For more information ask your healthcare provider or pharmacist</p>
    <p class="p2">Tell your healthcare provider if you have any side effect that bothers you or that does not go away.</p>
    <p class="p3">Call your healthcare provider for medical advice about side effects. You may report suspected side effects to Akebia Therapeutics, Inc. at <a class="gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="call 1-844-445-3799" href="tel:1844453799">1-844-445-3799</a> or FDA at <a class="gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="call 1-800-FDA-1088" href="tel:18003321088">1-800-FDA-1088</a> or <a target="_blank" href="https://www.fda.gov/safety/medwatch-fda-safety-information-and-adverse-event-reporting-program" class="gtm-cta medWatchLinkIsi" data-gtm-event-category="Outbound Link" data-gtm-event-action="Click" data-gtm-event-label="www.fda.gov/medwatch" target="_blank" href="https://www.fda.gov/safety/medwatch-fda-safety-information-and-adverse-event-reporting-program">www.fda.gov/medwatch</a></p>
    <p class="p3">How should I store AURYXIA?</p>
   <ul class="ulBlock">
       <li><span>Store AURYXIA between 68 to 77°F (20 to 25°C) </span></li>
       <li><span>Keep AURYXIA tablets dry</span></li>
   </ul>
   <p class="p2"><span class="refBold">For more information see full <a data-gtm-event-category="PDF" data-gtm-event-action="Download" data-gtm-event-label="Prescribing Information" href="/iron-deficiency-anemia/patient/pdf/Auryxia_Full-PI.pdf" target="_blank" class="orangeLink gtm-pdf">Prescribing Information</a> or call <a data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="call 1-844-445-3799" href="tel:18444453799" class="orangeLink gtm-cta">1-844-445-3799</span></a></p>
</div>