<div class="desktopNav gtm-top-nav-parent">
<div class="container-fluid outerNav">
        <div class="container outerContainer ">
            <div class="container innerContainer innerNavTop">
               <!-- <a href="/iron-deficiency-anemia/patient"> <img src="images/logo-2017.png" alt="" class="headerLogo"></a> -->
               <a class="gtm-header" data-gtm-event-category="Header" data-gtm-event-action="Click" data-gtm-event-label="Auryxia Logo" href="/"> <img  src=" <?php echo $rootOneUri . '/iron-deficiency-anemia/patient/images/logo-2017.png' ?>" alt="Auryxia" class="headerLogo gtm-header"></a>
              

                <ul class="nav justify-content-end topNavRight ">
                        <li class="nav-item">
                          <a  class="nav-link active" data-gtm-event-category="Header" href="/iron-deficiency-anemia/patient/important-safety-information">Important Safety Information</a>
                        </li>
                        <div class="borderRight"></div>
                        <li class="nav-item">
                          <a class="nav-link" data-gtm-event-category="PDF" data-gtm-event-action="Download" target="_blank" href="/iron-deficiency-anemia/patient/pdf/Auryxia_Full-PI.pdf">Full Prescribing Information</a>
                        </li>
                        <div class="borderRight"></div>
                        <li class="nav-item">
                          <a class="nav-link" data-gtm-event-category="Header" href="/akebiacares/">AkebiaCares</a>
                        </li>
                        <div class="borderRight"></div>
                        <li class="nav-item">
                          <a class="nav-link" data-gtm-event-category="Header" href="/iron-deficiency-anemia/">For U.S. Healthcare Professionals</a>
                        </li>
                        <div class="borderRight"></div>
                        <li class="nav-item">
                          <a class="nav-link" data-gtm-event-category="Header" href="/hyperphosphatemia/patients/">For Patients On Dialysis</a>
                        </li>
                      </ul>

                      <ul class="nav justify-content-end">
                         
                            <li class="nav-item navItemDisabled navItemResidents">
                              <a class="nav-link disabled" >THIS SITE IS FOR U.S. RESIDENTS ONLY</a>
                            </li>
                          </ul>
            </div>
        </div>
    </div>


    <div class="container-fluid outerNavTwo">
       <div class="container outerContainer navOuterContainer">




                           <div class="submenu">
                           <div class="submenu-item submenu3">
                               <a data-gtm-event-category="Sub Navigation" href="/iron-deficiency-anemia/patient/101">HOW AURYXIA WORKS</a>
                       </div>
                             
                             <div class="submenu-item submenu1">
                               <a data-gtm-event-category="Sub Navigation" href="/iron-deficiency-anemia/patient/Taking-AURYXIA">TAKING AURYXIA</a>
                       </div>
                             <div class="submenu-item submenu2">
                               <a data-gtm-event-category="Sub Navigation" href="/iron-deficiency-anemia/patient/what-to-tell-your-doctor">TALKING&nbsp;TO&nbsp;YOUR&nbsp;DOCTOR</a>
                       </div>
                       
                       </div>


            <div class="container innerContainer innerNavMain">
                    <ul class="nav justify-content-end topNavRight topNavRightBottom">
                            <li class="nav-item homeLink">
                              <a  class="nav-link active gtm-top-nav" href="/iron-deficiency-anemia/patient/">
                                  <img data-gtm-event-category="Top Navigation" data-gtm-event-action="Click" data-gtm-event-label="Home" src="images/homeIcon.png" alt="Home" class="gtm-top-nav homeIcon">
                                  <img data-gtm-event-category="Top Navigation" data-gtm-event-action="Click" data-gtm-event-label="Home" src="images/homeIconOrange.png" alt="Home" class="gtm-top-nav homeIconOrange">

                              </a>
                              <div class="nav-triangle"></div>
                            </li>
                            <div class="borderRight "></div>
                            <li class="nav-item diseaseLink">
                              <a data-gtm-event-category="Top Navigation" class="nav-link" href="/iron-deficiency-anemia/patient/disease-101">DISEASE 101</a>
                              <div class="nav-triangle2"></div>
                            </li>
                            <div class="borderRight"></div>

                          
                            <li class="nav-item auryxiaLink has-submenu auryxiaDesktopLink" id="auryxiaNav" >
                              <a data-gtm-event-category="Top Navigation" class="nav-link auryxiaNavItem navDisable" disabled>AURYXIA 101</a>
                              <div class="nav-triangle3"></div>
                            </li>
        
                            
                            <div class="borderRight"></div>
                            <li class="nav-item supportLink">
                              <a data-gtm-event-category="Top Navigation" class="nav-link" href="/iron-deficiency-anemia/patient/access-support">SUPPORT PROGRAMS</a>
                              <div class="nav-triangle4"></div>
                            </li>
                            <li class="nav-item faqLink">
                                    <a data-gtm-event-category="Top Navigation" class="nav-link " href="/iron-deficiency-anemia/patient/faqs">FAQs</a>
                                    <div class="nav-triangle5"></div>
                                  </li>


                                       <li class="nav-item resourceLink">
                                    <a data-gtm-event-category="Top Navigation" class="nav-link " href="/iron-deficiency-anemia/patient/resources">RESOURCES</a>
                                    <div class="nav-triangle6"></div>
                                  </li>
                          </ul>
            </div>
       </div>
    </div>

    </div>



    <div class="mobileNav">

    <ul class="nav justify-content-center topNavMobile">
  <li class="nav-item">
    <a  data-gtm-event-category="Header" class="nav-link active gtm-header" href="/iron-deficiency-anemia/patient/important-safety-information">Important Safety Information</a>
  </li>
  <div class="borderTopNavMobile"></div>
  <li class="nav-item">
    <a data-gtm-event-category="PDF" data-gtm-event-action="Download" class="nav-link gtm-header" target="_blank" href="/iron-deficiency-anemia/patient/pdf/Auryxia_Full-PI.pdf">Full Prescribing Information</a>
  </li>

</ul>


<div class="bannerResidents">
  <p>THIS SITE IS FOR U.S. RESIDENTS ONLY</p>
</div>


<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-toggler">
<!-- <a href="/iron-deficiency-anemia/patient"> <img src="images/logo-2017.png" alt="" class="headerLogo"></a> -->
<a class="gtm-header" data-gtm-event-category="Header" data-gtm-event-action="Click" data-gtm-event-label="Auryxia Logo" href="/"> <img src=" <?php echo $rootOneUri . '/iron-deficiency-anemia/patient/images/logo-2017.png' ?>" alt="Auryxia" class="headerLogo gtm-header"></a>

  <button class="navBtn" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse mobileNavInner" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">

     <li class="nav-item active">
        <a data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" href="/iron-deficiency-anemia/patient/">Home</a>
      </li>


      <li class="nav-item active">
        <a data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" href="/iron-deficiency-anemia/patient/disease-101">DISEASE 101</a>
      </li>
    
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="/auryxia" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        AURYXIA 101
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/Taking-AURYXIA">TAKING AUURYXIA</a>
        
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/what-to-tell-your-doctor">TALKING TO YOUR DOCTOR</a>
        </div>
      </li> -->

       <li class="nav-item">
        <a data-gtm-event-category="Top Navigation" class="nav-link disabled gtm-top-nav-parent" disabled> AURYXIA 101</a>
      </li>
      <li class="nav-item dropdownTaking">
        <a data-gtm-event-category="Sub Navigation"class="nav-link gtm-sub-nav-parent" href="/iron-deficiency-anemia/patient/101">HOW AURYXIA WORKS</a>
      </li>
       <li class="nav-item dropdownTaking">
        <a data-gtm-event-category="Sub Navigation" class="nav-link gtm-sub-nav-parent" href="/iron-deficiency-anemia/patient/Taking-AURYXIA">TAKING AURYXIA</a>
      </li>

       <li class="nav-item dropdownTaking">
        <a data-gtm-event-category="Sub Navigation" class="nav-link gtm-sub-nav-parent" href="/iron-deficiency-anemia/patient/what-to-tell-your-doctor">TALKING TO YOUR DOCTOR</a>
      </li>

        <li class="nav-item">
        <a data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" href="/iron-deficiency-anemia/patient/access-support">SUPPORT PROGRAMS</a>
      </li>
      <li class="nav-item">
        <a data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" href="/iron-deficiency-anemia/patient/faqs">FAQs</a>
      </li>

        <li class="nav-item">
        <a data-gtm-event-category="Top Navigation" class="nav-link gtm-top-nav-parent" href="/iron-deficiency-anemia/patient/resources">RESOURCES</a>
      </li>


      <li class="nav-item heatlhCareLi">
        <a data-gtm-event-category="Header" class="nav-link gtm-header"  target="_blank" href="/" disbaled>FOR U.S. HEALTHCARE PROFESSIONALS</a>
      </li>

        <li class="nav-item akebiaLi">
        <a data-gtm-event-category="Header" class="nav-link gtm-header" target="_blank" href="/akebiacares/">AkebiaCares</a>
      </li>

         <li class="nav-item hyperphosphatemiaLi">
        <a data-gtm-event-category="Header" class="nav-link gtm-header" target="_blank" href="/hyperphosphatemia/">For Patients On Dialysis</a>
      </li>
    </ul>
    
  </div>
</nav>



    </div>

  