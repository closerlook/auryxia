const gulp = require('gulp');
const babel = require('gulp-babel');
const plumber = require('gulp-plumber');
var sass = require('gulp-sass');

//Babel
gulp.task('es6', ()=>{
    return gulp.src('./src/*.js')
    .pipe(plumber())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('dist'));
});

//Sass
gulp.task('sass', function(){
    return gulp.src('./sass/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./css'));
});

gulp.task('watch',function(){
    gulp.watch("./sass/*.scss", gulp.series('sass'));
    gulp.watch("./src/*.js", gulp.series('es6'));
});