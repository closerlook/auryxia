<!DOCTYPE html>
<html lang="en">
<head>
  <!-- OneTrust Cookies Consent Notice start -->
  <script src="https://cookie-cdn.cookiepro.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="20038081-6e78-46f0-8bde-230c7dcd52c2"></script>
  <script type="text/javascript">
      function OptanonWrapper() { }
  </script>
  <!-- OneTrust Cookies Consent Notice end -->
  <!-- Google Tag Manager --> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NNJLGDX');</script> <!-- End Google Tag Manager -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


              <link rel="image_src" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO" />
    <meta itemprop="image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

    <meta property="og:image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

       <meta name="description" content='Always take AURYXIA® (ferric citrate) exactly as it was prescribed' />

   <meta property="og:title" content='AURYXIA | Taking AURYXIA' />
   <meta property="og:description" content='Always take AURYXIA® (ferric citrate) exactly as it was prescribed' />
   <meta property="og:url" content='https://www.AURYXIA.com/iron-deficiency-anemia/patient/Taking-AURYXIA' />
   <!-- <meta property="og:image" content='/static/sofvel/www-epclusa-com/v3/images/logo.png' /> -->
   <link rel="canonical" href='https://www.AURYXIA.com/iron-deficiency-anemia/patient/Taking-AURYXIA' />
   <link rel="alternate" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/Taking-AURYXIA" hreflang="en-us" />





    <title>AURYXIA | Taking AURYXIA</title>

    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/nav.css" />
    <link rel="stylesheet" type="text/css" href="./css/disease.css" />
    <link rel="stylesheet" type="text/css" href="./css/auryxia.css" />
</head>
<body class="takingAuryxiaPage">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNJLGDX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- Nav Start -->
  <?php include 'includes/nav.php'; ?>
   <!-- Nav End -->

<div class="layoutBody">
    <div class="container outerContainer ">
    <div class="backdropContainer">
        <img src="images/Graphics_Backdrop.png" alt="" class="bgImgFaq">
</div>
        <div class="bannerStripe">
             <p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
             <div class="left-triangle"></div>
             <div class="right-triangle"></div>
        </div>
        <div class="container innerContainer">
        <img src="images/bannerThree.png" alt="Taking Auryxia" class="headerImg">
        <img src="images/auryxia/takingAuryxiaMobileHeader.png" alt="Taking Auryxia" class="bannerMobile">

            <div class="contentInner">
            <span class="patientText-Header">Hypothetical patient portrayals.</span>
              <div class="row">


                            <div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
                            <br class="hiddenBrDesktop">
                                <p class="diseaseP1">Here's what you need to know</p>
                                <p class="diseaseP2">Keep the information below in mind, but remember to always<br class="desktopBr"> take AURYXIA exactly as it was prescribed by your healthcare provider</p>

                              <br>
                                 <div class="takingUl">
                                     <img src="images/auryxia/Icon1.png" alt="">
                                        <ul>

                                        <p class="takingP1">DOSING SCHEDULE</p>


                                            <li><span>
                                                <p class="takingP2">Starting dose:<br class="mobileBrOnly"> 1 tablet 3 times a day</p>
                                            </span></li>

                                              <li><span>
                                                <p class="takingP2">Maximum dose:<br class="mobileBrOnly"> 12 tablets a day</p>
                                            </span></li>

                                              <li><span>
                                                <p class="takingP2">Take AURYXIA with meals </p>
                                            </span></li>

                                              <li><span>
                                                <p class="takingP2">AURYXIA should only be swallowed and not chewed or crushed</p>
                                            </span></li>
                                        </ul>

                                 </div>


                                          <div class="takingUl takingUlTwo">
                                     <img src="images/auryxia/Icon2.png" alt="">
                                        <ul>

                                        <p class="takingP1">IF YOU MISS A DOSE</p>


                                            <li><span>
                                                <p class="takingP2">Do not take extra medicine to make up the missed dose</p>
                                            </span></li>

                                              <li><span>
                                                <p class="takingP2">Take the next dose with your meal, as prescribed</p>

                                            </span></li>

                                            <p class="takingP3">Your doctor may adjust the dose to fit your needs.<br>Always take AURYXIA as it is prescribed.</p>
                                        </ul>

                                 </div>


                                      <div class="takingUl takingUlThree">
                                     <img src="images/auryxia/Icon3.png" alt="">
                                        <ul>

                                        <p class="takingP1">TAKING AURYXIA WITH OTHER MEDICATIONS</p>


                                            <li><span>
                                                <p class="takingP2">Doxycycline—Take at least 1 hour before AURYXIA</p>
                                            </span></li>

                                              <li><span>
                                                <p class="takingP2">Ciprofloxacin—Take at least 2 hours before or after AURYXIA</p>

                                            </span></li>

                                            <p class="takingP3">Be sure to tell your doctor about all the medications you take, including<br> over-the-counter medicine, vitamins, and herbal supplements.</p>
                                        </ul>

                                 </div>


                                        <div class="takingUl takingUlThree">
                                     <img src="images/auryxia/Icon4.png" alt="">
                                        <ul>

                                        <p class="takingP1">YOU MAY NOTICE</p>


                                            <li><span>
                                                <p class="takingP2">Dark stools, which is considered normal with oral medications containing iron</p>
                                            </span></li>

                                              <li><span>
                                                <p class="takingP2">Diarrhea, which was reported as having no disruption to a moderate disruption<br class="desktopBr"> in normal daily activity in the majority of cases during the clinical trials<sup>1-3</sup></p>

                                            </span></li>

                                              <li><span>
                                                <p class="takingP2">Nausea, constipation, vomiting, abdominal pain, and cough</p>

                                            </span></li>

                                            <p class="takingP3">Talk to your doctor if you are taking other medications that may<br class="desktopBr"> affect these symptoms, such as stool softeners or laxatives.</p>
                                        </ul>

                                 </div>


<p class="footnote footNoteTaking"><span class="refBold">References: 1.</span> Fishbane S, Block GA, Loram L, et al. Effects of ferric citrate in patients with nondialysis-dependent CKD and iron deficiency anemia. <em>J Am Soc Nephrol.</em> 2017;28(6):1851-1858. <span class="refBold">2.</span> Data on File 13, Akebia Therapeutics, Inc. <span class="refBold">3.</span> Data on File 27, Akebia Therapeutics, Inc.</p>
<br>
                            </div> <!-- CONTENT HERE END -->





                  <div class="col-md-3 rightContent">

                     <div class="rightBoxOne takingBox1 gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Auryxia 101">
                          <p class="rightP1">AURYXIA 101</p>
                          <p id="rightP2" class="rightP2 takingRightP2">Explore how AURYXIA can help</p>
                      </div>

                      <div class="rightBoxTwo takingBox2 gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="What is Iron Deficiency Anemia">
                      <p class="rightP1">WHAT IS IRON DEFICIENCY ANEMIA?</p>
                          <p id="rightP2" class="rightP2 rightP2Disease">Learn more about your condition</p>
                      </div>


                      <div class="right-vertical-line takingVerticalLine"></div>
                      <p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

                  </div>
              </div>




<div class="isi">
<?php include 'includes/isi.php'; ?>
</div>




            </div>



        </div>
    </div>
</div>


<div class="footerInclude">
<?php include 'includes/footer.php'; ?>
</div>





    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
    crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
    crossorigin="anonymous"></script>
<script>window.jQuery.fn.modal || document.write('<script src="js/bootstrap.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/is-in-viewport/3.0.4/isInViewport.min.js"></script>


    <script src="./dist/script.js"></script>    <script src="./dist/fcbTracker_v8.js"></script>
</body>
</html>
