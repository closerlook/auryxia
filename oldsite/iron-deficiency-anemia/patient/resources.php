<!DOCTYPE html>
<html lang="en">
<head>
  <!-- OneTrust Cookies Consent Notice start -->
  <script src="https://cookie-cdn.cookiepro.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="20038081-6e78-46f0-8bde-230c7dcd52c2"></script>
  <script type="text/javascript">
      function OptanonWrapper() { }
  </script>
  <!-- OneTrust Cookies Consent Notice end -->
  <!-- Google Tag Manager --> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NNJLGDX');</script> <!-- End Google Tag Manager -->
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

 

    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


                  <link rel="image_src" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO" />
    <meta itemprop="image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

    <meta property="og:image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

       <meta name="description" content='With AkebiaCares, access to AURYXIA® (ferric citrate) has never been easier ' />
   
   <meta property="og:title" content='AURYXIA | Resources' />
   <meta property="og:description" content='With AkebiaCares, access to AURYXIA® (ferric citrate) has never been easier ' />
   <meta property="og:url" content='https://www.AURYXIA.com/iron-deficiency-anemia/patient/resources' />
   <!-- <meta property="og:image" content='/static/sofvel/www-epclusa-com/v3/images/logo.png' /> -->
   <link rel="canonical" href='https://www.AURYXIA.com/iron-deficiency-anemia/patient/resources' />
   <link rel="alternate" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/resources" hreflang="en-us" />




    <title>AURYXIA | Resources</title>
    <link rel="stylesheet" type="text/css" href="./css/home.css" />
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/nav.css" />
    <link rel="stylesheet" type="text/css" href="./css/disease.css" />
    <link rel="stylesheet" type="text/css" href="./css/auryxia.css" />
    <link rel="stylesheet" type="text/css" href="./css/support.css" />

</head>
<body class="resourcesPage">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNJLGDX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Nav Start -->
<?php include 'includes/nav.php'; ?>
   <!-- Nav End -->


<div class="layoutBody">
    <div class="container outerContainer ">
    <div class="backdropContainer">
        <img src="images/Graphics_Backdrop.png" alt="" class="bgImgFaq">
        </div>
        <div class="bannerStripe">
             <p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
             <div class="left-triangle"></div>
             <div class="right-triangle"></div>
        </div>
        <div class="container innerContainer">
        <img src="images/bannerEight.jpg" alt="Resources" class="headerImg-Resources">
        <img src="images/mobileResource.png" alt="Resources" class="bannerMobile">

            <div class="contentInner">
            <span class="patientText-Header">Hypothetical patient portrayals.</span>
              
              <div class="row">
                  <div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
                  <br class="hiddenBrDesktop">
                                <p class="diseaseP1">Helpful tools you can download</p>     
                                <br> 
                                <img style="border: 1px solid #005f9f" src="images/IDAPatientTearsheet.png" alt="" class="pdfImg">
                                <p class="diseaseP1" id="resourceP1">AURYXIA at a glance</p>
                                <p class="diseaseP3" id="resourceP2">Use this cheat sheet to get the best start to your treatment journey</p>
                               <a class="gtm-pdf reseroucePDFA" data-gtm-event-category="PDF" data-gtm-event-action="Download" data-gtm-event-label="Auryxia-at-a-glance" target="_blank" href="pdf/IDA_Patient_Tearsheet.pdf"> <button class="resourceBtn gtm-pdf "><img src="images/downloadIcon.png" alt="" class="downloadIcon"><span>DOWNLOAD PDF</span></button></a>





                 </div> <!-- CONTENT HERE END -->

                  <div class="col-md-3 rightContent">

                       <div class="rightBoxOne takingBox1 gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Auryxia 101">
                          <p class="rightP1">AURYXIA 101</p>
                          <p id="rightP2" class="rightP2 takingRightP2">Explore how AURYXIA can help</p>
                      </div>

                      <div class="rightBoxTwo gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Save on the Cost fo Auryxia">
                      <p class="rightP1">SAVE ON THE COST OF AURYXIA</p>
                          <p id="rightP2" class="rightP2 rightP2Disease">Depending on your insurance,<br class="desktopBr"> you can get AURYXIA for free</p>
                      </div>


                      <div class="right-vertical-line"></div>
                      <p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

                  </div>
              </div>




<div class="isi">
<?php include 'includes/isi.php'; ?>
</div>




            </div>

         

        </div>
    </div>
</div>


<div class="footerInclude">
<?php include 'includes/footer.php'; ?>
</div>






    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
    crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
    crossorigin="anonymous"></script>
<script>window.jQuery.fn.modal || document.write('<script src="js/bootstrap.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/is-in-viewport/3.0.4/isInViewport.min.js"></script>


    <script src="./dist/script.js"></script>    <script src="./dist/fcbTracker_v8.js"></script>
</body>
</html>