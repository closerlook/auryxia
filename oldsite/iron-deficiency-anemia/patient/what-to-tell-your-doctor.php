<!DOCTYPE html>
<html lang="en">
<head>
  <!-- OneTrust Cookies Consent Notice start -->
  <script src="https://cookie-cdn.cookiepro.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="20038081-6e78-46f0-8bde-230c7dcd52c2"></script>
  <script type="text/javascript">
      function OptanonWrapper() { }
  </script>
  <!-- OneTrust Cookies Consent Notice end -->
  <!-- Google Tag Manager --> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NNJLGDX');</script> <!-- End Google Tag Manager -->
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">



    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">



                        <link rel="image_src" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO" />
    <meta itemprop="image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

    <meta property="og:image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

       <meta name="description" content='Talk to your doctor before starting AURYXIA® (ferric citrate)' />

   <meta property="og:title" content='AURYXIA | What to tell your doctor' />
   <meta property="og:description" content='Talk to your doctor before starting AURYXIA® (ferric citrate)' />
   <meta property="og:url" content='https://www.AURYXIA.com/iron-deficiency-anemia/patient/what-to-tell-your-doctor' />
   <!-- <meta property="og:image" content='/static/sofvel/www-epclusa-com/v3/images/logo.png' /> -->
   <link rel="canonical" href='https://www.AURYXIA.com/iron-deficiency-anemia/patient/what-to-tell-your-doctor' />
   <link rel="alternate" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/what-to-tell-your-doctor" hreflang="en-us" />




    <title>AURYXIA | What to tell your doctor</title>

    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/nav.css" />
    <link rel="stylesheet" type="text/css" href="./css/disease.css" />
    <link rel="stylesheet" type="text/css" href="./css/auryxia.css" />
</head>
<body class="talkingDoctorPage">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNJLGDX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- Nav Start -->
  <?php include 'includes/nav.php'; ?>
   <!-- Nav End -->

<div class="layoutBody">
    <div class="container outerContainer ">
    <div class="backdropContainer">
        <img src="images/Graphics_Backdrop.png" alt="" class="bgImgFaq">
</div>
        <div class="bannerStripe">
             <p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
             <div class="left-triangle"></div>
             <div class="right-triangle"></div>
        </div>
        <div class="container innerContainer">
        <img src="images/bannerFour.png" alt="Talking To Your Doctor" class="headerImg">
        <img src="images/auryxia/talkingDocMobileHeader.png" alt="Talking To Your Doctor" class="bannerMobile">
            <div class="contentInner">
            <span class="patientText-Header">Hypothetical patient portrayals.</span>
              <div class="row">


                            <div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
                            <br class="hiddenBrDesktop">
                                <p class="diseaseP1">Your healthcare provider is here to help</p>
                                <p class="diseaseP3  docDiseaseP3">Talking to your doctor throughout your journey with AURYXIA can help<br> them personalize a treatment plan for your specific needs</p>



                                <p class="doctorP1">WHAT TO TELL YOUR HEALTHCARE PROVIDER</p>
                                <p class="doctorP2">Tell your healthcare provider about all the medicines you are currently taking.</p>
                                <p class="doctorP3">Before you take AURYXIA, and even after you begin treatment, it’s important to talk to your healthcare provider about all the medications you might be taking. This includes doxycycline and ciprofloxacin (antibiotics) as well as any other prescription and over-the-counter medicines, vitamins, and herbal supplements.</p>

                                <!-- <p class="doctorP2 doctorP2Margin">Be sure to tell your doctor about all the medications you take, including<br> over-the-counter medicine, vitamins, and herbal supplements.</p> -->
<p class="doctorP2MArgin"></p>



                                 <div class="doctorUl">

                                        <ul>

                                        <p class="takingP1">ALSO BE SURE TO TELL YOUR HEALTHCARE PROVIDER IF YOU:</p>


                                            <li><span>
                                                <p class="takingP2">Have had a condition called iron overload (hemochromatosis)</p>
                                            </span></li>

                                              <li><span>
                                                <p class="takingP2">Have any other medical conditions</p>
                                            </span></li>

                                              <li><span>
                                                <p class="takingP2">Are pregnant, plan to become pregnant, or plan to breastfeed</p>
                                            </span></li>


                                        </ul>

                                 </div>



                                    <div class="doctorUl">

                                   <ul>
                                   <p class="takingP4">For the treatment of iron deficiency anemia in adult patients with CKD not on dialysis </p>
                                   <p class="takingP1">THE MOST COMMON SIDE EFFECTS OF AURYXIA ARE: </p>


                                       <li><span>
                                           <p class="takingP2">Diarrhea</p>
                                       </span></li>

                                         <li><span>
                                           <p class="takingP2">Constipation</p>
                                       </span></li>

                                         <li><span>
                                           <p class="takingP2">Nausea</p>
                                       </span></li>
                                       <li><span>
                                           <p class="takingP2">Abdominal pain</p>
                                       </span></li>

                                            <li><span>
                                           <p class="takingP2">High levels of potassium in the blood </p>
                                       </span></li>


                                       <p class="takingP3">AURYXIA contains iron and may cause dark stools, which is considered normal<br> with oral medications containing iron.</p>

                                       <p class="takingP3">Call your healthcare provider for medical advice about side effects. You may report suspected side effects to Akebia Therapeutics, Inc. at <a href="">1&#xfeff;-&#xfeff;844&#xfeff;-&#xfeff;445&#xfeff;-&#xfeff;3799</a> or FDA at <a href="">1&#xfeff;-&#xfeff;800&#xfeff;-&#xfeff;FDA&#xfeff;-&#xfeff;1088</a> or <a href="https://www.fda.gov/safety/medwatch-fda-safety-information-and-adverse-event-reporting-program" target="_blank">www.fda.gov/medwatch</a>.</p>
                                       <p class="takingP3">Tell your healthcare provider if you have children under 6 years old at home.</p>
                                       <p class="takingP5">
                                       AURYXIA contains iron. Keep it away from children to prevent an accidental ingestion of iron and potentially fatal poisoning. Call a poison control center or your healthcare provider if a child swallows AURYXIA.
                                       </p>
                                   </ul>

                            </div>



                                     <div class="doctorUl">

                                   <ul>

                                   <p class="takingP1">ADDITIONAL QUESTIONS TO ASK YOUR HEALTHCARE PROVIDER</p>


                                       <li><span>
                                           <p class="takingP2">How does iron deficiency anemia in CKD affect my existing medical conditions?</p>
                                       </span></li>

                                         <li><span>
                                           <p class="takingP2">Do I have to stop taking or adjust any of my over-the-counter or prescription medicines?</p>
                                       </span></li>

                                         <li><span>
                                           <p class="takingP2">Do I need to limit any of my daily activities?</p>
                                       </span></li>


                                         <li><span>
                                           <p class="takingP2"> Should I change my current diet?</p>
                                       </span></li>


                                   </ul>

                            </div>

<!-- <p class="footnote">Reference: 1. AURYXIA [package insert]. Boston, MA: Akebia Therapeutics, Inc.; 2017. </p> -->


                            </div> <!-- CONTENT HERE END -->





                  <div class="col-md-3 rightContent">

                     <div class="rightBoxOne takingBox1 gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Auryxia 101">
                          <p class="rightP1">AURYXIA 101</p>
                          <p id="rightP2" class="rightP2 takingRightP2">Explore how AURYXIA can help</p>
                      </div>

                      <div class="rightBoxTwo gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Save on the Cost fo Auryxia">
                      <p class="rightP1">SAVE ON THE COST<br class="desktopBr"> OF AURYXIA</p>
                          <p id="rightP2" class="rightP2 rightP2Disease">Depending on your insurance,<br class="desktopBr"> you can get AURYXIA for free</p>
                      </div>


                      <div class="right-vertical-line"></div>
                      <p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

                  </div>
              </div>




<div class="isi">
<?php include 'includes/isi.php'; ?>
</div>




            </div>



        </div>
    </div>
</div>


<div class="footerInclude">
<?php include 'includes/footer.php'; ?>
</div>





    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
    crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
    crossorigin="anonymous"></script>
<script>window.jQuery.fn.modal || document.write('<script src="js/bootstrap.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/is-in-viewport/3.0.4/isInViewport.min.js"></script>


    <script src="./dist/script.js"></script>    <script src="./dist/fcbTracker_v8.js"></script>
</body>
</html>
