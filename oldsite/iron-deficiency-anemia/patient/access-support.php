<!DOCTYPE html>
<html lang="en">
<head>
  <!-- OneTrust Cookies Consent Notice start -->
  <script src="https://cookie-cdn.cookiepro.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="20038081-6e78-46f0-8bde-230c7dcd52c2"></script>
  <script type="text/javascript">
      function OptanonWrapper() { }
  </script>
  <!-- OneTrust Cookies Consent Notice end -->
  <!-- Google Tag Manager --> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NNJLGDX');</script> <!-- End Google Tag Manager -->
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

 

    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


                  <link rel="image_src" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO" />
    <meta itemprop="image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

    <meta property="og:image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

       <meta name="description" content='With AkebiaCares, access to AURYXIA® (ferric citrate) has never been easier ' />
   
   <meta property="og:title" content='AURYXIA | Access Support' />
   <meta property="og:description" content='With AkebiaCares, access to AURYXIA® (ferric citrate) has never been easier ' />
   <meta property="og:url" content='https://www.AURYXIA.com/iron-deficiency-anemia/patient/access-support' />
   <!-- <meta property="og:image" content='/static/sofvel/www-epclusa-com/v3/images/logo.png' /> -->
   <link rel="canonical" href='https://www.AURYXIA.com/iron-deficiency-anemia/patient/access-support' />
   <link rel="alternate" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/access-support" hreflang="en-us" />




    <title>AURYXIA | Access Support</title>

    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/nav.css" />
    <link rel="stylesheet" type="text/css" href="./css/disease.css" />
    <link rel="stylesheet" type="text/css" href="./css/auryxia.css" />
    <link rel="stylesheet" type="text/css" href="./css/support.css" />

</head>
<body class="supportPage">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNJLGDX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Nav Start -->
<?php include 'includes/nav.php'; ?>
   <!-- Nav End -->


<div class="layoutBody">
    <div class="container outerContainer ">
    <div class="backdropContainer">
        <img src="images/Graphics_Backdrop.png" alt="" class="bgImgFaq">
</div>
        <div class="bannerStripe">
             <p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
             <div class="left-triangle"></div>
             <div class="right-triangle"></div>
        </div>
        <div class="container innerContainer">
        <img src="images/bannerFive.jpg" alt="Support Programs" class="headerImg">
        <img src="images/support/IDA_Patient_Website_Mobile_Header_0003_Supportprograms.png" alt="Support Programs" class="bannerMobile">

            <div class="contentInner">
            <span class="patientText-Header">Hypothetical patient portrayals.</span>
              <div class="row">
         
                  <div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
                  <br class="hiddenBrDesktop">
                  <img class="acLogo" src="images/support/acLogo.png" alt="">
                  <div class="headerOverlay">
                  
                  <p class="diseaseP1">Access to AURYXIA has never been easier </p>
                  <p class="diseaseP2 accessDiseaseP2">3 comprehensive programs and dedicated case managers to help you with your access needs </p>
                  </div>
                 


                  <div class="supportUl marginTop">
                  <img src="images/support/supportIconOne.png" alt="">
                        <p class="supportP1">REIMBURSEMENT HELP</p>
                        <div class="supportMobile">
                        <p class="supportP2">One-on-one support for assistance with insurance questions and challenges</p>
                        <p class="supportP3">WHAT DOES IT OFFER?</p>
                        <ul>
                            <li>
                                <p class="supportli"><span>Verify insurance and prescription drug benefits for AURYXIA</span></p>
                            </li>
                            <li>
                                <p class="supportli"><span>Coordinate prior authorizations for AURYXIA</span></p>
                            </li>
                            <li>
                                <p class="supportli"><span>Identify patient eligibility status for Low-income Subsidy Assistance,<br class="desktopBr"> Copay Assistance, or State-funded programs*</span></p>
                            </li>
                        </ul>

                        <p class="supportP3">WHO IS IT FOR?</p>
                            <ul>
                                <li>
                                <p class="supportli"><span>Anyone who has been prescribed AURYXIA</span></p>
                                </li>
                            </ul>     
                            
                            <p class="supportP3">WHAT FORM IS NEEDED?</p>
                            <ul>
                                <li>
                                <p class="supportli"><span>Download the <a href="/iron-deficiency-anemia/patient/pdf/AkebiaCares-Enrollment-Form.pdf" class="supportLink gtm-pdf" target="_blank" data-gtm-event-category="PDF" data-gtm-event-action="Download" data-gtm-event-label="AkebiaCares Application -Reimbursemnet Help">AkebiaCares Application</a><sup>†</sup></span></p>
                                </li>
                            </ul>   

                            </div>

                        </div>




                    <div class="supportUl">
                    <img src="images/support/supportIconTwo.png" alt="">
                            <p class="supportP1">COPAY PROGRAM</p>
                            <div class="supportMobile">
                            <p class="supportP2">Financial assistance for patients with commercial insurance</p>
                            <p class="supportP3">WHAT DOES IT OFFER?</p>
                            <ul>
                                <li>
                                    <p class="supportli"><span>The majority of patients with commercial insurance pay as little as $0 for AURYXIA<sup>&Dagger;</sup></span></p>
                                       <ul>
                                       <li class="subLi"><span id="liDash">-</span> Up to $500 off prescriptions for 90 tablets or less; up to $1,000 off prescriptions for 91-180 tablets; up to $1,500 off prescriptions for 181 tablets or more</li>
                                       </ul>

                                </li>
                            
                            </ul>

                            <p class="supportP3">WHO IS IT FOR?</p>
                                <ul>
                                    <li>
                                    <p class="supportli"><span>Eligible patients with commercial insurance<sup>&sect;</sup></span></p>
                                    </li>
                                </ul>     
                                
                                <p class="supportP3">WHAT FORM IS NEEDED?</p>
                                <ul>
                                    <li>
                                    <p class="supportli"><span>Many pharmacies will apply the coupon when filling AURYXIA.<br class="desktopBr"> Download the <a  target="_blnak" href="pdf/11196703_CopayCoupon_Digital_Reskin_V2.pdf" class="supportLink gtm-pdf" data-gtm-event-category="PDF" data-gtm-event-action="Download" data-gtm-event-label="Copay Coupon">Copay Coupon</a></span></p>
                                    </li>
                                </ul>   
</div>
                        </div>





                        <div class="supportUl">
                        <img src="images/support/supportIconThree.png" class="supportIconThree" alt="">
                            <p class="supportP1 supportP1UlThree">PATIENT ASSISTANCE PROGRAM (PAP)</p>
                            <div class="supportMobile">
                            <p class="supportP2">Free AURYXIA may be available for eligible patients<sup style="position:relative;top:-5px; font-size: 11px;">&parallel;</sup></p>
                            <p class="supportP3">WHAT DOES IT OFFER?</p>
                            <ul>
                                <li>
                                    <p class="supportli"><span>Medication free of charge for eligible patients</span></p>
                                </li>
                            
                            </ul>

                            <p class="supportP3">WHO IS IT FOR?</p>
                                <ul>
                                    <li>
                                    <p class="supportli"><span>For patients who are uninsured, patients who have Medicare Part D insurance but cannot afford their copays, and patients whose insurance does not cover AURYXIA</span></p>
                                    </li>
                                    <!-- <li>
                                    <p class="supportli"><span>Medicare Part D patients who cannot afford copays</span></p>
                                    </li> -->
                                </ul>     
                                
                                <p class="supportP3">WHAT FORM IS NEEDED?</p>
                                <ul>
                                    <li>
                                    <p class="supportli"><span>Download the <a  href="/iron-deficiency-anemia/patient/pdf/AkebiaCares-Enrollment-Form.pdf" class="supportLink gtm-pdf" target="_blank" data-gtm-event-category="PDF" data-gtm-event-action="Download" data-gtm-event-label="AkebiaCares Application -PAP">AkebiaCares Application</a><sup>†</sup></span></p>
                                    </li>
                                </ul>   
</div>
                        </div>



                        <p class="supportFootnote"> <span class="refBold">AkebiaCares does not guarantee coverage and/or reimbursement for all patients.</span></p>

<p class="supportFootnote" style="margin-left: 4px"><span style="margin-left: -5px;"><sup>*</sup></span>Patients may be eligible for Low-Income Subsidy (LIS) if they have an income of ≤150% of the Federal Poverty Level (FPL).</p>
<p class="supportFootnote" style="margin-left: 4px"><span style="margin-left: -5px;"><sup>†</sup></span>Akebia is entitled to request additional documentation for income attestation and medication drug list.</p>
<p class="supportFootnote" style="margin-left: 4px"><span style="margin-left: -5px;"><sup>‡</sup></span>Restrictions may apply. Copay assistance is not valid for prescriptions reimbursed under Medicare, Medicaid, or similar federal or state programs.</p>
<p class="supportFootnote" style="margin-left: 4px"><span style="margin-left: -5px;"><sup>&sect;</sup></span>Copay assistance is not valid for prescriptions reimbursed under Medicare, Medicaid, or similar federal or state programs.</p>
<p class="supportFootnote" style="margin-left: 4px"><span style="margin-left: -5px;"><sup>&parallel;</sup></span>Medicare Part D patients with an annual income of ≤150% of the FPL are eligible for LIS assistance (also called "Extra Help").</p>




                 </div> <!-- CONTENT HERE END -->

                  <div class="col-md-3 rightContent">

                      <div class="rightBoxOne gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="What is Iron Deficiency Anemia">
                          <p class="rightP1">WHAT IS IRON DEFICIENCY ANEMIA?</p>
                          <p id="rightP2" class="rightP2">Learn more about your<br class="desktopBr"> condition</p>
                      </div>

                      <div class="rightBoxTwo gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Save on the Cost fo Auryxia">
                      <p class="rightP1">SAVE ON THE COST OF AURYXIA</p>
                          <p id="rightP2" class="rightP2 rightP2Disease">Depending on your insurance,<br class="desktopBr"> you can get AURYXIA for free</p>
                      </div>


                      <div class="right-vertical-line"></div>
                      <p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

                  </div>
              </div>




<div class="isi">
<?php include 'includes/isi.php'; ?>
</div>




            </div>

         

        </div>
    </div>
</div>


<div class="footerInclude">
<?php include 'includes/footer.php'; ?>
</div>






    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
    crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
    crossorigin="anonymous"></script>
<script>window.jQuery.fn.modal || document.write('<script src="js/bootstrap.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/is-in-viewport/3.0.4/isInViewport.min.js"></script>


    <script src="./dist/script.js"></script>    <script src="./dist/fcbTracker_v8.js"></script>
</body>
</html>