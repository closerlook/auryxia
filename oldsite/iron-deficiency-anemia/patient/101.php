<!DOCTYPE html>
<html lang="en">
<head>
  <!-- OneTrust Cookies Consent Notice start -->
  <script src="https://cookie-cdn.cookiepro.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="20038081-6e78-46f0-8bde-230c7dcd52c2"></script>
  <script type="text/javascript">
      function OptanonWrapper() { }
  </script>
  <!-- OneTrust Cookies Consent Notice end -->
  <!-- Google Tag Manager --> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NNJLGDX');</script> <!-- End Google Tag Manager -->
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

 

    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">



    <link rel="image_src" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO" />
    <meta itemprop="image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

    <meta property="og:image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

       <meta name="description" content='See how AURYXIA can help ' />
   
   <meta property="og:title" content='AURYXIA | AURYXIA 101' />
   <meta property="og:description" content='See how AURYXIA can help ' />
   <meta property="og:url" content='https://www.AURYXIA.com/iron-deficiency-anemia/patient/101' />
   <!-- <meta property="og:image" content='/static/sofvel/www-epclusa-com/v3/images/logo.png' /> -->
   <link rel="canonical" href='https://www.AURYXIA.com/iron-deficiency-anemia/patient/101' />
   <link rel="alternate" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/101" hreflang="en-us" />



    <title>AURYXIA | AURYXIA 101</title>

    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/nav.css" />
    <link rel="stylesheet" type="text/css" href="./css/disease.css" />
    <link rel="stylesheet" type="text/css" href="./css/auryxia.css" />
</head>
<body class="AuryxiaPage">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNJLGDX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- Nav Start -->
  <?php include 'includes/nav.php'; ?>
   <!-- Nav End -->

<div class="layoutBody">
    <div class="container outerContainer ">
    <div class="backdropContainer" id="img101">
        <img src="images/Graphics_Backdrop.png" alt="" class="bgImgFaq" >
</div>
        <div class="bannerStripe">
             <p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
             <div class="left-triangle"></div>
             <div class="right-triangle"></div>
        </div>
        <div class="container innerContainer">
        <img src="images/bannerTwo.png" alt="Auryxia 101" class="headerImg">
        <img src="images/auryxia/auryxiaMobileHeader.png" alt="Auryxia 101" class="bannerMobile">
            <div class="contentInner">
            <span class="patientText-Header">Hypothetical patient portrayals.</span>
              <div class="row">


                            <div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
                            <br class="hiddenBrDesktop">
                                <p class="diseaseP1">The only oral iron designed for you</p>
                                <p class="diseaseP2">AURYXIA<span style="margin-left: -3px">®</span>(ferric citrate) is the only oral iron tablet that’s approved to treat iron deficiency anemia specifically in adults with chronic kidney disease (CKD) not on dialysis</p>

   <p class="diseaseP1 auryxiaFont">HOW AURYXIA (ah-RICKS-ee-ah) WORKS</p>

<div class="aurBox auryxiaBoxOne">
    <div class="aurInnerBoxLeft">
        <p class="boxHeader" style="margin-left: 18px"><span style="margin-left: -20px;">1.</span> CHANGES FORM SO IT'S ABSORBED LIKE IRON FROM YOUR DIET</p>
        <p class="boxTxt">Once swallowed, the iron in AURYXIA changes form so it can be absorbed by the gut, similar to the way your body absorbs iron from food </p>
    </div>

    <div class="aurInnerBoxRight">
        <img class="aurImgOne" src="images/auryxia/aur-IconOne.png" alt="Changes form so it’s absorbed like food ">
    </div>

</div>

<div class="aurBox auryxiaBoxTwo">
     <div class="aurInnerBoxLeft">
         <p class="boxHeader" style="margin-left: 18px"><span style="margin-left: -20px;">2.</span> GETS TRANSPORTED INTO THE BLOODSTREAM</p>
         <p class="boxTxt">The iron is then transported through the gut and into the blood. Once in the bloodstream, the iron changes back to its original form</p>
    </div>

    <div class="aurInnerBoxRight">
    <img class="aurImgTwo" src="images/auryxia/aur-IconTwo.png" alt="Gets transported into the bloodstream ">
    </div>

</div>

<div class="aurBox auryxiaBoxThree">
     <div class="aurInnerBoxLeft">
         <p class="boxHeader">3. COMBINED WITH HEMOGLOBIN</p>
         <p class="boxTxt">Now that iron is back in its original form, the iron can be combined with hemoglobin, the part of the red blood cells that helps carry oxygen</p>
    </div>

    <div class="aurInnerBoxRight">
    <img class="aurImgThree" src="images/auryxia/aur-IconThree.png" alt="Turns into hemoglobin ">
    </div>

</div>


                                <span class="disease-question">
                                    <img src="images/disease/questionMarkIcon.png" class="diseaseQuestionMarkImg" alt="">
                                    <p class="diseaseP1 auryxiaFont">NOT YOUR FIRST TIME TAKING AN ORAL IRON?</p>
                                </span>
                            
                                <p class="diseaseP3 marginTop" id="marginTop">Even if a previous oral iron medicine was unsuccessful, AURYXIA is clinically proven to increase hemoglobin and iron levels </p>

                               


                                  <p class="diseaseP1 auryxiaFont">AURYXIA IMPROVED HEMOGLOBIN AND IRON LEVELS</p>
                                  <!-- <p class="diseaseP1 auryxiaFont">AURYXIA IMPROVED IRON LEVELS WITHOUT THE HELP<br class="desktopBr"> OF OTHER MEDICATIONS</p> -->



                                <p class="diseaseP3 ">AURYXIA increased hemoglobin and iron levels without the use of intravenous (IV) iron or erythropoiesis-stimulating agents</p>

                    
                                

                               

 <p class="diseaseP1 auryxiaFont ">WHEN TAKING AURYXIA, HERE'S SOMETHING TO KEEP IN MIND</p>

    
     <p class="diseaseP3"><span>AURYXIA can not only raise iron levels, but it is also approved to lower phosphorus levels in adults with CKD on dialysis. Your doctor will monitor your levels throughout your time taking AURYXIA</span></p>
  
   


<!-- <p class="footnote "><span class="refBold">References: 1.</span> National Kidney Foundation. K/DOQI clinical practice guidelines for bone metabolism and disease in chronic kidney disease. <em>Am J Kidney Dis.</em> 2003;42(4 Suppl 3):S1-S201. <span class="refBold">2.</span> Supplement to: Fishbane S, Block GA, Loram L, et al. Effects of ferric citrate in patients with nondialysis-dependent CKD and iron deficiency anemia. <em>J Am Soc Nephrol.</em> 2017;28(6):1851-1858.</p> -->




                            </div> <!-- CONTENT HERE END -->





                  <div class="col-md-3 rightContent">

                      <div class="rightBoxOne gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="What is Iron Deficiency Anemia">
                          <p class="rightP1">WHAT IS IRON DEFICIENCY ANEMIA?</p>
                          <p id="rightP2" class="rightP2">Learn more about your<br class="desktopBr"> condition</p>
                      </div>

                      <div class="rightBoxTwo gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Save on the Cost fo Auryxia">
                      <p class="rightP1">SAVE ON THE COST OF AURYXIA</p>
                          <p id="rightP2" class="rightP2 rightP2Disease">Depending on your insurance,<br class="desktopBr"> you can get AURYXIA for free</p>
                      </div>


                      <div class="right-vertical-line"></div>
                      <p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

                  </div>
              </div>




<div class="isi">
<?php include 'includes/isi.php'; ?>
</div>




            </div>

         

        </div>
    </div>
</div>


<div class="footerInclude">
<?php include 'includes/footer.php'; ?>
</div>





    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
    crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
    crossorigin="anonymous"></script>
<script>window.jQuery.fn.modal || document.write('<script src="js/bootstrap.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/is-in-viewport/3.0.4/isInViewport.min.js"></script>


    <script src="./dist/script.js"></script>    <script src="./dist/fcbTracker_v8.js"></script>
</body>
</html>