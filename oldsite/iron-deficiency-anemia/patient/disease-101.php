<!DOCTYPE html>
<html lang="en">
<head>
  <!-- OneTrust Cookies Consent Notice start -->
  <script src="https://cookie-cdn.cookiepro.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="20038081-6e78-46f0-8bde-230c7dcd52c2"></script>
  <script type="text/javascript">
      function OptanonWrapper() { }
  </script>
  <!-- OneTrust Cookies Consent Notice end -->
  <!-- Google Tag Manager --> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NNJLGDX');</script> <!-- End Google Tag Manager -->
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

 

    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">




    <link rel="image_src" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO" />
    <meta itemprop="image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

    <meta property="og:image" content="https://www.AURYXIA.com/iron-deficiency-anemia/patient/AURYXIA_LOGO">

       <meta name="description" content='What is iron deficiency anemia in CKD? ' />
   
   <meta property="og:title" content='AURYXIA | Disease 101' />
   <meta property="og:description" content='What is iron deficiency anemia in CKD? ' />
   <meta property="og:url" content='https://www.AURYXIA.com/iron-deficiency-anemia/patient/disease-101' />
   <!-- <meta property="og:image" content='/static/sofvel/www-epclusa-com/v3/images/logo.png' /> -->
   <link rel="canonical" href='https://www.AURYXIA.com/iron-deficiency-anemia/patient/disease-101' />
   <link rel="alternate" href="https://www.AURYXIA.com/iron-deficiency-anemia/patient/disease-101" hreflang="en-us" />






    <title>AURYXIA | Disease 101</title>

    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/nav.css" />
    <link rel="stylesheet" type="text/css" href="./css/disease.css" />
</head>
<body class="diseasePage">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNJLGDX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- Nav Start -->
  <?php include 'includes/nav.php'; ?>
   <!-- Nav End -->

<div class="layoutBody">
    <div class="container outerContainer ">
    <div class="backdropContainer">
        <img src="images/Graphics_Backdrop.png" alt="Disease 101" class="bgImgFaq">
</div>
        <div class="bannerStripe">
             <p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
             <div class="left-triangle"></div>
             <div class="right-triangle"></div>
        </div>
        <div class="container innerContainer">
        <img src="images/bannerOne.jpg" alt="Disease 101" class="headerImg">
        <!-- <span class="patientText-Header">Hypothetical patient portrayals.</span> -->

        <img src="images/disease/mobileDiseaseHeader.png" alt="" class="bannerMobile">
            <div class="contentInner">
            <span class="patientText-Header">Hypothetical patient portrayals.</span>
              <div class="row">


                            <div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
<br class="hiddenBrDesktop">
                                <p class="diseaseP1 header-diseaseP1">What is iron deficiency anemia in<br class="desktopBr"> chronic kidney disease (CKD)?</p>
                                <p class="diseaseP2">A common condition that occurs when your body doesn't have enough iron to produce enough healthy red blood cells due to reduced kidney function</p>

                                <span class="disease-question">
                                    <img src="images/disease/questionMarkIcon.png" alt="">
                                    <p class="diseaseP1">WHAT IS CKD?</p>
                                </span>
                            
                                <p class="diseaseP3 marginTop">To keep you healthy, your kidneys are responsible for a lot of important functions such as balancing fluids, regulating blood pressure, producing certain hormones, and removing waste products from the blood. </p>

                                  <p class="diseaseP3">CKD is defined as a gradual loss of kidney function, which is measured by your estimated glomerular filtration rate (eGFR). CKD is most often caused by diabetes and high blood pressure, but may also be caused by other factors.</p>

                                    <p class="diseaseP3"><span class="p3-SpanOne">Estimated glomerular filtration rate, or eGFR</span> <span class="p3-spanTwo">|</span> A number based on your blood test for creatinine. It tells how well your kidneys are working.</p>


                                <span class="disease-question ironIDA">
                                    <img src="images/disease/questionMarkIcon.png" alt="">
                                    <p class="diseaseP1">WHAT IS IRON DEFICIENCY ANEMIA?</p>
                                </span>



                                <p class="diseaseP3 marginTop">Common in patients with CKD, anemia is a condition that happens when there aren't enough healthy red blood cells to carry the oxygen your body needs. To make healthy red blood cells, your body needs iron and erythropoietin (a hormone made by the kidneys).</p>

                                <p class="diseaseP3">Anemia in patients with CKD is often caused by iron deficiency, or not enough iron. This lack of iron may be caused by not having enough iron in your diet, not absorbing enough iron from your diet based on the foods you eat, or losing iron from blood loss.</p>
                                

                                 <span class="disease-question symptomsQ">
                                    <img src="images/disease/questionMarkIcon.png" alt="">
                                    <p class="diseaseP1">WHAT ARE SOME COMMON SYMPTOMS?</p>
                                </span>

                                <img class="marginTop desktopImg" src="images/disease/Common_symptoms.png" alt="Some common symptoms of iron deficiency anemia in CKD">
                                <img src="images/disease/common-symptoms-mobile.png" alt="Some common symptoms of iron deficiency anemia in CKD" class="mobileDiseaseImg">

                                <p class="footnote">Please note, AURYXIA is not indicated to treat these symptoms. </p>

                                <p class="footnote">Please note, many of the symptoms may have multiple causes beyond iron deficiency anemia. </p>
                                <p class="footnote">Be sure to talk to your doctor if you are experiencing any symptoms so they can properly diagnose you. </p>
<br>
                            </div> <!-- CONTENT HERE END -->





                  <div class="col-md-3 rightContent">

                      <div class="rightBoxOne gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Auryxia 101">
                          <p class="rightP1">AURYXIA 101</p>
                          <p id="rightP2" class="rightP2">Explore how AURYXIA<br class="desktopBr"> can help</p>
                      </div>

                      <div class="rightBoxTwo gtm-cta rightBoxDiseaseTwo" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Taking Auryxia">
                      <p class="rightP1">TAKING AURYXIA</p>
                          <p id="rightP2" class="rightP2 rightP2Disease">Learn how and when you<br class="desktopBr"> should take AURYXIA</p>
                      </div>


                      <div class="right-vertical-line diseaseVerticalLine"></div>
                      <p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

                  </div>
              </div>




<div class="isi">
<?php include 'includes/isi.php'; ?>
</div>




            </div>

         

        </div>
    </div>
</div>


<div class="footerInclude">
<?php include 'includes/footer.php'; ?>
</div>





    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
    crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
    crossorigin="anonymous"></script>
<script>window.jQuery.fn.modal || document.write('<script src="js/bootstrap.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/is-in-viewport/3.0.4/isInViewport.min.js"></script>


    <script src="./dist/script.js"></script>    <script src="./dist/fcbTracker_v8.js"></script>
</body>
</html>