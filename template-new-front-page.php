<?php
/**
 * Template Name: Root Page
 */
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8"><![endif]-->
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, maximum-scale=1.0 , initial-scale=1.0" />
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<section id='page-gateway'>
	<header id='header'>
		<section class='gateway-top container'>
			<div class='row'>
				<div class='col-md-12 col-centered'>
					<div class='header-links<?php if (!is_front_page()) {echo " isiHeaderLinks";}?>'>
						<a href='#isi' data-element="default" data-category="Header" data-action="Click" data-label="ISI">Important Safety Information</a>
						<span class='sep'>|</span>
						<a href='/wp-content/uploads/Auryxia_PI.pdf' target='_blank' class='pi-link' data-element="default" data-category="Header" data-action="Click" data-label="Full PI">Full Prescribing Information</a>
						<span class='sep desktop'>|</span>
						<a href='/akebiacares' class='nav-copy keyxNav' data-element="default" data-category="Header" data-action="Click" data-label="Keryx Patient Plus">AkebiaCares</a>
					</div>
					<div class='intent'<?php if (!is_front_page()) {echo ' id="intentIsi"';}?>>
						THIS SITE IS FOR U.S. RESIDENTS ONLY
					</div>
				</div>
			</div>
			<?php if (!is_front_page()) { ?><a href="/"> <img id='Isilogo' src='<?php echo get_template_directory_uri(); ?>/assets/img/Auryxia-logo.svg' alt='Auryxia'></a><?php } ?>
		</section>
		<?php if (is_front_page()) { get_template_part('template-parts/content', 'home-page');} else { echo "<br><br>"; } ?>

	</header>
	<!-- HTML includes -->
	<section id='isi-content' class="getaway-isi<?php if (!is_front_page()) {echo " isiSafety";}?>">
		<?php get_template_part('template-parts/content', 'isi-home'); ?>
	</section>

	<div class="footerBg">
		<div class="container-fluid footerContainer footerDesktop gtm-footer">
			<div class="container innerContainer">
				<a class="gtm-footer" target="_blank"  href="https://akebia.com/"><img class="footerImgLogo" src="<?php echo get_template_directory_uri(); ?>/assets/img/Akebia_Ribbon_Logo_FullColor_RGB.png" alt=""></a>
				<ul class="nav justify-content-center footerLinks">
					<li class="nav-item">
						<a  class="nav-link active gtm-footer" data-gtm-event-category="Footer" data-gtm-event-action="Click" data-gtm-event-label="Contact Us" target="_blank" href="https://akebia.com/contact/">CONTACT US</a>
					</li>
					<div class="borderRightFooterDesktop"></div>
					<li class="nav-item">
						<a class="nav-link gtm-footer" data-gtm-event-category="Footer" target="_blank" href="https://akebia.com/legal/">TERMS AND CONDITIONS</a>
					</li>
					<div class="borderRightFooterDesktop"></div>
					<li class="nav-item">
						<a class="nav-link active gtm-footer" data-gtm-event-category="Footer" target="_blank" href="https://akebia.com/legal/privacy-policy.aspx">PRIVACY POLICY</a>
					</li>
					<div class="borderRightFooterDesktop"></div>
					<li class="nav-item">
						<a class="nav-link gtm-footer" data-gtm-event-category="Footer" target="_blank" href="https://akebia.com/">AKEBIA.COM</a>
					</li>

				</ul>
				<div style="font-size: 10.5px; color: #5a6870; text-align: center; margin-top: 30px;">&copy;2019 Akebia Therapeutics, Inc.&emsp;&emsp;&emsp;&emsp;&emsp;PP-AUR-US-0892&emsp;&emsp;09/19</div>
			</div>
		</div>

		<div class="footerMobile">
			<img class="footerImgLogo" src="<?php echo get_template_directory_uri(); ?>/assets/img/Akebia_Ribbon_Logo_FullColor_RGB.png" alt="">

			<ul class="nav justify-content-left mobileCopyRight">
				<li class="nav-item">
					<p>&copy;2019 Akebia Therapeutics, Inc.</p>
				</li>
				<div class="marginFooter"></div>
				<li class="nav-item ">
					<p class="mobileFootCode">PP-AUR-US-0892&emsp;&emsp;09/19</p>
				</li>
			</ul>
			<br>
			<div class="row text-center">
				<div class="col-md-12">
				</div>
			</div>
			<div class="homeFooterMobile">
				<ul class="nav justify-content-left mobileTopFooter mobileFootUl">
					<li class="nav-item">
						<a class="nav-link active gtm-footer" data-gtm-event-category="Footer" target="_blank" href="https://akebia.com/contact/">CONTACT US</a>
					</li>
					<div class="borderRightFooter"></div>
					<li class="nav-item">
						<a class="nav-link gtm-footer" data-gtm-event-category="Footer"  target="_blank" href="https://akebia.com/legal/">TERMS AND CONDITIONS</a>
					</li>

				</ul>
				<ul class="nav justify-content-left mobileBottomFooter mobileFootUl">
					<li class="nav-item">
						<a class="nav-link active gtm-footer" data-gtm-event-category="Footer"  target="_blank" href="https://akebia.com/legal/privacy-policy.aspx">PRIVACY POLICY</a>
					</li>
					<div class="borderRightFooter"></div>
					<li class="nav-item">
						<a class="nav-link gtm-footer" data-gtm-event-category="Footer"  target="_blank"  href="https://akebia.com/">AKEBIA.COM</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<?php wp_footer(); ?>

</body>
</html>
