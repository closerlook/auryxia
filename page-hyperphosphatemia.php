<?php get_header(); if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php include_once(TEMPLATEPATH . '/includes/sub-nav.php'); ?>

	<div class="grid-12 interior cf">
		<?php include_once(TEMPLATEPATH . '/includes/post-titles.php'); ?>
		<div class="clear"></div>

		<div class="grid-9 left copy">
			<?php
			the_content();
			$study = get_field('study_design');
			if ($study){
				echo '<div class="study-design">';
				echo '<span>Study Design</span>' . $study;
				echo '</div>';
			}

			$formulation = get_field('formulation_text', 'option');
			if ($formulation){
				echo '<br /><div class="formulation">';
				echo '<span>Formulation</span>' . $formulation . '</div>';
			}
			?>
		</div>

		<?php include_once(TEMPLATEPATH . '/includes/sidebar-callouts.php'); ?>
	</div><!-- Single Page -->

	<?php
	include_once(TEMPLATEPATH . '/includes/isi-and-references.php');
endwhile; else : endif; get_footer();
?>
