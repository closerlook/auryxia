<?php
/**
 * Template Name: Iron Deficieny Anemia Sub Page
 */

// @TODO fix meta descriptions for each page

$body_id = '';

if ( is_front_page() ) {
	$body_id = 'id="home"';
} else {
	$body_id = is_page() || is_single() ? 'id="' . get_post_field( 'post_name' ) . '"' : '';
}

?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, maximum-scale=1.0 , initial-scale=1.0">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php echo $body_id; ?> <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<section>
		<header id="header"><?php get_template_part("template-parts/header", "ida"); ?></header>
		<section id="navigation"><?php get_template_part("template-parts/nav", "ida"); ?></section>
		<?php if (!is_page(array('iron-deficiency-anemia','important-safety-information'))) { ?>
			<section id='indication-banner' class='container col-centered'>
				<div id='indication-flag-left' >
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="50" viewBox="0 0 20 50">
						<polyline points="0 24 20 24 20 50" fill="#69c"/>
						<rect width="20" height="24" fill="#005F9E"/>
					</svg>
				</div>
				<div id='indication-flag-right' >
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="50" viewBox="0 0 20 50">
						<rect width="20" height="24" fill="#005F9E"/>
						<polyline points="20 24 0 24 0 50" fill="#69c"/>
					</svg>
				</div>
				<h2>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></h2>
			</section>
		<?php } ?>
		<?php if (is_page(array('sign-up', 'thank-you', 'error'))) { ?>
			<section id='sign-up-indication-banner'>
				<div class="sign-up-banner ida">
					<div class="container">
						<div class="col-md-12 col-centered">
							<div id="warning-container" class="col-md-10">
								<div class="row no-gutters">
									<div class="col">
										<strong>Iron Deficiency Anemia</strong> CKD Not On Dialysis
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		<?php } ?>
		<?php

		//		get_template_part('template-parts/nav','akebiacares');

		// @TODO Put content here
		$the_slug = $post->post_name;
		if ( is_page('iron-deficiency-anemia') ) { $the_slug = 'index'; }

		get_template_part("template-parts/content-ida", $the_slug);


		?>
		<footer id="footer">
			<?php get_template_part('template-parts/footer', 'ida'); ?>
		</footer>
	</section>

<?php wp_footer(); ?>

<?php

  $theme_version = wp_get_theme()->get( 'Version' );
  // Switch between the raw ES6 javascript & the compiled javascript based on
  // the environment we're running in
  if (auryxia_use_raw_javascript()) { ?>

  <!--
	We're running locally (triggered by HTTP_HOST "akebia-web.test"), which means:
	  - The raw ES6 javascript is referenced instead of the bundled main.min.js file
	  - The QA API is used instead of the production API
	  - A random number is tacked onto the main.js reference to try and avoid caching
  -->
  <script type="module">
	import { Auryxia } from "<?php echo get_template_directory_uri() . '/js/main.js?r=' . random_int(0, 100000); ?>";

	new Auryxia({"version":<?php echo json_encode($theme_version); ?>, "buildDate":<?php echo json_encode(date('c')) ?>, enableGridOverlay: true});
  </script>

<?php } else { ?>

  <script type="text/javascript">
	new Auryxia({"version":<?php echo json_encode($theme_version); ?>, "buildDate":<?php echo json_encode(date('c')) ?>});
  </script>

<?php } ?>

</body>
</html>
