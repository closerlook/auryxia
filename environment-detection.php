<?php

// This will switch between the production & QA API; generally this should be left as "false"
$forceProductionAPI = false;

// This will switch between the compiled vs. raw ES6 javascript; generally this should be left as "false"
$forceProductionJavascript = false;

function auryxia_use_production_api() {
	global $forceProductionAPI;
	static $isProductionMode;

	if ($forceProductionAPI)
		return true;

	if (isset($isProductionMode))
		return $isProductionMode;

	$hostname = strtolower(getenv('HTTP_HOST'));
	switch ($hostname) {
		case 'auryxia.com':
			$isProductionMode = true;
			break;
		
		case 'www.auryxia.com':
			$isProductionMode = true;
			break;
		
		default:
			$isProductionMode = false;
			break;
	}

	return $isProductionMode;
}

function auryxia_use_raw_javascript() {
	global $forceProductionJavascript;
	static $isLocalMode;

	if ($forceProductionJavascript)
		return false;

	if (isset($isLocalMode))
		return $isLocalMode;

	$isLocalMode = strtolower(getenv('HTTP_HOST')) == 'akebia-web.test';
	return $isLocalMode;
}
