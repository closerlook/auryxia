<?php
/**
 * Template Name: Akebia Cares Sub-Page
 */
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8"><![endif]-->
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, maximum-scale=1.0 , initial-scale=1.0" />
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header>
	<div class="grid cf">
		<a href="/" data-element="default" data-category="Header" data-action="Click" data-label="Logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/auryxia-logo-2.png" id="logo" class="desktop"></a>
		<div class="header-links cf">
			<a href="/important-safety-information" class="nom" data-element="default" data-category="Header" data-action="Click" data-label="ISI">Important Safety Information</a>
			<span class="sep">|</span>
			<a href="/wp-content/uploads/Auryxia_PI.pdf" target="_blank" class="pi-link" data-element="default" data-category="Header" data-action="Click" data-label="Full PI">Full Prescribing Information</a>
			<span class="sep desktop">|</span>
			<a href="/" class="desktop nav-copy" data-element="default" data-category="Header" data-action="Click" data-label="For Patients">For Patients</a>
			<span class="sep desktop">|</span>
			<a href="https://www.auryxiahcp.com" class="desktop nav-copy" data-element="default" data-category="Header" data-action="Click" data-label="For US Healthcare Professionals">For U.S. Healthcare Professionals</a>
			<div class="disclaimer">This site is for U.S. residents only</div>
		</div>
		<a href="/" data-element="default" data-category="Header" data-action="Click" data-label="Logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/logo-mobile.png" id="logo" class="mobile"></a>
		<a href="#" class="nt mobile" id="mobile-menu">Menu</a>
	</div>
</header>

<?php
get_template_part('template-parts/nav','akebiacares');

// @TODO Put content here
$the_slug = $post->post_name;
if ( is_page('akebiacares') ) { $the_slug = 'index'; }

get_template_part("template-parts/content-akebiacares", $the_slug);

get_template_part('template-parts/footer','akebiacares') ?>
</body>
</html>
