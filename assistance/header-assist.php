<link rel="stylesheet" href="/hyperphosphatemia/wp-content/themes/keryx/assets/css/assistance.css">
<div id="assist-lines">

  <header>
    <div class="grid cf">
    
      <h1>
        <a href="<?php echo home_url(); ?>/patient-services" data-element="default" data-category="Header" data-action="Click" data-label="Logo">
          <img id="logo" src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>" />
        </a>
      </h1>
      
      <div class="grid-10 right">
        
        <div class="header-links">
          <a href="/hyperphosphatemia/important-safety-information/"  target="_blank" class="nom" data-element="default" data-category="Header" data-action="Click" data-label="ISI">Important Safety Information</a>
          <span class="sep desktop">|</span>    
          <a href="/hyperphosphatemia/prescribing-information/" target="_blank" class="pi-link" data-element="default" data-category="Header" data-action="Click" data-label="Full PI">Full Prescribing Information</a>
          <span class="sep desktop">|</span>
          <a href="/hyperphosphatemia/patients" class="desktop nav-copy" data-element="default" data-category="Header" data-action="Click" data-label="For Patients">For Patients</a>
          <span class="sep desktop">|</span> 
          <a href="/" class="desktop nav-copy" data-element="default" data-category="Header" data-action="Click" data-label="For US Healthcare Professionals">For U.S. Healthcare Professionals</a>
        </div>
        
        <div class="intent">
          ThIs site is for U.S. residents only
        </div>
        
        <div class="clear"></div>
        <a href="#" class="nt mobile" id="mobile-menu">Menu</a>
        <nav>
          <?php wp_nav_menu(array('menu' => 'Patient Assistance')); ?>
        </nav>
        
      </div><!-- Grid 10 -->
     
    </div><!-- Grid -->
  </header>


