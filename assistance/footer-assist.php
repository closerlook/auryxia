  <footer class="cf">
    <div class="grid-12 cf">
    
    
      <div class="grid-7 left">
       <a href="<?php the_field('footer_logo_link', 'option'); ?>" target="_blank" data-element="default" data-category=“Footer” data-action="Click" data-label="Keryx Logo">
          <img class="grid-2"
            src="<?php the_field('footer_logo', 'option'); ?>" 
            alt="<?php the_field('footer_brand_name', 'option'); ?>" 
          />
        </a>
        
        <div class="legal">
          <?php the_field('assistance_copyright', 'option'); ?>
          <div class="footer-spacer"></div>
          <?php the_field('assistance_legal_code', 'option'); ?>
          <div class="footer-spacer"></div>
          <?php the_field('assistance_legal_date', 'option'); ?>
        </div>
      </div>
      
      
      <div id="footer-info" class="grid-5 right footer-links">
        <?php //wp_nav_menu(array('menu' => 'Footer Links','container_class' => 'false')); ?>
        <div class="false">
          <ul id="menu-footer-links" class="menu">
            <li>
              <a target="_blank" href="http://keryx.com/about/contact-us/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Contact Us">Contact Us</a>
            </li>
            <li>
              <a target="_blank" href="http://keryx.com/terms-conditions/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Terms & Conditions">Terms and Conditions</a>
            </li>
            <li>
              <a target="_blank" href="http://keryx.com" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Keryx.Com">KERYX.COM</a>
            </li>
          </ul>
        </div>
      </div>
    
    </div>
  </footer>


</div><!-- Assist Lines -->