<?php /* Template Name: PDF */
  
  $pdfURl = get_field('prescribing_information_pdf', 'option'); 
  
  // Coupon
  if (is_page(632)){
    $pdfURl = get_field('coupon_pdf', 'option'); 
  }
  
  // Application Form
  if (is_page(1471)){
    $pdfURl = get_field('patient_plus_app', 'option'); 
  }
  
  wp_redirect($pdfURl); exit; 
  
?>