<div class="content content-1">
	<div class="grid interior cf">
		<div class="post-titles">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-resources.jpg" class="header-desktop" alt="Resources" title="Resources">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-resources-mobile.jpg" class="header-mobile" alt="Resources" title="Resources" style="width: 100%">
		</div>
		<div class="grid-12 interior resources-interior cf">
			<div class="copy"><!-- grid-9 left  -->
				<h2>Getting Access to Auryxia</h2>

				<div class="cf">
					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/Auryxia_Coverage_FC.jpg" alt="AURYXIA Coverage Flashcard" title="AURYXIA Coverage Flashcard">
						</div>
						<div class="resource-description">
							<div class="resource-title">AURYXIA Coverage Flashcard</div>
							<p>Download below to determine how to get access to AURYXIA by insurance coverage type</p>
							<p>To access our electronic prescription benefits verification portal, click <a href="http://www.AuryxiaRxBenefits.com" class="text-link" target="_blank">HERE</a></p>
							<a href="/wp-content/uploads/Auryxia_Coverage_FC.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>

					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares-CopayCoupon_Digital_Reskin.jpg" alt="Commercial Copay Coupon" title="Commercial Copay Coupon">
						</div>
						<div class="resource-description">
							<div class="resource-title">Commercial Copay Coupon</div>
							<p>Eligible patients with commercial insurance can pay as little as $0 for AURYXIA</p>
							<a href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>
				</div>

				<div class="cf">
					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares_Enrollment_Form_Update.jpg" alt="AkebiaCares Enrollment Form (English/Spanish)" title="AkebiaCares Enrollment Form (English/Spanish)">
						</div>
						<div class="resource-description">
							<div class="resource-title">AkebiaCares Enrollment Form (English/Spanish)</div>
							<p>Download and fill out this form to get access to a dedicated case manager</p>
							<a href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" class="download-link" target="_blank">Download PDF (English)</a>
							<a href="/wp-content/uploads/AkebiaCares-Enrollment-Form-Sp.pdf" class="download-link" target="_blank">Download PDF (Spanish)</a>
						</div>
					</div>

					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares-Enrollment-Form-Tips.jpg" alt="AkebiaCares Enrollment Form Tips" title="AkebiaCares Enrollment Form Tips">
						</div>
						<div class="resource-description">
							<div class="resource-title">AkebiaCares Enrollment Form Tips</div>
							<p>Helpful tips to consider when filling out the AkebiaCares Enrollment Form</p>
							<a href="/wp-content/uploads/AkebiaCares_Enrollment_Form_TIPS.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>
				</div>

				<div class="cf">
					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares-Re-Enrollment-Form.jpg" alt="Guide to How to Apply for Extra Help/Low Income Subsidy (LIS) Guide" title="Guide to How to Apply for Extra Help/Low Income Subsidy (LIS) Guide">
						</div>
						<div class="resource-description">
							<div class="resource-title">Patient Assistance Program <span class="nobreak">Re-enrollment</span> Form for AURYXIA</div>
							<p>Re-enrollment form for patients who were previously enrolled in AkebiaCares</p>
							<a href="/wp-content/uploads/AkebiaCares_Re-Enrollment_Form.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>
					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares-Affordability-FC.jpg" alt="Discover resources available for every type of insurance" title="Discover resources available for every type of insurance">
						</div>
						<div class="resource-description">
							<div class="resource-title">AURYXIA Affordability Brochure</div>
							<p>Discover resources available for every type of insurance</p>
							<a href="/wp-content/uploads/AkebiaCares-Affordability-FC.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>
				</div>
				<div class="cf">
					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares-Pharmacy-Options-LB.jpg" alt="Find pharmacies that best fit your patients’ needs and get connected to a pharmacy locator" title="Find pharmacies that best fit your patients’ needs and get connected to a pharmacy locator">
						</div>
						<div class="resource-description">
							<div class="resource-title">Pharmacy Locator</div>
							<p>Find pharmacies that best fit your patients’ needs and get connected to a specialty pharmacy locator </p>
							<a href="/wp-content/uploads/AkebiaCares-Pharmacy-Locator-FC.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>
					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares-Refer-Specialty-Pharmacy.jpg" alt="Refer a prescription to a specialty pharmacy form" title="Refer a prescription to a specialty pharmacy form">
						</div>
						<div class="resource-description">
							<div class="resource-title">Refer a prescription to a specialty pharmacy form</div>
							<p>If you and your patient decided to use a specialty pharmacy, use this form to refer the prescription.</p>
							<a href="/wp-content/uploads/AkebiaCares-Refer-Specialty-Pharmacy.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>
				</div>

				<h2>Help for Patients with Medicare</h2>

				<div class="cf">
					<div class="resource-container cf coupon">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares-RFPL-Income-Guide.jpg" alt="Does Your Patient Qualify for Additional Financial Assistance From Medicare?" title="Does Your Patient Qualify for Additional Financial Assistance From Medicare?">
						</div>
						<div class="resource-description">
							<div class="resource-title">Does Your Patient Qualify for Additional Financial Assistance From Medicare?</div>
							<p>Review the current Federal Poverty Level guidelines to determine if your patient’s household size and income may qualify them for Extra Help/LIS</p>
							<a href="/wp-content/uploads/AkebiaCares-FPL-Leave-Behind.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>

					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares-LIS_Patient_Guide.jpg" alt="Patient-friendly Guide on How to Apply for Extra Help/Low Income Subsidy (LIS) Guide (English/Spanish)" title="Patient-friendly Guide on How to Apply for Extra Help/Low Income Subsidy (LIS) Guide (English/Spanish)">
						</div>
						<div class="resource-description">
							<div class="resource-title">Patient-Friendly Guide on How to Apply for Extra Help/Low-Income Subsidy (LIS) Guide (English/Spanish)</div>
							<p>An easy-to-follow guide for patients who need Extra Help/LIS through Medicare Part D</p>
							<a href="/wp-content/uploads/AkebiaCares_LIS_Guide_Patient.pdf" class="download-link" target="_blank">Download PDF (English)</a>
							<a href="/wp-content/uploads/AkebiaCares_LIS_Guide_Patient-Sp.pdf" class="download-link" target="_blank">Download PDF (Spanish)</a>
						</div>
					</div>
				</div>

				<div class="cf">
					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares_How_to_Apply_for_Extra_Help_(LIS)_Guide_AFD.jpg" alt="Guide to How to Apply for Extra Help/Low Income Subsidy (LIS) Guide" title="Guide to How to Apply for Extra Help/Low Income Subsidy (LIS) Guide">
						</div>
						<div class="resource-description">
							<div class="resource-title">How to Apply for Extra Help/Low-Income Subsidy (LIS) Guide</div>
							<p>A detailed guide with information on Extra Help/LIS through Medicare Part D</p>
							<a href="/wp-content/uploads/AkebiaCares_How_to_Apply_for_Extra_Help_(LIS)_Guide_AFD.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>
				</div>

				<h2>Prior Authorization and Appeal Guidelines</h2>

				<div class="cf">
					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares-Prior-Authorizations-PA-Tips_Guidelines.jpg" alt="Prior Authorizations (PA) Tips/Guidelines" title="Prior Authorizations (PA) Tips/Guidelines">
						</div>
						<div class="resource-description">
							<div class="resource-title">Prior Authorizations (PA) Tips/Guidelines</div>
							<p>What to know when submitting a Prior Authorization</p>
							<a href="/wp-content/uploads/AkebiaCares_PA_Tips_Guide.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>

					<div class="resource-container cf">
						<div class="resource">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/thumbs/AkebiaCares-Prior-Authorizations.jpg" alt="Appeal Letter Tips/Guidelines" title="Appeal Letter Tips/Guidelines">
						</div>
						<div class="resource-description">
							<div class="resource-title">Appeal Letter Tips/Guidelines</div>
							<p>Helpful tips to write and submit an Appeal Letter</p>
							<a href="/wp-content/uploads/AkebiaCares_Prior_Authrztns_PA_Appeal.pdf" class="download-link" target="_blank">Download PDF</a>
						</div>
					</div>
				</div>

			</div>
		</div>
		<?php get_template_part('template-parts/content','isi-akebiacares'); ?>
	</div>
</div><!-- Content -->
