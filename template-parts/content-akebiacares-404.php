<?php ?>
<div class="content content-1">
	<div class="grid interior cf">
		<div class="post-titles"><img class="header-desktop" title="404" src="/wp-content/themes/keryx/oldsite/akebiacares/images/header-404.jpg" alt="404" /></div>
		<div class="grid-12 interior cf">
			<div class="grid-9 left copy">
				<h2>Looking for AkebiaCares?</h2>
				<p class="subtitle">404 error. Page not found. <br>
				Go to the AkebiaCares home page.</p>
				<a class="button-box" href="/akebiacares/">Home</a>
				<br><br><br>
			</div>
			<div class="grid-3 right desktop"></div>
		</div>
		<!-- Single Page -->

	</div>
</div>
<!-- Content -->