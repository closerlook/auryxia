<section class='container'>
	<div class='row'>
		<div class='col-md-12 col-centered'>
			<div class="header-links mobile">
				<a href="/iron-deficiency-anemia/important-safety-information/"  data-element="default" data-category="Header" data-action="Click" data-label="ISI">Important Safety Information</a>
				<span class="sep">|</span>
				<a href="/wp-content/uploads/Auryxia_PI.pdf" target="_blank" data-element="default" data-category="Header" data-action="Click" data-label="Full PI">Full Prescribing Information</a>
			</div>

			<div class='intent mobile'>
				This site is for u.s. healthcare professionals only
			</div>

			<a href="/" data-element="default" data-category="Header" data-action="Click" data-label="Logo">
				<img id="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/Auryxia-logo.svg" alt="Auryxia">
			</a>

			<div class="header-links desktop">
				<a href="/iron-deficiency-anemia/important-safety-information"  data-element="default" data-category="Header" data-action="Click" data-label="ISI">Important Safety Information</a>
				<span class="sep desktop">|</span>
				<a href="/wp-content/uploads/Auryxia_PI.pdf" target="_blank" data-element="default" data-category="Header" data-action="Click" data-label="Full PI">Full Prescribing Information</a>
				<span class="sep desktop">|</span>
				<a href="/iron-deficiency-anemia/patient/"  class="desktop nav-copy">For Patients</a>
				<span class="sep desktop">|</span>
				<a href="/akebiacares" class="desktop" data-element="default" data-category="Header" data-action="Click" data-label="AkebiaCares">AkebiaCares</a>
				<span class="sep desktop">|</span>
				<a href="/hyperphosphatemia" class="desktop" data-element="default" data-category="Header" data-action="Click" data-label="Hyperphosphatemia">Hyperphosphatemia</a>
			</div>

			<div class="intent desktop">
				This site is for u.s. healthcare professionals only
			</div>

			<a href="javascript:void(0)" class="mobile open" id="mobile-menu"></a>
		</div>
	</div>
</section>
