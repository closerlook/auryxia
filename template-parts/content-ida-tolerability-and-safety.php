<section class='container col-centered page-content'>
	<section class='col-md-12 col-centered interior'>

		<div class='post-titles'>
			<h2>Safety and tolerability profile<br class="desktopBrIDA"> evaluated in a pivotal clinical trial</h2>
		</div>
		<section class='col-md-9 left copy'>

			<section>
				<!--TABS COMPONENT HERE-->
				<div id="tab-wrap">

					<div class="tab-names">
						<a href="javascript:void(0)" class="animC active" data-element="Chart" data-category="Chart" data-action="Click" data-label="AEs Tab">AEs</a>
						<a href="javascript:void(0)" class="animC" data-element="Chart" data-category="Chart" data-action="Click" data-label="Discontinuations & SAEs">Discontinuations & SAEs</a>
					</div>

					<div class="tabs">

						<div class="tab active">
							<h3>Adverse events (AEs) reported during the 16-week randomized period of the pivotal trial<sup>1</sup></h3>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/safety/tab1a.png" alt="" class="desktop">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/safety/tab1a-mobile.png" alt="" class="mobile">
							<ul>
								<li style="margin-left: -4px;">The most common AE was diarrhea (20.5%), the majority of which was mild to moderate in severity<sup>1,2</sup></li>
							</ul>
							<p>In a pooled analysis from 2 trials<sup>3</sup></p>
							<ul>
								<li style="margin-left: -4px;">Adverse reactions reported in 2 clinical trials in at least 5% of patients receiving AURYXIA (AURYXIA vs placebo, respectively): Discolored feces (22% vs 0%), diarrhea (21% vs 12%), constipation (18% vs 10%), nausea (10% vs 4%), abdominal pain (5% vs 2%), hyperkalemia (5% vs 3%)</li>
							</ul>
							<h3>During the 16-week randomized period, decreases in mean phosphorus levels were observed<sup>3</sup></h3>
							<h3>Mean phosphorus levels started and stayed within a range recommended by clinical practice guidelines<sup>4</sup></h3>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/safety/tab1b.png" alt="" class="desktop">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/safety/tab1b-mobile.png" alt="" class="mobile">
							<h3>In a pooled analysis from 2 trials</h3>
							<ul>
								<li style="margin-left: -4px;">Incidence of hypophosphatemia, reported as a treatment-related adverse event, occurred in 1% of patients treated with AURYXIA (1 patient) and 2% of patients treated with placebo (2 patients)<sup>5</sup></li>
							</ul>
						</div>

						<div class="tab">
							<h3>Similar rate of discontinuations due to adverse reactions and similar incidence of serious adverse events (SAEs) between AURYXIA and placebo groups during the 16-week randomized period<sup>1,3</sup></h3>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/safety/tab2a.png" alt="" class="desktop">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/safety/tab2a-mobile.png" alt="" class="mobile">
							<ul>
								<li style="margin-left: -4px;">The most common adverse reaction leading to discontinuation of AURYXIA was diarrhea at 2.6%<sup>3</sup></li>
							</ul>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/safety/tab2b.png" alt="" class="desktop">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/safety/tab2b-mobile.png" alt="" class="mobile">
							<ul>
								<li style="margin-left: -4px;">None of the SAEs were suspected to be study-drug related<sup>1</sup></li>
								<li style="margin-left: -4px;">SAEs (by system organ class) occurring in ≥2% of patients treated with AURYXIA included cardiac disorders (2.6%), infections and infestations (3.4%), injury, poisoning, and procedural complications (2.6%), metabolism
									and nutrition disorders (2.6%), nervous system disorders (3.4%), renal and urinary disorders (3.4%), and respiratory, thoracic and mediastinal disorders (3.4%)<sup>5</sup></li>
								<li style="margin-left: -4px;">SAEs (by system organ class) occurring in ≥2% of patients treated with placebo included cardiac disorders (2.6%), infections and infestations (5.2%), and renal and urinary disorders (4.3%)<sup>5</sup></li>
							</ul>
						</div>

					</div>
				</div>
			</section>
			<!--TABS COMPONENT END-->

			<section id='trial-design'>
				<!--Bootstrap Collapse start -->

				<div class="collapse" id="trial-design-collapse">
					<span>Trial Design<sup>3</sup></span>
					<p>In a 24-week study consisting of a 16-week, randomized, double-blind, placebo-controlled
						efficacy period followed by an 8-week, open-label safety extension period, this trial evaluated the efficacy and safety of AURYXIA for the treatment of iron deficiency anemia in adult patients with CKD not on dialysis. Patients who were intolerant of or have had an inadequate therapeutic response to oral iron supplements, with hemoglobin ≥9.0 g/dL and ≤11.5 g/dL, serum ferritin ≤200 ng/mL,
						and TSAT ≤25% were enrolled. Patients were randomized to treatment with either
						AURYXIA (n=117) or placebo (n=117).</p>

					<p> The primary endpoint was the proportion of patients achieving a ≥1.0 g/dL increase
						in hemoglobin at any time point during the 16-week efficacy period. Use of oral iron,
						IV iron, and ESAs was not permitted at any time during the trial.</p>

					<div id='trial-design-expanded'>
						<h3>Studied in a Phase III pivotal trial of patients who were intolerant of or had inadequate response to traditional oral iron therapy</h3>
						<img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/efficacy/trial-design.png' alt='' class="desktop">
						<img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/efficacy/Phase-3.png' alt='' class="mobile">

						<h3>Primary efficacy endpoint<sup>3</sup></h3>
						<ul>
							<li style="margin-left: -10px;">Proportion of patients achieving an increase in hemoglobin concentration of ≥1.0&nbsp;g/dL from baseline at any point through the end of the randomized period (Week 16)</li>
						</ul>

						<h3>Key secondary efficacy endpoints included<sup>1</sup>:</h3>
						<ul>
							<li style="margin-left: -10px;">Mean changes in hemoglobin, TSAT, ferritin, and phosphorus from baseline to Week 16</li>
							<li style="margin-left: -10px;">Proportion of patients experiencing a sustained treatment effect on hemoglobin (≥0.75 g/dL mean change in hemoglobin from baseline over any 4-week period provided that an increase of at least 1.0&nbsp;g/dL had occurred
								during that 4-week period)</li>
						</ul>

						<h3>Key inclusion criteria<sup>3</sup></h3>
						<ul>
							<li style="margin-left: -10px;">Adult patients with CKD not on dialysis (eGFR
								<60 mL/min/1.73 m<sup>2</sup>) and iron deficiency anemia (hemoglobin ≥9.0 and ≤11.5 g/dL, ferritin&nbsp;≤200&nbsp;ng/mL and TSAT ≤25%) at screening</li>
							<li style="margin-left: -10px;">Intolerant of or have had an inadequate therapeutic response to oral iron supplements</li>
						</ul>

						<h3>Key exclusion criteria<sup>5</sup></h3>
						<ul>
							<li style="margin-left: -10px;">IV iron, ESAs, or blood transfusion administered within 4 weeks prior to screening</li>
							<li style="margin-left: -10px;">Serum phosphorus
								<3.5 mg/dL at screening</li>
							<li style="margin-left: -10px;">Cause of anemia other than iron deficiency or CKD</li>
						</ul>
					</div>
				</div>

				<a role="button" class="tab-toggle" class="collapsed" data-toggle="collapse" href="#trial-design-collapse" aria-expanded="false" aria-controls="trial-design-collapse" data-element="Toggle" data-category="Trial Design" data-action="Click" data-label="Closed">
					<div id='trial-design-toggle' class='trial-design-toggle'>
						<span class='toggle-on'>Show Full Trial Design</span>
						<span class='toggle-off'>Hide Full Trial Design</span>
						<div class='trial-design-arrow'></div>
					</div>
				</a>

			</section>
			<!--Bootstrap Collapse end -->

			<h6>KDOQI=Kidney Disease Outcomes Quality Initiative; ESAs=erythropoiesis-stimulating agents; IV=intravenous; eGFR=estimated Glomerular Filtration Rate.</h6>

		</section>

		<section class="col-md-3 right">
			<div class="cta-links">
				<a href="/iron-deficiency-anemia/dosing" class="cta animC cta-1" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Dosing">
					<span class="cta-title">DOSING</span>
					<span class="cta-text">Convenient mealtime dosing</span>
				</a>
				<a href="/iron-deficiency-anemia/efficacy" class="cta animC cta-2" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Efficacy">
					<span class="cta-title">EFFICACY</span>
					<span class="cta-text">See results from our clinical trial</span>
				</a>
				<a href="/akebiacares" class="cta animC cta-3" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Comprehensive Coverage">
					<span class="cta-title">COMPREHENSIVE COVERAGE</span>
					<span class="cta-text">
							Eligible patients with commercial insurance can pay as little as $0 for AURYXIA</span>
				</a>
			</div>
			<div class="cta-isi-sidebar">
				<a href="javascript:void(0)" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="ISI"><span>See <span style="text-decoration: underline;">Important Safety Information</span> below</span></a>
			</div>
			<img class="desktop" src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/sidebar-peacock.png' alt=''>
		</section>

	</section>
</section>
<!-- HTML include -->
<section id='isi-content'>
	<?php get_template_part('template-parts/content', 'isi'); ?>
</section>
<!-- References -->
<section class="references container">
	<div class='col-md-12 col-centered'>
		<span>References</span>
		<ol>
			<li>Fishbane S, Block GA, Loram L, et al. Effects of ferric citrate in patients with nondialysis-dependent CKD and iron deficiency anemia. <i class='nowrap'>J Am Soc Nephrol.</i> 2017;28(6):1851-1858.</li>
			<li>Data on File 13, Akebia Therapeutics, Inc.</li>
			<li>AURYXIA [package insert]. Cambridge, MA: Akebia Therapeutics, Inc.; 2017.</li>

			<li>National Kidney Foundation. K/DOQI clinical practice guidelines for bone metabolism and disease in chronic kidney disease. <i class='nowrap'>Am J Kidney Dis.</i> 2003;42(4 Suppl 3):S1-S201.</li>
			<li>Supplement to: Fishbane S, Block GA, Loram L, et al. Effects of ferric citrate in patients with nondialysis-dependent CKD and iron deficiency anemia. <em>J Am Soc Nephrol</em>. 2017;28(6):1851-1858.</li>
		</ol>
	</div>
</section>
