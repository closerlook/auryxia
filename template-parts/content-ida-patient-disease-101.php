<div class="layoutBody">
	<div class="container outerContainer ">
		<div class="backdropContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/Graphics_Backdrop.png" alt="Disease 101" class="bgImgFaq">
		</div>
		<div class="bannerStripe">
			<p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
			<div class="left-triangle"></div>
			<div class="right-triangle"></div>
		</div>
		<div class="container innerContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/bannerOne.jpg" alt="Disease 101" class="headerImg">
			<!-- <span class="patientText-Header">Hypothetical patient portrayals.</span> -->

			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/disease/mobileDiseaseHeader.png" alt="" class="bannerMobile">
			<div class="contentInner">
				<span class="patientText-Header">Hypothetical patient portrayals.</span>
				<div class="row">


					<div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
						<br class="hiddenBrDesktop">
						<p class="diseaseP1 header-diseaseP1">What is iron deficiency anemia in<br class="desktopBr"> chronic kidney disease (CKD)?</p>
						<p class="diseaseP2">A common condition that occurs when your body doesn't have enough iron to produce enough healthy red blood cells due to reduced kidney function</p>

						<span class="disease-question">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/disease/questionMarkIcon.png" alt="">
                                    <p class="diseaseP1">WHAT IS CKD?</p>
                                </span>

						<p class="diseaseP3 marginTop">To keep you healthy, your kidneys are responsible for a lot of important functions such as balancing fluids, regulating blood pressure, producing certain hormones, and removing waste products from the blood. </p>

						<p class="diseaseP3">CKD is defined as a gradual loss of kidney function, which is measured by your estimated glomerular filtration rate (eGFR). CKD is most often caused by diabetes and high blood pressure, but may also be caused by other factors.</p>

						<p class="diseaseP3"><span class="p3-SpanOne">Estimated glomerular filtration rate, or eGFR</span> <span class="p3-spanTwo">|</span> A number based on your blood test for creatinine. It tells how well your kidneys are working.</p>


						<span class="disease-question ironIDA">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/disease/questionMarkIcon.png" alt="">
                                    <p class="diseaseP1">WHAT IS IRON DEFICIENCY ANEMIA?</p>
                                </span>



						<p class="diseaseP3 marginTop">Common in patients with CKD, anemia is a condition that happens when there aren't enough healthy red blood cells to carry the oxygen your body needs. To make healthy red blood cells, your body needs iron and erythropoietin (a hormone made by the kidneys).</p>

						<p class="diseaseP3">Anemia in patients with CKD is often caused by iron deficiency, or not enough iron. This lack of iron may be caused by not having enough iron in your diet, not absorbing enough iron from your diet based on the foods you eat, or losing iron from blood loss.</p>


						<span class="disease-question symptomsQ">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/disease/questionMarkIcon.png" alt="">
                                    <p class="diseaseP1">WHAT ARE SOME COMMON SYMPTOMS?</p>
                                </span>

						<img class="marginTop desktopImg" src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/disease/Common_symptoms.png" alt="Some common symptoms of iron deficiency anemia in CKD">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/disease/common-symptoms-mobile.png" alt="Some common symptoms of iron deficiency anemia in CKD" class="mobileDiseaseImg">

						<p class="footnote">Please note, AURYXIA is not indicated to treat these symptoms. </p>

						<p class="footnote">Please note, many of the symptoms may have multiple causes beyond iron deficiency anemia. </p>
						<p class="footnote">Be sure to talk to your doctor if you are experiencing any symptoms so they can properly diagnose you. </p>
						<br>
					</div> <!-- CONTENT HERE END -->





					<div class="col-md-3 rightContent">

						<div class="rightBoxOne gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Auryxia 101">
							<p class="rightP1">AURYXIA 101</p>
							<p id="rightP2" class="rightP2">Explore how AURYXIA<br class="desktopBr"> can help</p>
						</div>

						<div class="rightBoxTwo gtm-cta rightBoxDiseaseTwo" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Taking Auryxia">
							<p class="rightP1">TAKING AURYXIA</p>
							<p id="rightP2" class="rightP2 rightP2Disease">Learn how and when you<br class="desktopBr"> should take AURYXIA</p>
						</div>


						<div class="right-vertical-line diseaseVerticalLine"></div>
						<p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

					</div>
				</div>




				<div class="isi">
					<?php //include 'includes/isi.php'; ?>
					<?php get_template_part('template-parts/content-ida-patient', 'isi'); ?>
				</div>




			</div>



		</div>
	</div>
</div>
