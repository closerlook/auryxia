<div class="container-fluid footerContainer footerDesktop gtm-footer">
	<div class="container innerContainer">
		<ul class="nav justify-content-center footerLinks">
			<li class="nav-item">
				<a  class="nav-link active gtm-footer" data-gtm-event-category="Footer" data-gtm-event-action="Click" data-gtm-event-label="Contact Us" target="_blank" href="https://akebia.com/contact/">CONTACT US</a></li>
			<div class="borderRightFooterDesktop"></div>
			<li class="nav-item">
				<a class="nav-link gtm-footer" data-gtm-event-category="Footer" target="_blank" href="https://akebia.com/legal/">TERMS AND CONDITIONS</a></li>
			<div class="borderRightFooterDesktop"></div>
			<li class="nav-item">
				<a class="nav-link active gtm-footer" data-gtm-event-category="Footer" target="_blank" href="https://akebia.com/legal/privacy-policy.aspx">PRIVACY POLICY</a></li>
			<div class="borderRightFooterDesktop"></div>
			<li class="nav-item">
				<a class="nav-link gtm-footer" data-gtm-event-category="Footer" target="_blank" href="https://akebia.com/">AKEBIA.COM</a></li>
		</ul>
		<ul class="nav justify-content-center footerText">
			<li class="nav-item">
				<p class="nav-link">&copy;2019 Akebia Therapeutics, Inc.</p></li>
			<li class="nav-item footerDate">
				<p class="nav-link" >PP-AUR-US-0828&emsp;10/19</p></li>
		</ul>
		<a class="nav-link gtm-footer" target="_blank"  href="https://akebia.com/"> <img  data-gtm-event-category="Footer" data-gtm-event-action="Click" data-gtm-event-label="AkebiaLogo" src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/footerLogoTest.png" alt="Akebia" class="footerLogo gtm-footer"></a>
	</div>
</div>

<div class="footerMobile">
	<br>
	<div class="row text-center">
		<div class="col-md-12">
			<a class="nav-link gtm-footer"  target="_blank"  href="https://akebia.com/"><img data-gtm-event-category="Footer" data-gtm-event-action="Click" data-gtm-event-label="AkebiaLogo" src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/footerLogoTest.png" alt="Akebia" class="footerLogo gtm-footer"></a>
		</div>
	</div>
	<ul class="nav justify-content-center mobileTopFooter mobileFootUl">
		<li class="nav-item">
			<a class="nav-link active gtm-footer" data-gtm-event-category="Footer" target="_blank" href="https://akebia.com/contact/">CONTACT US</a></li>
		<div class="borderRightFooter"></div>
		<li class="nav-item">
			<a class="nav-link gtm-footer" data-gtm-event-category="Footer"  target="_blank" href="https://akebia.com/legal/">TERMS AND CONDITIONS</a>
		</li>
		<div class="borderRightFooter"></div>
	</ul>
	<ul class="nav justify-content-center mobileBottomFooter mobileFootUl">
		<li class="nav-item">
			<a class="nav-link active gtm-footer" data-gtm-event-category="Footer"  target="_blank" href="https://akebia.com/legal/privacy-policy.aspx">PRIVACY POLICY</a>
		</li>
		<div class="borderRightFooter"></div>
		<li class="nav-item">
			<a class="nav-link gtm-footer" data-gtm-event-category="Footer"  target="_blank"  href="https://akebia.com/">AKEBIA.COM</a>
		</li>
	</ul>
	<ul class="nav justify-content-center mobileCopyRight">
		<li class="nav-item">
			<p>&copy;2019 Akebia Therapeutics, Inc.</p></li>
		<div class="marginFooter"></div>
		<li class="nav-item ">
			<p>PP-AUR-US-0828 10/19</p></li>
	</ul>
</div>
