<div class="layoutBody">
	<div class="container outerContainer ">
		<div class="backdropContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/Graphics_Backdrop.png" alt="" class="bgImgFaq">
		</div>
		<div class="bannerStripe">
			<p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
			<div class="left-triangle"></div>
			<div class="right-triangle"></div>
		</div>
		<div class="container innerContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/bannerFour.png" alt="Talking To Your Doctor" class="headerImg">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/auryxia/talkingDocMobileHeader.png" alt="Talking To Your Doctor" class="bannerMobile">
			<div class="contentInner">
				<span class="patientText-Header">Hypothetical patient portrayals.</span>
				<div class="row">


					<div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
						<br class="hiddenBrDesktop">
						<p class="diseaseP1">Your healthcare provider is here to help</p>
						<p class="diseaseP3  docDiseaseP3">Talking to your doctor throughout your journey with AURYXIA can help<br> them personalize a treatment plan for your specific needs</p>



						<p class="doctorP1">WHAT TO TELL YOUR HEALTHCARE PROVIDER</p>
						<p class="doctorP2">Tell your healthcare provider about all the medicines you are currently taking.</p>
						<p class="doctorP3">Before you take AURYXIA, and even after you begin treatment, it’s important to talk to your healthcare provider about all the medications you might be taking. This includes doxycycline and ciprofloxacin (antibiotics) as well as any other prescription and over-the-counter medicines, vitamins, and herbal supplements.</p>

						<!-- <p class="doctorP2 doctorP2Margin">Be sure to tell your doctor about all the medications you take, including<br> over-the-counter medicine, vitamins, and herbal supplements.</p> -->
						<p class="doctorP2MArgin"></p>



						<div class="doctorUl">

							<ul>

								<p class="takingP1">ALSO BE SURE TO TELL YOUR HEALTHCARE PROVIDER IF YOU:</p>


								<li><span>
                                                <p class="takingP2">Have had a condition called iron overload (hemochromatosis)</p>
                                            </span></li>

								<li><span>
                                                <p class="takingP2">Have any other medical conditions</p>
                                            </span></li>

								<li><span>
                                                <p class="takingP2">Are pregnant, plan to become pregnant, or plan to breastfeed</p>
                                            </span></li>


							</ul>

						</div>



						<div class="doctorUl">

							<ul>
								<p class="takingP4">For the treatment of iron deficiency anemia in adult patients with CKD not on dialysis </p>
								<p class="takingP1">THE MOST COMMON SIDE EFFECTS OF AURYXIA ARE: </p>


								<li><span>
                                           <p class="takingP2">Diarrhea</p>
                                       </span></li>

								<li><span>
                                           <p class="takingP2">Constipation</p>
                                       </span></li>

								<li><span>
                                           <p class="takingP2">Nausea</p>
                                       </span></li>
								<li><span>
                                           <p class="takingP2">Abdominal pain</p>
                                       </span></li>

								<li><span>
                                           <p class="takingP2">High levels of potassium in the blood </p>
                                       </span></li>


								<p class="takingP3">AURYXIA contains iron and may cause dark stools, which is considered normal<br> with oral medications containing iron.</p>

								<p class="takingP3">Call your healthcare provider for medical advice about side effects. You may report suspected side effects to Akebia Therapeutics, Inc. at <a href="">1&#xfeff;-&#xfeff;844&#xfeff;-&#xfeff;445&#xfeff;-&#xfeff;3799</a> or FDA at <a href="">1&#xfeff;-&#xfeff;800&#xfeff;-&#xfeff;FDA&#xfeff;-&#xfeff;1088</a> or <a href="https://www.fda.gov/safety/medwatch-fda-safety-information-and-adverse-event-reporting-program" target="_blank">www.fda.gov/medwatch</a>.</p>
								<p class="takingP3">Tell your healthcare provider if you have children under 6 years old at home.</p>
								<p class="takingP5">
									AURYXIA contains iron. Keep it away from children to prevent an accidental ingestion of iron and potentially fatal poisoning. Call a poison control center or your healthcare provider if a child swallows AURYXIA.
								</p>
							</ul>

						</div>



						<div class="doctorUl">

							<ul>

								<p class="takingP1">ADDITIONAL QUESTIONS TO ASK YOUR HEALTHCARE PROVIDER</p>


								<li><span>
                                           <p class="takingP2">How does iron deficiency anemia in CKD affect my existing medical conditions?</p>
                                       </span></li>

								<li><span>
                                           <p class="takingP2">Do I have to stop taking or adjust any of my over-the-counter or prescription medicines?</p>
                                       </span></li>

								<li><span>
                                           <p class="takingP2">Do I need to limit any of my daily activities?</p>
                                       </span></li>


								<li><span>
                                           <p class="takingP2"> Should I change my current diet?</p>
                                       </span></li>


							</ul>

						</div>

						<!-- <p class="footnote">Reference: 1. AURYXIA [package insert]. Boston, MA: Akebia Therapeutics, Inc.; 2017. </p> -->


					</div> <!-- CONTENT HERE END -->





					<div class="col-md-3 rightContent">

						<div class="rightBoxOne takingBox1 gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Auryxia 101">
							<p class="rightP1">AURYXIA 101</p>
							<p id="rightP2" class="rightP2 takingRightP2">Explore how AURYXIA can help</p>
						</div>

						<div class="rightBoxTwo gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Save on the Cost fo Auryxia">
							<p class="rightP1">SAVE ON THE COST<br class="desktopBr"> OF AURYXIA</p>
							<p id="rightP2" class="rightP2 rightP2Disease">Depending on your insurance,<br class="desktopBr"> you can get AURYXIA for free</p>
						</div>


						<div class="right-vertical-line"></div>
						<p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

					</div>
				</div>




				<div class="isi">
					<?php //include 'includes/isi.php'; ?>
					<?php get_template_part('template-parts/content-ida-patient', 'isi'); ?>
				</div>




			</div>



		</div>
	</div>
</div>
