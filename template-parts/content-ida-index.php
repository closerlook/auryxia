<section id='callout-hero' class='container'>
	<div class="sub-buttons-container">
		<div class="sub-button"><a href="http://www.auryxiarxlocator.com" target="_blank">Find specialty pharmacies that dispense AURYXIA</a></div>
		<div class="sub-button"><a href="https://akebiarxcoverage.caremetx.com/" target="_blank">Check your patient's prescription benefits</a></div>
	</div>
	<video class='desktop desktopVideo' autoplay style="width:1000px;height:400px;" poster="<?php echo get_template_directory_uri(); ?>/assets/img/ida/peacockPoster.png">
		<source src="<?php echo get_template_directory_uri(); ?>/assets/video/Peacock_Homepage_400_v02.webm" type='video/webm;codecs="vp8, vorbis"'/>
		<source src="<?php echo get_template_directory_uri(); ?>/assets/video/Peacock_Homepage_400_v02.mp4"  type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"' />
	</video>
	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/home-mobile-hero-peacock.png" class='mobile'>
	<span class="mobile callout-hero-intro">For the treatment of iron deficiency anemia in adult patients with CKD not on dialysis</span>
	<span class="mobile callout-hero-header">DESIGNED TO BE DIFFERENT</span>
	<span class='callout-hero-subtext col-md-6 right mobile'>AURYXIA is the only oral iron tablet approved by the FDA for the treatment of iron deficiency anemia specifically in adult patients with CKD not on dialysis</span>
</section>
<section id='callouts-home' class='container'>
	<div class="col-md-12 col-centered callouts">
		<a href="/hyperphosphatemia" class="callout col-md-4 animC left callout-1" data-element="default" data-category="CTA" data-action="Click" data-label="Dialysis Indication">
			<span class="callout-top"></span>
			<span class="main_text">DIALYSIS INDICATION</span>

			<span class="sub_text">Learn about AURYXIA’s impact on hyperphosphatemia in adult patients with CKD on dialysis</span>
		</a>
		<a href="/iron-deficiency-anemia/efficacy" class="callout col-md-4 animC left callout-2" data-element="default" data-category="CTA" data-action="Click" data-label="Efficacy Results">
			<span class="callout-top"></span>
			<span class="main_text">EFFICACY RESULTS</span>

			<span class="sub_text">See results from our iron deficiency anemia clinical trial in adult patients with CKD not on dialysis</span>
		</a>
		<a href="/akebiacares" class="callout animC nom col-md-4 left callout-3" data-element="default" data-category="CTA" data-action="Click" data-label="Expanded Coverage">
			<span class="callout-top"></span>
			<span class="main_text">PATIENT COVERAGE</span>

			<span class="sub_text">Eligible patients with commercial insurance can pay as little as $0 for AURYXIA</span>
		</a>
	</div>
	<div class="cta-isi-homepage">
		<a href="javascript:void(0)" data-element="default" data-category="CTA" data-action="Click" data-label="ISI"><span>Please see <span style="text-decoration: underline;">Important Safety Information</span> below</span></a>
	</div>
</section>
<section id='isi-content'>
	<?php get_template_part('template-parts/content', 'isi'); ?>
</section>


