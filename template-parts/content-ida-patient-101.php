
<div class="layoutBody">
	<div class="container outerContainer">
		<div class="backdropContainer" id="img101">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/Graphics_Backdrop.png" alt="" class="bgImgFaq" >
		</div>
		<div class="bannerStripe">
			<p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
			<div class="left-triangle"></div>
			<div class="right-triangle"></div>
		</div>
		<div class="container innerContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/bannerTwo.png" alt="Auryxia 101" class="headerImg">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/auryxia/auryxiaMobileHeader.png" alt="Auryxia 101" class="bannerMobile">
			<div class="contentInner">
				<span class="patientText-Header">Hypothetical patient portrayals.</span>
				<div class="row">


					<div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
						<br class="hiddenBrDesktop">
						<p class="diseaseP1">The only oral iron designed for you</p>
						<p class="diseaseP2">AURYXIA<span style="margin-left: -3px">®</span>(ferric citrate) is the only oral iron tablet that’s approved to treat iron deficiency anemia specifically in adults with chronic kidney disease (CKD) not on dialysis</p>

						<p class="diseaseP1 auryxiaFont">HOW AURYXIA (ah-RICKS-ee-ah) WORKS</p>

						<div class="aurBox auryxiaBoxOne">
							<div class="aurInnerBoxLeft">
								<p class="boxHeader" style="margin-left: 18px"><span style="margin-left: -20px;">1.</span> CHANGES FORM SO IT'S ABSORBED LIKE IRON FROM YOUR DIET</p>
								<p class="boxTxt">Once swallowed, the iron in AURYXIA changes form so it can be absorbed by the gut, similar to the way your body absorbs iron from food </p>
							</div>

							<div class="aurInnerBoxRight">
								<img class="aurImgOne" src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/auryxia/aur-IconOne.png" alt="Changes form so it’s absorbed like food ">
							</div>

						</div>

						<div class="aurBox auryxiaBoxTwo">
							<div class="aurInnerBoxLeft">
								<p class="boxHeader" style="margin-left: 18px"><span style="margin-left: -20px;">2.</span> GETS TRANSPORTED INTO THE BLOODSTREAM</p>
								<p class="boxTxt">The iron is then transported through the gut and into the blood. Once in the bloodstream, the iron changes back to its original form</p>
							</div>

							<div class="aurInnerBoxRight">
								<img class="aurImgTwo" src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/auryxia/aur-IconTwo.png" alt="Gets transported into the bloodstream ">
							</div>

						</div>

						<div class="aurBox auryxiaBoxThree">
							<div class="aurInnerBoxLeft">
								<p class="boxHeader">3. COMBINED WITH HEMOGLOBIN</p>
								<p class="boxTxt">Now that iron is back in its original form, the iron can be combined with hemoglobin, the part of the red blood cells that helps carry oxygen</p>
							</div>

							<div class="aurInnerBoxRight">
								<img class="aurImgThree" src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/auryxia/aur-IconThree.png" alt="Turns into hemoglobin ">
							</div>

						</div>


						<span class="disease-question">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/disease/questionMarkIcon.png" class="diseaseQuestionMarkImg" alt="">
                                    <p class="diseaseP1 auryxiaFont">NOT YOUR FIRST TIME TAKING AN ORAL IRON?</p>
                                </span>

						<p class="diseaseP3 marginTop" id="marginTop">Even if a previous oral iron medicine was unsuccessful, AURYXIA is clinically proven to increase hemoglobin and iron levels </p>




						<p class="diseaseP1 auryxiaFont">AURYXIA IMPROVED HEMOGLOBIN AND IRON LEVELS</p>
						<!-- <p class="diseaseP1 auryxiaFont">AURYXIA IMPROVED IRON LEVELS WITHOUT THE HELP<br class="desktopBr"> OF OTHER MEDICATIONS</p> -->



						<p class="diseaseP3 ">AURYXIA increased hemoglobin and iron levels without the use of intravenous (IV) iron or erythropoiesis-stimulating agents</p>






						<p class="diseaseP1 auryxiaFont ">WHEN TAKING AURYXIA, HERE'S SOMETHING TO KEEP IN MIND</p>


						<p class="diseaseP3"><span>AURYXIA can not only raise iron levels, but it is also approved to lower phosphorus levels in adults with CKD on dialysis. Your doctor will monitor your levels throughout your time taking AURYXIA</span></p>




						<!-- <p class="footnote "><span class="refBold">References: 1.</span> National Kidney Foundation. K/DOQI clinical practice guidelines for bone metabolism and disease in chronic kidney disease. <em>Am J Kidney Dis.</em> 2003;42(4 Suppl 3):S1-S201. <span class="refBold">2.</span> Supplement to: Fishbane S, Block GA, Loram L, et al. Effects of ferric citrate in patients with nondialysis-dependent CKD and iron deficiency anemia. <em>J Am Soc Nephrol.</em> 2017;28(6):1851-1858.</p> -->




					</div> <!-- CONTENT HERE END -->





					<div class="col-md-3 rightContent">

						<div class="rightBoxOne gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="What is Iron Deficiency Anemia">
							<p class="rightP1">WHAT IS IRON DEFICIENCY ANEMIA?</p>
							<p id="rightP2" class="rightP2">Learn more about your<br class="desktopBr"> condition</p>
						</div>

						<div class="rightBoxTwo gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Save on the Cost fo Auryxia">
							<p class="rightP1">SAVE ON THE COST OF AURYXIA</p>
							<p id="rightP2" class="rightP2 rightP2Disease">Depending on your insurance,<br class="desktopBr"> you can get AURYXIA for free</p>
						</div>


						<div class="right-vertical-line"></div>
						<p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

					</div>
				</div>
				<div class="isi">
					<?php //include 'includes/isi.php'; ?>
					<?php get_template_part('template-parts/content-ida-patient', 'isi'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
