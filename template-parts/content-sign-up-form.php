<section class="container form-container sign-up-form">
	<form id="form-step-sign-up">
		<section>
			<div class="col-md-12 col-centered">
				<div class="instructions">
					<h2>Sign up to get the latest news and resources for you and your patients</h2>
					<p class="latest-info">Complete the form below to get access to the latest information on AURYXIA and other updates from Akebia.</p>
					<p class="required-field-note">Fields marked with an asterisk (*) are required.</p>
				</div>

				<div class="form-field">
					<label for="npi">NPI NUMBER<span class="required">*</span></label>
					<div><input id="npi" type="text" maxlength="10" tabindex="1"></div>
					<div class="error"></div>
				</div>

				<div id="npi-help">
					<p>Need your NPI number? <span id="npi-lookup-button" data-toggle="modal" data-target="#npi-lookup-modal">Look it up here.</span></p>
					<p class="footnote">Your NPI number must be in your name and not shared across your facility or in the name of any other person. If you are a healthcare professional and you do not have an NPI number in your name, select the checkbox below.</p>

					<div class="checkbox-field">
						<div>
							<input id="no-npi" type="checkbox" tabindex="2">
							<label for="no-npi">I do not have an NPI number</label>
						</div>
						<div class="error"></div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="col-md-12 col-centered">
				<div class="form-field">
					<label for="first-name">FIRST NAME<span class="required">*</span></label>
					<div><input id="first-name" type="text" maxlength="100" tabindex="3"></div>
					<div class="error"></div>
				</div>

				<div class="form-field">
					<label for="last-name">LAST NAME<span class="required">*</span></label>
					<div><input id="last-name" type="text" maxlength="100" tabindex="4"></div>
					<div class="error"></div>
				</div>

				<div class="form-field">
					<label for="email">EMAIL ADDRESS<span class="required">*</span></label>
					<div><input id="email" type="text" maxlength="100" tabindex="5"></div>
					<div class="error"></div>
				</div>

			</div>
		</section>

		<section id="form-buttons">
			<div class="col-md-12 col-centered">
				<div class="instructions">
					<p>By providing my information above, I agree to the Akebia Therapeutics, Inc. (“Akebia”) <u><a href="https://akebia.com/privacy-policy/" target="_blank">Privacy Policy</a></u> and give Akebia, its affiliates, and business partners permission to contact me via email for marketing purposes or otherwise provide me with information about company products, services, and programs or other topics of interest, conduct market research, or otherwise ask me about my experience with or thoughts about such topics. Akebia will not sell or transfer my information to any unrelated third party for marketing purposes without my express permission. I may opt out at any time by clicking the unsubscribe link within any email I receive.</p>
				</div>

				<div class="form-field">
					<input id="sign-up-button" type="submit" value="Sign Up" tabindex="7">
				</div>
			</div>
		</section>
	</form>

	<!-- Modal -->

	<form id="npi-lookup-modal" class="cl-modal-template">
		<section>
			<div>
				<div class="instructions">
					<p>To find your NPI number, please fill out the information below.</p>
					<p class="required-field-note">Fields marked with an asterisk (*) are required.</p>
				</div>

				<div class="form-field">
					<label for="npi-first-name">FIRST NAME</label>
					<div><input id="npi-first-name" type="text" maxlength="100" tabindex="1" /></div>
					<div class="error"></div>
				</div>

				<div class="form-field">
					<label for="npi-last-name">LAST NAME<span class="required">*</span></label>
					<div><input id="npi-last-name" type="text" maxlength="100" tabindex="2" /></div>
					<div class="error"></div>
				</div>

				<div class="form-field">
					<label for="npi-city">CITY</label>
					<div><input id="npi-city" type="text" maxlength="100" tabindex="3" /></div>
					<div class="error"></div>
				</div>

				<div class="form-field">
					<label for="npi-state">STATE<span class="required">*</span></label>
					<div>
						<select id="npi-state" tabindex="4">
							<option>Select</option>
							<option value="AL">Alabama</option>
							<option value="AK">Alaska</option>
							<option value="AZ">Arizona</option>
							<option value="AR">Arkansas</option>
							<option value="CA">California</option>
							<option value="CO">Colorado</option>
							<option value="CT">Connecticut</option>
							<option value="DE">Delaware</option>
							<option value="DC">District of Columbia</option>
							<option value="FL">Florida</option>
							<option value="GA">Georgia</option>
							<option value="HI">Hawaii</option>
							<option value="ID">Idaho</option>
							<option value="IL">Illinois</option>
							<option value="IN">Indiana</option>
							<option value="IA">Iowa</option>
							<option value="KS">Kansas</option>
							<option value="KY">Kentucky</option>
							<option value="LA">Louisiana</option>
							<option value="ME">Maine</option>
							<option value="MD">Maryland</option>
							<option value="MA">Massachusetts</option>
							<option value="MI">Michigan</option>
							<option value="MN">Minnesota</option>
							<option value="MS">Mississippi</option>
							<option value="MO">Missouri</option>
							<option value="MT">Montana</option>
							<option value="NE">Nebraska</option>
							<option value="NV">Nevada</option>
							<option value="NH">New Hampshire</option>
							<option value="NJ">New Jersey</option>
							<option value="NM">New Mexico</option>
							<option value="NY">New York</option>
							<option value="NC">North Carolina</option>
							<option value="ND">North Dakota</option>
							<option value="OH">Ohio</option>
							<option value="OK">Oklahoma</option>
							<option value="OR">Oregon</option>
							<option value="PA">Pennsylvania</option>
							<option value="PR">Puerto Rico</option>
							<option value="RI">Rhode Island</option>
							<option value="SC">South Carolina</option>
							<option value="SD">South Dakota</option>
							<option value="TN">Tennessee</option>
							<option value="TX">Texas</option>
							<option value="UT">Utah</option>
							<option value="VT">Vermont</option>
							<option value="VA">Virginia</option>
							<option value="WA">Washington</option>
							<option value="WV">West Virginia</option>
							<option value="WI">Wisconsin</option>
							<option value="WY">Wyoming</option>
						</select>
					</div>
					<div class="error"></div>
				</div>
			</div>
		</section>

		<section>
			<div>
				<div class="form-field">
					<input id="npi-sign-up-button" class="cta-primary" type="submit" value="Search" tabindex="5" />
				</div>
			</div>
		</section>

		<section id="npi-lookup-no-results">
			<div>
				<h2>No results found</h2>
				<p>Refine your search and try again.</p>
			</div>
		</section>

		<section id="npi-lookup-results">
			<div>
				<h2>Search Results</h2>
				<p>Select the entry with your name and address information. If you cannot find your information from these results, please cancel and register without an NPI number.</p>

				<table>
					<thead>
						<tr>
							<th>Name</th>
							<th>NPI number</th>
							<th>State</th>
							<th>ZIP code</th>
							<th></th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>
			</div>
		</section>
	</form>
	</div>
	</div>
	</div>
	</div>

</section>
