<div class="content content-1">
	<div class="grid interior cf">
		<div class="post-titles">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-support-programs.jpg" class="header-desktop" alt="Support Programs" title="Support Programs">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-support-programs-mobile.jpg" class="header-mobile" alt="Support Programs" title="Support Programs" style="width: 100%">
		</div>
		<div class="grid-12 interior cf">
			<div class="grid-9 left copy">
				<h2>YOUR PARTNER IN COVERAGE SOLUTIONS</h2>
				<p class="subtitle">Discover how AkebiaCares can help identify AURYXIA coverage solutions for your&nbsp;patients</p>
				<div class="program cf">
					<div class="program-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/icon-operator.png">
					</div>
					<div class="program-txt">
						<h3 class="operator-icon">REIMBURSEMENT HELP</h3>
						<p><strong>One-on-one support for assistance with insurance questions and challenges</strong></p>
						<h4>What does it offer?</h4>
						<ul>
							<li>Verify insurance and prescription drug benefits for AURYXIA</li>
							<li>Coordinate prior authorizations for AURYXIA</li>
							<li>Identify patient eligibility status for Low-Income Subsidy assistance, copay assistance, or&nbsp;state&#8209;funded programs*</li>
						</ul>
						<h4>Who is it for?</h4>
						<ul>
							<li>Anyone who has been prescribed AURYXIA</li>
						</ul>
						<h4>What form is needed?</h4>
						<ul>
							<li>Download the <a href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank">AkebiaCares Enrollment Form</a><sup>&dagger;</sup></li>
						</ul>
					</div>
				</div>
				<div class="program cf">
					<div class="program-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/icon-dollar-bill.png">
					</div>
					<div class="program-txt">
						<h3 class="bill-icon">COPAY PROGRAM</h3>
						<p><strong>Financial assistance for patients with commercial insurance</strong></p>
						<h4>What does it offer?</h4>
						<p>The majority of patients with commercial insurance pay as little as $0 for AURYXIA<sup>&Dagger;</sup></p>
						<ul>
							<li>Up to $500 off prescriptions for 90 tablets or less; up to $1,000 off prescriptions for 91-180 tablets; up to $1,500 off prescriptions for 181 tablets or more</li>
						</ul>
						<h4>Who is it for?</h4>
						<p>Eligible patients with commercial insurance<sup>&sect;</sup></p>
						<h4>What form is needed?</h4>
						<p>Many pharmacies will apply the coupon when filling AURYXIA. Download the  <a href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" target="_blank">Copay Coupon</a></p>
					</div>
				</div>
				<div class="program cf">
					<div class="program-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/icon-people.png">
					</div>
					<div class="program-txt">
						<h3 class="people-icon">PATIENT ASSISTANCE PROGRAM (PAP)</h3>
						<p><strong>Free AURYXIA may be available for eligible patients<sup>&parallel;</sup></strong></p>
						<h4>What does it offer?</h4>
						<ul>
							<li>Medication free of charge for eligible patients</li>
						</ul>
						<h4>Who is it for?</h4>
						<p>For patients who are uninsured, patients who have Medicare Part D insurance but cannot afford their copays, and patients whose insurance does not cover AURYXIA</p>
						<h4>What form is needed?</h4>
						<ul>
							<li>Download the <a href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank">AkebiaCares Enrollment Form</a><sup>&dagger;</sup></li>
						</ul>
					</div>
				</div>
				<p class="footnote-title"><strong>AkebiaCares does not guarantee coverage and/or reimbursement for all patients.</strong></p>
				<p class="footnote no-space">*Patients may be eligible for Low-Income Subsidy (LIS) if they have an income of ≤150% of the Federal Poverty Level (FPL).</p>
				<p class="footnote no-space"><sup>&dagger;</sup>Akebia is entitled to request additional documentation for income attestation and medication drug list.</p>
				<p class="footnote no-space"><sup>&Dagger;</sup>Restrictions may apply. Copay assistance is not valid for prescriptions reimbursed under Medicare, Medicaid, or similar federal or state programs.</p>
				<p class="footnote no-space"><sup>&sect;</sup>Copay assistance is not valid for prescriptions reimbursed under Medicare, Medicaid, or similar federal or state programs.</p>
				<p class="footnote"><sup>&parallel;</sup>Medicare Part D patients with an annual income of ≤150% of the FPL are eligible for LIS assistance (also called "Extra Help").</p>
			</div>

			<div class="grid-3 right line-bg desktop">
				<div class="cta-links">
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Download Copay Coupon Here" href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="cta animC cta-1">
						<span class="cta-title">Download the Enrollment Form</span>
						<span class="cta-text">AkebiaCares Case Managers are standing by to help your patients find coverage</span>
					</a>
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Connect with your Personal Case Manager" href="/akebiacares/reimbursement-help/" class="cta animC cta-2">
						<span class="cta-title">Connect with a specialist</span>
						<span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
					</a>
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Keryx Patient Plus Forms" href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" target="_blank" class="cta animC cta-3">
						<span class="cta-title">DOWNLOAD THE COPAY COUPON</span>
						<span class="cta-text">Eligible patients with commercial insurance can pay as little as $0</span>
					</a>
				</div>
			</div>
			<div class="grid-3 right desktop">
				<p class="isi-jump">See <a href="#important-safety-information">Important Safety Information</a> below</p>
			</div>
		</div><!-- Single Page -->
		<?php get_template_part('template-parts/content','isi-akebiacares'); ?>
		<div class="cta-links mobile">
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Download Copay Coupon Here" href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="cta animC cta-1">
				<span class="cta-title">Download the Enrollment Form</span>
				<span class="cta-text">AkebiaCares Case Managers are standing by to help your patients find coverage</span>
			</a>
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Connect with your Personal Case Manager" href="/akebiacares/reimbursement-help" class="cta animC cta-2">
				<span class="cta-title">Connect with a specialist</span>
				<span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
			</a>
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Keryx Patient Plus Forms" href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" target="_blank" class="cta animC cta-3">
				<span class="cta-title">DOWNLOAD THE COPAY COUPON</span>
				<span class="cta-text">Eligible patients with commercial insurance can pay as little as $0</span>
			</a>
		</div>
	</div>
</div>
</div><!-- Content -->
