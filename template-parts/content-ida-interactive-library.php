<section class='container col-centered page-content'>
	<section class='col-md-12 col-centered interior'>
		<div class='post-titles'>
			<h2>INTERACTIVE LIBRARY</h2>
		</div>
		<section class='col-md-9 left copy'>
			<img class='desktop library-peacock right' src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/sidebar-peacock.png' alt=''>
			<div id='library-items'>
				<div class='library-item'>
					<div class='left library-item-img2'></div>
					<div class='library-item-content'>
						<h3><strong>Access Flashcard</strong></h3>
						<h2>Explore AURYXIA coverage specific to this indication</h2>
						<a class="download-btn animC" href="/wp-content/uploads/AkebiaCares-IDA-Access-Flashcard.pdf" target="_blank" data-element="Ignore">
								<span class="icon-download-pdf">
			  <svg xmlns="http://www.w3.org/2000/svg" width="21" height="18">
				<path class="icon-download-color" d="M6 8.4h3.2V0h3v8.4h3.1c-1.6 2.4-4.6 5.6-4.6 5.6S7.6 10.8 6 8.4z"/>
				<path class="icon-download-color" d="M19.2 10.1H21V18H0v-7.9h1.9v6.3h17.3v-6.3z"/>
			  </svg>
			</span>
							<span>DOWNLOAD PDF</span>
						</a>
					</div>
				</div>
			</div>
		</section>
		<section class="col-md-3 right">
			<div class="cta-links">
				<a href="/iron-deficiency-anemia/moa" class="cta animC cta-1" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="MOA">
					<span class="cta-title">MOA</span>
					<span class="cta-text">Discover how AURYXIA works</span>
				</a>
				<a href="/iron-deficiency-anemia/efficacy" class="cta animC cta-2" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Efficacy">
					<span class="cta-title">EFFICACY</span>
					<span class="cta-text">See results from our clinical trial</span>
				</a>
				<a href="/akebiacares" class="cta animC cta-3" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Expanded Coverage">
					<span class="cta-title">Comprehensive COVERAGE</span>
					<span class="cta-text">Eligible patients with commercial insurance can pay as little as $0 for AURYXIA</span>
				</a>
			</div>
			<div class="cta-isi-sidebar">
				<a href="javascript:void(0)" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="ISI"><span>See <span style="text-decoration: underline;">Important Safety Information</span> below</span></a>
			</div>
			<!--no peacock on the sidebar here-->
		</section>
	</section>
</section>
<!-- HTML includes -->
<section id='isi-content'>
	<?php get_template_part('template-parts/content', 'isi'); ?>
</section>

