<div id="isi" class="grid-12">
	<h2>INDICATIONS</h2>
	<p>AURYXIA<sup>&reg;</sup> (ferric citrate) is indicated for:</p>
	<ul>
		<li>The control of serum phosphorus levels in adult patients with chronic kidney disease on dialysis</li>
		<li>The treatment of iron deficiency anemia in adult patients with chronic kidney disease not on dialysis</li>
	</ul>
	<h2 id="important-safety-information">IMPORTANT SAFETY INFORMATION</h2>
	<p><strong>CONTRAINDICATION</strong></p>
	<p class="bottom-space">AURYXIA<sup>&reg;</sup> (ferric citrate) is contraindicated in patients with iron overload syndromes, e.g., hemochromatosis</p>
	<p><strong>WARNINGS AND PRECAUTIONS</strong></p>
	<ul>
		<li><strong>Iron Overload:</strong> Increases in serum ferritin and transferrin saturation (TSAT) were observed in clinical trials with AURYXIA in patients with chronic kidney disease (CKD) on dialysis treated for hyperphosphatemia, which may lead to excessive elevations in iron stores. Assess iron parameters prior to initiating AURYXIA and monitor while on therapy. Patients receiving concomitant intravenous (IV) iron may require a reduction in dose or discontinuation of IV iron therapy</li>
		<li><strong>Risk of Overdosage in Children Due to Accidental Ingestion:</strong> Accidental ingestion and resulting overdose of iron-containing products is a leading cause of fatal poisoning in children under 6 years of age. Advise patients of the risks to children and to keep AURYXIA out of the reach of children</li>
	</ul>
	<p><strong>ADVERSE REACTIONS</strong></p>
	<p>The most common adverse reactions reported with AURYXIA in clinical trials were:</p>
	<ul class="indented">
		<li><span>Hyperphosphatemia in CKD on Dialysis</span>: Diarrhea&nbsp;(21%), discolored&nbsp;feces&nbsp;(19%), nausea&nbsp;(11%), constipation&nbsp;(8%), vomiting&nbsp;(7%) and cough&nbsp;(6%)</li>
		<li><span>Iron Deficiency Anemia in CKD Not on Dialysis</span>: Discolored&nbsp;feces&nbsp;(22%), diarrhea&nbsp;(21%), constipation&nbsp;(18%), nausea&nbsp;(10%), abdominal&nbsp;pain&nbsp;(5%) and hyperkalemia&nbsp;(5%)</li>
	</ul>
	<p><strong>SPECIFIC POPULATIONS</strong></p>
	<ul>
		<li><strong>Pregnancy and Lactation:</strong> There are no available data on AURYXIA use in pregnant women to inform a drug-associated risk of major birth defects and miscarriage. However, an overdose of iron in pregnant women may carry a risk for spontaneous abortion, gestational diabetes and fetal malformation. Data from rat studies have shown the transfer of iron into milk, hence, there is a possibility of infant exposure when AURYXIA is administered to a nursing woman</li>
	</ul>
	<p class="phone-fix">To report suspected adverse reactions, contact Akebia Therapeutics, Inc. at <a href="tel:+18444453799"><span>1-844-445-3799</span></a></p>
	<p><strong>Please see full <a href="/wp-content/uploads/Auryxia_PI.pdf" target="_blank">Prescribing Information</a></strong></p>
</div>
