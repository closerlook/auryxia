<?php ?>
<div class="content content-1">
	<div class="grid interior cf">
		<div class="post-titles">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-patient-coverage.jpg" class="header-desktop" alt="Patient Coverage" title="Patient Coverage" style="width: 100%">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-patient-coverage-mobile.jpg" class="header-mobile" alt="Patient Coverage" title="Patient Coverage" style="width: 100%">
		</div>
		<div class="grid-12 interior cf">
			<div class="grid-9 left copy">
				<h2>EXPLORE AURYXIA COVERAGE BY INDICATION</h2>
				<p class="subtitle">Depending on your patient&#8217;s type of insurance, they can get AURYXIA for little or no&nbsp;cost</p>
				<div id='tab-wrap' class='tab-patient-coverage'>
					<div class='tab-names cf'>
						<a href='#' class='animC active'>Hyperphosphatemia in CKD-DD</a>
						<a href='#' class='animC '>Iron Deficiency Anemia in CKD-NDD</a>
					</div>
					<div class='tabs cf'>
						<div class='tab active'>
							<h2>Broad Insurance Coverage for Adult Patients With Hyperphosphatemia in CKD&nbsp;on&nbsp;Dialysis<sup>1</sup></h2>
							<div class="grid-9 no-padding">
								<div class="patient-coverage cf">
									<div class="patient-coverage-img">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/shield-navy.png" alt="" />
									</div>
									<div class="patient-coverage-txt">
										<h3 class="navy">Commercial Insurance</h3>
										<ul>
											<li>AURYXIA is covered by the majority of commercial plans
												<ul>
													<li>No prior authorizations</li>
													<li>No step edits</li>
												</ul></li>
											<li>The majority of patients with commercial insurance <span>pay as little as $0</span> for AURYXIA*
												<br><br>With the Copay Coupon, eligible patients may receive<sup>&dagger;</sup>:</li>
										</ul>
										<div class="tablets">
											<div class="cf">
												<div class="tablets-90"></div><div class="tablet-text"><span>For 90 tablets or less</span> <br/> Up to $500 off prescriptions</div>
											</div>
											<div class="cf">
												<div class="tablets-91-180"></div><div class="tablet-text"><span>For 91-180 tablets</span> <br/>  Up to $1,000 off prescriptions</div>
											</div>
											<div class="cf">
												<div class="tablets-181-plus"></div><div class="tablet-text"><span>For 181 tablets or more</span> <br/>  Up to $1,500 off prescriptions</div>
											</div>
											<p class="footnote"><sup>&dagger;</sup>AURYXIA is supplied as bottles of 200-count 210 mg ferric iron tablets.</p>
										</div>
									</div>
								</div>
								<div class="patient-coverage cf">
									<div class="patient-coverage-img">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/shield-teal.png" alt="" />
									</div>
									<div class="patient-coverage-txt">
										<h3 class="teal">Medicare LIS/dual eligible</h3>
										<ul>
											<li>AURYXIA is on formulary at all major Medicare Part D Plans
												<ul>
													<li>Prior authorization required to confirm diagnosis</li>
												</ul></li>
											<li>All Medicare full Low-Income Subsidy (LIS)/dual-eligible patients pay <strong>no more than $10.00</strong> per fill of AURYXIA</li>
										</ul>
									</div>
								</div>
								<div class="patient-coverage cf">
									<div class="patient-coverage-img">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/shield-dk-gray.png" alt="" />
									</div>
									<div class="patient-coverage-txt">
										<h3 class="dk-gray">Standard medicare part&nbsp;d</h3>
										<ul>
											<li>AURYXIA is on formulary at all major Medicare Part D Plans
												<ul>
													<li>Prior authorization required to confirm diagnosis
													<li>Free AURYXIA may be provided to Medicare Part D patients who cannot afford their copays<sup>&Dagger;</sup></li>
												</ul></li>
										</ul>
									</div>
								</div>
								<div class="patient-coverage cf">
									<div class="patient-coverage-img">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/shield-gray.png" alt="" />
									</div>
									<div class="patient-coverage-txt">
										<h3 class="gray">Uninsured Patients</h3>
										<ul>
											<li>Free AURYXIA may be provided to uninsured patients<sup>&Dagger;</sup></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="tab">
							<h2>Insurance Coverage for Adult Patients With Iron Deficiency Anemia in CKD&nbsp;not on&nbsp;Dialysis<sup>1</sup></h2>
							<div class="grid-9 no-padding">
								<div class="patient-coverage cf">
									<div class="patient-coverage-img">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/shield-navy.png" alt="" />
									</div>
									<div class="patient-coverage-txt">
										<h3 class="navy">Commercial Insurance</h3>
										<ul>
											<li>AURYXIA is covered by the majority of commercial plans
												<ul>
													<li>No prior authorizations</li>
													<li>No step edits</li>
												</ul></li>
											<li>The majority of patients with commercial insurance <span>pay as little as $0 </span> for AURYXIA*
												<br><br>With the Copay Coupon, eligible patients may receive<sup>&dagger;</sup>:</li>
										</ul>
										<div class="tablets">
											<div class="cf">
												<div class="tablets-90"></div><div class="tablet-text"><span>For 90 tablets or less</span> <br/> Up to $500 off prescriptions</div>
											</div>
											<div class="cf">
												<div class="tablets-91-180"></div><div class="tablet-text"><span>For 91-180 tablets</span> <br/> Up to $1,000 off prescriptions</div>
											</div>
											<div class="cf">
												<div class="tablets-181-plus"></div><div class="tablet-text"><span>For 181 tablets or more</span> <br/>  Up to $1,500 off prescriptions</div>
											</div>
											<p class="footnote"><sup>&dagger;</sup>AURYXIA is supplied as bottles of 200-count 210 mg ferric iron tablets.</p>
										</div>
									</div>
								</div>
								<div class="patient-coverage cf">
									<div class="patient-coverage-img">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/shield-gray.png" alt="" />
									</div>
									<div class="patient-coverage-txt">
										<h3 class="gray">Uninsured Patients</h3>
										<ul>
											<li>Free AURYXIA may be provided to uninsured patients<sup>&Dagger;</sup></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="grid-9">
					<div class="contact-lg patient-coverage">
						<p class="title">Reimbursement help</p>
						<p><strong>Looking for personalized access assistance regardless of insurance type?</strong></p>
						<p>Call one of our dedicated <a href="/akebiacares/reimbursement-help">AkebiaCares Case Managers</a></p>

					</div>
				</div>
				<div class="grid-9">
					<p class="footnote-asterisk">*Restrictions may apply. Copay assistance is not valid for prescriptions reimbursed under Medicare, Medicaid, or similar federal or state programs.</p>
					<p class="footnote-asterisk"><sup>&Dagger;</sup>Patients with an income of up to 400% Federal Poverty Level (FPL) may be eligible for free AURYXIA.  Additional restrictions apply. For details, please contact an AkebiaCares Case Manager at 855-686-8601.</p>
					<p class="footnote-ref"><strong>Reference: 1.</strong> Data on File 24, Akebia Therapeutics, Inc.</p>
				</div>
			</div>

			<div class="grid-3 right line-bg desktop">
				<div class="cta-links">
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Keryx Patient Plus Forms" href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" target="_blank" class="cta animC cta-1">
						<span class="cta-title">Download the Copay Coupon </span>
						<span class="cta-text">Eligible patients with commercial insurance can pay as little as $0</span>
					</a>
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Download Copay Coupon Here" href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="cta animC cta-2">
						<span class="cta-title">Download the Enrollment Form</span>
						<span class="cta-text">AkebiaCares Case Managers are standing by to help your patients find coverage</span>
					</a>
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Connect with your Personal Case Manager" href="/akebiacares/reimbursement-help" class="cta animC cta-3">
						<span class="cta-title">Connect with a specialist</span>
						<span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
					</a>
				</div>
			</div>
			<div class="grid-3 right desktop">
				<p class="isi-jump">See <a href="#important-safety-information">Important Safety Information</a> below</p>
			</div>

		</div><!-- Single Page -->
		<?php get_template_part('template-parts/content','isi-akebiacares'); ?>
		<div class="cta-links mobile">
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Keryx Patient Plus Forms" href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" target="_blank" class="cta animC cta-1">
				<span class="cta-title">Download the Copay Coupon </span>
				<span class="cta-text">Eligible patients with commercial insurance can pay as little as $0</span>
			</a>
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Download Copay Coupon Here" href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="cta animC cta-2">
				<span class="cta-title">Download the Enrollment Form</span>
				<span class="cta-text">AkebiaCares Case Managers are standing by to help your patients find coverage</span>
			</a>
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Connect with your Personal Case Manager" href="/akebiacares/reimbursement-help" class="cta animC cta-3">
				<span class="cta-title">Connect with a specialist</span>
				<span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
			</a>
		</div>
	</div>
</div><!-- Content -->
