<div class="faqBody">
	<div class="container outerContainer ">
		<div class="backdropContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/Graphics_Backdrop.png" alt="" class="bgImgFaq">
		</div>
		<div class="bannerStripe">
			<p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
			<div class="left-triangle"></div>
			<div class="right-triangle"></div>
		</div>
		<div class="container innerContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/bannerSix.jpg" alt="FAQs" class="headerImg">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/faqMobileHeader.png" alt="FAQs" class="bannerMobile">
			<div class="contentInner">
				<span class="patientText-Header">Hypothetical patient portrayals.</span>
				<br>
				<p class="HeaderTxt" style="font-size:20px; line-heihgt: 23px;">FREQUENTLY ASKED QUESTIONS</p>
				<button class="btn expandBtn gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="Expand All">EXPAND ALL</button>
				<button class="btn collapseBtn gtm-accordion" data-gtm-event-action="Close" data-gtm-event-category="Accordion" data-gtm-event-label="Close All">COLLAPSE ALL</button>

				<div class="accordionSection">
					<ul class="accordion">

						<li class="accordionWrapper accordion1">
							<div class="toggle toggle1 gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="Most Important Information">
								<p class="faqP1">What is the most important information I should know about AURYXIA?</p>
								<img class="plusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/plus-icon.png">
								<img class="minusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/minus-icon.png">
							</div>
							<ul class="inner hide">

								<p class="innerP1">AURYXIA contains iron. Keep it away from children to prevent an accidental ingestion of iron and potentially fatal poisoning. Call a poison control center or your healthcare provider if a child swallows AURYXIA.</p>
								<p class="innerP1">AURYXIA can increase iron levels in your blood. Iron absorbed from AURYXIA may also increase iron in your body. Your healthcare provider will monitor your iron levels. If you are receiving intravenous (IV) iron, your IV iron dose may be adjusted or discontinued. </p>
							</ul>
						</li>


						<li class="accordionWrapper accordion2">
							<div class="toggle toggle2 gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="What is Auryxia">
								<p class="faqP1">What is AURYXIA?</p>
								<img class="plusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/plus-icon.png">
								<img class="minusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/minus-icon.png">
							</div>
							<ul class="inner hide">

								<p class="innerP1">For adults who have iron deficiency anemia, as well as CKD, and are NOT on dialysis, AURYXIA is a prescription medicine that can increase hemoglobin levels and increase iron levels in the body.</p>
							</ul>
						</li>


						<li class="accordionWrapper accordion3">
							<div class="toggle toggle3 gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="What is IDA">
								<p class="faqP1">What is iron deficiency anemia?</p>
								<img class="plusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/plus-icon.png">
								<img class="minusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/minus-icon.png">
							</div>
							<ul class="inner hide">

								<p class="innerP1">Common in patients with CKD, anemia is a condition that happens when there aren't enough healthy red blood cells to carry the oxygen your body needs. To make healthy red blood cells, your body needs iron and erythropoietin (a hormone made by the kidneys).</p>
								<p class="innerP1">Anemia in patients with CKD is often caused by iron deficiency, or not enough iron. This lack of iron may be caused by not having enough iron in your diet, not absorbing enough iron from your diet, or losing iron from blood loss.</p>

							</ul>
						</li>


						<li class="accordionWrapper accordion4">
							<div class="toggle toggle4 gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="How does Auryxia help">
								<p class="faqP1">How does AURYXIA help my iron deficiency anemia?</p>
								<img class="plusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/plus-icon.png">
								<img class="minusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/minus-icon.png">
							</div>
							<ul class="inner hide">

								<p class="innerP1">Once swallowed, the iron in AURYXIA changes form so it can be absorbed by the gut in the same way your body absorbs iron<br class="desktopBr"> from food. </p>
								<p class="innerP1">The iron is then transported through the gut and into the blood.</p>
								<p class="innerP1">Once in the bloodstream, the iron changes back to its original form so it can be combined with hemoglobin, the part of red blood cells that helps carry oxygen. </p>


							</ul>
						</li>


						<li class="accordionWrapper accordion5">
							<div class="toggle toggle5 gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="Who should not take Auryxia">
								<p class="faqP1">Who should not take AURYXIA?</p>
								<img class="plusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/plus-icon.png">
								<img class="minusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/minus-icon.png">
							</div>
							<ul class="inner hide">

								<p class="innerP1">Do not take AURYXIA if you have been diagnosed with an iron overload syndrome, such as hemochromatosis.</p>
								<p class="innerP1">AURYXIA has not been studied in children. </p>

							</ul>
						</li>



						<li class="accordionWrapper accordion6">
							<div class="toggle toggle6 gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="What should I tell my HCP before taking">
								<p class="faqP1">What should I tell my healthcare provider BEFORE taking AURYXIA?</p>
								<img class="plusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/plus-icon.png">
								<img class="minusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/minus-icon.png">
							</div>
							<ul class="inner hide">

								<p class="innerP1">AURYXIA may not be right for you. Before starting AURYXIA, tell your healthcare provider if you:</p>
								<ul class="ulBlock">
									<li><span>have any other medical conditions</span></li>
									<li><span>are pregnant, plan to become pregnant, are breastfeeding or plan to breastfeed</span></li>

								</ul>
								<p class="innerP1"><span class="refBold">Tell your healthcare provider about all the medicines you take, including:</span></p>
								<ul class="ulBlock">
									<li><span>the antibiotics doxycycline or ciprofloxacin</span></li>
									<li><span>prescription and over-the-counter medicines, vitamins, and herbal supplements</span></li>

								</ul>
								<p class="innerP1">Know the medicines you take. Keep a list of them and show it to your healthcare provider and pharmacist when you get a new medicine.</p>
							</ul>
						</li>



						<li class="accordionWrapper accordion7">
							<div class="toggle toggle7 gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="What should I tell my HCP after taking">
								<p class="faqP1">What should I tell my healthcare provider AFTER taking AURYXIA?</p>
								<img class="plusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/plus-icon.png">
								<img class="minusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/minus-icon.png">
							</div>
							<ul class="inner hide">

								<ul class="ulBlock ulBlock-Left">
									<li><span>Tell your healthcare provider if you have any side effect that bothers you or that does not go away</span></li>
									<li> <span class="AuryxiaBold">Call your healthcare provider for medical advice about side effects. You may report suspected side effects to Akebia&nbsp;Therapeutics,&nbsp;Inc. at <a class="gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="After taking 1-844-445-3799" href="tel:18444453799">1&#xfeff;-&#xfeff;844&#xfeff;-&#xfeff;445&#xfeff;-&#xfeff;3799</a> or FDA at <a href="tel:18003321088" class="gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="After taking 1-800-FDA-1088">1&#xfeff;-&#xfeff;800&#xfeff;-&#xfeff;FDA&#xfeff;-&#xfeff;1088</a> or <a target="_blank" href="https://www.fda.gov/safety/medwatch-fda-safety-information-and-adverse-event-reporting-program" class="gtm-cta" data-gtm-event-category="Outbound Link" data-gtm-event-action="Click" data-gtm-event-label="After taking www.fda.gov/medwatch">www.fda.gov/medwatch</a></span></li>
									<li><span>Let your healthcare provider know if you start a new medication. It may affect your AURYXIA treatment</span></li>

								</ul>
							</ul>
						</li>



						<li class="accordionWrapper accordion8">
							<div class="toggle toggle8 gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="How should I take">
								<p class="faqP1">How should I take AURYXIA?</p>
								<img class="plusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/plus-icon.png">
								<img class="minusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/minus-icon.png">
							</div>
							<ul class="inner hide">

								<ul class="ulBlock ulBlock-Left">
									<li><span>Take AURYXIA exactly as prescribed by your healthcare provider </span></li>
									<li><span>Take AURYXIA with meals and adhere to any diet prescribed by your healthcare provider</span></li>
									<li><span>Your healthcare provider will tell you how much AURYXIA to take and may change your dose if necessary</span></li>
									<li><span>Swallow AURYXIA whole. Do not chew or crush.</span></li>
									<li><span>If you are taking the antibiotics doxycycline or ciprofloxacin, you will need to take it separately from AURYXIA. Follow your healthcare provider’s instructions on when to take doxycycline or ciprofloxacin while you are also taking AURYXIA</span></li>

								</ul>
							</ul>
						</li>




						<li class="accordionWrapper accordion9">
							<div class="toggle toggle9 gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="What are possible side effects">
								<p class="faqP1">What are possible or reasonably likely side effects of AURYXIA?</p>
								<img class="plusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/plus-icon.png">
								<img class="minusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/minus-icon.png">
							</div>
							<ul class="inner hide">
								<p class="innerP1">The most common side effects of AURYXIA for use in the treatment of iron deficiency anemia in adult patients with CKD not on dialysis include:</p>

								<ul class="ulBlock">
									<li><span>diarrhea</span></li>
									<li><span>constipation</span></li>
									<li><span>nausea</span></li>
									<li><span>abdominal pain</span></li>
									<li><span>high levels of potassium in the blood</span></li>


								</ul>
								<p class="innerP1">AURYXIA contains iron and may cause dark stools, which are considered normal with oral medications containing iron.</p>

								<p class="innerP1"> These are not all the side effects of AURYXIA. For more information ask your healthcare provider or pharmacist.</p>
								<p class="innerP1">Tell your healthcare provider if you have any side effect that bothers you or that does not go away. </p>
								<p class="innerP1 strongText">Call your healthcare provider for medical advice about side effects. You may report suspected side effects to Akebia Therapeutics, Inc. at <a class="gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Side effects 1-844-445-3799" href="tel:18444453799">1&#xfeff;-&#xfeff;844&#xfeff;-&#xfeff;445&#xfeff;-&#xfeff;3799</a> or FDA at <a href="tel:18003321088" class="gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Side effects 1-800-FDA-1088">1&#xfeff;-&#xfeff;800&#xfeff;-&#xfeff;FDA&#xfeff;-&#xfeff;1088</a> or <a target="_blank" href="https://www.fda.gov/safety/medwatch-fda-safety-information-and-adverse-event-reporting-program" class="gtm-cta" data-gtm-event-category="Outbound Link" data-gtm-event-action="Click" data-gtm-event-label="Side effects www.fda.gov/medwatch">www.fda.gov/medwatch</a></p>

							</ul>
						</li>




						<li class="accordionWrapper accordion10">
							<div class="toggle toggle10 gtm-accordion" data-gtm-event-action="Expand" data-gtm-event-category="Accordion" data-gtm-event-label="How should I store">
								<p class="faqP1">How should I store AURYXIA?</p>
								<img class="plusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/plus-icon.png">
								<img class="minusIcon"  src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/minus-icon.png">
							</div>
							<ul class="inner hide">

								<ul class="ulBlock ulBlock-Left">
									<li><span>Store AURYXIA between 68 to 77°F (20 to 25°C)</span></li>
									<li><span>Keep AURYXIA tablets dry</span></li>
								</ul>
							</ul>
						</li>

					</ul>
				</div>






				<div class="isi">
					<?php //include 'includes/isi.php'; ?>
					<?php get_template_part('template-parts/content-ida-patient', 'isi'); ?>
				</div>



			</div>



		</div>
	</div>
</div>
