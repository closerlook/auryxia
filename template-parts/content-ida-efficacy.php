<section class='container col-centered page-content'>
	<section class='col-md-12 col-centered interior'>
		<div class='post-titles'>
			<h2>EFFICACY</h2>
		</div>
		<section class='col-md-9 left copy'>
			<h2>Efficacy was demonstrated in patients who were previously intolerant of or had an inadequate therapeutic response to traditional oral iron supplements<sup>1</sup></h2>
			<section>
				<!--TABS COMPONENT HERE-->
				<div id="tab-wrap">

					<div class="tab-names">
						<a href="javascript:void(0)" class="animC active" data-element="Chart" data-category="Chart" data-action="Click" data-label="Hemoglobin Tab">Hemoglobin</a>
						<a href="javascript:void(0)" class="animC" data-element="Chart" data-category="Chart" data-action="Click" data-label="Iron Parameters Tab">Iron Parameters</a>
					</div>
					<div class="tabs">
						<div class="tab active">
							<h3>52% of patients treated with AURYXIA achieved a hemoglobin (Hgb) increase of ≥1.0 g/dL at any time point by Week 16<sup>1</sup></h3>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/efficacy/tab1a.png" alt="" class='desktop'>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/efficacy/tab1a-mobile.png" alt="" class='mobile'>
							<h3>Mean Hgb increased from 10.4 g/dL to 11.4 g/dL by the end of the 16-week period<sup>2</sup></h3>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/efficacy/tab1b.png" alt="" class='desktop'>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/efficacy/tab1b-mobile.png" alt="" class='mobile'>
						</div>

						<div class="tab">
							<h3>Mean TSAT increased from 20.2% at baseline to 35.6% at Week 16<sup>5</sup></h3>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/efficacy/tab2.png" alt="" class='desktop'>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/efficacy/tab2-mobileEfficacy.png" alt="" class='mobile'>
							<h3>In the pivotal trial, mean TSAT in patients treated with AURYXIA was 36% at the end of the 16-week period<sup>5</sup></h3>
							<h3>Patients on AURYXIA also achieved a mean increase in serum ferritin of 163 ng/mL from baseline (85.9 ng/mL) at Week 16<sup>1,3</sup></h3>
						</div>
					</div>
				</div>
				<!--tab-wrap end-->
			</section>
			<!--TABS COMPONENT END-->
			<section id='trial-design'>
				<!--Bootstrap Collapse start -->

				<div class="collapse" id="trial-design-collapse">
					<span>Trial Design<sup>1</sup></span>
					<p>In a 24-week study consisting of a 16-week, randomized, double-blind, placebo-controlled
						efficacy period followed by an 8-week, open-label safety extension
						period, this trial evaluated the efficacy and safety of AURYXIA for the treatment
						of iron deficiency anemia in adult patients with CKD not on dialysis. Patients who
						were intolerant of or have had an inadequate therapeutic response to oral iron
						supplements, with hemoglobin ≥9.0 g/dL and ≤11.5 g/dL, serum ferritin ≤200 ng/mL,
						and TSAT ≤25% were enrolled. Patients were randomized to treatment with either
						AURYXIA (n=117) or placebo (n=117).</p>
					<p>The primary endpoint was the proportion of patients achieving a ≥1.0 g/dL increase
						in hemoglobin at any time point during the 16-week efficacy period. Use of oral iron,
						IV iron, and ESAs was not permitted at any time during the trial.</p>

					<div id='trial-design-expanded'>
						<h3>Studied in a Phase III pivotal trial of patients who were intolerant of or had inadequate response to traditional oral iron therapy</h3>
						<img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/efficacy/trial-designEfficacy.png' alt='' class='desktop'>
						<img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/efficacy/Phase_3_Chart.png' alt='' class='mobile'>

						<h3>Primary efficacy endpoint<sup>1</sup></h3>
						<ul>
							<li style="margin-left: -10px;">Proportion of patients achieving an increase in hemoglobin concentration of ≥1.0&nbsp;g/dL from baseline at any point through the end of the randomized period (Week 16)</li>
						</ul>

						<h3>Key secondary efficacy endpoints included<sup>3</sup>:</h3>
						<ul>
							<li style="margin-left: -10px;">Mean changes in hemoglobin, TSAT, ferritin, and phosphorus from baseline to Week 16</li>
							<li style="margin-left: -10px;">Proportion of patients experiencing a sustained treatment effect on hemoglobin (≥0.75 g/dL mean change in hemoglobin from baseline over any 4-week period provided that an increase of at least 1.0&nbsp;g/dL had occurred
								during that 4-week period)</li>
						</ul>

						<h3>Key inclusion criteria<sup>1</sup></h3>
						<ul>
							<li style="margin-left: -10px;">Adult patients with CKD not on dialysis (eGFR
								<60 mL/min/1.73 m<sup>2</sup>) and iron deficiency anemia (hemoglobin ≥9.0 and ≤11.5 g/dL, ferritin&nbsp;≤200&nbsp;ng/mL and TSAT ≤25%) at screening</li>
							<li style="margin-left: -10px;">Intolerant of or have had an inadequate therapeutic response to oral iron supplements</li>
						</ul>

						<h3>Key exclusion criteria<sup>4</sup></h3>
						<ul>
							<li style="margin-left: -10px;">IV iron, ESAs, or blood transfusion administered within 4 weeks prior to screening</li>
							<li style="margin-left: -10px;">Serum phosphorus
								<3.5 mg/dL at screening</li>
							<li style="margin-left: -10px;">Cause of anemia other than iron deficiency or CKD</li>
						</ul>
					</div>
				</div>

				<a role="button" class="tab-toggle collapsed" data-toggle="collapse" href="#trial-design-collapse" aria-expanded="false" aria-controls="trial-design-collapse" data-element="Toggle" data-category="Trial Design Views" data-action="Toggle Click" data-label="Closed">
					<div id='trial-design-toggle' class='trial-design-toggle'>
						<span class='toggle-on'>Show Full Trial Design</span>
						<span class='toggle-off'>Hide Full Trial Design</span>
						<div class='trial-design-arrow'></div>
					</div>
				</a>

			</section>
			<!--Bootstrap Collapse end -->

			<h6>ESAs=erythropoiesis-stimulating agents; IV=intravenous; eGFR=estimated Glomerular Filtration Rate.</h6>

		</section>
		<section class="col-md-3 right">
			<div class="cta-links">
				<a href="/akebiacares" class="cta animC cta-1" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Eligible Patients Pay $0">
					<span class="cta-title">ELIGIBLE PATIENTS CAN PAY $0</span>
					<span class="cta-text">Patients with commercial insurance can pay as little as $0 for AURYXIA</span>
				</a>
				<a href="/iron-deficiency-anemia/dosing" class="cta animC cta-2" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Dosing">
					<span class="cta-title">DOSING</span>
					<span class="cta-text">210 mg of elemental iron per tablet</span>
				</a>
				<a href="/iron-deficiency-anemia/tolerability-and-safety" class="cta animC cta-3" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Safety">
					<span class="cta-title">SAFETY</span>
					<span class="cta-text">Safety and tolerability profile evaluated in a pivotal clinical trial</span>
				</a>
			</div>
			<div class="cta-isi-sidebar">
				<a href="javascript:void(0)" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="ISI"><span>See <span style="text-decoration: underline;">Important Safety Information</span> below</span></a>
			</div>
			<img class='desktop' src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/sidebar-peacock.png' alt=''>
		</section>
	</section>
</section>
<!-- HTML include -->
<section id='isi-content'>
	<?php get_template_part('template-parts/content', 'isi'); ?>
</section>
<!-- References -->
<section class="references container">
	<div class='col-md-12 col-centered'>
		<span>References</span>
		<ol>
			<li>AURYXIA [package insert]. Cambridge, MA: Akebia Therapeutics, Inc.; 2017.</li>
			<li>Data on File 16, Akebia Therapeutics, Inc.</li>
			<li>Fishbane S, Block GA, Loram L, et al. Effects of ferric citrate in patients with nondialysis-dependent CKD and iron deficiency anemia. <i class='nowrap'>J Am Soc Nephrol.</i> 2017;28(6):1851-1858.</li>
			<li>Supplement to: Fishbane S, Block GA, Loram L, et al. Effects of ferric citrate in patients with nondialysis-dependent CKD and iron deficiency anemia. <i class='nowrap'>J Am Soc Nephrol.</i> 2017;28(6):1851-1858.</li>
			<li>Data on File 14, Akebia Therapeutics, Inc.</li>


		</ol>
	</div>
</section>

