<section class='gateway-bottom container'>
	<div class='col-centered'>
		<img id='logo' src='<?php echo get_template_directory_uri(); ?>/assets/img/Auryxia-logo.svg' alt='Auryxia'>
	</div>

	<div class="rightSide">
		<div class="idaFlag">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/IDAflag.png" alt="" class="blueFlag">
			<div class="idaText">
				<p class="idaTxtHead">Iron Deficiency Anemia <span class="lightCopy">CKD Not on Dialysis</span></p>
				<hr class="idaHr">
				<p class="idaTxtBody">Learn about the only oral iron approved for adults with CKD not on dialysis</p>
			</div>

			<a href="/iron-deficiency-anemia/"> <button class="btn ida-Hcp">  <img src="<?php echo get_template_directory_uri(); ?>/assets/img/medIcon.png" alt="" class="leftIcon">VISIT HCP SITE<img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow.png" alt="" class="arrowIcon"></button></a>

			<a href="/iron-deficiency-anemia/patient/"><button class="btn ida-patient"> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/PatientIcon.png" alt="" class="leftIcon"><span class="patientTxt">VISIT PATIENT SITE<img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow.png" alt="" class="arrowIcon"></span></button></a>
		</div>


		<div class="hyperFlag">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/HPFlag.png" alt="" class="orangeFlag">

			<div class="idaText">
				<p class="idaTxtHead">Hyperphosphatemia <span class="lightCopy">CKD on Dialysis</span></p>
				<hr class="idaHr">
				<p class="idaTxtBody">Explore a non-calcium, non-chewable choice for the control of serum phosphorus levels</p>
			</div>

			<a href="/hyperphosphatemia"> <button class="btn hyper-Hcp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/medIcon1.png" alt="" class="leftIcon">VISIT HCP SITE<img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow.png" alt="" class="arrowIcon"></button></a>

			<a href="/hyperphosphatemia/patients/">    <button class="btn hyper-patient"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/PatientIcon1.png" alt="" class="leftIcon"><span class="patientTxt">VISIT PATIENT SITE<img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow.png" alt="" class="arrowIcon"></span></button></a>


		</div>

	</div>

	<div class="ctaSectionMobile">

		<p class="chooseTxt">Choose an AURYXIA indication below</p>

		<div class="boxOne">

			<p>Iron Deficiency Anemia<br><span>CKD Not on Dialysis</span></p>

			<div class="buttonOverlay">
				<a href="/iron-deficiency-anemia/"><button class="btn ida-HcpMobile"><span class="mobileBtnSpan">VISIT HCP SITE</span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow.png" alt="" class="arrowIconMobile"></button></a>

				<a href="/iron-deficiency-anemia/patient/"><button class="btn ida-patientMobile"><span class="mobileBtnSpan">VISIT PATIENT SITE</span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow.png" alt="" class="arrowIconMobile"></span></button></a>
			</div>
		</div>

		<div class="boxTwo">

			<p>Hyperphosphatemia<br><span>CKD on Dialysis</span></p>

			<div class="buttonOverlay">
				<a href="/hyperphosphatemia"><button class="btn ida-HcpMobile2"><span class="mobileBtnSpan">VISIT HCP SITE</span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow.png" alt="" class="arrowIconMobile"></button></a>

				<a href="/hyperphosphatemia/patients/"><button class="btn ida-patientMobile2"><span class="mobileBtnSpan">VISIT PATIENT SITE</span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow.png" alt="" class="arrowIconMobile"></span></button></a>
			</div>

		</div>
	</div>
</section>
