<div class="content content-1">
	<div class="grid interior cf">
		<div class="post-titles">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-reimbursement-help.jpg" class="header-desktop" alt="Reimbursement Help" title="Reimbursement Help">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-reimbursement-help-mobile.jpg" class="header-mobile" alt="Reimbursement Help" title="Reimbursement Help" style="width: 100%">
		</div>
		<div class="grid-12 interior cf">
			<div class="grid-9 left copy">
				<h2>ONE-ON-ONE SUPPORT FOR INSURANCE QUESTIONS AND CHALLENGES</h2>

				<p class="subtitle">With AkebiaCares, your patients will have a dedicated case manager with the knowledge and resources to help make understanding access to AURYXIA as easy as&nbsp;possible</p>

				<div id="tab-wrap">
					<div class='tab-names cf'>
						<a href="#" class="animC active">BEFORE ENROLLMENT</a>
						<a href="#" class="animC ">AFTER ENROLLMENT</a>
					</div>
					<div class="tabs cf">
						<div class="tab active">
							<h2>Even without filling out the AkebiaCares Enrollment Form, our case managers still offer a variety of services</h2>
							<ul>
								<li>Share reimbursement services offered</li>
								<li>Handle questions about the AkebiaCares Enrollment Form</li>
								<li>Provide education about the Copay and Patient Assistance Programs</li>
								<li>Research eligibility status for Low-Income Subsidies (LIS)</li>
							</ul>
							<a href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="download-link">Download the Enrollment Form</a>
						</div>
						<div class="tab">
							<h2>After the completed AkebiaCares Enrollment Form has been received, your patient’s dedicated case manager can:</h2>
							<ul>
								<li>Share reimbursement services offered</li>
								<li>Handle questions about the AkebiaCares Enrollment Form</li>
								<li>Provide education about the Copay and Patient Assistance Programs</li>
								<li>Research eligibility status for Medicare and/or Low-Income Subsidies (LIS)</li>
								<li><strong>Verify patient benefits</strong></li>
								<li><strong>Coordinate prior authorizations</strong></li>
								<li><strong>Assist patients experiencing therapy interruption</strong></li>
								<li><strong>Investigate options for patients with financial obstacles to AURYXIA therapy</strong></li>
							</ul>
							<p class="connect phone-fix">Connect with an AkebiaCares Case Manager now at <span>855-686-8601</span></p>
						</div>
					</div>
				</div>

				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/reimbursement-help.jpg" class="desktop" border="0">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/reimbursement-help-mobile.jpg" class="mobile" border="0">

				<p class="footnote phone-fix">*If you have any questions about an enrollment form after it has been submitted to AkebiaCares, please call 855-686-8601.</p>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/reimbursement-help-overview.jpg" class="desktop" border="0">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/reimbursement-help-overview-mobile.jpg" class="mobile" border="0">

				<p class="reimbursement-subhead"><strong>CALLING AFTER HOURS?</strong></p>
				<p>Leave a message and our AkebiaCares experts will return your call by the next business day</p>
			</div>

			<div class="grid-3 right line-bg desktop">
				<div class="cta-links">
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Keryx Patient Plus Forms" href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="cta animC cta-1">
						<span class="cta-title">Download the Enrollment Form</span>
						<span class="cta-text">AkebiaCares Case Managers are standing by to help your patients find coverage</span>
					</a>
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Connect with your Personal Case Manager" href="/akebiacares/reimbursement-help" class="cta animC cta-2">
						<span class="cta-title">Connect with a specialist</span>
						<span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
					</a>
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Download Copay Coupon Here" href="/akebiacares/patient-coverage" class="cta animC cta-3">
						<span class="cta-title">Explore Coverage by Indication</span>
						<span class="cta-text">See what types of insurance cover AURYXIA</span>
					</a>
				</div>
			</div>
			<div class="grid-3 right desktop">
				<p class="isi-jump">See <a href="#important-safety-information">Important Safety Information</a> below</p>
			</div>
		</div><!-- Single Page -->
		<?php get_template_part('template-parts/content','isi-akebiacares'); ?>
		<div class="cta-links mobile">
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Keryx Patient Plus Forms" href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="cta animC cta-1">
				<span class="cta-title">Download the Enrollment Form</span>
				<span class="cta-text">AkebiaCares Case Managers are standing by to help your patients find coverage</span>
			</a>
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Connect with your Personal Case Manager" href="/akebiacares/reimbursement-help" class="cta animC cta-2">
				<span class="cta-title">Connect with a specialist</span>
				<span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
			</a>
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Download Copay Coupon Here" href="/akebiacares/patient-coverage" class="cta animC cta-3">
				<span class="cta-title">Explore Coverage by Indication</span>
				<span class="cta-text">See what types of insurance cover AURYXIA</span>
			</a>
		</div>
	</div>
</div><!-- Content -->
