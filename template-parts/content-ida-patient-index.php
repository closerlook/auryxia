<div class="layoutBody">
	<div class="container outerContainer ">
		<div class="backdropContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/Graphics_Backdrop.png" alt="" class="bgImgFaq">
		</div>
		<div class="container innerContainer">
			<div class="homeImgContainer">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/home/homeHeaderImg.png" alt="The only oral iron designed for you" class="headerImg index-DesktopHeader indexDesktopBg">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/home/homeHeaderImgOld.png" alt="The only oral iron designed for you" class="headerImg index-DesktopHeader indexDesktopbg991">
				<p>AURYXIA is the only iron tablet that's approved to treat<br class="desktopBr"> iron deficiency anemia specifically in adults with chronic<br class="desktopBr">kidney disease (CKD) not on dialysis</p>
				<span class="patientTextHome">Hypothetical patient portrayals.</span>
			</div>

			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/mobileHeader-Index.png" alt="" class="bannerMobile index-MobileHeader" id="MobileHeader">
			<!-- <span class="patientTextHome-mobile">Hypothetical patient portrayals.</span> -->
			<div class="contentInner">

				<div class="container boxContainers">
					<div class="row">

						<div class="col-md box boxOne gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="What is Iron Deficiency Anemia">
							<p class="p1">WHAT IS IRON DEFICIENCY ANEMIA?</p>
							<p class="p2" id="homep2Txt">Learn more about your condition</p>
						</div>

						<div class="col-md box boxTwo gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Auryxia 101">
							<p class="p1">AURYXIA 101</p>
							<p class="p2" id="homep2Txt">Explore how AURYXIA can help</p>
						</div>

						<div class="col-md box boxThree gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Save on the Cost of Auryxia">
							<p class="p1">SAVE ON THE COST OF AURYXIA</p>
							<p class="p2 " id="homep2Txt">Depending on your insurance, you may get AURYXIA for free</p>
						</div>
					</div>
				</div>


				<div class="mobileMidBox">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/home/assistance-icon.png" alt="Access assistant icon" class="assistance-icon">
					<p class="assistance-p1">LOOKING FOR PERSONALIZED ACCESS ASSISTANCE?</p>
					<p class="assistance-p2">Call one of our dedicated AkebiaCares Case Managers</p>
					<div class="centerBox">


						<ul class="nav justify-content-center">
							<li class="nav-item navBorder">
								<a class="nav-link gtm-cta" href="tel:8556868601" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="call 855-686-8601">855-686-8601</a>
							</li>
							<li class="nav-item navBorder">
								<a class="nav-link disabled">Monday-Friday</a>
							</li>
							<li class="nav-item">
								<a class="nav-link disabled" >8<span class="AM">AM</span>-7<span class="AM">PM</span> ET</a>
							</li>
						</ul>
					</div>
					<p class="isiTxt">See <a data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important Safety Information</a> below</p>
				</div>


				<div class="container">
					<div class="row">
						<div class="col-md-3">

						</div>
						<div class="col-md-6 midBox">
							<div class="assistanceBox">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/home/assistance-icon.png" alt="Access assistant icon" class="assistance-icon">
								<div class="textBox">
									<p class="assistance-p1">LOOKING FOR PERSONALIZED ACCESS ASSISTANCE?</p>
									<p class="assistance-p2">Call one of our dedicated AkebiaCares Case Managers</p>

									<div class="centerBox">


										<ul class="nav">

											<li class="nav-item navBorder">
												<a class="nav-link gtm-cta" href="tel:8556868601" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="call 855-686-8601">855-686-8601</a>
											</li>

											<li class="nav-item navBorder">
												<a class="nav-link disabled" >Monday-Friday</a>
											</li>
											<li class="nav-item">
												<a class="nav-link disabled" >8<span class="AM">AM</span>-7<span class="AM">PM</span> ET</a>
											</li>
										</ul>

									</div>


									<p class="isiTxt homeIsiSee">See <a class="gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important Safety Information</a> below</p>
								</div>
							</div>

						</div>
						<div class="col-md-3">

						</div>
					</div>
				</div>


				<div class="isi">
					<?php //include 'includes/isi.php'; ?>
					<?php get_template_part('template-parts/content-ida-patient', 'isi'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
