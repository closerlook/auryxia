<section class='container deskIdaFoot'>
	<div class="col-md-12 col-centered">
		<div id="footer-info" class="col-md-9 footer-links">
			<div class="mobileOverlayIDA">
				<ul id="menu-footer-links" class="menu idaMenu">
					<li class="menu-item idaFooterMenu-Item"><a target="_blank" href="https://akebia.com/contact/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Contact Us">Contact Us</a> <div class="borderRightFooterDesktop"></div></li>

					<li class="menu-item idaFooterMenu-Item"><a target="_blank" href="https://akebia.com/legal/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Terms & Conditions">Terms and Conditions</a><div class="borderRightFooterDesktop"></div></li>

					<li class="menu-item idaFooterMenu-Item"><a target="_blank" href="https://akebia.com/legal/privacy-policy.aspx" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Privacy Policy">Privacy Policy</a><div class="borderRightFooterDesktop"></div></li>

					<li class="menu-item idaFooterMenu-Item"><a target="_blank" href="https://akebia.com/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Akebia.Com">AKEBIA.COM</a></li>
				</ul>
				<?php if (is_page('iron-deficiency-anemia')) { ?>
					<div class="legal idaLegal">©2020 Akebia Therapeutics, Inc.
						<div class="footer-spacer"></div><span class="nowrap">PP-AUR-US-0981</span>
						<div class="footer-spacer"></div>01/20
					</div>

				<?php } else if ((is_page('sign-up')) || (is_page('thank-you')) || (is_page('error')))  {  ?>
					<div class="legal idaLegal">©2020 Akebia Therapeutics, Inc.
						<div class="footer-spacer"></div><span class="nowrap">PP-AUR-US-1219 11/20</span>
					</div>

				<?php } else {  ?>
					<div class="legal idaLegal">©2019 Akebia Therapeutics, Inc.
						<div class="footer-spacer"></div><span class="nowrap">PP-AUR-US-0925</span>
						<div class="footer-spacer"></div>10/19
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="col-md-3 left footer-legal">
			<a href="http://akebia.com" target="_blank" class="whitelisted" data-element="default" data-category=“Footer” data-action="Click" data-label="Akebia Logo"><img class="idaFooterImg" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-akebia-therapeutics.png" alt="Akebia"></a>
		</div>
	</div>
</section>
