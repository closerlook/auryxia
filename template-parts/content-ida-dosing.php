<section class='container col-centered page-content'>
	<section class='col-md-12 col-centered interior'>
		<div class='post-titles'>
			<h2>DOSING AND ADMINISTRATION<sup>1</sup></h2>
		</div>
		<section id='page-dosing' class='col-md-9 left page-content copy'>
			<h3 id='page-dosing-first'>210 mg of elemental iron per tablet with convenient mealtime dosing</h3>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/dosing/dosing-img1.png" alt="" class='dosing-img1 desktop'>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/dosing/dosing-img1-mobile.png" alt="" class='dosing-img1 mobile'>
			<h3>Oral drugs that have to be <span>separated</span> from AURYXIA and meals<sup>1</sup></h3>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/dosing/dosing-bottle.png" alt="" class='dosing-bottle-img'>
			<ul>
				<li style="margin-left: 45px;">Doxycycline—Take at least 1 hour before AURYXIA</li>
				<li style="margin-left: 45px;">Ciprofloxacin—Take at least 2 hours before or after AURYXIA</li>
			</ul>
			<h3>Oral drugs that can be <span>administered concomitantly</span> with AURYXIA<sup>1</sup></h3>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/dosing/dosing-img2.png" alt="" class='dosing-img2 desktop'>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida/dosing/dosing-img2-mobile.png" alt="" class='dosing-img2 mobile'>
		</section>
		<section class="col-md-3 right">
			<div class="cta-links">
				<a href="/iron-deficiency-anemia/tolerability-and-safety" class="cta animC cta-1" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Safety">
					<span class="cta-title">SAFETY</span>
					<span class="cta-text">Safety and tolerability profile evaluated in a pivotal clinical trial</span>
				</a>
				<a href="/iron-deficiency-anemia/moa" class="cta animC cta-2" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="MOA">
					<span class="cta-title">MOA</span>
					<span class="cta-text">Discover how AURYXIA works</span>
				</a>
				<a href="/akebiacares" class="cta animC cta-3" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Expanded Coverage">
					<span class="cta-title">Comprehensive COVERAGE</span>
					<span class="cta-text">Eligible patients with commercial insurance can pay as little as $0 for AURYXIA</span>
				</a>
			</div>
			<div class="cta-isi-sidebar">
				<a href="javascript:void(0)" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="ISI"><span>See <span style="text-decoration: underline;">Important Safety Information</span> below</span></a>
			</div>
			<img class='desktop' src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/sidebar-peacock.png' alt=''>
		</section>
	</section>
</section>
<!-- HTML includes -->
<section id='isi-content'>
	<?php get_template_part('template-parts/content', 'isi'); ?>
</section>
<!-- References -->
<section class="references container">
	<div class='col-md-12 col-centered'>
		<span>Reference</span>
		<ol>
			<li>AURYXIA [package insert]. Cambridge, MA: Akebia Therapeutics, Inc.; 2017.</li>
		</ol>
	</div>
</section>
