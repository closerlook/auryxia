<div class="content content-1">
	<div class="grid interior cf">
		<div class="post-titles">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-patient-information.jpg" class="header-desktop" alt="Patient Information" title="Patient Information">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-patient-information-mobile.jpg" class="header-mobile" alt="Patient Information" title="Patient Information" style="width: 100%">
		</div>
		<div class="grid-12 interior cf">
			<div class="grid-9 left copy">
				<h2>DISCOVER WHICH AKEBIACARES PROGRAM BEST FITS YOUR TYPE OF INSURANCE</h2>
				<p class="subtitle">If you have been prescribed AURYXIA, AkebiaCares is a way for you to get extra support so you can get the most out of your treatment</p>
				<h3>Have medicare?</h3>
				<div class="cf">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/medicare-yes.png" width="330" class="left">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/medicare-no.png" width="330" class="right">
				</div>
				<h3>Have commercial insurance?</h3>
				<div class="cf">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/insurance-yes.png" width="330" class="left">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/insurance-no.png" width="330" class="right">
				</div>
				<h3>Have questions?</h3>
				<div class="cf">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/questions-yes.png" width="330" class="left">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/questions-no.png" width="330" class="right">
				</div>
				<div class="contact-lg">
					<p class="title phone-fix">questions about Akebiacares? <span class="nobreak">Call 855-686-8601</span></p>
					<p class="phone-fix">Your healthcare professional can call <span>855-686-8601</span> to learn more about the services we offer patients. However, if you have questions about getting financial support through our programs, call one of our AkebiaCares Case Managers directly.</p>
				</div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/reimbursement-help-overview.jpg" class="desktop" border="0">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/reimbursement-help-overview-mobile.jpg" class="mobile" border="0">
				<p>AkebiaCares does not guarantee coverage and/or reimbursement for all patients.</p>
			</div>

			<div class="grid-3 right line-bg desktop">
				<div class="cta-links">
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="DOWNLOAD YOUR KERYX PATIENT PLUS APPLICATION" href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" target="_blank" class="cta animC cta-1">
						<span class="cta-title">Download the copay coupon</span>
						<span class="cta-text">Eligible patients with commercial insurance can pay as little as $0</span>
					</a>
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="EXPLORE COVERAGE BY INDICATION" href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="cta animC cta-2">
						<span class="cta-title">Download the Enrollment Form</span>
						<span class="cta-text">AkebiaCares Case Managers are standing by to help your patients find coverage</span>
					</a>
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="QUESTIONS? CALL YOUR KERYX CASE MANAGER" href="/akebiacares/reimbursement-help" class="cta animC cta-3">
						<span class="cta-title">Connect with a specialist </span>
						<span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
					</a>
				</div>
			</div>
			<div class="grid-3 right desktop">
				<p class="isi-jump">See <a href="#important-safety-information">Important Safety Information</a> below</p>
			</div>
		</div><!-- Single Page -->

		<?php get_template_part('template-parts/content','isi-akebiacares'); ?>

		<div class="cta-links mobile">
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="DOWNLOAD YOUR KERYX PATIENT PLUS APPLICATION" href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" target="_blank" class="cta animC cta-1">
				<span class="cta-title">Download the copay coupon</span>
				<span class="cta-text">Eligible patients with commercial insurance can pay as little as $0</span>
			</a>
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="EXPLORE COVERAGE BY INDICATION" href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="cta animC cta-2">
				<span class="cta-title">Download the Enrollment Form</span>
				<span class="cta-text">AkebiaCares Case Managers are standing by to help your patients find coverage</span>
			</a>
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="QUESTIONS? CALL YOUR KERYX CASE MANAGER" href="/akebiacares/reimbursement-help" class="cta animC cta-3">
				<span class="cta-title">Connect with a specialist </span>
				<span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
			</a>
		</div>
	</div>
</div><!-- Content -->
