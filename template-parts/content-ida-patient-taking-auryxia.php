<?php
?>
<div class="layoutBody">
	<div class="container outerContainer ">
		<div class="backdropContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/Graphics_Backdrop.png" alt="" class="bgImgFaq">
		</div>
		<div class="bannerStripe">
			<p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
			<div class="left-triangle"></div>
			<div class="right-triangle"></div>
		</div>
		<div class="container innerContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/bannerThree.png" alt="Taking Auryxia" class="headerImg">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/auryxia/takingAuryxiaMobileHeader.png" alt="Taking Auryxia" class="bannerMobile">

			<div class="contentInner">
				<span class="patientText-Header">Hypothetical patient portrayals.</span>
				<div class="row">


					<div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
						<br class="hiddenBrDesktop">
						<p class="diseaseP1">Here's what you need to know</p>
						<p class="diseaseP2">Keep the information below in mind, but remember to always<br class="desktopBr"> take AURYXIA exactly as it was prescribed by your healthcare provider</p>

						<br>
						<div class="takingUl">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/auryxia/Icon1.png" alt="">
							<ul>

								<p class="takingP1">DOSING SCHEDULE</p>


								<li><span>
                                                <p class="takingP2">Starting dose:<br class="mobileBrOnly"> 1 tablet 3 times a day</p>
                                            </span></li>

								<li><span>
                                                <p class="takingP2">Maximum dose:<br class="mobileBrOnly"> 12 tablets a day</p>
                                            </span></li>

								<li><span>
                                                <p class="takingP2">Take AURYXIA with meals </p>
                                            </span></li>

								<li><span>
                                                <p class="takingP2">AURYXIA should only be swallowed and not chewed or crushed</p>
                                            </span></li>
							</ul>

						</div>


						<div class="takingUl takingUlTwo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/auryxia/Icon2.png" alt="">
							<ul>

								<p class="takingP1">IF YOU MISS A DOSE</p>


								<li><span>
                                                <p class="takingP2">Do not take extra medicine to make up the missed dose</p>
                                            </span></li>

								<li><span>
                                                <p class="takingP2">Take the next dose with your meal, as prescribed</p>

                                            </span></li>

								<p class="takingP3">Your doctor may adjust the dose to fit your needs.<br>Always take AURYXIA as it is prescribed.</p>
							</ul>

						</div>


						<div class="takingUl takingUlThree">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/auryxia/Icon3.png" alt="">
							<ul>

								<p class="takingP1">TAKING AURYXIA WITH OTHER MEDICATIONS</p>


								<li><span>
                                                <p class="takingP2">Doxycycline—Take at least 1 hour before AURYXIA</p>
                                            </span></li>

								<li><span>
                                                <p class="takingP2">Ciprofloxacin—Take at least 2 hours before or after AURYXIA</p>

                                            </span></li>

								<p class="takingP3">Be sure to tell your doctor about all the medications you take, including<br> over-the-counter medicine, vitamins, and herbal supplements.</p>
							</ul>

						</div>


						<div class="takingUl takingUlThree">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/auryxia/Icon4.png" alt="">
							<ul>

								<p class="takingP1">YOU MAY NOTICE</p>


								<li><span>
                                                <p class="takingP2">Dark stools, which is considered normal with oral medications containing iron</p>
                                            </span></li>

								<li><span>
                                                <p class="takingP2">Diarrhea, which was reported as having no disruption to a moderate disruption<br class="desktopBr"> in normal daily activity in the majority of cases during the clinical trials<sup>1-3</sup></p>

                                            </span></li>

								<li><span>
                                                <p class="takingP2">Nausea, constipation, vomiting, abdominal pain, and cough</p>

                                            </span></li>

								<p class="takingP3">Talk to your doctor if you are taking other medications that may<br class="desktopBr"> affect these symptoms, such as stool softeners or laxatives.</p>
							</ul>

						</div>


						<p class="footnote footNoteTaking"><span class="refBold">References: 1.</span> Fishbane S, Block GA, Loram L, et al. Effects of ferric citrate in patients with nondialysis-dependent CKD and iron deficiency anemia. <em>J Am Soc Nephrol.</em> 2017;28(6):1851-1858. <span class="refBold">2.</span> Data on File 13, Akebia Therapeutics, Inc. <span class="refBold">3.</span> Data on File 27, Akebia Therapeutics, Inc.</p>
						<br>
					</div> <!-- CONTENT HERE END -->





					<div class="col-md-3 rightContent">

						<div class="rightBoxOne takingBox1 gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Auryxia 101">
							<p class="rightP1">AURYXIA 101</p>
							<p id="rightP2" class="rightP2 takingRightP2">Explore how AURYXIA can help</p>
						</div>

						<div class="rightBoxTwo takingBox2 gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="What is Iron Deficiency Anemia">
							<p class="rightP1">WHAT IS IRON DEFICIENCY ANEMIA?</p>
							<p id="rightP2" class="rightP2 rightP2Disease">Learn more about your condition</p>
						</div>


						<div class="right-vertical-line takingVerticalLine"></div>
						<p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

					</div>
				</div>




				<div class="isi">
					<?php //include 'includes/isi.php'; ?>
					<?php get_template_part('template-parts/content-ida-patient', 'isi'); ?>
				</div>




			</div>



		</div>
	</div>
</div>
