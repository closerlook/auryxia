<?php
	$the_slug = $post->post_name;
	$job_code = "PP-AUR-US-0982 (v4.0)";
	$job_code_date = "12/21";

	if ($the_slug == "resources") {
		$job_code = "PP-AUR-US-0982 (v4.0)";
		$job_code_date = "12/21";
	}
?>

<footer class="cf">
	<div class="grid-12 cf">
		<div class="grid-2 right">
			<a href="https://akebia.com/" target="_blank" data-element="default" data-category=“Footer” data-action="Click" data-label="Keryx Logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/logo-akebia-therapeutics.png" id="akebia-logo-footer" alt="Akebia Logo" /></a>
		</div>
		<div class="grid-2 left desktop">&nbsp;</div>
		<div id="footer-info" class="left footer-links">
			<ul id="menu-footer-links" class="menu">
				<li>
					<a target="_blank" href="https://akebia.com/contact/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Contact Us">Contact Us</a></li>
				<li><span>|</span></li>
				<li>
					<a target="_blank" href="https://akebia.com/legal/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Terms & Conditions">Terms and Conditions</a></li>
				<li><span>|</span></li>
				<li>
					<a target="_blank" href="https://akebia.com/legal/privacy-policy.aspx" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Terms & Conditions">Privacy Policy</a></li>
				<li><span>|</span></li>
				<li>
					<a target="_blank" href="https://akebia.com/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Keryx.Com">AKEBIA.COM</a></li>
			</ul>
			<div class="legal">&copy;2021 Akebia Therapeutics, Inc.<div class="footer-spacer"></div><?php echo $job_code ?> <div class="footer-spacer"></div><?php echo $job_code_date ?></div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
