<?php ?>
  <div class="content content-1">
    <div class="grid interior cf">
      <div class="post-titles">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-patient-assistance-program.jpg" class="header-desktop" alt="Patient Assistance Program" title="Patient Assistance Program">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-patient-assistance-program-mobile.jpg" class="header-mobile" alt="Patient Assistance Program" title="Patient Assistance Program" style="width: 100%">
      </div>
      <div class="grid-12 interior cf">
        <div class="grid-9 left copy">
          <h2>FREE AURYXIA FOR ELIGIBLE PATIENTS WHO ARE UNABLE TO AFFORD THEIR COPAYS</h2>
          <p class="subtitle">With AkebiaCares, eligible uninsured and Medicare Part D patients can get AURYXIA free of charge</p>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/how-does-akebia-cares-work.jpg" class="desktop" border="0">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/how-does-akebia-cares-work-mobile.jpg" class="mobile" border="0">
          <p class="footnote phone-fix">*If you have any questions about an enrollment form after it has been submitted to AkebiaCares, please call 855-686-8601.</p>
          <h3>PATIENT ASSISTANCE PROGRAM ELIGIBILITY</h3>
          <p><strong>AkebiaCares provides AURYXIA free of charge to patients who meet certain eligibility requirements, including:</strong></p>
          <ul>
            <li><span class="navy"><strong>Uninsured</strong></span> or without prescription drug coverage</li>
            <li><span class="navy"><strong>Medicare Part D</strong></span> patients who cannot afford out-of-pocket costs<sup>&dagger;</sup></li>
          </ul>
          <p class="footnote"><sup>&dagger;</sup>Depending on which indication AURYXIA was prescribed for.</p>

          <p class="contact">Do you have questions about eligibility? <a href="/akebiacares/reimbursement-help">Contact an AkebiaCares Case Manager</a></p>

          <p class="phone-fix">Certain qualified patients or patients with an income up to 400% of the Federal Poverty Level (FPL) may be eligible for additional assistance through AkebiaCares. If you're unsure about your patient's eligibility status, be sure to reach out to your AkebiaCares Case Manager for help at <span class="phone-number">855-686-8601</span>.</p>
        </div>

        <div class="grid-3 right line-bg desktop">
          <div class="cta-links">
            <a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Download Copay Coupon Here" href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="cta animC cta-1">
            <span class="cta-title">Download the Enrollment Form</span>
            <span class="cta-text">AkebiaCares Case Managers are standing by to help your patients find coverage</span>
            </a>
            <a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Connect with your Personal Case Manager" href="/akebiacares/reimbursement-help" class="cta animC cta-2">
            <span class="cta-title">Connect with a specialist</span>
            <span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
            </a>
            <a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Keryx Patient Plus Forms" href="/akebiacares/patient-coverage" class="cta animC cta-3">
            <span class="cta-title">Explore coverage by indication</span>
            <span class="cta-text">See what types of insurance cover AURYXIA</span>
            </a>
          </div>
        </div>
        <div class="grid-3 right desktop">
          <p class="isi-jump">See <a href="#important-safety-information">Important Safety Information</a> below</p>
        </div>
      </div><!-- Single Page -->
	  <?php get_template_part('template-parts/content','isi-akebiacares'); ?>
      <div class="cta-links mobile">
        <a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Download Copay Coupon Here" href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="cta animC cta-1">
        <span class="cta-title">Download the Enrollment Form</span>
        <span class="cta-text">AkebiaCares Case Managers are standing by to help your patients find coverage</span>
        </a>
        <a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Connect with your Personal Case Manager" href="/akebiacares/reimbursement-help" class="cta animC cta-2">
        <span class="cta-title">Connect with a specialist</span>
        <span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
        </a>
        <a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Keryx Patient Plus Forms" href="/akebiacares/patient-coverage" class="cta animC cta-3">
        <span class="cta-title">Explore coverage by indication</span>
        <span class="cta-text">See what types of insurance cover AURYXIA</span>
        </a>
      </div>
    </div>
  </div><!-- Content -->
