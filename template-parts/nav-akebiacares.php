<nav>
	<div class="grid cf">
		<ul>
			<li class="home<?php if (is_page('akebiacares')){ echo " active"; }?>"><a href="/akebiacares"><img src="<?php echo get_template_directory_uri();?>/assets/img/akebiacares/icon-home<?php if (!is_page('akebiacares')) {echo "-off";} ?>.png" class="desktop"><span class="mobile">Home</span></a></li>
			<li class="menu-item-has-children<?php if (is_page('patient-coverage')) {echo " active";}?>"><a href="#">Patient Coverage</a>
				<ul class="sub-menu">
					<li><a href="/akebiacares/patient-coverage?tab=hyp">hyperphosphatemia in CKD-DD</a></li>
					<li><a href="/akebiacares/patient-coverage?tab=iron">iron deficiency anemia in CKD-NDD</a></li>
				</ul></li>
			<li class="menu-item-has-children<?php if (is_page(array('support-programs','reimbursement-help','copay-program','patient-assistance-program'))) {echo " active";} ?>"><a href="#">Support Programs</a>
				<ul class="sub-menu">
					<li<?php if (is_page('support-programs')) {echo " class='active'";}?>><a href="/akebiacares/support-programs">Support Summary</a></li>
					<li<?php if (is_page('reimbursement-help')) {echo " class='active'";}?>><a href="/akebiacares/reimbursement-help">Reimbursement Help</a></li>
					<li<?php if (is_page('copay-program')) {echo " class='active'";}?>><a href="/akebiacares/copay-program">Copay Program</a></li>
					<li<?php if (is_page('patient-assistance-program')) {echo " class='active'";} ?>><a href="/akebiacares/patient-assistance-program">Patient Assistance Program</a></li>
				</ul></li>
			<li<?php if (is_page('patient-information')) {echo " class='active'";}?>><a href="/akebiacares/patient-information">Patient Information</a></li>
			<li<?php if (is_page('resources')) {echo " class='active'";}?>><a href="/akebiacares/resources">Resources</a></li>
		</ul>
		<ul class="mobile">
			<li class="gray"><a href="/">For Patients</a></li>
			<li class="gray"><a href="https://www.auryxiahcp.com">For U.S. Healthcare Professionals</a></li>
			<?php if (is_page('akebiacares')) { ?>
			<li><a href="http://www.auryxiarxlocator.com" target="_blank">Specialty Pharmacy Locator</a></li>
			<li><a href="https://akebiarxcoverage.caremetx.com/" target="_blank">Prescription Benefits Portal</a></li>
			<?php } ?>
		</ul>
	</div>
</nav>
<?php if (is_page('akebiacares')) { ?>
<div class="sub-buttons-container">
	<div class="sub-button"><a href="http://www.auryxiarxlocator.com" target="_blank">Find specialty pharmacies that dispense AURYXIA</a></div>
	<div class="sub-button"><a href="https://akebiarxcoverage.caremetx.com/" target="_blank">Check your patient's prescription benefits</a></div>
</div>
<?php } ?>

