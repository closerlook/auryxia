<?php ?>
<div class="grid cf">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/hero-home.jpg" class="hero desktop">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/hero-home-mobile.png" class="mobile" style="width: 100%;">
	<div class="grid cf" style="background-color: #fff;">
		<div class="grid-12">
			<div class="callouts cf">
				<a href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf" target="_blank" class="callout grid-4 animC left callout-1" data-element="default" data-category="CTA" data-action="Click" data-label="SUPPORT STARTS HERE!">
					<span class="callout-top"></span>
					<span class="main_text">Download the enrollment form</span>
					<span class="sub_text">AkebiaCares Case Managers are standing by to help your patients find coverage </span>
					<span class="square"></span>
				</a>
				<a href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" target="_blank" class="callout grid-4 animC left callout-2" data-element="default" data-category="CTA" data-action="Click" data-label="Personalized Reimbursement Help">
					<span class="callout-top"></span>
					<span class="main_text">Download the Copay Coupon</span>
					<span class="sub_text">Eligible patients with commercial insurance can pay as&nbsp;little&nbsp;as&nbsp;$0</span>
					<span class="square"></span>
				</a>
				<a href="/akebiacares/patient-coverage" class="callout animC nom grid-4 left callout-3" data-element="default" data-category="CTA" data-action="Click" data-label="EXPLORE COVERAGE BY INDICATION">
					<span class="callout-top"></span>
					<span class="main_text">Explore coverage by indication </span>
					<span class="sub_text">See what types of insurance cover AURYXIA</span>
					<span class="square"></span>
				</a>
			</div>
			<div class="contact-lg home">
				<p class="title">Looking for personalized access assistance?</p>
				<p class="phone-fix">Call one of our dedicated AkebiaCares Case Managers<br>
					855-686-8601 <span class="bold-blue">|</span> Monday-Friday <span class="bold-blue">|</span> 8<span class="small">AM</span>-7<span class="small">PM</span> ET</p>
			</div>
		</div>
		<?php get_template_part('template-parts/content','isi-akebiacares'); ?>
	</div><!-- 12 -->
</div><!-- Grid -->
<!--/div--><!-- Content -->
