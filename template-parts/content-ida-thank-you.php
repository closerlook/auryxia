

<section id="thank-you-container" class="container">
	<div class="col-md-12 col-centered">
		<div class="header">
			<p>Thank you</p>
		</div>
		<div class="body-copy">
			<p>You have successfully signed up to get the latest information on AURYXIA. Check your inbox for a confirmation email and be on the lookout for future AURYXIA news.</p>
		</div>

	</div>
</section>

<section id='callouts-home' class='container'>
	<div class="col-md-12 col-centered callouts">
		<a href="/iron-deficiency-anemia/efficacy" class="callout col-md-4 animC left callout-1" data-element="default" data-category="CTA" data-action="Click" data-label="Efficacy Results">
			<span class="callout-top"></span>
			<span class="main_text">EFFICACY RESULTS</span>

			<span class="sub_text">Review trial results for the first oral iron specifically indicated for adults with CKD not on dialysis.</span>
		</a>
		<a href="/iron-deficiency-anemia/moa" class="callout col-md-4 animC left callout-2" data-element="default" data-category="CTA" data-action="Click" data-label="MOA">
			<span class="callout-top"></span>
			<span class="main_text">MECHANISM OF ACTION</span>

			<span class="sub_text">Watch a video to learn how AURYXIA, an oral iron tablet, works in adult patients.</span>
		</a>
		<a href="/akebiacares/support-programs" class="callout animC nom col-md-4 left callout-3" data-element="default" data-category="CTA" data-action="Click" data-label="Patient Coverage">
			<span class="callout-top"></span>
			<span class="main_text">PATIENT COVERAGE</span>

			<span class="sub_text">The leading regional and national health plans include AURYXIA on their formularies.<sup>1</sup> Check your patients’ coverage.</span>
		</a>
	</div>
</section>

<section id='isi-content'>
	<?php get_template_part('template-parts/content', 'isi'); ?>
</section>

<section class="references container">
	<div class='col-md-12 col-centered'>
		<span>Reference</span>
		<ol>
			<li>Data on File 24, Akebia Therapeutics, Inc.</li>
		</ol>
	</div>
</section>

