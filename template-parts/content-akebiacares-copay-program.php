<div class="content content-1">
	<div class="grid interior cf">
		<div class="post-titles">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-copay-program.jpg" class="header-desktop" alt="Copay Program" title="Copay Program">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/header-copay-program-mobile.jpg" class="header-mobile" alt="Copay Program" title="Copay Program" style="width: 100%">
		</div>
		<div class="grid-12 interior cf">
			<div class="grid-9 left copy">
				<h2>FINANCIAL ASSISTANCE FOR PATIENTS WITH COMMERCIAL INSURANCE</h2>

				<p class="subtitle">With the <a href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" target="_blank">Copay Coupon</a>, eligible patients can pay as little as $0 for AURYXIA</p>


				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/copay-steps.png" class="desktop" border="0">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/akebiacares/copay-steps-mobile.png" class="mobile" border="0">

				<h3 class="bottom-margin">COPAY SAVINGS ELIGIBILITY</h3>
				<p><strong>In order to qualify for the AURYXIA Copay Coupon, patients must:</strong></p>
				<ul class="bottom-margin">
					<li>Have commercial insurance only (commercial health insurance is any type of health benefit not affiliated with a federal or state government program)</li>
					<li>Not be registered for Medicare, Medicaid, Veterans Affairs, or similar federal or state programs</li>
				</ul>

				<h3 class="bottom-margin">COPAY MAIL-IN REBATE OPTION</h3>
				<p><strong>Some pharmacies may require patients to mail in their Copay Coupon.<br>To redeem their copay, patients must submit:</strong></p>
				<ul class="bottom-margin">
					<li>A copy of the Copay Coupon OR the ID number and Group number located on the coupon</li>
					<li>The original purchase receipt with prescription number, pill quantity, and name of pharmacy where filled</li>
					<li>Patient's name, full address, phone number, and date of birth</li>
				</ul>

				<p>Please send all necessary documents to:</p>

				<p><strong>AURYXIA Claims Processing Dept</strong></p>
				<p><strong>PO Box 7017</strong></p>
				<p><strong>Bedminster, NJ 07921-7017</strong></p>

				<p>Allow up to 6 weeks for processing. Same Copay Coupon rules and regulations apply.</p>

				<p class="teal"><strong>TIP: Be sure your patient keeps a photocopy of all submitted materials for their records.</strong></p>
			</div>

			<div class="grid-3 right line-bg desktop">
				<div class="cta-links">
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Keryx Patient Plus Forms" href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" target="_blank" class="cta animC cta-1">
						<span class="cta-title">Download the Copay Coupon </span>
						<span class="cta-text">Eligible patients with commercial insurance can pay as little as $0</span>
					</a>
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Connect with your Personal Case Manager" href="/akebiacares/reimbursement-help" class="cta animC cta-2">
						<span class="cta-title">Connect with a specialist</span>
						<span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
					</a>
					<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Download Copay Coupon Here" href="/akebiacares/patient-coverage" class="cta animC cta-3">
						<span class="cta-title">Explore Coverage by Indication</span>
						<span class="cta-text">See what types of insurance cover AURYXIA</span>
					</a>
				</div>
			</div>
			<div class="grid-3 right desktop">
				<p class="isi-jump">See <a href="#important-safety-information">Important Safety Information</a> below</p>
			</div>
		</div><!-- Single Page -->
		<?php get_template_part('template-parts/content','isi-akebiacares'); ?>
		<div class="cta-links mobile">
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Keryx Patient Plus Forms" href="/wp-content/uploads/AkebiaCares-Copay-Coupon.pdf" class="cta animC cta-1">
				<span class="cta-title">Download the Copay Coupon </span>
				<span class="cta-text">Eligible patients with commercial insurance can pay as little as $0</span>
			</a>
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Connect with your Personal Case Manager" href="/akebiacares/reimbursement-help" class="cta animC cta-2">
				<span class="cta-title">Connect with a specialist</span>
				<span class="cta-text phone-fix">855-686-8601<br>Monday-Friday<br>8<span class="small">AM</span>-7<span class="small">PM</span> ET</span>
			</a>
			<a data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Download Copay Coupon Here" href="/akebiacares/patient-coverage" class="cta animC cta-3">
				<span class="cta-title">Explore Coverage by Indication</span>
				<span class="cta-text">See what types of insurance cover AURYXIA</span>
			</a>
		</div>
	</div>
</div><!-- Content -->
