<div class="layoutBody">
	<div class="container outerContainer ">
		<div class="backdropContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/Graphics_Backdrop.png" alt="" class="bgImgFaq">
		</div>
		<div class="bannerStripe">
			<p>Iron Deficiency Anemia <span>CKD Not On Dialysis</span></p>
			<div class="left-triangle"></div>
			<div class="right-triangle"></div>
		</div>
		<div class="container innerContainer">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/bannerEight.jpg" alt="Resources" class="headerImg-Resources">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/mobileResource.png" alt="Resources" class="bannerMobile">

			<div class="contentInner">
				<span class="patientText-Header">Hypothetical patient portrayals.</span>

				<div class="row">
					<div class="col-md-9 leftContent"> <!-- CONTENT HERE -->
						<br class="hiddenBrDesktop">
						<p class="diseaseP1">Helpful tools you can download</p>
						<br>
						<img style="border: 1px solid #005f9f" src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/IDAPatientTearsheet.png" alt="" class="pdfImg">
						<p class="diseaseP1" id="resourceP1">AURYXIA at a glance</p>
						<p class="diseaseP3" id="resourceP2">Use this cheat sheet to get the best start to your treatment journey</p>
						<a class="gtm-pdf reseroucePDFA" data-gtm-event-category="PDF" data-gtm-event-action="Download" data-gtm-event-label="Auryxia-at-a-glance" target="_blank" href="/wp-content/uploads/IDA_Patient_Tearsheet.pdf"> <button class="resourceBtn gtm-pdf "><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ida-patient/downloadIcon.png" alt="" class="downloadIcon"><span>DOWNLOAD PDF</span></button></a>





					</div> <!-- CONTENT HERE END -->

					<div class="col-md-3 rightContent">

						<div class="rightBoxOne takingBox1 gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Auryxia 101">
							<p class="rightP1">AURYXIA 101</p>
							<p id="rightP2" class="rightP2 takingRightP2">Explore how AURYXIA can help</p>
						</div>

						<div class="rightBoxTwo gtm-cta" data-gtm-event-category="Main CTA" data-gtm-event-action="Click" data-gtm-event-label="Save on the Cost fo Auryxia">
							<p class="rightP1">SAVE ON THE COST OF AURYXIA</p>
							<p id="rightP2" class="rightP2 rightP2Disease">Depending on your insurance,<br class="desktopBr"> you can get AURYXIA for free</p>
						</div>


						<div class="right-vertical-line"></div>
						<p class="rightIsi">See&nbsp;<a class="rightIsiLink gtm-cta" data-gtm-event-action="Click" data-gtm-event-category="Main CTA" data-gtm-event-label="Important Safety Information" href="#important-safety-information">Important&nbsp;Safety&nbsp;Information</a>&nbsp;below</p>

					</div>
				</div>




				<div class="isi">
					<?php //include 'includes/isi.php'; ?>
					<?php get_template_part('template-parts/content-ida-patient', 'isi'); ?>
				</div>




			</div>



		</div>
	</div>
</div>
