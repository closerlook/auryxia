<section class='container col-centered page-content'>
	<section class='col-md-12 col-centered interior'>
		<div class='post-titles'>
			<h2>HOW AURYXIA WORKS</h2>
		</div>
		<section class='col-md-9 left copy'>
			<h2>AURYXIA reduces to the ferrous form (Fe<sup>2+</sup>) allowing the body to absorb the iron component in the same form as dietary iron<sup>1</sup></h2>
			<!--INTERACTIVE CONTENT GOES HERE-->
			<section id='moa-interactive' class='flexcontainer'>
				<div class='moa-img-list desktop'>
					<ul>
						<li><img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/moa/moa-img1.png' alt='' class='moa-img1 desktop'></li>
						<li><img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/moa/moa-img2.png' alt='' class='moa-img2 desktop'></li>
						<li><img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/moa/moa-img3.png' alt='' class='moa-img3 desktop'></li>
					</ul>
				</div>
				<div class='moa-btn-list desktop'>
					<ul>
						<li>
							<div class='moa-btn1'></div>
							<p>The ferric iron (Fe<sup>3+</sup>) component is reduced from the ferric to the ferrous form (Fe<sup>2+</sup>) by ferric reductase in the gastrointestinal tract.<sup>1</sup></p>
						</li>
						<li>
							<div class='moa-btn2'></div>
							<p>The ferrous iron is transported through the enterocytes into the blood.<sup>1</sup></p>
						</li>
						<li>
							<div class='moa-btn3'></div>
							<p>Oxidized ferric iron circulates bound to the plasma protein transferrin, and can be incorporated into hemoglobin.<sup>1</sup></p>
						</li>
					</ul>
				</div>
				<div class='moa-btn-list-mobile mobile'>
					<div class='moa-holder'>
						<img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/moa/moa-img1.png' alt='' class='moa-img1-mobile'>
						<p class='moa-txt1'>The ferric iron (Fe<sup>3+</sup>) component is reduced from the ferric to the ferrous form (Fe<sup>2+</sup>) by ferric reductase in the gastrointestinal tract.<sup>1</sup></p>
					</div>
					<div class='moa-holder'>
						<img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/moa/moa-img2.png' alt='' class='moa-img2-mobile'>
						<p class='moa-txt2'>The ferrous iron is transported through the enterocytes into the blood.<sup>1</sup></p>
					</div>
					<div class='moa-holder'>
						<img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/moa/moa-img3.png' alt='' class='moa-img3-mobile'>
						<p class='moa-txt3'>Oxidized ferric iron circulates bound to the plasma protein transferrin, and can be incorporated into hemoglobin.<sup>1</sup></p>
					</div>
				</div>
			</section>
			<div class='moa-legend'>
				<img src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/moa/moa-legend-img.png' alt=''>
				<p>=transferrin</p>
			</div>
		</section>
		<section class="col-md-3 right">
			<div class="cta-links">
				<a href="/iron-deficiency-anemia/dosing" class="cta animC cta-1" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Dosing">
					<span class="cta-title">DOSING</span>
					<span class="cta-text">210 mg of elemental iron per tablet</span>
				</a>
				<a href="/iron-deficiency-anemia/efficacy" class="cta animC cta-2" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Efficacy">
					<span class="cta-title">EFFICACY</span>
					<span class="cta-text">See results from our clinical trial</span>
				</a>
				<a href="/iron-deficiency-anemia/tolerability-and-safety" class="cta animC cta-3" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="Safety">
					<span class="cta-title">SAFETY</span>
					<span class="cta-text">Safety and tolerability profile evaluated in a pivotal clinical trial</span>
				</a>
			</div>
			<div class="cta-isi-sidebar">
				<a href="javascript:void(0)" data-element="default" data-category="CTA – Right Side" data-action="Click" data-label="ISI"><span>See <span style="text-decoration: underline;">Important Safety Information</span> below</span></a>
			</div>
			<img class='desktop' src='<?php echo get_template_directory_uri(); ?>/assets/img/ida/sidebar-peacock.png' alt=''>
		</section>
	</section>
</section>
<!-- HTML includes -->
<section id='isi-content'>
	<?php get_template_part('template-parts/content', 'isi'); ?>
</section>
<!-- References -->
<section class="references container">
	<div class='col-md-12 col-centered'>
		<span>Reference</span>
		<ol>
			<li>AURYXIA [package insert]. Cambridge, MA: Akebia Therapeutics, Inc.; 2017.                            </li>
		</ol>
	</div>
</section>

