<?php

// Custom Footer
function wpfme_footer_admin () {
	echo 'Built by <a href="http://fcbhealthcare.com">FCB Health</a>';
}
add_filter('admin_footer_text', 'wpfme_footer_admin');

/* Remove Admin Menu Items */
function remove_admin_menu_items() {
	$remove_menu_items = array( __('Comments'), __('Tools'),  __('Plugins'), __('Posts'));
	global $menu;
	end ($menu);
	while (prev($menu)){
		$item = explode(' ',$menu[key($menu)][0]);
		if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
		unset($menu[key($menu)]);}
	}
}
////add_action('admin_menu', 'remove_admin_menu_items');


/* Disable Comments */
function filter_media_comment_status( $open, $post_id ) {
	$post = get_post( $post_id );
	if( $post->post_type == 'attachment' ) {
		return false;
	}
	return $open;
}
add_filter( 'comments_open', 'filter_media_comment_status', 10 , 2 );
