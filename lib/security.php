<?php
// Remove the version number of WP
remove_action('wp_head', 'wp_generator');

// Obscure Login Error Messages
function wpfme_login_obscure(){ return '<strong>Sorry</strong>: Incorrect Login!';}
add_filter( 'login_errors', 'wpfme_login_obscure' );

/* Remove Image Links */
add_filter( 'the_content', 'attachment_image_link_remove_filter' );
function attachment_image_link_remove_filter( $content ) {
  $content =
  preg_replace(
    array('{<a(.*?)(wp-att|wp-content/uploads)[^>]*><img}',
          '{ wp-image-[0-9]*" /></a>}'),
    array('<img','" />'),
    $content
  );
  return $content;
}


// fix ssl handling for background_image theme_mod
function fix_theme_mod_background_image_ssl($url) {
if ( is_ssl() )
  $url = str_replace( 'http://', 'https://', $url );
else
  $url = str_replace( 'https://', 'http://', $url );
  return $url;
}
add_filter('theme_mod_background_image', 'fix_theme_mod_background_image_ssl', 10, 1);


// Remove Admin Bar, Version Number Logo
add_filter( 'show_admin_bar', '__return_false' );

function annointed_admin_bar_remove() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);
