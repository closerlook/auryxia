<?php
  $redirectURL = home_url(); 
  $pagePath = $_SERVER['REQUEST_URI'];
  $urlParts = explode('/', $pagePath, 4);

  if ($urlParts[2] && $urlParts[2] == 'patients') {
    $redirectURL = home_url() . '/patients/';
  } else if ($urlParts[1] == 'hyperphosphatemia') { 
    $redirectURL = home_url();
  } else if ($urlParts[1] == 'akebiacares') {
    $redirectURL = home_url() . '/akebiacares/404/';
  } else {
    $redirectURL = home_url();
  }
  
  wp_redirect($redirectURL);
  exit;
?>