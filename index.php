<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
			<?php the_permalink() ?>
			<?php the_title(); ?>
			<?php the_content(); ?>
		</div>

	<?php endwhile; ?>

		<?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?>
		<?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?>

	<?php else : ?>
		Page Not Found
	<?php endif; ?>


<?php get_footer(); ?>
