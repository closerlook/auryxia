<?php /* Template Name: Patients Home */ 
get_header(); if (have_posts()) : while (have_posts()) : the_post(); ?>


<div class="grid cf">
  <div class="grid-12">
  
    <?php include_once(TEMPLATEPATH . '/includes/home-campaign.php'); ?>
    <?php include_once(TEMPLATEPATH . '/includes/home-callouts.php'); ?>
    
    <?php if (get_field('campaign_small_text')){ ?>
    <div class="campaign-btm right grid-8 cf">
      <div class="campaign-line"></div>
      <div class="cmp-wrap"><?php the_field('campaign_small_text'); ?></div>
    </div>
    <div class="clear"></div>
    <?php } ?>
    <?php 
    $formulation = get_field('formulation_text', 'option');
    if ($formulation){
      echo '<br /><div class="formulation">';
      echo '<span>Formulation</span>' . $formulation . '</div>';
    }
    ?>
  
  </div><!-- 12 -->
</div><!-- Grid -->



 	
 	
<?php 
  include_once(TEMPLATEPATH . '/includes/isi-and-references.php');
  endwhile; else : endif; get_footer(); 
?>
