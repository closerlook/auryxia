<footer class="cf">
  <div class="grid-12 cf">
  
  
    <div class="grid-9 left footer-links">
      <?php //wp_nav_menu(array('menu' => 'Footer Links','container_class' => 'false')); ?>
        <div class="false">
          <ul id="menu-footer-links" class="menu">
            <li>
              <a target="_blank" href="https://akebia.com/contact/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Contact Us">Contact Us</a>
            </li>
            <li>
              <a target="_blank" href="https://akebia.com/legal/" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Terms & Conditions">Terms and Conditions</a>
            </li>
            <li>
              <a target="_blank" href="https://akebia.com/legal/privacy-policy.aspx" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Privacy Policy">Privacy Policy</a>
            </li>
            <li>
              <a target="_blank" href="https://akebia.com" class="whitelisted" data-element="default" data-category="Footer" data-action="Click" data-label="Keryx.Com">AKEBIA.COM</a>
            </li>
          </ul>
        </div>
      <div class="legal">
        <?php the_field('patient_copyright', 'option'); ?>
        <div class="footer-spacer"></div>
        <?php the_field('patient_legal_code', 'option'); ?>
        <div class="footer-spacer"></div>
        <?php the_field('patient_legal_date', 'option'); ?>
      </div>
    </div>
    
    <div id="footer-info" class="grid-3 right">
      <a href="<?php the_field('footer_logo_link', 'option'); ?>" target="_blank" data-element="default" data-category=“Footer” data-action="Click" data-label="Keryx Logo">
        <img 
          class="grid-2"
          src="<?php the_field('footer_logo', 'option'); ?>" 
          alt="<?php the_field('footer_brand_name', 'option'); ?>" 
        />
      </a>
    </div>
  
  </div>
</footer>