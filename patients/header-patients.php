<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P5T5HJB"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>
  <div class="grid cf">
  
       <div class="header-links-patient cf mobile">
      <a href="/hyperphosphatemia/patients/patient-important-safety-information/" class="nom " data-element="default" data-category="Header" data-action="Click" data-label="ISI">Important Safety Information</a>
      <span class="sep  ">|</span>    
      <a href="/hyperphosphatemia/prescribing-information/" target="_blank" class="pi-link" data-element="default" data-category="Header" data-action="Click" data-label="Full PI">Full Prescribing Information</a> 
    </div> 

      <div class="intent-mobile mobile">
          This site is for U.S. residents only
      </div>
    <h1>
      <a href="/" data-element="default" data-category="Header" data-action="Click" data-label="Logo">
        <img id="logo" src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>" />
      </a>
    </h1>
    
    <div class="grid-10 right">
      <div class="header-links desktop">
        <a href="/hyperphosphatemia/patients/patient-important-safety-information/" class="nom" data-element="default" data-category="Header" data-action="Click" data-label="ISI">Important Safety Information</a>
        <span class="sep desktop">|</span>  
        <a href="/hyperphosphatemia/prescribing-information/" target="_blank" class="pi-link" data-element="default" data-category="Header" data-action="Click" data-label="Full PI">Full Prescribing Information</a>  
        <span class="sep desktop">|</span>
        <a href="/akebiacares" class="desktop nav-copy" data-element="default" data-category="Header" data-action="Click" data-label="AkebiaCares">AkebiaCares</a>
        <span class="sep desktop">|</span> 
        <a href="/hyperphosphatemia/" class="desktop nav-copy" data-element="default" data-category="Header" data-action="Click" data-label="For US Healthcare Professionals">For U.S. Healthcare Professionals</a>
        <span class="sep desktop">|</span> 
        <a href="/iron-deficiency-anemia/patient" class="desktop nav-copy" data-element="default" data-category="Header" data-action="Click" data-label="For US Healthcare Professionals">For Patients Not on Dialysis</a>
      </div>
      
      <div class="intent desktop">
      This site is for U.S. residents only
    </div>

      <div class="clear"></div>
      <a href="#" class="nt mobile" id="mobile-menu">Menu</a>
      <nav class="grid-12">
        <?php wp_nav_menu(array('menu' => 'Patient Navigation')); ?>
      </nav>
      
    </div><!-- Grid 10 -->
   
  </div><!-- Grid -->
</header>


