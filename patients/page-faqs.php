<?php /* Template Name: Patients FAQs */ 
get_header(); if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php include_once(TEMPLATEPATH . '/includes/sub-nav.php'); ?>

<div class="grid-12 interior cf">
  <?php include_once(TEMPLATEPATH . '/includes/post-titles.php'); ?>
  
  <div class="grid-9 left copy">
    
    <div class="faq-controls cf">
      <a href="#" class="faq-expand animC" data-element="default" data-category="Internal Link" data-action="Click" data-label="Expand All">Expand All</a>
      <a href="#" class="faq-collapse animC" data-element="default" data-category="Internal Link" data-action="Click" data-label="Collapse All">Collapse All</a>
    </div>
    
    <?php if(have_rows('faq')): ?>
    <div class="faqs">
      <?php while(have_rows('faq')): the_row(); ?>
      <div class="faq">
        <div class="icon"></div>
        <div class="faq-question"><?php echo get_sub_field('question'); ?></div>
        <div class="faq-answer"><?php echo get_sub_field('answer'); ?></div>
      </div>
      <?php endwhile; ?>
    </div>
    <?php endif; ?>
    <?php
      $formulation = get_field('formulation_text', 'option');
      if ($formulation){
        echo '<br /><div class="formulation">';
        echo '<span>Formulation</span>' . $formulation . '</div>';
      }
    ?>
    
  </div>
   
  <?php include_once(TEMPLATEPATH . '/includes/sidebar-callouts.php'); ?>
</div><!-- Single Page -->


<?php 
  include_once(TEMPLATEPATH . '/includes/isi-and-references.php'); 
  endwhile; else : endif; get_footer();
?>