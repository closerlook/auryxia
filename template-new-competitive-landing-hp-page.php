<?php
/**
 * Template Name: New Competitive Landing HP Page
 */
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, maximum-scale=1.0 , initial-scale=1.0">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body id="template-new-competitive-landing-hp-page" <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div class="mobile-nav d-md-none">
		<div class="top-links text-center">
			<a target="_blank" href="/hyperphosphatemia/important-safety-information">Important Safety Information</a> |
			<a target="_blank" href="/hyperphosphatemia/prescribing-information/">Full Prescribing Information</a>
		</div>
		<div class="hcp-warning text-center">This site is for U.S. Healthcare Professionals Only</div>
		<nav class="navbar">
			<a class="navbar-brand" href="/">
				<img alt="AURYXIA® (ferric citrate) tablets" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/logo.png">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile-menu" aria-controls="mobile-menu" aria-expanded="false" aria-label="Toggle Navigation">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/mobile-menu.png" class="navbar-toggler-icon">
			</button>
		</nav>
		<div class="indication-banner hcp text-center"><strong>For the control of serum phosphorus levels in adults with CKD on dialysis</strong></div>
		<div class="collapse navbar-collapse" id="mobile-menu">
			<div class="navbar-nav">
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/">Home</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/efficacy/unmet-need/">Efficacy</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/efficacy/unmet-need/">Unmet Need</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/efficacy/trial-endpoints/">Pivotal Phase III Trial</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/efficacy/efficacy-results/">Efficacy Results</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/dosing/dosing-guidelines/">Dosing</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/dosing/dosing-guidelines/">Guidelines</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/dosing/administration/">Administration</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/safety/adverse-events/">Safety</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/safety/adverse-events/">Safety and Tolerability</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/clinical-pharmacology/pharmacodynamics/">Pharmacodynamics</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/clinical-pharmacology/mechanism-of-action/">MOA</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/library/">Library</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/sign-up/">Sign Up</a>
				<a class="nav-link color-for-patients-link" target="_blank" href="/hyperphosphatemia/patients/">For Patients</a>
				<a class="nav-link color-akebiacares-link" target="_blank" href="/akebiacares/">AkebiaCares</a>
				<a class="nav-link color-ida-link" href="/iron-deficiency-anemia/">Iron Deficiency Anemia</a>
			</div>
		</div>
	</div>
	<div class="desktop-nav d-none d-md-block">
		<div class="top-section">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters">
					<div id="logo-container" class="col-md-10">
						<div class="row no-gutters">
							<div class="col-md-3">
								<a class="navbar-brand" href="/">
									<img alt="AURYXIA® (ferric citrate) tablets" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/logo.png">
								</a>
							</div>
							<div class="col-md-9">
								<div class="top-links text-right">
									<a href="/hyperphosphatemia/important-safety-information/">Important Safety Information</a> |
									<a target="_blank" href="/hyperphosphatemia/prescribing-information/">Full Prescribing Information</a> |
									<a href="/hyperphosphatemia/patients/">For Patients</a> |
									<a href="/akebiacares/">AkebiaCares</a> | <a href="/iron-deficiency-anemia/">Iron Deficiency Anemia</a>
								</div>
								<div class="hcp-warning text-right"><img class="icon-intent" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/icon-intent.png">This site is for U.S. Healthcare Professionals Only</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="desktop-menu container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="nav-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<nav class="navbar navbar-expand-md navbar-light">
								<a class="navbar-brand" href="/hyperphosphatemia/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/Home_Icon-1.png"></a>
								<ul class="navbar-nav">
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#" id="efficacyDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Efficacy</a>
										<div class="dropdown-menu efficacy" aria-labelledby="efficacyDropdownMenuLink">
											<a class="dropdown-item" href="/hyperphosphatemia/efficacy/unmet-need/">Unmet Need</a>
											<a class="dropdown-item" href="/hyperphosphatemia/efficacy/trial-endpoints/">Pivotal Phase III Trial</a>
											<a class="dropdown-item" href="/hyperphosphatemia/efficacy/efficacy-results/">Efficacy Results</a>
										</div>
									</li>
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#" id="dosingDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dosing</a>
										<div class="dropdown-menu" aria-labelledby="dosingDropdownMenuLink">
											<a class="dropdown-item" href="/hyperphosphatemia/dosing/dosing-guidelines/">Guidelines</a>
											<a class="dropdown-item" href="/hyperphosphatemia/dosing/administration/">Administration</a>
										</div>
									</li>
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#" id="safetyDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Safety</a>
										<div class="dropdown-menu" aria-labelledby="safetyDropdownMenuLink">
											<a class="dropdown-item" href="/hyperphosphatemia/safety/adverse-events/">Safety and Tolerability</a>
											<a class="dropdown-item" href="/hyperphosphatemia/clinical-pharmacology/pharmacodynamics/">Pharmacodynamics</a>
										</div>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="/hyperphosphatemia/clinical-pharmacology/mechanism-of-action/">MOA</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="/hyperphosphatemia/library/">Library</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="/hyperphosphatemia/sign-up/">Sign Up</a>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="indication-banner hcp">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters align-items-center">
					<div id="warning-container" class="col-md-10">
						<div class="row no-gutters">
							<div class="col">
								<strong>For the control of serum phosphorus levels in adults with CKD on dialysis</strong>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<header>
		<div class="container-fluid">
			<div class="row no-gutters">
				<div class="col-md-10 offset-md-1">
					<div class="row no-gutters">
						<div class="col-md-7">
							<h1>Need a different choice for hyperphosphatemia? <span class="nowrap">Reach for</span> AURYXIA.</h1>
							<p>Give your patients the confidence of sustained phosphorus control </p>
							<p><a href="/hyperphosphatemia/sign-up/">Get the latest news and resources</a></p>
						</div>
					</div>
					<span class="footnote">Not actual patient</span>
				</div>
			</div>
		</div>
	</header>

	<section id="standard-of-care">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10 offset-md-1">
					<div class="row cta-container">
						<div class="col">
							<div>
								<h2>Strong and sustained phosphorus control</h2>
								<h3>AURYXIA helped patients reach and stay in the <strong>KDOQI</strong> range of 3.5-5.5 mg/dL during a 56-week trial<sup>1,2</sup>*</h3>
								<h3>Patients had a mean serum phosphorus level of 7.41 mg/dL at baseline and 4.88 mg/dL at Week 56.<sup>2</sup></h3>
								<p class="footnote">*KDOQI=Kidney Disease Outcomes Quality Initiative</p>
								<div class="cta-btn-container">
									<a class="cta-btn" href="/hyperphosphatemia/efficacy/efficacy-results/">Explore efficacy</a>
								</div>
								<div id="trial-design-btn-container">
									<strong><a id="trial-design-btn" href="#trial-design"><u>See trial design below</u><i class="arrow"></i></a></strong>
								</div>
<!--								<a class="popup" href="#trial-design"></a>-->

							</div>
						</div>
						<div class="col">
							<div>
								<h2>Did you know?</h2>
								<h3>When dietary phosphorus modifications are unsuccessful, clinical practice guidelines recommend a phosphate binder, like AURYXIA<sup>3</sup></h3>

								<div class="cta-btn-container">
									<a class="cta-btn" href="/hyperphosphatemia/efficacy/unmet-need/">Explore unmet need</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="proven-safety-profile">
		<div>
			<h2>A proven safety profile</h2>
			<p><a href="/hyperphosphatemia/safety/adverse-events/">See safety and tolerability in a <span class="nowrap">52-week</span> trial<sup>4,5</sup></a></p>
		</div>
	</section>

	<section id="mechanism-of-action">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-5 offset-md-1">
					<h2>A unique formulation</h2>
					<p>The mechanism of action for AURYXIA leads to decreased phosphate absorption and reduces serum phosphate concentrations<sup>4</sup></p>
					<p><a href="/hyperphosphatemia/clinical-pharmacology/mechanism-of-action/">Watch video</a></p>
				</div>
			</div>
			<a href="/hyperphosphatemia/clinical-pharmacology/mechanism-of-action/"></a>
		</div>
	</section>

	<?php /*<section id="titration">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div>
						<h2>Individualized treatment with titration control</h2>
						<p>Support your patients with the regularity of <span class="nowrap">mealtime dosing</span></p>
						<p><a href="/hyperphosphatemia/dosing/dosing-guidelines/">Learn more about dosing.</a></p>
					</div>

					<div id="titration-dosing-guidelines">
						<h3>Dosing guidelines</h3>
						<div>
							<div>
								<span>Take AURYXIA at mealtimes<sup>3*</sup></span>
							</div>
							<div>
								<span>Starting dose <strong>2 tablets 3 times/day</strong><sup>3*</sup></span>
							</div>
							<div>
								<span>Adjust dose every week (or longer) <strong>as needed</strong><sup>3*</sup></span>
							</div>
							<div>
								<span>Maximum dose <strong>12 tablets daily</strong><sup>3*</sup></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> */ ?>

	<section id="akebia-cares">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10 offset-md-1">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-competitive-landing/logo-akebiacares-lg.svg" alt="AkebiaCares" />
					<h2>Your trusted partner in coverage solutions</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 offset-md-1">
					<div class="row">
						<div class="col-md akebia-cares-coverage">
							<div>
								<h3>Coverage solutions made simple</h3>
								<p>AkebiaCares provides eligible patients with coverage support and medicines at no cost</p>
								<ul>
									<li>Reimbursement help</li>
									<li>Patient Assistance Program (PAP)</li>
									<li>Copay program</li>
								</ul>

								<a href="/akebiacares/reimbursement-help/">Connect with a specialist</a>
							</div>
						</div>
						<div class="col-md akebia-cares-insurance">
							<div>
								<h3>Broad insurance coverage for your patients</h3>

								<h4>MEDICARE LIS/DUAL ELIGIBLE</h4>
								<ul class="custom-bullet">
									<li>AURYXIA is on formulary at all major Medicare <span class="nowrap">Part D</span> Plans<sup>6</sup>
										<ul class="custom-bullet">
											<li>Prior authorization required to confirm diagnosis</li>
										</ul>
									</li>
									<li>All Medicare full Low-Income Subsidy (LIS)/dual-eligible patients pay <strong>no more than $10.00 per fill</strong> of AURYXIA</li>
								</ul>

								<h4>COMMERCIAL INSURANCE</h4>
								<ul class="custom-bullet">
									<li>AURYXIA is on formulary with the majority of commercial plans<sup>7</sup>
										<ul class="custom-bullet">
											<li>With the AURYXIA Copay Coupon, eligible patients can pay as little as <strong>$0 per fill*</strong></li>
										</ul>
									</li>
								</ul>

								<h4>UNINSURED/MEDICARE <span class="nowrap">PART D</span></h4>
								<ul class="custom-bullet">
									<li><strong>FREE AURYXIA</strong> may be provided to eligible patients<sup>&#x2020;</sup>:
										<ul class="custom-bullet">
											<li>With no insurance</li>
											<li>Who have Medicare <span class="nowrap">Part D</span> insurance but cannot afford their copays</li>
											<li>Whose insurance does not cover AURYXIA</li>
										</ul>
									</li>
								</ul>

								<p class="footnote">*Restrictions may apply. Copay assistance is not valid for prescriptions reimbursed under Medicare, Medicaid, or similar federal or state programs.</p>
								<p class="footnote"><sup>&#x2020;</sup>Patients with an income of up to 400% Federal Poverty Level (FPL) may be eligible for free AURYXIA. Additional restrictions apply. For details, please contact an AkebiaCares Case Manager at <a href="tel:+18556868601">855-686-8601</a>.</p>

								<a href="/wp-content/uploads/AkebiaCares-Enrollment-Form.pdf">Enroll in AkebiaCares</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="sign-up">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 offset-md-2">
					<h2>Get the latest news and resources <span>on AURYXIA for you and your patients</span></h2>
					<a href="/hyperphosphatemia/sign-up/">Sign up now</a>
				</div>
			</div>
		</div>
	</section>

	<section id="trial-design">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10 offset-md-1">
					<div class="trial-design-container">
						<p><strong>Trial design<sup>4,8</sup></strong></p>
						<p>A multicenter, randomized, open-label, Phase III trial evaluated the safety and efficacy of AURYXIA as a phosphate binder in controlling serum phosphorus levels in adult patients with CKD on hemodialysis and peritoneal dialysis over 56 weeks. Eligible patients were on dialysis for ≥3 months before screening, were prescribed 3 to 18 doses of commercially available phosphate binder, and had serum ferritin <1000 μg/ml, serum TSAT <50%, and serum phosphorus ≥2.5 and ≤8.0 mg/dl at the screening visit. Patients who were intolerant to calcium acetate and sevelamer carbonate were not included in the trial.</p>
						<p>The safety and efficacy of AURYXIA was studied in the 52-week Active Control Period (AURYXIA n=292, Active Control n=149). At the final Active Control Period visit, AURYXIA patients were re-randomized to either continue AURYXIA treatment or receive placebo as part of the Placebo-Controlled Period (AURYXIA n=96, placebo n=96). The primary endpoint of the pivotal trial was the change in serum phosphorus from baseline (Week 52) to Week 56 between AURYXIA and placebo in the 4-week Placebo-Controlled Period.</p>
						<p class="footnote">CKD=chronic kidney disease; TSAT=transferrin saturation; Active Control=sevelamer carbonate and/or calcium acetate.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div id="psp" class="psp">
		<div class="psp-bar">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters">
					<div class="col-md-10">
						<div class="row no-gutters psp-bar-content">
							<p>IMPORTANT SAFETY INFORMATION</p>
							<div class="psp-bar-controls">
								<div class="psp-bar-show-more-button">
									<span>Show more</span>
									<span>Show less</span>
									<svg viewBox="0 0 129 80"><path fill="none" stroke="currentColor" stroke-width="23" stroke-miterlimit="10" d="M8.9 71.6l55.9-55.1 55.6 55.1"></path></svg>
								</div>
								<div class="psp-bar-collapse-button">
									<span>Show less</span>
									<svg viewBox="0 0 129 80"><path fill="none" stroke="currentColor" stroke-width="23" stroke-miterlimit="10" d="M8.9 71.6l55.9-55.1 55.6 55.1"></path></svg>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="isi">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters">
					<div id="isi-container" class="col-md-10">
						<div class="row no-gutters">
							<div class="col">
								<h2>CONTRAINDICATION</h2>
								<p>AURYXIA<sup>&#x00ae;</sup> (ferric citrate) is contraindicated in patients with iron overload syndromes, e.g., hemochromatosis</p>

								<h3>WARNINGS AND PRECAUTIONS</h3>
								<ul class="custom-bullet">
									<li><strong>Iron Overload:</strong> Increases in serum ferritin and transferrin saturation (TSAT) were observed in clinical trials with AURYXIA in patients with chronic kidney disease (CKD) on dialysis treated for hyperphosphatemia, which may lead to excessive elevations in iron stores. Assess iron <span class="cl-psp-close-position-desktop cl-psp-close-position-mobile">parameters</span> prior to initiating AURYXIA and monitor while on therapy. Patients receiving concomitant intravenous (IV) iron may require a reduction in dose or discontinuation of IV iron therapy</li>
									<li><strong>Risk of Overdosage in Children Due to Accidental Ingestion:</strong> Accidental ingestion and resulting overdose of iron-containing products is a leading cause of fatal poisoning in children under 6 years of age. Advise patients of the risks to children and to keep AURYXIA out of the reach of children</li>
								</ul>

								<h3>ADVERSE REACTIONS</h3>
								<p class="d-no-bottom-margin">The most common adverse reactions reported with AURYXIA in clinical trials were:</p>
								<ul class="custom-bullet">
									<li><u>Hyperphosphatemia in CKD on Dialysis</u>: Diarrhea (21%), discolored feces (19%), nausea (11%), constipation (8%), vomiting (7%) and cough (6%)</li>
								</ul>

								<h3>SPECIFIC POPULATIONS</h3>
								<ul class="custom-bullet">
									<li><strong>Pregnancy and Lactation:</strong> There are no available data on AURYXIA use in pregnant women to inform a drug-associated risk of major birth defects and miscarriage. However, an overdose of iron in pregnant women may carry a risk for spontaneous abortion, gestational diabetes and fetal malformation. Data from rat studies have shown the transfer of iron into milk, hence, there is a possibility of infant exposure when AURYXIA is administered to a nursing woman</li>
								</ul>
								<p>To report suspected adverse reactions, contact Akebia Therapeutics, Inc. at <a href="tel:+18444453799">1-844-445-3799</a></p>

								<h2>INDICATION</h2>
								<p class="d-no-bottom-margin">AURYXIA<sup>&#x00ae;</sup> (ferric citrate) is indicated for:</p>
								<ul class="custom-bullet">
									<li>The control of serum phosphorus levels in adult patients with chronic kidney disease on dialysis</li>
								</ul>
								<p><strong>Please see full <a target="_blank" href="/hyperphosphatemia/prescribing-information/">Prescribing Information</a></strong></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="references-section">
		<div class="container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="reference-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<h2>References</h2>
							<ol>
								<li>National Kidney Foundation, K/DOQI clinical practice guidelines for bone metabolism and disease in chronic kidney disease. <em>Am J Kidney Dis.</em> 2003;42(4 Suppl 3):S1-S201.</li>
								<li>Data on File 1, Akebia Therapeutics, Inc.</li>
								<li>Kidney Disease: Improving Global Outcomes (KDIGO) CKD-MBD Update Work Group. KDIGO 2017 Clinical Practice Guideline Update for the Diagnosis, Evaluation, Prevention, and Treatment of Chronic Kidney Disease-Mineral and Bone Disorder (CKD-MDB). <em>Kidney Intl Suppl.</em> 2017;7(1):1-59.</li>
								<li>AURYXIA<sup>&#x00ae;</sup> [package insert]. Cambridge, MA: Akebia Therapeutics, Inc.; 2021.</li>
								<li>Data on File 10, Akebia Therapeutics, Inc.</li>
								<li>Data on File 29, Akebia Therapeutics, Inc.</li>
								<li>Data on File 30, Akebia Therapeutics, Inc.</li>
								<li>Umanath K, Sika M, Niecestro R, et al; for Collaborative Study Group. Rationale and study design of a three-period, 58-week trial of ferric citrate as a phosphate binder in patients with ESRD on dialysis. <em>Hemodial Int.</em> 2013;17(1):67-74. 19. Data on File 24, Akebia Therapeutics, Inc</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer>
		<div class="container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="footer-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col no-gutters">
							<div id="mobile-footer" class="col d-md-none">
								<div class="footer-nav">
									<ul class="list-unstyled">
										<li><a target="_blank" href="https://akebia.com/contact/">Contact Us</a></li>
										<li><a target="_blank" href="https://akebia.com/legal/">Terms and Conditions</a></li>
										<li><a target="_blank" href="https://akebia.com/legal/privacy-policy.aspx">Privacy Policy</a></li>
										<li><a target="_blank" href="http://akebia.com/">Akebia.com</a></li>
									</ul>
								</div>
								<div class="footer-attributes">
									<div class="attribution">&copy;2021 Akebia Therapeutics, Inc.</div>
									<div class="pcr-number"><span style="padding-right: 0.5rem">PP-AUR-US-1367</span> 04/21</div>
									<a target="_blank" href="http://akebia.com/">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/logo-akebia-therapeutics.png">
									</a>
								</div>
							</div>
							<div id="desktop-footer" class="d-none d-md-block">
								<div class="row">
									<div class="col-md-9">
										<div class="footer-links">
											<a target="_blank" href="https://akebia.com/contact/">Contact Us</a>  |
											<a target="_blank" href="https://akebia.com/legal/">Terms and Conditions</a>  |
											<a target="_blank" href="https://akebia.com/legal/privacy-policy.aspx">Privacy Policy</a>  |
											<a target="_blank" href="http://akebia.com/">Akebia.com</a>
										</div>
										<div class="attribution">&copy;2021 Akebia Therapeutics, Inc. <span class="pcr-number"><span style="padding-right: 0.5rem">PP-AUR-US-1367</span> 04/21</span></div>

									</div>
									<div class="col-md-3">
										<a target="_blank" href="http://akebia.com/">
											<img class="footer-logo d-block ml-auto" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/Akebia_Ribbon_Logo_FullColor_RGB.png">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>

	<?php

	$theme_version = wp_get_theme()->get( 'Version' );
	// Switch between the raw ES6 javascript & the compiled javascript based on
	// the environment we're running in
	if (auryxia_use_raw_javascript()) { ?>

		<!--
		We're running locally (triggered by HTTP_HOST "akebia-web.test"), which means:
			- The raw ES6 javascript is referenced instead of the bundled main.min.js file
			- The QA API is used instead of the production API
			- A random number is tacked onto the main.js reference to try and avoid caching
		-->
		<script type="module">
			import { Auryxia } from "<?php echo get_template_directory_uri() . '/js/main.js?r=' . random_int(0, 100000); ?>";

			new Auryxia({"version":<?php echo json_encode($theme_version); ?>, "buildDate":<?php echo json_encode(date('c')) ?>, enableGridOverlay: true});
		</script>

	<?php } else { ?>

		<script type="text/javascript">
			new Auryxia({"version":<?php echo json_encode($theme_version); ?>, "buildDate":<?php echo json_encode(date('c')) ?>});
		</script>

	<?php } ?>
</body>
</html>

