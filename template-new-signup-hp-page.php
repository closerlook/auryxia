<?php
/**
 * Template Name: New Signup HP Page
 */

$body_id = '';

if ( is_front_page() ) {
	$body_id = 'id="home"';
} else {
	$body_id = is_page() || is_single() ? 'id="' . get_post_field( 'post_name' ) . '"' : '';
}
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, maximum-scale=1.0 , initial-scale=1.0">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php echo $body_id; ?> <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div class="mobile-nav d-md-none">
		<div class="top-links text-center">
			<a target="_blank" href="/hyperphosphatemia/important-safety-information">Important Safety Information</a> |
			<a target="_blank" href="/hyperphosphatemia/prescribing-information/">Full Prescribing Information</a>
		</div>
		<div class="hcp-warning text-center">This site is for U.S. Healthcare Professionals Only</div>
		<nav class="navbar">
			<a class="navbar-brand" href="/">
				<img alt="AURYXIA® (ferric citrate) tablets" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/logo.png">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile-menu" aria-controls="mobile-menu" aria-expanded="false" aria-label="Toggle Navigation">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/mobile-menu.png" class="navbar-toggler-icon">
			</button>
		</nav>
		<div class="indication-banner hcp text-center"><strong>Hyperphosphatemia</strong> CKD On Dialysis</div>
		<div class="collapse navbar-collapse" id="mobile-menu">
			<div class="navbar-nav">
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/">Home</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/efficacy/unmet-need/">Efficacy</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/efficacy/unmet-need/">Unmet Need</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/efficacy/trial-endpoints/">Pivotal Phase III Trial</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/efficacy/efficacy-results/">Efficacy Results</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/dosing/dosing-guidelines/">Dosing</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/dosing/dosing-guidelines/">Guidelines</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/dosing/administration/">Administration</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/safety/adverse-events/">Safety</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/safety/adverse-events/">Safety and Tolerability</a>
				<a class="nav-link color-subnav-link" href="/hyperphosphatemia/clinical-pharmacology/pharmacodynamics/">Pharmacodynamics</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/clinical-pharmacology/mechanism-of-action/">MOA</a>
				<a class="nav-link color-parent-link" href="/hyperphosphatemia/library/">Library</a>
				<a class="nav-link color-parent-link active" href="/hyperphosphatemia/sign-up">Sign Up</a>
				<a class="nav-link color-for-patients-link" target="_blank" href="/hyperphosphatemia/patients/">For Patients</a>
				<a class="nav-link color-akebiacares-link" target="_blank" href="/akebiacares/">AkebiaCares</a>
				<a class="nav-link color-ida-link" href="/iron-deficiency-anemia/">Iron Deficiency Anemia</a>
			</div>
		</div>
	</div>
	<div class="desktop-nav d-none d-md-block">
		<div class="top-section">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters">
					<div id="logo-container" class="col-md-10">
						<div class="row no-gutters">
							<div class="col-md-3">
								<a class="navbar-brand" href="/">
									<img alt="AURYXIA® (ferric citrate) tablets" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/logo.png">
								</a>
							</div>
							<div class="col-md-9">
								<div class="top-links text-right">
									<a target="_blank" href="/hyperphosphatemia/important-safety-information/">Important Safety Information</a> |
									<a target="_blank" href="/hyperphosphatemia/prescribing-information/">Full Prescribing Information</a> | <a target="_blank" href="/hyperphosphatemia/patients/">For Patients</a> |
									<a target="_blank" href="/akebiacares/">AkebiaCares</a> | <a href="/iron-deficiency-anemia/">Iron Deficiency Anemia</a>
								</div>
								<div class="hcp-warning text-right"><img class="icon-intent" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/icon-intent.png">This site is for U.S. Healthcare Professionals Only</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="desktop-menu container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="nav-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<nav class="navbar navbar-expand-md navbar-light">
								<a class="navbar-brand" href="/hyperphosphatemia/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/Home_Icon-1.png"></a>
								<ul class="navbar-nav">
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#" id="efficacyDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Efficacy</a>
										<div class="dropdown-menu efficacy" aria-labelledby="efficacyDropdownMenuLink">
											<a class="dropdown-item" href="/hyperphosphatemia/efficacy/unmet-need/">Unmet Need</a>
											<a class="dropdown-item" href="/hyperphosphatemia/efficacy/trial-endpoints/">Pivotal Phase III Trial</a>
											<a class="dropdown-item" href="/hyperphosphatemia/efficacy/efficacy-results/">Efficacy Results</a>
										</div>
									</li>
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#" id="dosingDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dosing</a>
										<div class="dropdown-menu" aria-labelledby="dosingDropdownMenuLink">
											<a class="dropdown-item" href="/hyperphosphatemia/dosing/dosing-guidelines/">Guidelines</a>
											<a class="dropdown-item" href="/hyperphosphatemia/dosing/administration/">Administration</a>
										</div>
									</li>
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#" id="safetyDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Safety</a>
										<div class="dropdown-menu" aria-labelledby="safetyDropdownMenuLink">
											<a class="dropdown-item" href="/hyperphosphatemia/safety/adverse-events/">Safety and Tolerability</a>
											<a class="dropdown-item" href="/hyperphosphatemia/clinical-pharmacology/pharmacodynamics/">Pharmacodynamics</a>
										</div>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="/hyperphosphatemia/clinical-pharmacology/mechanism-of-action/">MOA</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="/hyperphosphatemia/library/">Library</a>
									</li>
									<li class="nav-item">
										<a class="nav-link active" href="/hyperphosphatemia/sign-up/">Sign Up</a>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="indication-banner hcp">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters align-items-center">
					<div id="warning-container" class="col-md-10">
						<div class="row no-gutters">
							<div class="col">
								<strong>Hyperphosphatemia</strong> CKD On Dialysis
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php if (is_page('sign-up')) { ?>
		<section id="sign-up-form">
			<?php get_template_part('template-parts/content', 'sign-up-form'); ?>
		</section>
	<?php } else if (is_page('error')) { ?>
		<section id="error-container" class="important-safety-information-section">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters">
					<div id="inner-thank-you-container" class="col-md-10">
						<div class="row no-gutters">
							<div class="col">
								<div class="header">
									<p>Our apologies</p>
								</div>
								<div class="body-copy">
									<p>The system is currently experiencing technical difficulties. This error has been recorded and will be addressed promptly.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	<?php } else if (is_page('thank-you'))  {  ?>
		<section id="thank-you-container" class="important-safety-information-section">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters">
					<div id="inner-thank-you-container" class="col-md-10">
						<div class="row no-gutters">
							<div class="col">
								<div class="header">
									<p>Thank you</p>
								</div>
								<div class="body-copy">
									<p>You have successfully signed up to get the latest information on AURYXIA. Check your inbox for a confirmation email and be on the lookout for future AURYXIA news.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="cta-container" class="important-safety-information-section">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters">
					<div id="inner-cta-container" class="col-md-10">
						<div class="row no-gutters">
							<div class="col">
								<div class="callouts">
									<a href="/hyperphosphatemia/efficacy/efficacy-results" class="callout col-md-4 animC left callout-1" data-element="default" data-category="CTA" data-action="Click" data-label="Efficacy Results">
										<span class="callout-top"></span>
										<span class="main_text">EFFICACY RESULTS</span>

										<span class="sub_text">AURYXIA is a non-chewable phosphate binder with clinically proven results.<sup>1</sup> See the efficacy results for AURYXIA. </span>
									</a>
									<a href="/hyperphosphatemia/clinical-pharmacology/mechanism-of-action" class="callout col-md-4 animC left callout-2" data-element="default" data-category="CTA" data-action="Click" data-label="MOA">
										<span class="callout-top"></span>
										<span class="main_text">MECHANISM OF ACTION</span>

										<span class="sub_text">Watch a video to learn how AURYXIA works in the body to help manage patients’ phosphorus levels.</span>
									</a>
									<a href="/akebiacares/support-programs" class="callout animC nom col-md-4 left callout-3" data-element="default" data-category="CTA" data-action="Click" data-label="Patient Coverage">
										<span class="callout-top"></span>
										<span class="main_text">PATIENT COVERAGE</span>

										<span class="sub_text">AkebiaCares provides patients with coverage support and medicines at no cost. See if your patients are eligible.</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

<!--		<section id='callouts-home' class='container'>-->
<!--			-->
<!--		</section>-->
	<?php }?>
	<?php if (!is_page('error')) { ?>
	<section id="important-safety-information" class="important-safety-information-section">
		<div class="container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="isi-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<h2>Indication</h2>
							<p>AURYXIA<sup>&reg;</sup> (ferric citrate) is indicated for:</p>
							<ul class="custom-bullet">
								<li>The control of serum phosphorus levels in adult patients with chronic kidney disease on dialysis</li>
							</ul>
							<h2>Important Safety Information</h2>
							<h3>Contraindication</h3>
							<p>AURYXIA<sup>&reg;</sup> (ferric citrate) is contraindicated in patients with iron overload syndromes, e.g., hemochromatosis</p>
							<h3>Warnings and Precautions</h3>
							<ul class="custom-bullet">
								<li><strong>Iron Overload:</strong> Increases in serum ferritin and transferrin saturation (TSAT) were observed in clinical trials with AURYXIA in patients with chronic kidney disease (CKD) on dialysis treated for hyperphosphatemia, which may lead to excessive elevations in iron stores. Assess iron parameters prior to initiating AURYXIA and monitor while on therapy. Patients receiving concomitant intravenous (IV) iron may require a reduction in dose or discontinuation of IV iron therapy</li>
								<li><strong>Risk of Overdosage in Children Due to Accidental Ingestion:</strong> Accidental ingestion and resulting overdose of iron-containing products is a leading cause of fatal poisoning in children under 6 years of age. Advise patients of the risks to children and to keep AURYXIA out of the reach of children</li>
							</ul>
							<h3>Adverse Reactions</h3>
							<p>The most common adverse reactions reported with AURYXIA in clinical trials were:</p>
							<ul class="custom-bullet">
								<li><u>Hyperphosphatemia in CKD on Dialysis</u>: Diarrhea (21%), discolored feces (19%), nausea (11%), constipation (8%), vomiting (7%) and cough (6%)</li>
							</ul>
							<h3>Specific Populations</h3>
							<ul class="custom-bullet">
								<li><strong>Pregnancy and Lactation:</strong> There are no available data on AURYXIA use in pregnant women to inform a drug-associated risk of major birth defects and miscarriage. However, an overdose of iron in pregnant women may carry a risk for spontaneous abortion, gestational diabetes and fetal malformation. Data from rat studies have shown the transfer of iron into milk, hence, there is a possibility of infant exposure when AURYXIA is administered to a nursing woman</li>
							</ul>
							<p>To report suspected adverse reactions, contact Akebia Therapeutics, Inc. at <a href="tel:+18444453799">1-844-445-3799</a></p>
							<p><strong>Please see full <a target="_blank" href="/hyperphosphatemia/prescribing-information/">Prescribing Information</a></strong></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php } ?>
	<?php if (is_page('thank-you')) { ?>
	<section class="references-section">
		<div class="container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="reference-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<h2><img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/icon-references.png" class="icon-references">Reference</h2>
							<ol>
								<li>AURYXIA<sup>®</sup> [package insert]. Cambridge, MA: Akebia Therapeutics, Inc.; 2019.</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php } ?>

	<footer>
		<div class="container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="footer-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<div id="mobile-footer" class="col d-md-none">
								<div class="footer-nav">
									<ul class="list-unstyled">
										<li><a target="_blank" href="https://akebia.com/contact/">Contact Us</a></li>
										<li><a target="_blank" href="https://akebia.com/legal/">Terms and Conditions</a></li>
										<li><a target="_blank" href="https://akebia.com/legal/privacy-policy.aspx">Privacy Policy</a></li>
										<li><a target="_blank" href="http://akebia.com/">Akebia.com</a></li>
									</ul>
								</div>
								<div class="footer-attributes">
									<div class="attribution">&copy;2020 Akebia Therapeutics, Inc.</div>
									<div class="pcr-number"><span style="padding-right: 0.5rem">PP-AUR-US-1218</span> 11/20</div>
									<a target="_blank" href="http://akebia.com/">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/logo-akebia-therapeutics.png">
									</a>
								</div>
							</div>
							<div id="desktop-footer" class="d-none d-md-block">
								<div class="row">
									<div class="col-md-9">
										<div class="footer-links">
											<a target="_blank" href="https://akebia.com/contact/">Contact Us</a>  |
											<a target="_blank" href="https://akebia.com/legal/">Terms and Conditions</a>  |
											<a target="_blank" href="https://akebia.com/legal/privacy-policy.aspx">Privacy Policy</a>  |
											<a target="_blank" href="http://akebia.com/">Akebia.com</a>
										</div>
										<div class="attribution">&copy;2020 Akebia Therapeutics, Inc. <span class="pcr-number"><span style="padding-right: 0.5rem">PP-AUR-US-1218</span> 11/20</span></div>

									</div>
									<div class="col-md-3">
										<a target="_blank" href="http://akebia.com/">
											<img class="footer-logo d-block ml-auto" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/Akebia_Ribbon_Logo_FullColor_RGB.png">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>

	<?php

	$theme_version = wp_get_theme()->get( 'Version' );
	// Switch between the raw ES6 javascript & the compiled javascript based on
	// the environment we're running in
	if (auryxia_use_raw_javascript()) { ?>

		<!--
		  We're running locally (triggered by HTTP_HOST "akebia-web.test"), which means:
			- The raw ES6 javascript is referenced instead of the bundled main.min.js file
			- The QA API is used instead of the production API
			- A random number is tacked onto the main.js reference to try and avoid caching
		-->
		<script type="module">
			import { Auryxia } from "<?php echo get_template_directory_uri() . '/js/main.js?r=' . random_int(0, 100000); ?>";

			new Auryxia({"version":<?php echo json_encode($theme_version); ?>, "buildDate":<?php echo json_encode(date('c')) ?>, enableGridOverlay: true});
		</script>

	<?php } else { ?>

		<script type="text/javascript">
			new Auryxia({"version":<?php echo json_encode($theme_version); ?>, "buildDate":<?php echo json_encode(date('c')) ?>});
		</script>

	<?php } ?>

</body>
</html>

