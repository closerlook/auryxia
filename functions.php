<?php /* Author: Mirza Rahman */

require_once('lib/security.php');
require_once('lib/custom.php');

include_once "environment-detection.php";

/* Global Variables */
function site_global_var() {
  global $dir;
  $dir = get_template_directory_uri() . '/assets';
  // *** update this version to enforce cache busting for deployments ***
  // changed it to manually set vs 'wp_get_theme()->get( 'Version' );'
  global $theme_version;
  $theme_version = '1.3';
}
add_action('parse_query', 'site_global_var');


/* Set if HCP, Patient or Patient Assistance */
function setSiteType($id) {

  if (get_top_ancestor($id) == 11) {
    return "patient";
  } else if (get_top_ancestor($id) == 13) {
    return "assistance";
  } else{
    return "hcp";
  }
};

function setSiteTypeTwo($id) {
	if ($id === 11 || in_array(11, get_post_ancestors($id))) {
		return "patient";
	} else if ($id === 13 || in_array(13, get_post_ancestors($id))) {
		return "assistance";
	} else {
		return "hcp";
	}
}


// Disable Image Links
function wpb_imagelink_setup() {
	$image_set = get_option( 'image_default_link_type' );
	if ($image_set !== 'none') {
		update_option('image_default_link_type', 'none');
	}
}
add_action('admin_init', 'wpb_imagelink_setup', 10);


/* If Page Parent or Child of Parent */
function get_top_ancestor($id){
  $current = get_post($id);
  if(!$current->post_parent){
    return $current->ID;
  } else {
    return get_top_ancestor($current->post_parent);
  }
}


/* Remove Hard Coded Image Width / Height */
function mytheme_remove_img_dimensions($html) {
  $html = preg_replace('/(width|height)=["\']\d*["\']\s?/', "", $html);
  return $html;
}
add_filter('post_thumbnail_html', 'mytheme_remove_img_dimensions', 10);
add_filter('the_content', 'mytheme_remove_img_dimensions', 10);
add_filter('get_avatar','mytheme_remove_img_dimensions', 10);


/* Register Menus */
register_nav_menus( array(
  'ida_header' => 'IDA Navigation',
	'hcp_header' => 'HCP Navigation',
	'patients_header' => 'Patients Navigation',
	'pa_header' => 'Assistance Nav',
	'footer_links' => 'Footer Links',
	'portal_nav' => 'Portal Nav',
) );


/* Stop Saving Multiple Image Sizes */
function add_image_insert_override($sizes){
    unset( $sizes['thumbnail']);
    unset( $sizes['medium']);
    unset( $sizes['large']);
}
add_filter('intermediate_image_sizes_advanced', 'add_image_insert_override' );


/* Relative Image URLs */
add_filter('the_content', 'replace_content');
add_filter('the_excerpt', 'replace_content');
function replace_content($content){
  if (strpos($content,'src') !== false) {
    $sp = strpos($content,"src=") + 5;
    $ep = strpos($content,"\"",$sp);
  	$imageurl = substr($content,$sp,$ep-$sp);
  	$hostProtocol = "http";
  	if($_SERVER['SERVER_PORT'] == '443'){
  	  $hostProtocol = "https";
  	}
  	$relativeURL = str_replace($hostProtocol . "://","",$imageurl);
  	$sp = strpos($relativeURL,"/");
  	$relativeURL = substr($relativeURL,$sp);
  	$content = str_replace($imageurl,$relativeURL,$content);
  }
	return $content;
}


/* Add Superscript / Subscript Buttons */
function my_mce_buttons_2($buttons) {
	$buttons[] = 'sup';
	$buttons[] = 'sub';

	return $buttons;
}
add_filter('mce_buttons_2', 'my_mce_buttons_2');


/* Options Page */
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Global Options',
		'menu_title'	=> 'Global Options',
		'menu_slug' 	=> 'global-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'HCP Options',
		'menu_title'	=> 'HCP Options',
		'menu_slug' 	=> 'hcp-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Patient Settings',
		'menu_title'	=> 'Patient Settings',
		'menu_slug' 	=> 'patient-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'PA Settings',
		'menu_title'	=> 'PA Settings',
		'menu_slug' 	=> 'assistance-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}



/* Return HCP ISI via AJAX */
add_action( 'wp_ajax_load_isi_hcp', 'prefix_ajax_load_isi_hcp' );
add_action( 'wp_ajax_nopriv_load_isi_hcp', 'prefix_ajax_load_isi_hcp' );
function prefix_ajax_load_isi_hcp() {
  $id = 31;
  $post = get_post($id);
  $html = wpautop($post->post_content);
  die($html);
}

/* Return Patient ISI via AJAX */
add_action( 'wp_ajax_load_isi_patient', 'prefix_ajax_load_isi_patient' );
add_action( 'wp_ajax_nopriv_load_isi_patient', 'prefix_ajax_load_isi_patient' );
function prefix_ajax_load_isi_patient() {
  $id = 234;
  $post = get_post($id);
  $html = wpautop($post->post_content);
  die($html);
}

/* Return Patient Assistance via AJAX */
add_action( 'wp_ajax_load_isi_assistance', 'prefix_ajax_load_isi_assistance' );
add_action( 'wp_ajax_nopriv_load_isi_assistance', 'prefix_ajax_load_isi_assistance' );
function prefix_ajax_load_isi_assistance() {
  $id = 31;
  $post = get_post($id);
  $html = wpautop($post->post_content);
  die($html);
}


/*
  Shortcode for Tab Layouts
  Usage: Insert [site_tabs id="page-id-of-tabs-content"] on WP Content Editor
*/
function site_tabs_func($attr) {
  $queryID = $attr['id'];
  $html = "<div id='tab-wrap'>";
  $tabContent = array();
  $count = 1;
  $class = "";

  // Query Page
  query_posts("page_id=$queryID");
  while (have_posts() ): the_post();
    if(get_field('tabs')){
      // Generate HTML Markup for Tab Names
      $count = 1;
      $html.= "<div class='tab-names cf'>";
      while(has_sub_field('tabs')){
        if ($count == 1){ $class = "active";} else { $class = "";}
        $html .= "<a href='#' class='animC $class'>". get_sub_field('tab_name') ."</a>";
        $tabContent[] = get_sub_field('tab_content');
        $count++;
      }
      $html .= "</div>";

      // Generate HTML Markup for Tab Content
      $count = 1;
      $html .= "<div class='tabs cf'>";
      foreach ($tabContent as $tabC){
        if ($count == 1){ $class = "active";} else { $class = "";}
        // Remove Image Links
        $html .= "<div class='tab $class'>$tabC</div>";
        $count++;
      }
      $html .= "</div>";
    }
  endwhile;
  wp_reset_query();
  $html .= "</div>";
  return $html;
}
add_shortcode( 'site_tabs', 'site_tabs_func' );



/*
  Shortcode for Chart Layouts
  Usage: Insert [site_chart id="page-id-of-tabs-content"] on WP Content Editor
*/
function site_chart_func($attr) {
  $queryID = $attr['id'];
  $html = "<div id='chart-wrap'>";
  $tabContent = array();
  $count = 1;
  $class = "";

  // Query Page
  query_posts("page_id=$queryID");
  while (have_posts() ): the_post();
    if(get_field('charts')){
      while(has_sub_field('charts')){
        $html.= "<div class='chart-row cf'>";
        $html .= "<img src='". get_sub_field('image') ."' ?>";
        $html .= "<div class='chart-right'>". get_sub_field('copy') ."</div>";
        $html .= "</div>";
      }
    }
  endwhile;
  wp_reset_query();
  $html .= "</div>";
  return $html;
}
add_shortcode( 'site_chart', 'site_chart_func' );

add_filter( 'wp_lazy_loading_enabled', '__return_false' );

// added by Will English IV

add_theme_support( 'title-tag' );

if ( ! function_exists( 'wp_body_open' ) ) {

	/**
	 * Shim for wp_body_open, ensuring backward compatibility with versions of WordPress older than 5.2.
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
}

add_filter( 'body_class', 'extra_body_class' );
// Add specific CSS class by filter
function extra_body_class( $classes ) {
	if ( is_front_page() ) {
		$classes[] = 'auryxiaDefault';
	}

	if ( is_page_template('template-new-front-page.php') && is_page('important-safety-information') ) {
		$classes[] = 'impSafeHome';
	}

	if ( is_page_template('template-ida-patient.php') ) {
		if ( is_page('patient') ) $classes[] = 'homePage';
		if ( is_page('disease-101') ) $classes[] = 'diseasePage';
		if ( is_page('101') ) $classes[] = 'AuryxiaPage';
		if ( is_page('101-2') ) $classes[] = 'AuryxiaPage';
		if ( is_page('taking-auryxia') ) $classes[] = 'takingAuryxiaPage';
		if ( is_page('access-support') ) $classes[] = 'supportPage';
		if ( is_page('faqs') ) $classes[] = 'faq-Body';
		if ( is_page('important-safety-information') ) $classes[] = 'isiPage';
		if ( is_page('resources') ) $classes[] = 'resourcesPage';
		if ( is_page('what-to-tell-your-doctor') ) $classes[] = 'talkingDoctorPage';
	}

	return $classes;
}

add_action('wp_enqueue_scripts', 'auryxia_register_scripts');
function auryxia_register_scripts() {
	wp_enqueue_script( 'jquery-new', get_template_directory_uri() . '/assets/js/vendor/jquery-3.5.1.min.js', array(), '3.5.1', true );
	global $post;
	global $theme_version;

	if ( is_page_template('template-new-front-page.php') ) {
		wp_enqueue_script( 'isInViewport', get_template_directory_uri() . '/assets/js/vendor/isInViewport.min.js', array( 'jquery-new' ), '3.0.4', true );
		wp_enqueue_script( 'TweenMax', get_template_directory_uri() . '/assets/js/vendor/TweenMax.min.js', array( 'jquery-new' ), '2.1.3', true );
		wp_enqueue_script( 'popper', get_template_directory_uri() . '/assets/js/vendor/popper.min.js', array( 'jquery-new' ), '1.14.3', true );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/vendor/bootstrap.min.js', array( 'popper' ), '4.1.3', true );
		wp_enqueue_script( 'fast-fonts', 'https://fast.fonts.net/jsapi/79ad4d54-144b-476c-9335-9765e30f0fae.js', array(), null, true );
		wp_enqueue_script( 'home-main', get_template_directory_uri() . '/assets/js/main-home.js', array( 'jquery-new' ), $theme_version, true );
	}

	if ( is_page_template('template-akebiacares.php') ) {
		wp_enqueue_script( 'akebiacares-main', get_template_directory_uri() . '/assets/js/akebiacares/main.js', array( 'jquery-new' ), $theme_version, true );
		wp_enqueue_script( 'akebiacares-modals', get_template_directory_uri() . '/assets/js/akebiacares/modals.js', array( 'akebiacares-main' ), $theme_version, true );
	}

	if ( is_page_template('template-ida.php') ) {
		wp_enqueue_script( 'TweenMax', get_template_directory_uri() . '/assets/js/vendor/TweenMax.min.js', array( 'jquery-new' ), '2.1.3', true );
		wp_enqueue_script( 'ScrollToPlugin', get_template_directory_uri() . '/assets/js/vendor/ScrollToPlugin.min.js', array( 'jquery-new' ), '1.9.0', true );
		wp_enqueue_script( 'bootstrap-three', get_template_directory_uri() . '/assets/js/vendor/bootstrap-3.3.7/bootstrap.min.js', array( 'jquery-new' ), '3.3.7', true );
//		wp_enqueue_script( 'ida-main', get_template_directory_uri() . '/assets/js/ida/main.js', array( 'jquery-new' ), $theme_version, true );
		wp_enqueue_script( 'ida-main', get_template_directory_uri() . '/assets/js/ida/main.new.js', array( 'jquery-new' ), $theme_version, true );
		wp_enqueue_script( 'fast-fonts', 'https://fast.fonts.net/jsapi/79ad4d54-144b-476c-9335-9765e30f0fae.js', array(), null, true );
	}

	if ( is_page_template( 'template-new-signup-hp-page.php' ) ) {
		wp_enqueue_script( 'isInViewport', get_template_directory_uri() . '/assets/js/vendor/isInViewport.min.js', array( 'jquery-new' ), '3.0.4', true );
		wp_enqueue_script( 'TweenMax', get_template_directory_uri() . '/assets/js/vendor/TweenMax.min.js', array( 'jquery-new' ), '2.1.3', true );
		wp_enqueue_script( 'popper', get_template_directory_uri() . '/assets/js/vendor/popper.min.js', array( 'jquery-new' ), '1.14.3', true );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/vendor/bootstrap.min.js', array( 'popper' ), '4.1.3', true );
	}

	if ( is_page_template('template-ida-patient.php') ) {
		wp_enqueue_script( 'isInViewport', get_template_directory_uri() . '/assets/js/vendor/isInViewport.min.js', array( 'jquery-new' ), '3.0.4', true );
		wp_enqueue_script( 'TweenMax', get_template_directory_uri() . '/assets/js/vendor/TweenMax.min.js', array( 'jquery-new' ), '2.1.3', true );
		wp_enqueue_script( 'popper', get_template_directory_uri() . '/assets/js/vendor/popper.min.js', array( 'jquery-new' ), '1.14.3', true );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/vendor/bootstrap.min.js', array( 'popper' ), '4.1.3', true );
		wp_enqueue_script( 'ida-patient-script', get_template_directory_uri() . '/assets/js/ida-patient/script.js', array( 'jquery-new' ), $theme_version, true );
		wp_enqueue_script( 'ida-patient-tracker', get_template_directory_uri() . '/assets/js/ida-patient/fcbTracker_v8.js', array( 'jquery-new' ), $theme_version, true );
	}

	if ( is_page(76)  || in_array( 76, get_post_ancestors( $post->id ) ) ) {
		wp_enqueue_script( 'fast-fonts', 'https://fast.fonts.net/jsapi/79ad4d54-144b-476c-9335-9765e30f0fae.js', array(), null, true );
		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/libs/modernizr.js', array(), '2.8.3', false );
		wp_enqueue_script( 'hp-plugins', get_template_directory_uri() . '/assets/js/libs/plugins.js', array('jquery-new'), $theme_version, true );
		wp_enqueue_script( 'hp-modals', get_template_directory_uri() . '/assets/js/modals.js', array('hp-plugins'), $theme_version, true );
		wp_enqueue_script( 'hp-main', get_template_directory_uri() . '/assets/js/main.js', array('hp-modals'), $theme_version, true );
	}

	if (
		is_page_template( 'template-new-moa-hp-page.php' ) ||
		is_page_template( 'template-new-moa-ida-page.php' )
	) {
//		wp_enqueue_script( 'hp-moa-bootstrap-js', get_template_directory_uri() . '/assets/js/hp-moa/bootstrap.bundle.min.js', array( 'jquery-new' ), $theme_version, true );
		wp_enqueue_script( 'hp-moa-fontawesome', 'https://use.fontawesome.com/releases/v5.13.0/js/all.js', array('jquery-new'), '5.13.0', false);
		wp_enqueue_script( 'fast-fonts-cl', 'https://fast.fonts.net/jsapi/662eda4e-a0fa-425c-9659-f89fbc8324c4.js', array('hp-moa-fontawesome'), null, true );
		wp_enqueue_script( 'hp-moa-bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js', array( 'fast-fonts-cl' ), '4.5.0', true );
		wp_enqueue_script( 'gsap-tweenmax', get_template_directory_uri() . '/assets/js/hp-moa/gsap/TweenMax.min.js', array( 'hp-moa-bootstrap-js' ), $theme_version, true );
		wp_enqueue_script( 'gsap-customease', get_template_directory_uri() . '/assets/js/hp-moa/gsap/easing/CustomEase.min.js', array( 'gsap-tweenmax' ), $theme_version, true );
		wp_enqueue_script( 'gsap-customwiggle', get_template_directory_uri() . '/assets/js/hp-moa/gsap/easing/CustomWiggle.min.js', array( 'gsap-customease' ), $theme_version, true );
		wp_enqueue_script( 'gsap-timelinemax', get_template_directory_uri() . '/assets/js/hp-moa/gsap/TimelineMax.min.js', array( 'gsap-customwiggle' ), $theme_version, true );
		wp_enqueue_script( 'hp-moa-brightcove', 'https://players.brightcove.net/5982844371001/eQq6v3QYZR_default/index.min.js', array('gsap-timelinemax'), $theme_version, true);
		wp_enqueue_script( 'hp-moa-scripts', get_template_directory_uri() . '/assets/js/hp-moa/scripts.js', array( 'hp-moa-brightcove' ), $theme_version, true );

	}

	if ( is_page_template( 'template-new-competitive-landing-hp-page.php' ) ) {
		wp_enqueue_script( 'hp-moa-fontawesome', 'https://use.fontawesome.com/releases/v5.13.0/js/all.js', array('jquery-new'), '5.13.0', false);
		wp_enqueue_script( 'fast-fonts-cl', 'https://fast.fonts.net/jsapi/662eda4e-a0fa-425c-9659-f89fbc8324c4.js', array('hp-moa-fontawesome'), null, true );
		wp_enqueue_script( 'hp-moa-bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js', array( 'fast-fonts-cl' ), '4.5.0', true );
		wp_enqueue_script( 'gsap-tweenmax', get_template_directory_uri() . '/assets/js/hp-moa/gsap/TweenMax.min.js', array( 'hp-moa-bootstrap-js' ), $theme_version, true );
		// wp_enqueue_script( 'gsap-customease', get_template_directory_uri() . '/assets/js/hp-moa/gsap/easing/CustomEase.min.js', array( 'gsap-tweenmax' ), $theme_version, true );
		// wp_enqueue_script( 'gsap-customwiggle', get_template_directory_uri() . '/assets/js/hp-moa/gsap/easing/CustomWiggle.min.js', array( 'gsap-customease' ), $theme_version, true );
		wp_enqueue_script( 'gsap-timelinemax', get_template_directory_uri() . '/assets/js/hp-moa/gsap/TimelineMax.min.js', array( 'gsap-customwiggle' ), $theme_version, true );
		// wp_enqueue_script( 'hp-moa-brightcove', 'https://players.brightcove.net/5982844371001/eQq6v3QYZR_default/index.min.js', array('gsap-timelinemax'), $theme_version, true);
		// wp_enqueue_script( 'hp-moa-scripts', get_template_directory_uri() . '/assets/js/hp-moa/scripts.js', array( 'hp-moa-brightcove' ), $theme_version, true );


	}

	wp_localize_script('jquery-new', 'myAjax', array(
		'ajaxurl' => admin_url('admin-ajax.php'),
		'signup_nonce' => wp_create_nonce("signup_nonce")
	));

	// If we're running locally, there'll be a <script type="module"> block
	// at the end of the page that loads the uncompiled ES6 javascript
	if (!auryxia_use_raw_javascript()) {
		wp_enqueue_script('auryxia-main', get_template_directory_uri() . '/js/main.min.js', array(/*'jquery', 'gsap-core', 'gsap-cssrule-plugin','tiny-slider'*/), $theme_version, true);
	}
}

add_action('wp_enqueue_scripts', 'auryxia_enqueue_styles');
function auryxia_enqueue_styles() {
	global $post;
	global $theme_version;

	if ( is_page_template( 'template-new-front-page.php' ) ) {
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/vendor/bootstrap.min.css', array(), '4.1.3' );
		wp_enqueue_style( 'bootstrap-three', get_template_directory_uri() . '/assets/css/vendor/bootstrap-3.3.7/bootstrap.min.css', array( 'bootstrap' ), '3.3.7' );
		wp_enqueue_style( 'home-main', get_template_directory_uri() . '/assets/css/ida/styles.css', array( 'bootstrap-three' ), $theme_version );
	}

	if ( is_page_template( 'template-akebiacares.php' ) ) {
		wp_enqueue_style( 'akebiacares-main', get_template_directory_uri() . '/assets/css/akebiacares/main.css', array(), $theme_version );
		wp_enqueue_style( 'akebiacares-patients', get_template_directory_uri() . '/assets/css/akebiacares/patients.css', array( 'akebiacares-main' ), $theme_version );
		wp_enqueue_style( 'akebiacares-media', get_template_directory_uri() . '/assets/css/akebiacares/media.css', array( 'akebiacares-patients' ), $theme_version );
		wp_enqueue_style( 'akebiacares-style', get_template_directory_uri() . '/assets/css/akebiacares/style.css', array( 'akebiacares-media' ), $theme_version );
	}

	if ( is_page_template( 'template-ida.php' ) ) {
		wp_enqueue_style( 'bootstrap-three', get_template_directory_uri() . '/assets/css/vendor/bootstrap-3.3.7/bootstrap.min.css', array(), '3.3.7' );
		wp_enqueue_style( 'ida-main', get_template_directory_uri() . '/assets/css/ida/styles.css', array( 'bootstrap-three' ), $theme_version );
	}

	if ( is_page_template( 'template-ida-patient.php' ) ) {
		wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.5.0/css/all.css', array(), '5.5.0' );
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/vendor/bootstrap.min.css', array(), '4.1.3' );

		wp_enqueue_style( 'ida-patient-style', get_template_directory_uri() . '/assets/css/ida-patient/style.css', array(), $theme_version );
		wp_enqueue_style( 'ida-patient-nav', get_template_directory_uri() . '/assets/css/ida-patient/nav.css', array(), $theme_version );

		if ( is_page( array( 'resources', 'patient', 'important-safety-information' ) ) ) {
			wp_enqueue_style( 'ida-patient-home', get_template_directory_uri() . '/assets/css/ida-patient/home.css', array(), $theme_version );
		}

		if ( is_page( array(
			'resources',
			'access-support',
			'101-2',
			'101',
			'taking-auryxia',
			'what-to-tell-your-doctor'
		) ) ) {
			wp_enqueue_style( 'ida-patient-auryxia', get_template_directory_uri() . '/assets/css/ida-patient/auryxia.css', array(), $theme_version );
		}


		if ( is_page( array( 'resources', 'access-support' ) ) ) {
			wp_enqueue_style( 'ida-patient-support', get_template_directory_uri() . '/assets/css/ida-patient/support.css', array(), $theme_version );
		}

		if ( is_page( array(
			'resources',
			'access-support',
			'101-2',
			'101',
			'taking-auryxia',
			'disease-101',
			'what-to-tell-your-doctor'
		) ) ) {
			wp_enqueue_style( 'ida-patient-disease', get_template_directory_uri() . '/assets/css/ida-patient/disease.css', array(), $theme_version );
		}

		if ( is_page( array( 'faqs' ) ) ) {
			wp_enqueue_style( 'ida-patient-faq', get_template_directory_uri() . '/assets/css/ida-patient/faq.css', array(), $theme_version );
		}
	}

	/*
		ALL HCP Subpages inherit from HCP styles
		Excluding the new MOA landing pages from that list first to remove conflicting styles
	*/

	if (
		is_page_template( 'template-new-moa-hp-page.php' ) ||
		is_page_template( 'template-new-moa-ida-page.php' ) ||
		is_page_template( 'template-new-signup-hp-page.php' )
	) {
		wp_enqueue_style( 'hp-moa-style', get_template_directory_uri() . '/assets/css/hp-moa/styles.css', array(), $theme_version );
	} else if ( is_page_template( 'template-new-competitive-landing-hp-page.php' ) ) {
		// Having to hard refresh during development was getting annoying
		$assetPath = get_template_directory_uri() . '/sass/pages/hp-competitive-landing/styles.min.css';
		if (auryxia_use_raw_javascript()) {
			$assetPath = $assetPath . '?r=' . random_int(0, 100000);
		}

		wp_enqueue_style( 'hp-competitive-landing-style', $assetPath, array(), $theme_version );
	} else if ( is_page(76) || in_array( 76, get_post_ancestors( $post->id ) ) ) {
		wp_enqueue_style( 'hp-main', get_template_directory_uri() . '/assets/css/main.css', array(), $theme_version );
		wp_enqueue_style( 'hp-patients', get_template_directory_uri() . '/assets/css/patients.css', array( 'hp-main' ), $theme_version );
		wp_enqueue_style( 'hp-media', get_template_directory_uri() . '/assets/css/media.css', array( 'hp-patients' ), $theme_version );
		wp_enqueue_style( 'hp-style', get_template_directory_uri() . '/style.css', array( 'hp-media' ), $theme_version );
		wp_enqueue_style( 'hp-hcp', get_template_directory_uri() . '/assets/css/hcp.css', array('hp-style'), $theme_version);
	}

	if ( ( is_page( array('sign-up', 'thank-you', 'error') ) ) ) {
//		wp_enqueue_style( 'hp-moa-style', get_template_directory_uri() . '/assets/css/hp-moa/styles.css', array(), $theme_version );
		wp_enqueue_script( 'hp-moa-fontawesome', 'https://use.fontawesome.com/releases/v5.13.0/js/all.js', array('jquery-new'), '5.13.0', false);
		wp_enqueue_script( 'fast-fonts-cl', 'https://fast.fonts.net/jsapi/662eda4e-a0fa-425c-9659-f89fbc8324c4.js', array('hp-moa-fontawesome'), null, true );
		wp_enqueue_script( 'fast-fonts', 'https://fast.fonts.net/jsapi/79ad4d54-144b-476c-9335-9765e30f0fae.js', array(), null, true );

	}

	wp_enqueue_style( 'auryxia-style', get_stylesheet_directory_uri() . '/sass/styles.min.css', array(),  $theme_version );
}



add_filter( 'style_loader_tag',  'add_integrity', 10, 4 );
function add_integrity( $html, $handle, $href, $media ) {
	if ( $handle === 'fontawesome' ) $html = str_replace('>', 'integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">', $html);

	return $html;
}

add_filter( 'script_loader_tag',  'add_integrity_scripts', 10, 3 );
function add_integrity_scripts( $html, $handle, $href ) {
	if ( $handle === 'hp-moa-fontawesome' ) $html = str_replace('>', ' crossorigin="anonymous">', $html);
	return $html;
}


add_action('wp_body_open', 'gtm_tag_closerlook_noscript', 5);
function gtm_tag_closerlook_noscript() {
	echo '<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P5T5HJB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->';
}

add_action('wp_head', 'add_favicon');
function add_favicon() {
	echo "<link rel=\"icon\" href=\"" . get_template_directory_uri() . "/favicon.ico\" />";
}

add_action('wp_head', 'gtm_tag_closerlook');
function gtm_tag_closerlook() {
	echo "<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P5T5HJB');</script>
<!-- End Google Tag Manager -->";
}

//add_action('wp_head', 'cl_cookiepro');
function cl_cookiepro() {
	echo '<!-- OneTrust Cookies Consent Notice start -->
	<script src="https://cookie-cdn.cookiepro.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="20038081-6e78-46f0-8bde-230c7dcd52c2"></script>
	<script type="text/javascript">
		function OptanonWrapper() { }
	</script>
	<!-- OneTrust Cookies Consent Notice end -->';
}

function cl_cookiepro_test() {
	echo '<!-- CookiePro Cookies Consent Notice start for auryxiastaging.wpengine.com -->

<script src="https://cookie-cdn.cookiepro.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="7c5b33a5-e52c-4b2e-95c3-860861102162-test" ></script>
<script type="text/javascript">
function OptanonWrapper() { }
</script>
<!-- CookiePro Cookies Consent Notice end for auryxiastaging.wpengine.com -->';
}

$cookie_allowed_hosts = array('auryxia.com', 'www.auryxia.com');
if (!isset($_SERVER['HTTP_HOST']) || !in_array($_SERVER['HTTP_HOST'], $cookie_allowed_hosts)) {
	// use production cookie pro only on prodction domain
	add_action('wp_head', 'cl_cookiepro_test');
} else {
	add_action('wp_head', 'cl_cookiepro');
}

add_action('wp_footer', 'cl_doubleclick_script');
function cl_doubleclick_script() {
	if (is_page_template('template-akebiacares.php')) { ?>
	<script type="text/javascript">
		const a =  Math.random() * 10000000000000;
		document.write('<iframe src="https://4416544.fls.doubleclick.net/activityi;src=4416544;type=Keryx0;cat=auryx000;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
	</script>
	<?php }
}

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/includes/template-functions.php';


