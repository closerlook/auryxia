<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php
  if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
    echo '<meta name="viewport" content="width=1024px; user-scalable=1; maximum-scale=1.0" >';
  } else{
    echo '<meta name="viewport" content="user-scalable=no, maximum-scale=1.0 , initial-scale=1.0" />';
  }
  ?>

  <?php if (is_page( array(1095,1097,1100))){ ?>
  <meta name="robots" content="noindex, nofollow">
  <?php } else { ?>
  <meta name="description" content="Official AURYXIA website for US Healthcare Professionals. Find all the information you need about FDA-approved AURYXIA (ferric citrate)" />
  <?php } ?>

  <title><?php bloginfo('name'); wp_title('|',true,'left'); ?></title>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/patients.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/media.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() .'/style.css'; ?>">
  <!-- OneTrust Cookies Consent Notice start -->
  <script src="https://cookie-cdn.cookiepro.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="20038081-6e78-46f0-8bde-230c7dcd52c2"></script>
  <script type="text/javascript">
      function OptanonWrapper() { }
  </script>
  <!-- OneTrust Cookies Consent Notice end -->
  <script type="text/javascript" src="https://fast.fonts.net/jsapi/79ad4d54-144b-476c-9335-9765e30f0fae.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/modernizr.js"></script>
  <!-- Google Tag Manager -->
  <script>
      (function(w, d, s, l, i) {
          w[l] = w[l] || [];
          w[l].push({
              'gtm.start': new Date().getTime(),
              event: 'gtm.js'
          });
          var f = d.getElementsByTagName(s)[0],
              j = d.createElement(s),
              dl = l != 'dataLayer' ? '&l=' + l : '';
          j.async = true;
          j.src =
              'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
          f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', 'GTM-P5T5HJB');
  </script>
  <!-- End Google Tag Manager -->

</head>
