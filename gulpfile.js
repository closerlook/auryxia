﻿/// <binding ProjectOpened='watch' />

const
    // Utilities
    { src, dest, series, task, watch } = require("gulp"),
    pipeline = require("readable-stream").pipeline,
    rename = require("gulp-rename"),
    namedWithPath = require("vinyl-named-with-path"),

    // SASS compilation
    sass = require("gulp-sass"),

    // Javascript compilation
    webpack = require("webpack"),
    webpackStream = require("webpack-stream");

const
    settings = {
        sass: {
            // Paths to watch for triggering recompilation
            watch: [
                "sass/**/*.scss"
            ],

            // SASS source files
            sourceFiles: [
                "sass/styles.scss",
                "sass/pages/hp-competitive-landing/styles.scss"
            ]
        },

        javascript: {
            // Paths to watch for triggering recompilation
            watch: [
                "js/**/*.js",
                "!js/**/*.min.js"
            ],
            
            // Javascript source files
            sourceFiles: [
                "js/main.js"
            ]
        }
    },

    swallowError = function (_) {
        this.emit('end')
    };

// "source" may be a single path string or an array of paths
function minifySass(source, cb) {
    pipeline(
        src(source, { base: "." }),

        // Compile the SASS to CSS
        sass(
            {
                outputStyle: "compressed"
            }),

        // Add ".min" to the CSS file's extension
        rename({
            suffix: ".min"
        }),

        dest("."),
        cb
    );
}

// "source" may be a single path string or an array of paths
function minifyJavascript(source, cb) {
    pipeline(
        src(source, { base: "." }),

        // This is necessary for webpack-stream to work with an
        // array of source files instead of a single path (apparently)
        namedWithPath(),

        // Run webpack, using babel-loader to transpile all the files
        webpackStream(
            {
                mode: "production",
                devtool: "source-map",
                output: {
                    filename: "[name].min.js"
                },
                module: {
                    rules: [
                        {
                            test: /\.js?$/,
                            exclude: /(node_modules)/,
                            loader: 'babel-loader',
                            query: {
                                presets: [
                                    [
                                        "@babel/env",
                                        {
                                            "targets": {
                                                "ie": "11"
                                            }
                                        }
                                    ]
                                ]
                            }
                        }
                    ]
                }
            },
            webpack),

        dest("."),
        cb
    );
}

function pathIsInArray(path, array) {
    path = path.replace(/\\/g, "/");

    for (let i = 0; i < array.length; i++) {
        if (array[i] == path)
            return true;
    }

    return false;
}

exports.watch = () => {
    // SASS
    watch(
        settings.sass.watch,
        {
            read: false
        })

        // Prevent errors from halting the watch process
        .on("error", swallowError)

        // Trigger on all events
        .on("all", function (event, path) {
            // Check if the path is one of the items in the sourceFiles array
            let source = settings.sass.sourceFiles;
            if (pathIsInArray(path, source)) {
                // Path is in our array - only minify this file
                source = [path];
            }

            task("minifySass", function (cb) {
                console.log(`Processing ${source.length} SASS file${source.length > 1 ? "s" : ""}`);
                minifySass(source, cb);
            });

            series("minifySass")();
        });

    // Javascript
    watch(
        settings.javascript.watch,
        {
            read: false
        })

        // Prevent errors from halting the watch process
        .on("error", swallowError)

        // Trigger on all events
        .on("all", function (event, path) {
            // Check if the path is one of the items in the sourceFiles array
            let source = settings.javascript.sourceFiles;
            if (pathIsInArray(path, source)) {
                // Path is in our array - only minify this file
                source = [path];
            }

            task("minifyJavascript", function (cb) {
                console.log(`Processing ${source.length} javascript file${source.length > 1 ? "s" : ""}`);
                minifyJavascript(source, cb);
            });

            series("minifyJavascript")();
        });
};

exports.minifySass = function (cb) {
    minifySass(settings.sass.sourceFiles, cb);
};

exports.minifyJavascript = function (cb) {
    minifyJavascript(settings.javascript.sourceFiles, cb);
};

exports.minifyEverything = function (cb) {
    task("minifySass", function (cb) {
        minifySass(settings.sass.sourceFiles, cb);
    });

    task("minifyJavascript", function (cb) {
        minifyJavascript(settings.javascript.sourceFiles, cb);
    });

    series(
        "minifySass",
        "minifyJavascript")(cb);
}

exports.default = exports.minifyEverything;
