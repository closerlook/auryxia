<?php
/**
 * Template Name: New MOA IDA Page
 */
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, maximum-scale=1.0 , initial-scale=1.0">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div class="mobile-nav d-md-none">
		<div class="top-links text-center">
			<a target="_blank" href="/iron-deficiency-anemia/important-safety-information/">Important Safety Information</a> |
			<a target="_blank" href="/wp-content/uploads/Auryxia_PI.pdf">Full Prescribing Information</a>
		</div>
		<div class="hcp-warning text-center">This site is for U.S. Healthcare Professionals Only</div>
		<nav class="navbar">
			<a class="navbar-brand" href="/">
				<img alt="AURYXIA® (ferric citrate) tablets" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/logo.png">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile-menu" aria-controls="mobile-menu" aria-expanded="false" aria-label="Toggle Navigation">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/mobile-menu.png" class="navbar-toggler-icon">
			</button>
		</nav>
		<div class="indication-banner ida text-center"><strong>Iron Deficiency Anemia</strong> CKD Not On Dialysis</div>
		<div class="collapse navbar-collapse" id="mobile-menu">
			<div class="navbar-nav">
				<a class="nav-link color-parent-link" href="/iron-deficiency-anemia/">Home</a>
				<a class="nav-link color-parent-link" href="/iron-deficiency-anemia/efficacy/">Efficacy</a>
				<a class="nav-link color-parent-link" href="/iron-deficiency-anemia/tolerability-and-safety/">Safety</a>
				<a class="nav-link color-parent-link" href="/iron-deficiency-anemia/dosing/">Dosing</a>
				<a class="nav-link color-parent-link active" href="/iron-deficiency-anemia/moa/">MOA</a>
				<a class="nav-link color-parent-link" href="/iron-deficiency-anemia/interactive-library/">Library</a>
				<a class="nav-link color-parent-link" href="/iron-deficiency-anemia/sign-up/">Sign Up</a>
				<a class="nav-link color-for-patients-link" target="_blank" href="/iron-deficiency-anemia/patient/">For Patients</a>
				<a class="nav-link color-akebiacares-link" target="_blank" href="/akebiacares/">AkebiaCares</a>
				<a class="nav-link color-hp-link" href="/hyperphosphatemia/">Hyperphosphatemia</a>
			</div>
		</div>
	</div>
	<div class="desktop-nav d-none d-md-block">
		<div class="top-section">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters">
					<div id="logo-container" class="col-md-10">
						<div class="row no-gutters">
							<div class="col-md-3">
								<a class="navbar-brand" href="/">
									<img alt="AURYXIA® (ferric citrate) tablets" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/logo.png">
								</a>
							</div>
							<div class="col-md-9">
								<div class="top-links text-right">
									<a href="/iron-deficiency-anemia/important-safety-information/">Important Safety Information</a> |
									<a target="_blank" href="/wp-content/uploads/Auryxia_PI.pdf">Full Prescribing Information</a> |
									<a href="/iron-deficiency-anemia/patient/">For Patients</a> |
									<a href="/akebiacares/">AkebiaCares</a> |
									<a href="/hyperphosphatemia/">Hyperphosphatemia</a>
								</div>
								<div class="hcp-warning text-right"><img class="icon-intent" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/icon-intent.png">This site is for U.S. Healthcare Professionals Only</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="desktop-menu container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="nav-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<nav class="navbar navbar-expand-md navbar-light">
								<a class="navbar-brand" href="/iron-deficiency-anemia/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/Home_Icon-1.png"></a>
								<ul class="navbar-nav">
									<li class="nav-item">
										<a class="nav-link" href="/iron-deficiency-anemia/efficacy/">Efficacy</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="/iron-deficiency-anemia/tolerability-and-safety/">Safety</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="/iron-deficiency-anemia/dosing/">Dosing</a>
									</li>
									<li class="nav-item">
										<a class="nav-link active" href="/iron-deficiency-anemia/moa/">MOA</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="/iron-deficiency-anemia/interactive-library/">Library</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="/iron-deficiency-anemia/sign-up/">Sign Up</a>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="indication-banner ida">
			<div class="container-fluid">
				<div class="row justify-content-center no-gutters h-100 align-items-center">
					<div id="warning-container" class="col-md-10">
						<div class="row no-gutters">
							<div class="col">
								<strong>Iron Deficiency Anemia</strong> CKD Not On Dialysis
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<header>
		<div class="container-fluid text-center">
			<div class="row justify-content-center no-gutters">
				<div id="header-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<h1>Mechanism of action of AURYXIA</h1>
							<p>Watch this video and see how AURYXIA, an oral iron tablet, works in adult patients.</p>
							<p class="isi-link">See <a href="#important-safety-information">Important Safety Information</a> below</p>
							<div class="video">
								<div class="video-js-wrap">
									<video-js data-account="5982844371001" data-player="eQq6v3QYZR" data-embed="default" data-video-id="6189938842001" data-playlist-id="" data-application-id="" id="home-bg-video" class="vjs-fluid" controls></video-js>
								</div>
								<div class="video-shadow"></div>
								<div class="video-controls">
									<div class="stick"></div>
									<a href="#" class="play-video">Play video<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/icon-play.svg"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<a class="collapse-button transcript-section" href="#transcriptCollapse" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="transcriptCollapse">
		<div class="container-fluid">
			<div class="row justify-content-center toggle-row no-gutters">
				<div id="transcript-container" class="col-md-10">
					<i class="fas fa-chevron-right"></i> View Transcript
				</div>
			</div>
		</div>
	</a>
	<section class="transcript-content-section">
		<div class="container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="transcript-content-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<div class="collapse" id="transcriptCollapse">
								<p>Chronic kidney disease, CKD, is characterized by a gradual loss of kidney function, causing insufficient synthesis of erythropoietin, a hormone that stimulates red blood cell (RBC) production and a lack of which causes anemia.<sup>1</sup> Iron deficiency also causes anemia in CKD patients.<sup>1</sup></p>
								<p>AURYXIA is a unique formulation of ferric citrate coordination complexes that treats iron deficiency anemia in adults with CKD not receiving dialysis.<sup>2-4</sup></p>
								<p>Upon swallowing AURYXIA, ferric iron is converted to ferrous iron by the enzyme ferric reductase in the gastrointestinal tract, where the molecules are absorbed in the same highly regulated physiological pathway as dietary iron.<sup>2,3,5,6</sup> In the bloodstream, ferrous iron is oxidized back to ferric iron, which binds to transferrin, an iron transfer protein that transports iron to the bone marrow for incorporation into hemoglobin for RBC production.<sup>2,3,5-7</sup></p>
								<p>AURYXIA treats iron deficiency anemia in non-dialysis patients with CKD.<sup>2,3</sup></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="how-it-works-section text-md-center">
		<div class="white-background">
			<div class="container-fluid">
				<div class="">
					<div id="how-it-works-container" style="margin: 0px auto;">
						<div class="row no-gutters">
							<div class="col">
								<div class="how-it-works-header">
									<h2>How AURYXIA works to treat iron deficiency anemia</h2>
									<p>AURYXIA is a unique formulation of ferric citrate coordination complexes that treats iron deficiency anemia in adults with CKD not receiving dialysis.<sup>2-4</sup></p>
								</div>
								<div class="row">
									<div class="col-md">
										<div class="hiw-card row no-gutters flex-md-column card-one">
											<div class="img-container col-4 col-md-12">
												<svg id="ida-icon-one" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 115.19479 115.9107"><defs><linearGradient id="linear-gradient" x1="54.35186" y1="562.24531" x2="125.14239" y2="610.37223" gradientTransform="translate(317.47324 -476.17821) rotate(34.73113)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffcd02"/><stop offset="0.36006" stop-color="#ffc001"/><stop offset="1" stop-color="#ff9f00"/></linearGradient></defs><g id="background"><circle cx="57.63925" cy="57.95535" r="57.5" fill="url(#linear-gradient)"/><path d="M91.25465,104.69375c-8.55141,6.37205-19.472,10.76162-33.70428,10.76162A57.50018,57.50018,0,1,1,91.23851,11.255C91.23851,15.6807,91.25465,98.927,91.25465,104.69375Z" fill="#d5d9d9"/></g><g id="bubble_1" data-name="bubble 1"><circle cx="38.37239" cy="57.77795" r="23.64811" fill="#8a9292"/><path id="first-path" d="M24.36992,62.57984a23.6437,23.6437,0,0,1,10.41892-28.3103A23.6482,23.6482,0,1,0,50.2581,78.06624,23.6437,23.6437,0,0,1,24.36992,62.57984Z" fill="#797e80"/><path d="M22.86285,50.979h9.62793v2.918H26.025v2.919h5.97949v2.91894H26.025v5.59473H22.86285Z" fill="#fff"/><path d="M43.69977,63.62642A4.84831,4.84831,0,0,1,41.855,65.06588a5.52469,5.52469,0,0,1-2.31055.50683,6.18891,6.18891,0,0,1-2.13867-.36523,5.15144,5.15144,0,0,1-1.74316-1.043,4.90738,4.90738,0,0,1-1.16553-1.63184,5.53272,5.53272,0,0,1,0-4.25683A4.90738,4.90738,0,0,1,35.66266,56.644a5.13794,5.13794,0,0,1,1.74316-1.04394,6.2118,6.2118,0,0,1,2.13867-.36524,4.86754,4.86754,0,0,1,1.91553.36524A3.98583,3.98583,0,0,1,42.91949,56.644a4.76694,4.76694,0,0,1,.92237,1.63184,6.58586,6.58586,0,0,1,.32421,2.12793v.95312H37.11188a2.40775,2.40775,0,0,0,.79052,1.38867,2.24086,2.24086,0,0,0,1.5.51661,2.27941,2.27941,0,0,0,1.26709-.335,3.396,3.396,0,0,0,.90186-.86132Zm-2.57422-4.33691a1.76464,1.76464,0,0,0-.50684-1.31836,1.80434,1.80434,0,0,0-1.35791-.54688,2.29713,2.29713,0,0,0-.89209.16211,2.24275,2.24275,0,0,0-.65869.416,1.81545,1.81545,0,0,0-.42578.58789,1.94238,1.94238,0,0,0-.17236.69922Z" fill="#fff"/><path d="M44.7193,51.60982H46.501V49.82759h1.06445v1.78223h1.78174V52.6733H47.56549v1.78222H46.501V52.6733H44.7193Z" fill="#fff"/><path id="two" style="opacity: 0" d="M49.99127,52.48507l2.33936-2.10156a4.265,4.265,0,0,0,.35742-.36621.70028.70028,0,0,0,.17871-.47656.578.578,0,0,0-.22119-.48047.81321.81321,0,0,0-.519-.17383.71214.71214,0,0,0-.55712.22071.89286.89286,0,0,0-.22559.54492l-1.27588-.09375a2.07258,2.07258,0,0,1,.19531-.8125,1.71164,1.71164,0,0,1,.45117-.57422,1.88673,1.88673,0,0,1,.65918-.34473,2.82945,2.82945,0,0,1,.82129-.11523,2.50124,2.50124,0,0,1,.76563.11523,1.78949,1.78949,0,0,1,.62109.33985,1.55762,1.55762,0,0,1,.4126.5664,1.95907,1.95907,0,0,1,.14893.791,2.13067,2.13067,0,0,1-.05957.52343,1.7699,1.7699,0,0,1-.166.42481,1.92979,1.92979,0,0,1-.251.35352,4.161,4.161,0,0,1-.31494.31445l-1.8291,1.57422h2.67139v1.123H49.99127Z" fill="#fff"/><path id="three" d="M51.66949,51.07759h.41407a2.3715,2.3715,0,0,0,.34619-.02539.98188.98188,0,0,0,.3081-.09765.58063.58063,0,0,0,.22412-.20215.62164.62164,0,0,0,.08448-.3418.55661.55661,0,0,0-.21582-.44824.80265.80265,0,0,0-.52784-.17676.7329.7329,0,0,0-.74316.55664l-1.39356-.28711a1.93014,1.93014,0,0,1,.3042-.66308,1.68768,1.68768,0,0,1,.48145-.44336,2.13557,2.13557,0,0,1,.6206-.249,3.19616,3.19616,0,0,1,.72217-.08008,2.94563,2.94563,0,0,1,.76416.09765,1.87675,1.87675,0,0,1,.64649.30371,1.526,1.526,0,0,1,.44336.51954,1.572,1.572,0,0,1,.16455.74316,1.39644,1.39644,0,0,1-.26172.85254,1.197,1.197,0,0,1-.76026.456v.02539a1.17345,1.17345,0,0,1,.47706.16114,1.27487,1.27487,0,0,1,.35058.3125,1.377,1.377,0,0,1,.21973.42187,1.61961,1.61961,0,0,1,.07617.498,1.70464,1.70464,0,0,1-.17334.78613,1.64564,1.64564,0,0,1-.46436.56152,1.98607,1.98607,0,0,1-.68408.333,3.07944,3.07944,0,0,1-.832.11035,2.497,2.497,0,0,1-1.38086-.37207,1.80122,1.80122,0,0,1-.76416-1.17382l1.334-.3125a.91253.91253,0,0,0,.2749.50683.88812.88812,0,0,0,.6123.18555.79483.79483,0,0,0,.61231-.21485.78692.78692,0,0,0,.19824-.55371.5774.5774,0,0,0-.37988-.59961,1.23177,1.23177,0,0,0-.38037-.08007c-.14063-.00879-.28125-.0127-.42188-.0127h-.2959Z" fill="#fff"/></g><g id="wall"><path d="M81.86227,5.55454c-1.60516,1.13014-6.05-.83991-6.32745,3.61417-.30485,4.8935,10.949.80191,10.70292,4.4581-.30791,4.57451-10.93645-.1526-11.0234,4.87093-.08978,5.18735,10.867.38441,10.97223,4.82583.08668,3.66034-11.20633-.84486-10.94412,4.36809.286,5.68552,10.79786.49041,10.55442,4.9154-.28444,5.17028-10.92356-.5222-11.01975,4.97836-.07793,4.45624,10.32361.97073,10.38318,5.07561.05615,3.86855-10.88794-.29809-10.91171,4.79749-.02416,5.178,12.31761-.08581,11.8716,4.98046-.27107,3.07906-11.69831-1.57026-11.84983,3.87833-.15278,5.49431,11.53169-.07064,11.4759,4.591-.06341,5.298-11.34248-.07378-11.68486,4.96534-.40879,6.0166,11.81679.43107,11.63662,5.43423-.17865,4.96132-11.43015-.35741-11.1153,5.94164.23241,4.64977,11.14054.38188,11.04978,5.67882-.06558,3.82749-10.72455-.28566-10.76241,4.58548-.04325,5.56352,10.46652,1.02475,10.68071,5.49229.1851,3.86081-10.94191-.83806-10.86842,4.7707.0683,5.21286,10.97228-.61134,10.7832,4.65555-.13989,3.89677-10.48834-.87828-10.46267,5.16209.01489,3.50328,4.27311,2.8992,5.24113,3.29059a53.03548,53.03548,0,0,0,9.61446-5.15079s-.36306-91.15263-.52364-96.00881A54.59131,54.59131,0,0,0,81.86227,5.55454Z" fill="#f2f2f2"/><path d="M96.78473,99.94912s-5.973,5.867-10.58073,7.94116c.0007-2.40671.91-98.89557.91062-99.59291a60.76435,60.76435,0,0,1,9.67011,7.31437Z" fill="#f2f2f2"/></g></svg>
											</div>
											<div class="content col-8 col-md-12">
												Ferric iron from the AURYXIA tablet is converted to ferrous iron by the enzyme ferric reductase in the gastrointestinal tract (GI tract)<sup>2,3,6</sup>
											</div>
										</div>
									</div>
									<div class="col-md">
										<div class="hiw-card row no-gutters flex-md-column card-two">
											<div class="img-container col-4 col-md-12">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 115.19479 115.9107" id="ida-icon-two"><defs><linearGradient id="linear-gradient" x1="29.30233" y1="482.14349" x2="100.09286" y2="530.27042" gradientTransform="translate(292.42372 -396.07639) rotate(34.73113)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffcd02"/><stop offset="0.36006" stop-color="#ffc001"/><stop offset="1" stop-color="#ff9f00"/></linearGradient></defs><g id="background"><circle cx="57.63925" cy="57.95535" r="57.5" fill="url(#linear-gradient)"/><path d="M91.25465,104.69375c-8.55141,6.37205-19.472,10.76162-33.70428,10.76162A57.50018,57.50018,0,1,1,91.23851,11.255C91.23851,15.6807,91.25465,98.927,91.25465,104.69375Z" fill="#d5d9d9"/></g><g id="bubble_1" data-name="bubble 1"><circle cx="35.76268" cy="49.51401" r="23.82192" fill="#ffc61f"/><path d="M21.65729,54.35119A23.81748,23.81748,0,0,1,32.15278,25.83281a23.822,23.822,0,1,0,15.583,44.1186A23.81748,23.81748,0,0,1,21.65729,54.35119Z" fill="#ffb51e"/><path d="M20.13981,42.66549h9.69873v2.93945H23.32487v2.94043H29.3488V51.4858H23.32487v5.63574H20.13981Z" fill="#fff"/><path d="M41.12956,55.4067a4.8817,4.8817,0,0,1-1.85791,1.44922,5.57394,5.57394,0,0,1-2.32764.51074,6.27464,6.27464,0,0,1-2.1543-.36719,5.18286,5.18286,0,0,1-1.75585-1.05176,4.93754,4.93754,0,0,1-1.17432-1.64355,5.57571,5.57571,0,0,1,0-4.28809,4.93754,4.93754,0,0,1,1.17432-1.64355,5.18286,5.18286,0,0,1,1.75585-1.05176,6.25184,6.25184,0,0,1,2.1543-.36817,4.90246,4.90246,0,0,1,1.92969.36817,4.019,4.019,0,0,1,1.46973,1.05176,4.79289,4.79289,0,0,1,.92919,1.64355,6.62453,6.62453,0,0,1,.32666,2.14356v.96H34.49382A2.42172,2.42172,0,0,0,35.2902,54.518a2.2486,2.2486,0,0,0,1.51074.52051,2.30184,2.30184,0,0,0,1.27637-.33691,3.43711,3.43711,0,0,0,.90869-.86719Zm-2.59278-4.37012a1.77492,1.77492,0,0,0-.51074-1.32715,1.81647,1.81647,0,0,0-1.36816-.55078,2.23247,2.23247,0,0,0-1.562.581,1.84691,1.84691,0,0,0-.42871.59278,1.939,1.939,0,0,0-.17334.7041Z" fill="#fff"/><path d="M42.15593,43.30123h1.79492V41.50631h1.07226v1.79492H46.818v1.07226H45.02311v1.79493H43.95085V44.37349H42.15593Z" fill="#fff"/><path d="M47.737,45.063l2.33936-2.10157a4.265,4.265,0,0,0,.35742-.36621.70028.70028,0,0,0,.17871-.47656.578.578,0,0,0-.22119-.48047.81321.81321,0,0,0-.519-.17383.71218.71218,0,0,0-.55712.22071.89286.89286,0,0,0-.22559.54492l-1.27588-.09375a2.07263,2.07263,0,0,1,.19531-.8125,1.71174,1.71174,0,0,1,.45117-.57422,1.8869,1.8869,0,0,1,.65918-.34473,2.82945,2.82945,0,0,1,.82129-.11523,2.50124,2.50124,0,0,1,.76563.11523,1.78966,1.78966,0,0,1,.62109.33985,1.55762,1.55762,0,0,1,.4126.5664,1.9591,1.9591,0,0,1,.14893.791,2.13076,2.13076,0,0,1-.05957.52344,1.76982,1.76982,0,0,1-.166.4248,1.92979,1.92979,0,0,1-.251.35352,4.161,4.161,0,0,1-.31494.31445l-1.8291,1.57422h2.67139v1.123H47.737Z" fill="#fff"/></g><g id="wall"><path d="M81.82041,5.73194c-1.60516,1.13014-6.05-.83991-6.32745,3.61417-.30485,4.8935,10.949.80191,10.70292,4.4581-.30791,4.57451-10.93645-.1526-11.0234,4.87093-.08978,5.18734,10.86705.38441,10.97223,4.82583.08668,3.66034-11.20633-.84487-10.94412,4.36809.286,5.68552,10.79786.49041,10.55442,4.9154-.28444,5.17028-10.92356-.52221-11.01975,4.97836-.07793,4.45624,10.32361.97073,10.38318,5.0756.05615,3.86856-10.88794-.29808-10.91171,4.79749-.02416,5.17806,12.31762-.0858,11.8716,4.98047-.27107,3.07906-11.69831-1.57026-11.84982,3.87833-.15279,5.49431,11.53168-.07064,11.47589,4.591-.06341,5.298-11.34248-.07378-11.68486,4.96534-.40879,6.0166,11.81679.43107,11.63662,5.43423-.17865,4.96132-11.43015-.35741-11.1153,5.94164.23241,4.64977,11.14054.38188,11.04978,5.67882-.06558,3.82749-10.72455-.28566-10.76241,4.58548-.04325,5.56352,10.46652,1.02475,10.68071,5.49229.1851,3.86081-10.94191-.83806-10.86842,4.7707.0683,5.21286,10.97228-.61134,10.7832,4.65555-.13989,3.89677-10.48834-.87828-10.46267,5.16209.01489,3.50328,4.27311,2.8992,5.24113,3.29058a53.035,53.035,0,0,0,9.61446-5.15078S89.45358,14.759,89.293,9.9028A54.59131,54.59131,0,0,0,81.82041,5.73194Z" fill="#f2f2f2"/><path d="M96.74287,100.12652s-5.973,5.867-10.58073,7.94116c.0007-2.40671.91-98.89557.91062-99.59291a60.7648,60.7648,0,0,1,9.67011,7.31436Z" fill="#f2f2f2"/><path class="cls-1" d="M115,58.71a57.5,57.5,0,0,1-115,0h0v57.2H115.14V58.71Z" fill="#ffffff"/></g></svg>
											</div>
											<div class="content col-8 col-md-12">
												The ferrous iron molecules are absorbed by the GI tract the same way as dietary iron<sup>2,5</sup>
											</div>
										</div>
									</div>
									<div class="col-md">
										<div class="hiw-card row no-gutters flex-md-column card-three">
											<div class="img-container col-4 col-md-12">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 115.19479 115.9107" id="ida-icon-three"><g id="background"><circle cx="57.59739" cy="57.95535" r="57.5" fill="#e6e6e6"/><circle cx="54.44919" cy="12.08341" r="8.72521" fill="#f2f2f2"/><circle cx="60.2399" cy="35.1284" r="8.72521" fill="#f2f2f2"/><circle cx="16.59151" cy="36.4032" r="8.72521" fill="#f2f2f2"/><circle cx="32.98415" cy="19.80862" r="8.72521" fill="#f2f2f2"/><circle cx="11.99331" cy="60.0285" r="8.72521" fill="#f2f2f2"/><circle cx="36.72398" cy="68.6308" r="8.72521" fill="#f2f2f2"/><circle cx="37.44919" cy="45.00487" r="8.72521" fill="#f2f2f2"/><circle cx="19.53374" cy="82.67447" r="8.72521" fill="#f2f2f2"/><circle cx="36.59151" cy="98.2047" r="8.72521" fill="#f2f2f2"/><circle cx="60.17439" cy="103.23356" r="8.72521" fill="#f2f2f2"/><circle cx="82.44751" cy="96.42304" r="8.72521" fill="#f2f2f2"/><circle cx="57.5147" cy="81.20029" r="8.72521" fill="#f2f2f2"/><circle cx="78.61992" cy="70.6308" r="8.72521" fill="#f2f2f2"/><circle cx="57.59739" cy="57.95535" r="8.72521" fill="#f2f2f2"/><circle cx="98.74407" cy="80.03994" r="8.72521" fill="#f2f2f2"/><circle cx="103.46927" cy="57.35693" r="8.72521" fill="#f2f2f2"/><circle cx="81.03062" cy="48.16228" r="8.72521" fill="#f2f2f2"/><circle cx="96.05397" cy="34.27966" r="8.72521" fill="#f2f2f2"/><circle cx="78.30541" cy="18.07945" r="8.72521" fill="#f2f2f2"/></g><g id="bubble_1" data-name="bubble 1"><circle cx="57.59739" cy="58.09382" r="24.10614" fill="#ffc61f"/><path id="first-path" d="M43.32371,62.98872A24.10166,24.10166,0,0,1,53.94443,34.13009a24.10622,24.10622,0,1,0,15.76889,44.645A24.10166,24.10166,0,0,1,43.32371,62.98872Z" fill="#ffb51e"/><path d="M41.78716,51.16353h9.81543v2.97559h-6.5918V57.1147H51.1065v2.97461H45.01079v5.70313H41.78716Z" fill="#fff"/><path d="M63.02837,64.05709a4.94362,4.94362,0,0,1-1.87988,1.46679,5.64326,5.64326,0,0,1-2.35547.51661,6.32482,6.32482,0,0,1-2.17969-.37207A5.23764,5.23764,0,0,1,54.836,64.604a4.99727,4.99727,0,0,1-1.18848-1.66308,5.6441,5.6441,0,0,1,0-4.33887A4.99731,4.99731,0,0,1,54.836,56.93892a5.23778,5.23778,0,0,1,1.77734-1.06445A6.32458,6.32458,0,0,1,58.793,55.5024a4.95476,4.95476,0,0,1,1.95215.37207,4.06761,4.06761,0,0,1,1.48828,1.06445A4.82941,4.82941,0,0,1,63.1729,58.602a6.69876,6.69876,0,0,1,.33106,2.16992v.9707H56.31353a2.44936,2.44936,0,0,0,.80566,1.415,2.27817,2.27817,0,0,0,1.5293.52735,2.33533,2.33533,0,0,0,1.291-.34082,3.49861,3.49861,0,0,0,.91992-.87793Zm-2.624-4.42188a1.79188,1.79188,0,0,0-.5166-1.34277,1.83645,1.83645,0,0,0-1.38379-.5586,2.32012,2.32012,0,0,0-.90918.166,2.27035,2.27035,0,0,0-.67188.42285,1.85633,1.85633,0,0,0-.43359.59961,1.9675,1.9675,0,0,0-.17578.71289Z" fill="#fff"/><path d="M64.06743,51.80611h1.81641V49.9897h1.085v1.81641h1.81641v1.085H66.9688v1.81641h-1.085V52.89107H64.06743Z" fill="#fff"/><path id="two" d="M69.71489,53.58834l2.36719-2.126a4.51357,4.51357,0,0,0,.36231-.37012.712.712,0,0,0,.18066-.48242.586.586,0,0,0-.22363-.48633.823.823,0,0,0-.52539-.17675.72473.72473,0,0,0-.56446.22363.90021.90021,0,0,0-.22754.55176l-1.292-.09473a2.09667,2.09667,0,0,1,.19824-.82227,1.72552,1.72552,0,0,1,.45606-.581,1.91039,1.91039,0,0,1,.668-.34863,2.8235,2.8235,0,0,1,.83008-.11621,2.50314,2.50314,0,0,1,.77539.11621,1.80417,1.80417,0,0,1,.62793.34375,1.57244,1.57244,0,0,1,.418.57226,1.98552,1.98552,0,0,1,.1504.80078,2.10289,2.10289,0,0,1-.06055.5293,1.79632,1.79632,0,0,1-.167.43066,1.94412,1.94412,0,0,1-.25489.35743,3.91566,3.91566,0,0,1-.31836.31836L71.2647,53.82076h2.70312v1.13672H69.71489Z" fill="#fff"/><path id="three" style="opacity: 0;" d="M71.15239,51.1257h.42188a2.46928,2.46928,0,0,0,.35351-.02539,1.01654,1.01654,0,0,0,.31348-.09961.58845.58845,0,0,0,.22852-.20605.63734.63734,0,0,0,.08593-.34961A.5667.5667,0,0,0,72.336,49.989a.81814.81814,0,0,0-.53809-.18066.75647.75647,0,0,0-.499.16406.74442.74442,0,0,0-.25781.4043l-1.4209-.293a1.96754,1.96754,0,0,1,.30957-.67578,1.71393,1.71393,0,0,1,.49121-.45215,2.18648,2.18648,0,0,1,.63281-.25391,3.20463,3.20463,0,0,1,.73633-.081,3.00931,3.00931,0,0,1,.77832.09863,1.91909,1.91909,0,0,1,.65918.30957,1.55868,1.55868,0,0,1,.45215.5293,1.61581,1.61581,0,0,1,.168.75781,1.42675,1.42675,0,0,1-.26758.87012,1.22216,1.22216,0,0,1-.77442.46484v.02539a1.21631,1.21631,0,0,1,.48633.16406,1.31677,1.31677,0,0,1,.35742.31836,1.389,1.389,0,0,1,.22364.43067,1.64426,1.64426,0,0,1,.07715.50781,1.73977,1.73977,0,0,1-.17579.80078,1.67965,1.67965,0,0,1-.47363.57227,2.01949,2.01949,0,0,1-.69726.33984,3.15809,3.15809,0,0,1-.84864.1123,2.54318,2.54318,0,0,1-1.40722-.3789,1.83869,1.83869,0,0,1-.7793-1.19727l1.36035-.31836a.91936.91936,0,0,0,.28027.51661.90182.90182,0,0,0,.624.18945.80858.80858,0,0,0,.624-.21973.79746.79746,0,0,0,.20215-.56347.65515.65515,0,0,0-.10742-.4004.6446.6446,0,0,0-.28028-.21093,1.24088,1.24088,0,0,0-.38671-.082c-.14356-.00879-.28711-.0127-.43067-.0127h-.30176Z" fill="#fff"/></g></svg>

											</div>
											<div class="content col-8 col-md-12">
												In the blood, the ferrous iron is oxidized back to ferric iron<sup>2,3,5,6</sup>
											</div>
										</div>
									</div>
									<div class="col-md">
										<div class="hiw-card row no-gutters flex-md-column card-four">
											<div class="img-container col-4 col-md-12">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 115.19479 115.9107" id="ida-icon-four"><g id="background"><circle cx="57.59739" cy="57.95535" r="57.5" fill="#e6e6e6"/><circle cx="54.44919" cy="12.08341" r="8.72521" fill="#f2f2f2"/><circle cx="60.2399" cy="35.1284" r="8.72521" fill="#f2f2f2"/><circle cx="16.59151" cy="36.4032" r="8.72521" fill="#f2f2f2"/><circle cx="32.98415" cy="19.80862" r="8.72521" fill="#f2f2f2"/><circle cx="11.99331" cy="60.0285" r="8.72521" fill="#f2f2f2"/><circle cx="36.72398" cy="68.6308" r="8.72521" fill="#f2f2f2"/><circle cx="37.44919" cy="45.00487" r="8.72521" fill="#f2f2f2"/><circle cx="19.53374" cy="82.67447" r="8.72521" fill="#f2f2f2"/><circle cx="36.59151" cy="98.2047" r="8.72521" fill="#f2f2f2"/><circle cx="60.17439" cy="103.23356" r="8.72521" fill="#f2f2f2"/><circle cx="82.44751" cy="96.42304" r="8.72521" fill="#f2f2f2"/><circle cx="57.5147" cy="81.20029" r="8.72521" fill="#f2f2f2"/><circle cx="78.61992" cy="70.6308" r="8.72521" fill="#f2f2f2"/><circle cx="57.59739" cy="57.95535" r="8.72521" fill="#f2f2f2"/><circle cx="98.74407" cy="80.03994" r="8.72521" fill="#f2f2f2"/><circle cx="103.46927" cy="57.35693" r="8.72521" fill="#f2f2f2"/><circle cx="81.03062" cy="48.16228" r="8.72521" fill="#f2f2f2"/><circle cx="96.05397" cy="34.27966" r="8.72521" fill="#f2f2f2"/><circle cx="78.30541" cy="18.07945" r="8.72521" fill="#f2f2f2"/><path d="M93.60651,36.93762c0,19.88729-16.12183,0-36.00912,0s-36.00911,19.88729-36.00911,0a36.00912,36.00912,0,1,1,72.01823,0Z" fill="#ffb51e"/></g><g id="bubble_1" data-name="bubble 1"><circle cx="57.59739" cy="86.47949" r="20.44831" fill="#8a9292"/><path d="M45.48958,90.63164A20.44449,20.44449,0,0,1,54.49873,66.152a20.44838,20.44838,0,1,0,13.37613,37.87061A20.44448,20.44448,0,0,1,45.48958,90.63164Z" fill="#797e80"/><path d="M44.18713,80.59908h8.3252v2.52344H46.92151v2.52441h5.16992v2.52344H46.92151v4.83789H44.18713Z" fill="#fff"/><path d="M62.20471,91.5356A4.1915,4.1915,0,0,1,60.61,92.78072a4.77831,4.77831,0,0,1-1.99805.4375,5.36026,5.36026,0,0,1-1.84961-.31543,4.4446,4.4446,0,0,1-1.50684-.90234,4.2253,4.2253,0,0,1-1.00781-1.41114,4.78373,4.78373,0,0,1,0-3.68066,4.235,4.235,0,0,1,1.00781-1.41113,4.46017,4.46017,0,0,1,1.50684-.90235,5.38346,5.38346,0,0,1,1.84961-.31543,4.2148,4.2148,0,0,1,1.65625.31543,3.45676,3.45676,0,0,1,1.26172.90235,4.1257,4.1257,0,0,1,.79785,1.41113,5.70441,5.70441,0,0,1,.28027,1.84082v.82324H56.50842a2.08181,2.08181,0,0,0,.6836,1.20117,1.9343,1.9343,0,0,0,1.29687.44629,1.97007,1.97007,0,0,0,1.09571-.28906,2.93825,2.93825,0,0,0,.78027-.74512Zm-2.22558-3.75a1.52277,1.52277,0,0,0-.43848-1.13965,1.5583,1.5583,0,0,0-1.17383-.47363,1.96669,1.96669,0,0,0-.77148.14063,1.926,1.926,0,0,0-.56934.35937,1.56494,1.56494,0,0,0-.36816.50781,1.67328,1.67328,0,0,0-.14942.60547Z" fill="#fff"/><path d="M63.08557,81.14693h1.541v-1.541h.91992v1.541h1.541v.91992h-1.541v1.541h-.91992v-1.541h-1.541Z" fill="#fff"/><path d="M69.09534,80.687h.3584a2.00153,2.00153,0,0,0,.29882-.02246.83575.83575,0,0,0,.26661-.084.48451.48451,0,0,0,.19336-.1748.5356.5356,0,0,0,.07324-.2959.47941.47941,0,0,0-.18555-.38672.695.695,0,0,0-.457-.15332.63333.63333,0,0,0-.64258.48145l-1.20508-.24805a1.67642,1.67642,0,0,1,.2627-.57324,1.45461,1.45461,0,0,1,.417-.38379,1.83522,1.83522,0,0,1,.53613-.21484,2.69429,2.69429,0,0,1,.625-.06934,2.53813,2.53813,0,0,1,.66016.084,1.62566,1.62566,0,0,1,.55957.2627,1.311,1.311,0,0,1,.38281.44922,1.36251,1.36251,0,0,1,.14258.64258,1.20335,1.20335,0,0,1-.22656.7373,1.0347,1.0347,0,0,1-.65723.39453v.02149a1.0256,1.0256,0,0,1,.41309.13965,1.11421,1.11421,0,0,1,.30273.26953,1.17044,1.17044,0,0,1,.18946.36523,1.39908,1.39908,0,0,1,.0664.43066,1.46152,1.46152,0,0,1-.15039.67969,1.41849,1.41849,0,0,1-.40137.48535,1.72562,1.72562,0,0,1-.59179.28907,2.68446,2.68446,0,0,1-.71875.09472,2.16588,2.16588,0,0,1-1.19434-.32129,1.56607,1.56607,0,0,1-.66113-1.01562l1.15429-.26953a.78585.78585,0,0,0,.23731.4375.76776.76776,0,0,0,.5293.16113.68706.68706,0,0,0,.52929-.18652.67829.67829,0,0,0,.17188-.47852.49693.49693,0,0,0-.3291-.51855,1.05125,1.05125,0,0,0-.32813-.06934c-.12207-.00684-.24316-.01074-.36523-.01074h-.25586Z" fill="#fff"/></g></svg>
											</div>
											<div class="content col-8 col-md-12">
												Transferrin binds to the ferric iron and transports it to the bone marrow for incorporation into hemoglobin for RBC production<sup>2,3,5-7</sup>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="patients-section">
		<div class="container-fluid">
			<div id="mobile-card-wrapper" class="row no-gutters">
				<div class="col-md-7 offset-md-1">
					<div class="card-wrapper">
						<div class="item item-three">
							<h2>Get to know AURYXIA dosing and administration</h2>
							<p>Now that you know how AURYXIA works in the body, find out what makes AURYXIA dosing different.</p>
							<a class="btn btn-hp" href="/iron-deficiency-anemia/dosing/">Get dosing details</a>
						</div>
					</div>
				</div>
				<div class="col-12 d-md-none">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/img-patient-ida-sm.png" class="small-lady d-block mx-auto img-fluid">
				</div>
			</div>
		</div>
	</section>
	<section class="patients-section d-none">
		<div class="container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="patients-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<div class="card-wrapper row no-gutters">
								<div class="col-md-8 mr-md-auto">
									<div class="patient-card">
										<div class="item-three">
											<h2>Get to know AURYXIA dosing and administration</h2>
											<p>Now that you know how AURYXIA works in the body, find out what makes AURYXIA dosing different.</p>
											<a class="btn btn-hp" href="/iron-deficiency-anemia/dosing/">Get dosing details</a>
										</div>
									</div>
								</div>
								<div class="col-md-4 d-md-none">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/img-patient-ida-sm.png" class="small-lady d-block mx-auto img-fluid">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="akebiacares-section">
		<div class="container-fluid">
			<div class="row background-wrapper">
				<div class="col-md akebiacares-image"></div>
				<div class="col-md-7 d-flex justify-content-center align-items-center akebiacares-content">
					<div class="content-wrapper">
					<a href="/akebiacares/">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/AkebiaCares.png" alt="AkebiaCares®" class="akebiacares-logo">
					</a>
						<h3>Our commitment is to help make access to AURYXIA as easy as possible for your patients</h3>
						<p>AkebiaCares offers a range of access solutions including:</p>
						<ul class="custom-bullet">
							<li>Specialty pharmacy locator</li>
							<li>Electronic benefits verification tool</li>
							<li>Prescription assistance programs</li>
						</ul>
						<a class="btn btn-akebiacares" href="/akebiacares/support-programs/">Find support options</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="important-safety-information" class="important-safety-information-section">
		<div class="container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="isi-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<h2>Indication</h2>
							<p>AURYXIA<sup>&reg;</sup> (ferric citrate) is indicated for:</p>
							<ul class="custom-bullet">
								<li>The treatment of iron deficiency anemia in adult patients with chronic kidney disease not on dialysis</li>
							</ul>
							<h2>Important Safety Information</h2>
							<h3>Contraindication</h3>
							<p>AURYXIA<sup>&reg;</sup> (ferric citrate) is contraindicated in patients with iron overload syndromes, e.g., hemochromatosis</p>
							<h3>Warnings and Precautions</h3>
							<ul class="custom-bullet">
								<li><strong>Iron Overload:</strong> Increases in serum ferritin and transferrin saturation (TSAT) were observed in clinical trials with AURYXIA in patients with chronic kidney disease (CKD) on dialysis treated for hyperphosphatemia, which may lead to excessive elevations in iron stores. Assess iron parameters prior to initiating AURYXIA and monitor while on therapy. Patients receiving concomitant intravenous (IV) iron may require a reduction in dose or discontinuation of IV iron therapy</li>
								<li><strong>Risk of Overdosage in Children Due to Accidental Ingestion:</strong> Accidental ingestion and resulting overdose of iron-containing products is a leading cause of fatal poisoning in children under 6 years of age. Advise patients of the risks to children and to keep AURYXIA out of the reach of children</li>
							</ul>
							<h3>Adverse Reactions</h3>
							<p>The most common adverse reactions reported with AURYXIA in clinical trials were:</p>
							<ul class="custom-bullet">
								<li><u>Iron Deficiency Anemia in CKD Not on Dialysis</u>: Discolored feces (22%), diarrhea (21%), constipation (18%), nausea (10%), abdominal pain (5%) and hyperkalemia (5%)</li>
							</ul>
							<h3>Specific Populations</h3>
							<ul class="custom-bullet">
								<li><strong>Pregnancy and Lactation</strong>: There are no available data on AURYXIA use in pregnant women to inform a drug-associated risk of major birth defects and miscarriage. However, an overdose of iron in pregnant women may carry a risk for spontaneous abortion, gestational diabetes and fetal malformation. Data from rat studies have shown the transfer of iron into milk, hence, there is a possibility of infant exposure when AURYXIA is administered to a nursing woman</li>
							</ul>
							<p>To report suspected adverse reactions, contact Akebia Therapeutics, Inc. at <a href="tel:+18444453799">1-844-445-3799</a></p>
							<p><strong>Please see full <a target="_blank" href="/wp-content/uploads/Auryxia_PI.pdf">Prescribing Information</a></strong></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="references-section">
		<div class="container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="reference-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<h2><img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/icon-references.png" class="icon-references">References</h2>
							<ol>
								<li>Webster AC, Nagler EV, Morton RL, Masson P. Chronic Kidney Disease. <em>Lancet</em>. 2017;389(10075):1238-1252.</li>
								<li>AURYXIA [package insert]. Cambridge, MA: Akebia Therapeutics, Inc.; 2019.</li>
								<li>Ganz T, Bino A, Salusky IB. Mechanism of Action and Clinical Attributes of Auryxia<sup>®</sup> (Ferric Citrate). <em>Drugs</em>. 2019;79(9):957-968.</li>
								<li>Pergola PE, Fishbane S, Ganz T. Novel Oral Iron Therapies for Iron Deficiency Anemia in Chronic Kidney Disease. <em>Adv Chronic Kidney Dis</em>. 2019;26(4):272-291.</li>
								<li class="break-the-url">Ems T, St Lucia K, Huecker MR. Biochemistry, Iron Absorption. [Updated 2020 Apr 30]. In: StatPearls [Internet]. Treasure Island (FL): StatPearls Publishing; 2020 Jan. Available from: https://www.ncbi.nlm.nih.gov/books/NBK448204/.</li>
								<li>Yagil Y, Fadem SZ, Kant KS, et al. Managing Hyperphosphatemia in Patients with Chronic Kidney Disease on Dialysis with Ferric Citrate: Latest Evidence and Clinical Usefulness. <em>Ther Adv Chronic Dis</em>. 2015;6(5):252-263.</li>
								<li>Kalantar-Zadeh K, Streja E, Miller JE, Nissenson AR. Intravenous Iron Versus Erythropoiesis-Stimulating Agents: Friends or Foes in Treating Chronic Kidney Disease Anemia? <em>Adv Chronic Kidney Dis</em>. 2009;16(2):143-151.</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<div class="container-fluid">
			<div class="row justify-content-center no-gutters">
				<div id="footer-container" class="col-md-10">
					<div class="row no-gutters">
						<div class="col">
							<div id="mobile-footer" class="col d-md-none">
								<div class="footer-nav">
									<ul class="list-unstyled">
										<li><a target="_blank" href="https://akebia.com/contact/">Contact Us</a></li>
										<li><a target="_blank" href="https://akebia.com/legal/">Terms and Conditions</a></li>
										<li><a target="_blank" href="https://akebia.com/legal/privacy-policy.aspx">Privacy Policy</a></li>
										<li><a target="_blank" href="http://akebia.com/">Akebia.com</a></li>
									</ul>
								</div>
								<div class="footer-attributes">
									<div class="attribution">&copy;2020 Akebia Therapeutics, Inc.</div>
									<div class="pcr-number"><span style="padding-right: 0.5rem">PP-AUR-US-1153</span> 10/20</div>
									<a target="_blank" href="http://akebia.com/">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/logo-akebia-therapeutics.png">
									</a>
								</div>
							</div>
							<div id="desktop-footer" class="d-none d-md-block">
								<div class="row">
									<div class="col-md-9">
										<div class="footer-links">
											<a target="_blank" href="https://akebia.com/contact/">Contact Us</a>  |
											<a target="_blank" href="https://akebia.com/legal/">Terms and Conditions</a>  |
											<a target="_blank" href="https://akebia.com/legal/privacy-policy.aspx">Privacy Policy</a>  |
											<a target="_blank" href="http://akebia.com/">Akebia.com</a>
										</div>
										<div class="attribution">&copy;2020 Akebia Therapeutics, Inc. <span class="pcr-number"><span style="padding-right: 0.5rem">PP-AUR-US-1153</span> 10/20</span></div>

									</div>
									<div class="col-md-3">
										<a target="_blank" href="http://akebia.com/">
											<img class="footer-logo d-block ml-auto" src="<?php echo get_template_directory_uri(); ?>/assets/img/hp-moa/Akebia_Ribbon_Logo_FullColor_RGB.png">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>

